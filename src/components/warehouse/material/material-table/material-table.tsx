import { BaseLoader } from '@/components/base';
import { MaterialTypeIdConstant } from '@/constants/warehouse.constants';
import { IMaterial } from '@/interfaces/warehouse';
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import { FC } from 'react';

interface Props {
  isLoading: boolean;
  data: Array<IMaterial>;
  maxWidth?: number;
}

export const MaterialTable: FC<Props> = (props) => {
  const { isLoading, data, maxWidth } = props;
  return (
    <TableContainer component={Paper}>
      <Table sx={{ maxWidth }}>
        <TableHead>
          <TableRow>
            <TableCell>Nama</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {isLoading ? (
            <caption>
              <BaseLoader position="relative" height={200} />
            </caption>
          ) : (
            data.map((material, index) => (
              <TableRow key={String(material.id)} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell>{material.name}</TableCell>
                <TableCell>
                  {material.material_type_id === MaterialTypeIdConstant.MaterialSticker ||
                  material.material_type_id === MaterialTypeIdConstant.MaterialBanner
                    ? material.stock_length + 'cm'
                    : '-'}
                </TableCell>
              </TableRow>
            ))
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

MaterialTable.defaultProps = {
  maxWidth: 1100,
};
