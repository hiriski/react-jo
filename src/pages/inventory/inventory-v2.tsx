import {
  BrandWithMaterialPaperList,
  DialogAddEditBrandMaterial,
  DialogAddStockMaterialPaper,
  DrawerDetailInventory,
} from '@components/inventory'
import { BoxSpinner, Card } from '@components/ui'
import BookOutlinedIcon from '@mui/icons-material/BookOutlined'
import Box from '@mui/material/Box'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import { MainLayout } from '@src/components/layouts'
import { useAppSelector } from '@src/store/hook'
import { fetchListMaterialPaperV2 } from '@src/store/inventory/actions'
import { FC, useEffect } from 'react'
import { useDispatch } from 'react-redux'

const InventoryPageV2: FC = () => {
  const dispatch = useDispatch()
  const { listBrandWithMaterialPapers } = useAppSelector((state) => state.inventory)
  const { isFetching, data } = listBrandWithMaterialPapers

  useEffect(() => {
    dispatch(fetchListMaterialPaperV2())
  }, [])

  return (
    <MainLayout>
      <Container maxWidth="lg">
        <Box
          sx={{
            mt: 8,
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <Card sx={{ px: { xs: 2, md: 5 }, py: { xs: 2, md: 4 } }}>
            {isFetching ? (
              <BoxSpinner height={360} />
            ) : (
              <>
                <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-start', mb: 4 }}>
                  <BookOutlinedIcon sx={{ color: 'text.disabled', fontSize: 24, mr: 2 }} />
                  <Typography variant="h5" component="h2">
                    Material Kertas
                  </Typography>
                </Box>
                {Array.isArray(data) && <BrandWithMaterialPaperList data={data} />}
              </>
            )}
          </Card>
        </Box>
      </Container>
      <DrawerDetailInventory />
      <DialogAddEditBrandMaterial />
      <DialogAddStockMaterialPaper />
    </MainLayout>
  )
}

export default InventoryPageV2
