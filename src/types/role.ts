export type TRolePermissions = {
  id: number
  name: string
  code: string
  description?: string
}

export type TRole = {
  id: number
  name: string
  slug: string
  permissions: Array<TRolePermissions>
  description?: string
}
