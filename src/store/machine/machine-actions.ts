// Enum action types.
import { MACHINE_ACTION_TYPES as ActionTypes } from './machine-action-types.enum';

// Interfaces.
import { IMachine } from '@/interfaces/machine';

// Actions definition
interface IMachineFetchListRequested {
  type: typeof ActionTypes.FETCH_LIST_MACHINE_REQUESTED;
  payload?: Record<string, string | number>;
}
interface IMachineFetchListLoading {
  type: typeof ActionTypes.FETCH_LIST_MACHINE_LOADING;
}
interface IMachineFetchListFailure {
  type: typeof ActionTypes.FETCH_LIST_MACHINE_FAILURE;
}
interface IMachineFetchListSuccess {
  type: typeof ActionTypes.FETCH_LIST_MACHINE_SUCCESS;
  payload: Array<IMachine>;
}

// Union action types
export type MachineActions =
  | IMachineFetchListRequested
  | IMachineFetchListLoading
  | IMachineFetchListFailure
  | IMachineFetchListSuccess;

// Actions creators.
export const machine_fetchList = (params?: Record<string, string | number>): IMachineFetchListRequested => ({
  type: ActionTypes.FETCH_LIST_MACHINE_REQUESTED,
  payload: params,
});
export const machine_fetchListLoading = (): IMachineFetchListLoading => ({
  type: ActionTypes.FETCH_LIST_MACHINE_LOADING,
});
export const machine_fetchListFailure = (): IMachineFetchListFailure => ({
  type: ActionTypes.FETCH_LIST_MACHINE_FAILURE,
});
export const machine_fetchListSuccess = (payload: Array<IMachine>): IMachineFetchListSuccess => ({
  type: ActionTypes.FETCH_LIST_MACHINE_SUCCESS,
  payload,
});
