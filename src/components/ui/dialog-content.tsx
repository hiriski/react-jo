import MuiDialogContent, { DialogContentProps } from '@mui/material/DialogContent';
import { SxProps } from '@mui/material/styles';
import { FC } from 'react';

type Props = DialogContentProps & { sx?: SxProps };

const DialogContent: FC<Props> = ({ children, sx }: Props) => (
  <MuiDialogContent sx={{ pt: 0, pb: 4, px: 5, overflowX: 'hidden', ...sx }}>{children}</MuiDialogContent>
);

DialogContent.defaultProps = {
  sx: {},
};

export default DialogContent;
