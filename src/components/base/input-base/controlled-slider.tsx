import { FC } from 'react';

// Mui components.
import Slider, { SliderProps } from '@mui/material/Slider';
import { styled } from '@mui/material/styles';

// React hook form.
import { Control, Controller, FieldErrors } from 'react-hook-form';

interface Props extends SliderProps {
  // eslint-disable-next-line
  control: Control<any>;
  errors: FieldErrors;
}

const StyledSlider = styled(Slider)(({ theme }) => ({
  color: theme.palette.primary.main,
  height: 6,
  '& .MuiSlider-rail ': {
    opacity: 0.2,
  },
  '& .MuiSlider-track': {
    border: 'none',
  },
  '& .MuiSlider-thumb': {
    height: 24,
    width: 24,
    backgroundColor: theme.palette.primary.main,
    border: '3px solid currentColor',
  },
  '& .MuiSlider-valueLabel': {
    lineHeight: 1.2,
    fontSize: 12,
    padding: 0,
    width: 36,
    height: 36,
    borderRadius: '50% 50% 50% 0',
    backgroundColor: theme.palette.primary.main,
    transformOrigin: 'bottom left',
    transform: 'translate(50%, -100%) rotate(-45deg) scale(0)',
    '&:before': { display: 'none' },
    '&.MuiSlider-valueLabelOpen': {
      transform: 'translate(50%, -100%) rotate(-45deg) scale(1)',
    },
    '& > *': {
      transform: 'rotate(45deg)',
    },
  },
  '& .MuiSlider-valueLabelOpen': {
    border: `1px solid ${theme.palette.primary.dark}`,
  },
}));

export const ControlledSlider: FC<Props> = (props) => {
  const { control, name, value, onChange, errors, ...rest } = props;

  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { value, onChange: onControlChange } }) => {
        return (
          <StyledSlider {...rest} track={false} value={value} onChange={(e, val) => onControlChange(Number(val))} />
        );
      }}
    />
  );
};
