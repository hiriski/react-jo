import { DialogContent, DialogTitle } from '@/components/ui'
import { setDialogDetailProduct } from '@/store/product/product-actions'
import { ROUTES } from '@/utils/constants'
import CloseIcon from '@mui/icons-material/Close'
import { IconButton } from '@mui/material'
import Box from '@mui/material/Box'
import Dialog from '@mui/material/Dialog'
import Zoom, { ZoomProps } from '@mui/material/Zoom'
import { useAppSelector } from '@src/store/hook'
import { FC, ReactElement, forwardRef } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router'

import { ProductDetail } from '.'

const Transition = forwardRef<unknown, ZoomProps>(
  (props, ref): ReactElement => <Zoom unmountOnExit ref={ref} {...props} />,
)
const DialogDetailProduct: FC = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { dialogDetailProduct: dialog, detailProduct } = useAppSelector((state) => state.product)

  const handleClose = (): void => {
    dispatch(setDialogDetailProduct(false, null))
    navigate(ROUTES.PRODUCT)
  }

  return (
    <Dialog
      open={dialog.open && Boolean(dialog.id)}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleClose}
      aria-describedby="dialog-input-down-payment"
      PaperProps={{ elevation: 1, sx: { position: 'relative', width: { xs: '94%', md: 780 } } }}
      maxWidth={false}
      fullWidth
    >
      <DialogTitle>Detail Product</DialogTitle>
      <IconButton
        onClick={handleClose}
        size="small"
        sx={{ position: 'absolute', top: (theme) => theme.spacing(2), right: (theme) => theme.spacing(2) }}
      >
        <CloseIcon />
      </IconButton>
      <DialogContent>
        <Box sx={{ display: 'flex', flexDirection: 'column' }}>
          <ProductDetail product={detailProduct.data} isLoading={detailProduct.isFetching} />
        </Box>
      </DialogContent>
    </Dialog>
  )
}

export default DialogDetailProduct
