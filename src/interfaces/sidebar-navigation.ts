import { ReactElement } from 'react'

export interface SidebarNavigation {
  label: string
  icon: ReactElement
  activeIcon: ReactElement
  path: string
}
