// App action types.
import { AppActionTypes as Types } from './app-actions.enum';

// Union actions type.
import { AppActions } from './app-actions';

import type { IPayloadSetAlert } from './app.interfaces';
import { boolean } from 'yup';

const DEFAULT_AHDR = 4000;

// App state definition.
export interface IAppState {
  app_isLoading: boolean;
  app_isLocked: boolean;
  alert: IPayloadSetAlert;
}

// Init state.
const initialState: IAppState = {
  app_isLoading: false,
  app_isLocked: false,
  alert: {
    show: false,
    messages: null,
    severity: undefined,
    autoHideDuration: DEFAULT_AHDR,
    variant: 'filled',
  },
};

// App state reducer.
const appReducer = (state: IAppState = initialState, action: AppActions): IAppState => {
  switch (action.type) {
    case Types.app_SET_LOADING:
      return {
        ...state,
        app_isLoading: action.payload,
      };
    case Types.app_SET_ALERT:
      return {
        ...state,
        alert: {
          show: action.payload.show,
          messages: action.payload.messages ?? '',
          severity: action.payload.severity ?? 'success',
          autoHideDuration: action.payload.autoHideDuration ?? DEFAULT_AHDR,
          variant: action.payload.variant ?? 'filled',
        },
      };

    case Types.app_SET_IS_LOCKED:
      return {
        ...state,
        app_isLocked: action.payload,
      };

    case Types.app_RESET_ALERT:
      return {
        ...state,
        alert: initialState.alert,
      };
    default:
      return state;
  }
};

export default appReducer;
