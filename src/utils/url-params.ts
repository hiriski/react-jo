import { useLocation } from 'react-router-dom'

export const useQueryParams = (): any =>
  new URLSearchParams(useLocation().search)
