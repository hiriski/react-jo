import type { Effect, SagaIterator } from '@redux-saga/types';
import { AxiosError, AxiosResponse } from 'axios';
import { SagaReturnType, put, call, takeLatest, select } from 'redux-saga/effects';
import Swal from 'sweetalert2';
import * as ActionTypes from './constants';
import { TRequestLogin, TRequestLoginWithSocialAccount, TRequestRegister } from '../../types/auth';
import { IApiResponseError } from '../../types/common';
import { httpResponseCreated, httpResponseOK, httpResponseUnprocessableEntity } from '../../utils/http';
import { removeToken, saveToken } from '../../utils/tokens';
import {
  auth_fetchAuthenticatedUserFailure,
  auth_fetchAuthenticatedUserLoading,
  auth_fetchAuthenticatedUserSuccess,
  auth_resetAuthState,
  auth_setIsAuthenticated,
  auth_unlockAppFailure,
  auth_unlockAppLoading,
  auth_unlockAppSuccess,
  loginWithSocialAccountSuccess,
  setLoginError,
  setRegisterError,
} from './actions';
import AuthAPI from './service';
import { app_setAlert, app_setIsLocked } from '../app/app-actions';
import { auth_rootSelector } from './auth-selectors';
import { ROUTES } from '@/utils/constants';

// Type definitions of return of result.
type ApiLoginResponse = SagaReturnType<typeof AuthAPI.login>;
type ApiRegisterReponse = SagaReturnType<typeof AuthAPI.register>;
type ApiLoginWithSocialAccountResponse = SagaReturnType<typeof AuthAPI.loginWithSocialAcccount>;
type ApiResponseUnlock = SagaReturnType<typeof AuthAPI.unlock>;
type ApiResponseGetAuthenticatedUser = SagaReturnType<typeof AuthAPI.getAuthenticatedUser>;

function* login({ payload }: Effect<TRequestLogin>): SagaIterator {
  yield put({ type: ActionTypes.LOGIN_LOADING });
  try {
    const response: ApiLoginResponse = yield call(AuthAPI.login, payload);
    if (httpResponseOK(response.status)) {
      saveToken(response.data.token);
      yield put(auth_setIsAuthenticated(true));
      yield put({ type: ActionTypes.LOGIN_SUCCESS, payload: response.data.user });
      window.location.href = '/';
    }
  } catch (reason) {
    const error = reason as AxiosError<IApiResponseError>;
    if (httpResponseUnprocessableEntity(Number(error.response?.status))) {
      // credentials incorrect.
      const message = error.response?.data.message || '';
      const payloadError = { status: true, message, messages: [] };
      yield put(setLoginError(payloadError));
      // yield put(setAlert({ open: true, message, severity: 'error', autoHideDuration: 5000 }))
    } else {
      // another server error.
      yield put({ type: ActionTypes.LOGIN_FAILURE });
      yield put(auth_setIsAuthenticated(false));
    }
    Swal.fire({
      icon: 'error',
      title: 'Login Gagal!',
      text: 'Mohon periksa email dan password kamu',
    });
  }
}

function* loginWithSocialAccount({ payload }: Effect<TRequestLoginWithSocialAccount>): SagaIterator {
  yield put({ type: ActionTypes.LOGIN_WITH_SOCIAL_ACCOUNT_LOADING });
  try {
    const response: ApiLoginWithSocialAccountResponse = yield call(AuthAPI.loginWithSocialAcccount, payload);
    if (httpResponseOK(response.status)) {
      saveToken(response.data.token);
      yield put(loginWithSocialAccountSuccess(response.data.provider, response.data.user));
      yield put(auth_setIsAuthenticated(true));
    }
  } catch (reason) {
    // const error = reason as AxiosError<IApiResponseError>
    // yield put(setAlert({ open: true, message: 'Login failed', severity: 'error', autoHideDuration: 5000 }))
    yield put({ type: ActionTypes.LOGIN_WITH_SOCIAL_ACCOUNT_FAILURE });
    yield put(auth_setIsAuthenticated(false));
  }
}

function* register({ payload }: Effect<TRequestRegister>): SagaIterator {
  yield put({ type: ActionTypes.REGISTER_LOADING });
  try {
    const response: ApiRegisterReponse = yield call(AuthAPI.register, payload);
    if (httpResponseCreated(response.status)) {
      saveToken(response.data.token);
      yield put({ type: ActionTypes.REGISTER_SUCCESS, payload: response.data.user });
      yield put(auth_setIsAuthenticated(true));
    }
  } catch (reason) {
    const error = reason as AxiosError<IApiResponseError>;
    if (error.response?.data) {
      const { message, messages } = error.response.data;
      const payloadError = { status: true, message, messages };
      yield put(setRegisterError(payloadError));
      yield put(auth_setIsAuthenticated(false));
      // yield put(setAlert({ open: true, message, severity: 'error', autoHideDuration: 5000 }))
    } else {
      yield put({ type: ActionTypes.REGISTER_FAILURE });
    }
  }
}

function* revokeToken(): SagaIterator {
  yield put({ type: ActionTypes.REVOKE_TOKEN_LOADING });
  try {
    const response: AxiosResponse = yield call(AuthAPI.revokeToken);
    if (httpResponseOK(response.status)) {
      removeToken();
      yield put({ type: ActionTypes.REVOKE_TOKEN_SUCCESS });
      yield put({ type: ActionTypes.RESET_AUTH_STATE });
    }
  } catch (reason) {
    removeToken();
    // const error = reason as AxiosError<IApiResponseError>
    yield put({ type: ActionTypes.REVOKE_TOKEN_FAILURE });
    yield put({ type: ActionTypes.RESET_AUTH_STATE });
  }
}

// Unlcok app.
function* saga_unlockApp({ payload }: Effect<typeof ActionTypes.UNLOCK_APP_REQUESTED, string>): SagaIterator {
  yield put(auth_unlockAppLoading());

  const authenticatedUserEmail = yield select((state) => auth_rootSelector(state).authenticatedUser.email);

  try {
    const { status }: ApiResponseUnlock = yield call(AuthAPI.unlock, {
      email: authenticatedUserEmail,
      password: payload,
    });
    if (httpResponseOK(status)) {
      yield put(app_setAlert({ show: true, severity: 'success', messages: 'App unlocked' }));
      yield put(auth_unlockAppSuccess());
      yield put(app_setIsLocked(false));
    }
  } catch (e) {
    yield put(app_setAlert({ show: true, severity: 'error', messages: 'Buka kunci gagal! Silahkan coba lagi' }));
    yield put(auth_unlockAppFailure());
  }
}

// Get authenticated user.
function* saga_getAuthenticatedUser(): SagaIterator {
  yield put(auth_fetchAuthenticatedUserLoading());

  try {
    const { status, data }: ApiResponseGetAuthenticatedUser = yield call(AuthAPI.getAuthenticatedUser);
    if (httpResponseOK(status)) {
      yield put(auth_fetchAuthenticatedUserSuccess(data));
    }
  } catch (e) {
    yield put(app_setAlert({ show: true, severity: 'error', messages: 'Failed to get authenticated user data' }));
    yield put(auth_fetchAuthenticatedUserFailure());
    yield put(auth_setIsAuthenticated(false));

    // yield put(auth_resetAuthState()); PLEASE DON'T DO THIS!
    setTimeout(() => {
      window.location.href = ROUTES.SIGNIN;
    }, 1500);
  }
}

export default function* authSaga(): SagaIterator {
  yield takeLatest(ActionTypes.LOGIN_REQUESTED, login);
  yield takeLatest(ActionTypes.REGISTER_REQUESTED, register);
  yield takeLatest(ActionTypes.LOGIN_WITH_SOCIAL_ACCOUNT_REQUESTED, loginWithSocialAccount);
  yield takeLatest(ActionTypes.REVOKE_TOKEN_REQUESTED, revokeToken);
  yield takeLatest(ActionTypes.UNLOCK_APP_REQUESTED, saga_unlockApp);
  yield takeLatest(ActionTypes.FETCH_AUTHENTICATED_USER_REQUESTED, saga_getAuthenticatedUser);
}
