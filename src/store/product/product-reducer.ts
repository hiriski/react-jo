import { LaravelPaginationLinks, LaravelPaginationMeta } from '@/types/laravel-pagination'
import { IProduct } from '@/types/product'

import { PRODUCT_ACTION_TYPES } from './product-action-types.enum'
import { ProductAction } from './product-actions'

export interface ProductState {
  drawerAddEditProduct: {
    open: boolean
    id: string | null
  }
  dialogDetailProduct: {
    open: boolean
    id: string | null
  }
  productList: {
    isFetching: boolean
    isError: boolean
    data: Array<IProduct>
    meta: LaravelPaginationMeta | null
    links: LaravelPaginationLinks | null
  }
  detailProduct: {
    isFetching: boolean
    isError: boolean
    data: IProduct | null
  }
  createProduct: {
    isLoading: boolean
    isError: boolean
    data?: IProduct
  }

  formIsDirty: boolean
}

const initialState = {
  drawerAddEditProduct: {
    open: false,
    id: null,
  },
  dialogDetailProduct: {
    open: false,
    id: null,
  },
  productList: {
    isFetching: false,
    isError: false,
    data: [],
    meta: null,
    links: null,
  },
  detailProduct: {
    isFetching: false,
    isError: false,
    data: undefined,
  },
  createProduct: {
    isLoading: false,
    isError: false,
    data: undefined,
  },

  formIsDirty: false,
}

const productReducer = (state: ProductState = initialState, action: ProductAction): ProductState => {
  switch (action.type) {
    // set drawer create or edit product.
    case PRODUCT_ACTION_TYPES.SET_DRAWER_ADD_EDIT_PRODUCT:
      return {
        ...state,
        drawerAddEditProduct: {
          open: action.payload.open,
          id: action.payload.id,
        },
      }

    // set dialog detail product.
    case PRODUCT_ACTION_TYPES.SET_DIALOG_DETAIL_PRODUCT:
      return {
        ...state,
        dialogDetailProduct: {
          open: action.payload.open,
          id: action.payload.id,
        },
      }

    // Fetch product list.
    case PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LIST_LOADING:
      return {
        ...state,
        productList: {
          ...state.productList,
          isFetching: true,
          isError: false,
        },
      }
    case PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LIST_FAILURE:
      return {
        ...state,
        productList: {
          ...state.productList,
          isFetching: false,
          isError: true,
        },
      }
    case PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LIST_SUCCESS:
      const shouldBeMerge = action.payload.meta?.current_page === 1 ? false : true
      return {
        ...state,
        productList: {
          ...state.productList,
          isFetching: false,
          isError: false,

          /**
           * data
           * links
           * meta
           */
          // ...action.payload,
          data: shouldBeMerge ? [...state.productList.data, ...action.payload.data] : action.payload.data,
          meta: action.payload.meta ?? null,
          links: action.payload.links ?? null,
        },
      }

    // Fetch product by id.
    case PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LOADING:
      return {
        ...state,
        detailProduct: {
          ...state.detailProduct,
          isFetching: true,
          isError: false,
        },
      }
    case PRODUCT_ACTION_TYPES.FETCH_PRODUCT_FAILURE:
      return {
        ...state,
        detailProduct: {
          ...state.detailProduct,
          isFetching: false,
          isError: true,
        },
      }
    case PRODUCT_ACTION_TYPES.FETCH_PRODUCT_SUCCESS:
      return {
        ...state,
        detailProduct: {
          ...state.detailProduct,
          isFetching: false,
          isError: false,
          data: action.payload,
        },
      }

    // Create product.
    case PRODUCT_ACTION_TYPES.CREATE_PRODUCT_LOADING:
      return {
        ...state,
        createProduct: {
          isLoading: true,
          isError: false,
          data: undefined,
        },
      }
    case PRODUCT_ACTION_TYPES.CREATE_PRODUCT_LOADING:
      return {
        ...state,
        createProduct: {
          isLoading: false,
          isError: true,
          data: undefined,
        },
      }
    case PRODUCT_ACTION_TYPES.CREATE_PRODUCT_SUCCESS:
      return {
        ...state,
        createProduct: {
          isLoading: false,
          isError: false,
          data: action.payload,
        },
      }

    // set form is dirty
    case PRODUCT_ACTION_TYPES.SET_FORM_IS_DIRTY:
      return {
        ...state,
        formIsDirty: action.payload,
      }
    default:
      return state
  }
}

export default productReducer
