import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import Zoom, { ZoomProps } from '@mui/material/Zoom'
import { BoxSpinner, DialogContent } from '@src/components/ui'
import { useAppSelector } from '@src/store/hook'
import { fetchUser, setDialogDetailUser } from '@src/store/user/actions'
import { FC, ReactElement, forwardRef, useEffect } from 'react'
import { useDispatch } from 'react-redux'

const Transition = forwardRef<unknown, ZoomProps>(
  (props, ref): ReactElement => <Zoom unmountOnExit ref={ref} {...props} />
)

const DialogDetailUser: FC = () => {
  const dispatch = useDispatch()
  const { dialogDetail, detail } = useAppSelector((state) => state.user)
  const { open, id } = dialogDetail
  const { isFetching, isError, data } = detail

  const handleClose = (): void => {
    dispatch(setDialogDetailUser(false, null))
  }

  useEffect(() => {
    if (open && id) {
      dispatch(fetchUser(id))
    }
  }, [open])

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleClose}
      aria-describedby="dialog-add-edit-inventory"
      PaperProps={{ elevation: 1, sx: { position: 'relative' } }}
      maxWidth="md"
      fullWidth
    >
      <DialogContent sx={{ pt: 4 }}>
        <h4>Detail user here</h4>
        {/* {isFetching ? <BoxSpinner height={360} /> : <DetailJobOrder data={data} />} */}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Close</Button>
      </DialogActions>
    </Dialog>
  )
}

export default DialogDetailUser
