import { Card } from '@/components/ui'
import { setDialogDetailProduct } from '@/store/product/product-actions'
import { IProduct } from '@/types/product'
import { ROUTES } from '@/utils/constants'
import Box from '@mui/material/Box'
import Paper from '@mui/material/Paper'
import Skeleton from '@mui/material/Skeleton'
import Typography from '@mui/material/Typography'
import { FC } from 'react'

const ProductCardSkeleton: FC = () => {
  return (
    <Card>
      <Box>
        <Skeleton animation="wave" variant="rectangular" height={168} />

        <Box sx={{ py: 2, px: 1.5 }}>
          <Typography variant="body1" component="h2">
            <Skeleton animation="wave" width="50%" />
          </Typography>
          <Skeleton animation="wave" width="70%" />
        </Box>
      </Box>
    </Card>
  )
}

export default ProductCardSkeleton
