import { persistor, store } from '@src/store/config-store'
import { FC, ReactNode } from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

type Props = {
  children: ReactNode
}

const ReduxProvider: FC<Props> = ({ children }: Props) => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      {children}
    </PersistGate>
  </Provider>
)

export default ReduxProvider
