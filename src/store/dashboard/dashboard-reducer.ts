// Enum action types.
import { DASHBOARD_ACTION_TYPES as ActionTypes } from './dashboard-action-types.enum';

// Union action types.
import { DashboardActions } from './dashboard-actions';

// Interfaces.
import { IJobOrder } from '@/interfaces/job-order';

export interface DashboardState {
  listJobOrderInProgress: {
    isFetching: boolean;
    isError: boolean;
    data: Array<IJobOrder> | null;
  };
  listJobOrderLatest: {
    isFetching: boolean;
    isError: boolean;
    data: Array<IJobOrder> | null;
  };
}

const initialState: DashboardState = {
  listJobOrderInProgress: {
    isFetching: false,
    isError: false,
    data: [],
  },
  listJobOrderLatest: {
    isFetching: false,
    isError: false,
    data: [],
  },
};

const dashboardReducer = (state: DashboardState = initialState, action: DashboardActions): DashboardState => {
  switch (action.type) {
    case ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_IN_PROGRESS_LOADING:
      return {
        ...state,
        listJobOrderInProgress: {
          ...state.listJobOrderInProgress,
          isFetching: true,
          isError: false,
        },
      };
    case ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_IN_PROGRESS_FAILURE:
      return {
        ...state,
        listJobOrderInProgress: {
          ...state.listJobOrderInProgress,
          isFetching: false,
          isError: true,
        },
      };
    case ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_IN_PROGRESS_SUCCESS:
      return {
        ...state,
        listJobOrderInProgress: {
          ...state.listJobOrderInProgress,
          isFetching: false,
          isError: false,
          data: action.payload,
        },
      };

    case ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_LATEST_LOADING:
      return {
        ...state,
        listJobOrderLatest: {
          ...state.listJobOrderLatest,
          isFetching: true,
          isError: false,
        },
      };
    case ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_LATEST_FAILURE:
      return {
        ...state,
        listJobOrderLatest: {
          ...state.listJobOrderLatest,
          isFetching: false,
          isError: true,
        },
      };
    case ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_LATEST_SUCCESS:
      return {
        ...state,
        listJobOrderLatest: {
          ...state.listJobOrderLatest,
          isFetching: false,
          isError: false,
          data: action.payload,
        },
      };

    default:
      return state;
  }
};

export default dashboardReducer;
