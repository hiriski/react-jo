import { TInvoiceGeneratorData } from '@src/types/invoice'
import { TJoInvoiceData } from '@src/types/jo'

import { InvoiceAction } from './actions'
import * as ActionTypes from './constants'
import { base64TypeScriptLogo } from './misc'

export interface InvoiceGeneratorState {
  pdfMode: boolean
  drawerEditable: boolean
  invoice: TInvoiceGeneratorData | null
}

const invoiceGenerator = {
  logo: base64TypeScriptLogo,
  title: 'Invoice v2',
  bill_to_name: '',
  bill_to_phone_number: '',
  bill_to_address: '',
  invoice_date: '',
  invoice_due_date: '',
  tax: 0,

  companyName: 'Company Name',
  companyAddress: 'Jl. Sini aja',
  companyCountry: 'Indonesia',
  billToLabel: 'Bill To :',
  customerName: '',
  customerPhoneNumber: '',
  customerEmail: '',
  customerAddress: '',

  invoiceNumberLabel: '#Invoice ',

  notes: 'It was great doing business with you.',
  notesLabel: 'Notes',
  termLabel: 'Terms & Conditions',
  term: 'Please make the payment by the due date.',
}

const initialState = {
  pdfMode: false,
  drawerEditable: false,
  invoice: null,
}

const invoiceGeneratorReducer = (
  state: InvoiceGeneratorState = initialState,
  action: InvoiceAction
): InvoiceGeneratorState => {
  switch (action.type) {
    case ActionTypes.SET_INVOICE_PDF_MODE:
      return {
        ...state,
        pdfMode: action.payload,
      }
    case ActionTypes.SET_DRAWER_EDITABLE_INVOICE:
      return {
        ...state,
        drawerEditable: action.payload,
      }
    case ActionTypes.HANDLE_INVOICE_CHANGE:
      return {
        ...state,
        invoice: {
          ...state.invoice,
          [action.payload.property]: action.payload.value,
        },
      }
    case ActionTypes.SET_INVOICE_GENERATOR_DATA:
      return {
        ...state,
        invoice:
          action.payload === null
            ? action.payload
            : {
                // Merge with invoice data from jo with invoiceGenerator object
                ...invoiceGenerator,
                ...action.payload,
              },
      }
    default:
      return state
  }
}

export default invoiceGeneratorReducer
