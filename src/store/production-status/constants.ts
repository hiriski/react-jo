export const FETCHING_LIST_PRODUCTION_STATUS_REQUESTED = 'paymentStatus/FETCHING_LIST_PRODUCTION_STATUS_REQUESTED'
export const FETCHING_LIST_PRODUCTION_STATUS_LOADING = 'paymentStatus/FETCHING_LIST_PRODUCTION_STATUS_LOADING'
export const FETCHING_LIST_PRODUCTION_STATUS_FAILURE = 'paymentStatus/FETCHING_LIST_PRODUCTION_STATUS_FAILURE'
export const FETCHING_LIST_PRODUCTION_STATUS_SUCCESS = 'paymentStatus/FETCHING_LIST_PRODUCTION_STATUS_SUCCESS'
