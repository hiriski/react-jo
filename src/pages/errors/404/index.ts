import { lazy } from 'react'

const PageNotFound = lazy(() => import('./404'))

export default PageNotFound
