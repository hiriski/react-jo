export interface ICreateProduct {
  title: string
  description: string
  category_id: number
  images: Array<string>
}
