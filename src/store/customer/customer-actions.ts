import { TResponseListCustomer } from '@src/types/customer';
import { AnyAction } from 'redux';

// Interfaces.
import { ICustomer, IRequestBodyCustomer } from '@/interfaces/customer';

import { CUSTOMER_ACTION_TYPES as ActionTypes } from './customer-action-types.enum';
import { UUID } from '@/interfaces/common';

// Actions definiition

interface SetDrawerAddEditCustomerRequested {
  type: typeof ActionTypes.SET_DRAWER_ADD_EDIT_CUSTOMER_REQUESTED;
  payload: {
    open: boolean;
    id: string | null;
  };
}
interface SetDrawerAddEditCustomer {
  type: typeof ActionTypes.SET_DRAWER_ADD_EDIT_CUSTOMER;
  payload: {
    open: boolean;
    id: string | null;
  };
}
interface SetDialogDetailCustomer {
  type: typeof ActionTypes.SET_DIALOG_DETAIL_CUSTOMER;
  payload: {
    open: boolean;
    id: string | null;
  };
}
interface FetchingCustomerList {
  type: typeof ActionTypes.FETCHING_CUSTOMER_LIST_REQUESTED;
  payload?: Record<string, string>;
}
interface FetchingCustomerListLoading {
  type: typeof ActionTypes.FETCHING_CUSTOMER_LIST_LOADING;
}
interface FetchingCustomerListFailure {
  type: typeof ActionTypes.FETCHING_CUSTOMER_LIST_FAILURE;
}
interface FetchingCustomerListSuccess {
  type: typeof ActionTypes.FETCHING_CUSTOMER_LIST_SUCCESS;
  payload: TResponseListCustomer;
}

interface FetchingCustomer {
  type: typeof ActionTypes.FETCH_CUSTOMER_REQUESTED;
  payload: UUID;
}
interface FetchingCustomerLoading {
  type: typeof ActionTypes.FETCH_CUSTOMER_LOADING;
}
interface FetchingCustomerFailure {
  type: typeof ActionTypes.FETCH_CUSTOMER_FAILURE;
}
interface FetchingCustomerSuccess {
  type: typeof ActionTypes.FETCH_CUSTOMER_SUCCESS;
  payload: ICustomer;
}

interface CreateCustomer {
  type: typeof ActionTypes.CREATE_CUSTOMER_REQUESTED;
  payload: IRequestBodyCustomer;
}
interface CreateCustomerLoading {
  type: typeof ActionTypes.CREATE_CUSTOMER_LOADING;
}
interface CreateCustomerSuccesss {
  type: typeof ActionTypes.CREATE_CUSTOMER_SUCCESS;
  payload: ICustomer;
}
interface CreateCustomerFailure {
  type: typeof ActionTypes.CREATE_CUSTOMER_FAILURE;
}

interface UpdateCustomer {
  type: typeof ActionTypes.UPDATE_CUSTOMER_REQUESTED;
  payload: {
    id: UUID;
    body: IRequestBodyCustomer;
  };
}
interface UpdateCustomerLoading {
  type: typeof ActionTypes.UPDATE_CUSTOMER_LOADING;
}
interface UpdateCustomerSuccess {
  type: typeof ActionTypes.UPDATE_CUSTOMER_SUCCESS;
  payload: ICustomer;
}
interface UpdateCustomerFailure {
  type: typeof ActionTypes.UPDATE_CUSTOMER_FAILURE;
}

interface DeleteCustomer {
  type: typeof ActionTypes.DELETE_CUSTOMER_REQUESTED;
  payload: string;
}
interface DeleteCustomerLoading {
  type: typeof ActionTypes.DELETE_CUSTOMER_LOADING;
}
interface DeleteCustomerFailure {
  type: typeof ActionTypes.DELETE_CUSTOMER_FAILURE;
}
interface DeleteCustomerSuccess {
  type: typeof ActionTypes.DELETE_CUSTOMER_SUCCESS;
}

interface IResetCreateCustomer {
  type: typeof ActionTypes.RESET_CREATE_CUSTOMER_STATE;
}

interface ISetFormIsHasChanges {
  type: typeof ActionTypes.SET_FORM_IS_HAS_CHANGES;
  payload: boolean;
}

// Union action types
export type CustomerAction =
  | SetDrawerAddEditCustomerRequested
  | SetDrawerAddEditCustomer
  | SetDialogDetailCustomer
  | FetchingCustomerList
  | FetchingCustomerListLoading
  | FetchingCustomerListFailure
  | FetchingCustomerListSuccess
  | FetchingCustomer
  | FetchingCustomerLoading
  | FetchingCustomerFailure
  | FetchingCustomerSuccess
  | CreateCustomer
  | CreateCustomerLoading
  | CreateCustomerSuccesss
  | CreateCustomerFailure
  | UpdateCustomer
  | UpdateCustomerLoading
  | UpdateCustomerSuccess
  | UpdateCustomerFailure
  | DeleteCustomer
  | DeleteCustomerLoading
  | DeleteCustomerSuccess
  | DeleteCustomerFailure
  | IResetCreateCustomer
  | ISetFormIsHasChanges;

// Actions creator.

export const setDrawerAddEditCustomer = (open: boolean, id: string | null): SetDrawerAddEditCustomerRequested => ({
  type: ActionTypes.SET_DRAWER_ADD_EDIT_CUSTOMER_REQUESTED,
  payload: { open, id },
});

export const setDialogDetaiLCustomer = (open: boolean, id: string | null): SetDialogDetailCustomer => ({
  type: ActionTypes.SET_DIALOG_DETAIL_CUSTOMER,
  payload: { open, id },
});

/** fetch customer list */
export const fetchCustomerList = (payload?: Record<string, string>): FetchingCustomerList | AnyAction => ({
  type: ActionTypes.FETCHING_CUSTOMER_LIST_REQUESTED,
  payload,
});
export const fetchingCustomerListSuccess = (payload: TResponseListCustomer): FetchingCustomerListSuccess => ({
  type: ActionTypes.FETCHING_CUSTOMER_LIST_SUCCESS,
  payload,
});

/** fetch customer by id */
export const customer_fetchCustomer = (payload: UUID): FetchingCustomer => ({
  type: ActionTypes.FETCH_CUSTOMER_REQUESTED,
  payload,
});
export const customer_fetchCustomerLoading = (): FetchingCustomerLoading => ({
  type: ActionTypes.FETCH_CUSTOMER_LOADING,
});
export const customer_fetchCustomerFailure = (): FetchingCustomerFailure => ({
  type: ActionTypes.FETCH_CUSTOMER_FAILURE,
});
export const customer_fetchCustomerSuccess = (payload: ICustomer): FetchingCustomerSuccess => ({
  type: ActionTypes.FETCH_CUSTOMER_SUCCESS,
  payload,
});

/** create customer */
export const createCustomer = (payload: IRequestBodyCustomer): CreateCustomer => ({
  type: ActionTypes.CREATE_CUSTOMER_REQUESTED,
  payload,
});
export const createCustomerLoading = (): CreateCustomerLoading => ({
  type: ActionTypes.CREATE_CUSTOMER_LOADING,
});
export const createCustomerFailure = (): CreateCustomerFailure => ({
  type: ActionTypes.CREATE_CUSTOMER_FAILURE,
});
export const createCustomerSuccess = (payload: ICustomer): CreateCustomerSuccesss => ({
  type: ActionTypes.CREATE_CUSTOMER_SUCCESS,
  payload,
});

export const customer_resetCreateMaterialState = (): IResetCreateCustomer => ({
  type: ActionTypes.RESET_CREATE_CUSTOMER_STATE,
});

/** update customer */
export const customer_updateCustomer = (id: UUID, body: IRequestBodyCustomer): UpdateCustomer => ({
  type: ActionTypes.UPDATE_CUSTOMER_REQUESTED,
  payload: { id, body },
});
export const customer_updateCustomerLoading = (): UpdateCustomerLoading => ({
  type: ActionTypes.UPDATE_CUSTOMER_LOADING,
});
export const customer_updateCustomerFailure = (): UpdateCustomerFailure => ({
  type: ActionTypes.UPDATE_CUSTOMER_FAILURE,
});
export const customer_updateCustomerSuccess = (payload: ICustomer): UpdateCustomerSuccess => ({
  type: ActionTypes.UPDATE_CUSTOMER_SUCCESS,
  payload,
});

/** delete customer */
export const deleteCustomer = (payload: string): DeleteCustomer => ({
  type: ActionTypes.DELETE_CUSTOMER_REQUESTED,
  payload,
});
export const deleteCustomerLoading = (): DeleteCustomerLoading => ({
  type: ActionTypes.DELETE_CUSTOMER_LOADING,
});
export const deleteCustomerFailure = (): DeleteCustomerFailure => ({
  type: ActionTypes.DELETE_CUSTOMER_FAILURE,
});
export const deleteCustomerSuccess = (): DeleteCustomerSuccess => ({
  type: ActionTypes.DELETE_CUSTOMER_SUCCESS,
});

export const customer_setFormIsHasChanges = (payload: boolean): ISetFormIsHasChanges => ({
  type: ActionTypes.SET_FORM_IS_HAS_CHANGES,
  payload,
});
