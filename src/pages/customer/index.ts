import { lazy } from 'react';

export const CustomerPage = lazy(() => import('./customer'));
export const AddEditCustomerPage = lazy(() => import('./add-edit-customer'));
