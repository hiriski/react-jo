import { FC, forwardRef, ReactElement, ReactNode } from 'react';

// Mui components.
import Zoom, { ZoomProps } from '@mui/material/Zoom';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import { Box, IconButton, styled, Typography, SxProps } from '@mui/material';

// Mui icons.
import CloseIcon from '@mui/icons-material/Close';

// Base components.
import { BaseButton } from '@/components/base';

// Transition component.
const Transition = forwardRef<unknown, ZoomProps>(
  (props, ref): ReactElement => <Zoom unmountOnExit ref={ref} {...props} />,
);

const DialogTitle = styled(Box)(({ theme }) => ({
  marginBottom: theme.spacing(2),
}));
const DialogContent = styled(Box)(({ theme }) => ({
  '& p': {
    marginBottom: theme.spacing(1.5),
    '&:last-child': {
      marginBottom: 0,
    },
  },
}));
const DialogActions = styled(Box)(({ theme }) => ({
  display: 'flex',
  marginTop: theme.spacing(2),
  justifyContent: 'space-between',
}));

interface Props extends Omit<DialogProps, 'title'> {
  paperStyles?: SxProps;
  title: ReactNode;
  icon?: ReactNode;
  iconColor?: string;
  onConfirm: () => void;
  confirmButtonText?: string;
  disableCloseButton?: boolean;
  disableCancelButton?: boolean;
  disableConfirmButton?: boolean;
  cancelButtonText?: string;
  disableDialogActions?: boolean;
}

export const BaseDialog: FC<Props> = (props) => {
  const {
    sx,
    open,
    icon,
    iconColor,
    title,
    onClose,
    maxWidth,
    children,
    paperStyles,
    onConfirm,
    confirmButtonText,
    disableCloseButton,
    disableCancelButton,
    disableConfirmButton,
    disableDialogActions,
    cancelButtonText,
    ...rest
  } = props;
  return (
    <Dialog
      fullWidth
      maxWidth={maxWidth}
      open={open}
      keepMounted
      onClose={onClose}
      PaperProps={{
        elevation: 1,
        sx: {
          position: 'relative',
          borderRadius: 2,
          m: 0,
          px: {
            xs: 6,
            md: 9,
          },
          py: {
            xs: 3,
            md: 6,
          },
          // p: {
          //   xs: (theme) => theme.spacing(4, 6),
          //   md: (theme) => theme.spacing(7, 9),
          // },
          ...paperStyles,
        },
      }}
      sx={{ ...sx }}
      {...rest}
    >
      {icon && (
        <Box
          sx={{
            pb: 2,
            lineHeight: 1,
            textAlign: 'center',
            '& svg': { height: '60px !important', width: '60px !important', color: `${iconColor} !important` },
          }}
        >
          {icon}
        </Box>
      )}
      <DialogTitle>
        <Typography sx={{ fontSize: 24, fontWeight: 'bold' }}>{title}</Typography>
      </DialogTitle>

      {!disableCloseButton && (
        <IconButton
          onClick={onClose as () => void}
          sx={{ position: 'absolute', top: (theme) => theme.spacing(2.6), right: (theme) => theme.spacing(3) }}
        >
          <CloseIcon sx={{ fontSize: 22 }} />
        </IconButton>
      )}
      <DialogContent>{children}</DialogContent>
      {!disableDialogActions && (
        <DialogActions>
          {!disableCancelButton && (
            <BaseButton onClick={onClose as () => void} disableHoverEffect color="dark" size="medium" variant="text">
              {cancelButtonText}
            </BaseButton>
          )}
          <BaseButton
            disabled={disableConfirmButton}
            onClick={onConfirm}
            disableHoverEffect
            size="medium"
            sx={{ ml: 'auto' }}
          >
            {confirmButtonText}
          </BaseButton>
        </DialogActions>
      )}
    </Dialog>
  );
};

BaseDialog.defaultProps = {
  disableCancelButton: false,
  cancelButtonText: 'Cancel',
  confirmButtonText: 'Ok',
  disableDialogActions: false,
  maxWidth: 'xs',
  iconColor: 'inherit',
};

export default BaseDialog;
