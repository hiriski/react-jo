import { BoxSpinner } from '@/components/ui'
import { DialogDetailProduct, DrawerAddEditProduct, ProductFAB, ProductListContainer } from '@/pages/product/components'
import { useAppSelector } from '@/store/hook'
import {
  fetchProduct,
  fetchProductList as fetchProducts,
  setDialogDetailProduct,
} from '@/store/product/product-actions'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import { MainLayout } from '@src/components/layouts'
import { FC, useEffect, useMemo } from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import { batch, useDispatch } from 'react-redux'
import { useParams } from 'react-router'

const ITEM_PER_PAGE = 12

const ProductPage: FC = () => {
  const dispatch = useDispatch()
  const { productList } = useAppSelector((state) => state.product)
  const { isFetching, meta, links } = productList
  const params = useParams()

  // Has more
  const hasMore = useMemo(() => {
    if (links?.next) return true
    else return false
  }, [links?.next])

  // Fetch product list.
  const fetchProductList = (params?: Record<string, string | number>): void => {
    if (!params) dispatch(fetchProducts({ limit: ITEM_PER_PAGE }))
    else dispatch(fetchProducts(params))
  }

  // Effect.
  useEffect(() => {
    if (!params.uuid) {
      fetchProductList({ limit: ITEM_PER_PAGE })
    } else {
      batch(() => {
        dispatch(setDialogDetailProduct(true, params.uuid))
        dispatch(fetchProduct(params.uuid))

        // Also page product list.
        // fetchProductList({ limit }) DON'T DO
      })
    }
  }, [params.uuid])

  // Handle refresh
  const handleRefresh = (): void => {
    fetchProductList()
  }

  // Handle next fetch
  const handleFetch = (): void => {
    fetchProductList({ page: meta.current_page + 1, limit: ITEM_PER_PAGE })
  }

  return (
    <MainLayout>
      <Box sx={{ py: 4 }}>
        <Typography variant="h5">Product Page</Typography>
        <InfiniteScroll
          dataLength={productList.data.length} //This is important field to render the next data
          next={handleFetch}
          hasMore={hasMore}
          loader={<BoxSpinner height={100} />}
          scrollThreshold="300px"
          endMessage={
            <Typography sx={{ pt: 2 }} textAlign="center">
              Yay! You have seen it all
            </Typography>
          }
          // below props only if you need pull down functionality
          // refreshFunction={handleRefresh}
          // pullDownToRefresh
          // pullDownToRefreshThreshold={100}
          // pullDownToRefreshContent={
          //   <Typography style={{ textAlign: 'center' }}>&#8595; Pull down to refresh</Typography>
          // }
          // releaseToRefreshContent={<Typography style={{ textAlign: 'center' }}>&#8593; Release to refresh</Typography>}
        >
          <ProductListContainer products={productList.data} isFetching={isFetching} />
        </InfiniteScroll>
      </Box>

      {/* Dialog detail product */}
      <DialogDetailProduct />

      {/* FAB add product */}
      <ProductFAB />

      {/* Drawer add or edit product */}
      <DrawerAddEditProduct />
    </MainLayout>
  )
}

export default ProductPage
