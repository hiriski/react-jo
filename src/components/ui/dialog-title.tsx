import { DialogTitleProps, DialogTitle as MuiDialogTitle } from '@mui/material'
import { FC } from 'react'

type Props = DialogTitleProps

const DialogTitle: FC<Props> = ({ children }: Props) => (
  <MuiDialogTitle sx={{ px: 5, py: 3 }}>{children}</MuiDialogTitle>
)

export default DialogTitle
