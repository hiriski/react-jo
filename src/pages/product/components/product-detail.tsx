import { IProduct } from '@/types/product'
import { createArraySkeleton } from '@/utils/skeleton'
import { Skeleton, useTheme } from '@mui/material'
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import { FC, memo, useMemo } from 'react'
import ReactIDSwiper from 'react-id-swiper'

interface Props {
  product: IProduct
  isLoading: boolean
}

const ProductDetail: FC<Props> = ({ product, isLoading }) => {
  const arrSkeleton = createArraySkeleton(3)
  const theme = useTheme()

  const isReady = useMemo(() => {
    return Boolean(product) && !isLoading
  }, [product, isLoading])

  const swiperConfig = {
    slidesPerView: 'auto' as number | 'auto',
    spaceBetween: 16,
    pagination: {
      el: '.swiper-pagination',
      // type: 'progressbar',
      clickable: true,
      renderBullet: (index, className) => {
        return '<span class="' + className + '">' + (index + 1) + '</span>'
      },
    },
  }

  return (
    <Box>
      {/* Product images */}
      <Box sx={{ mb: 3 }}>
        {isReady ? (
          Array.isArray(product.images) &&
          product.images.length > 0 && (
            // <Grid container spacing={2}>
            <ReactIDSwiper {...swiperConfig}>
              {product.images.map((img, index) => (
                <Box
                  key={img.id}
                  sx={{
                    backgroundColor: '#eee',
                    height: 150,
                    width: 270,
                    overflow: 'hidden',
                    borderRadius: 1,
                    '& img': { width: '100%', height: 'auto' },
                  }}
                >
                  <img src={img.image_sm} alt={product.title + '- image ' + index} />
                </Box>
              ))}
            </ReactIDSwiper>
            // </Grid>
          )
        ) : (
          <Box
            sx={{
              position: 'relative',
              width: '100%',
              height: '100%',
              zIndex: '1',
              display: 'flex',
              transitionProperty: 'transform',
              boxSizing: 'content-box',
            }}
          >
            <div className="swiper-wrapper" style={{ transform: 'translate3d(0px, 0px, 0px)' }}>
              {arrSkeleton.map((item, index) => (
                <Skeleton
                  animation="wave"
                  variant="rectangular"
                  height={150}
                  sx={{ height: 150, width: 270, borderRadius: 1, flexShrink: 0, mr: 2, position: 'relative' }}
                />
              ))}
            </div>
          </Box>
        )}
      </Box>

      {/* Product title */}
      <Box sx={{ mb: 2 }}>
        <Typography component="h1" variant="h3">
          {isReady ? product.title : <Skeleton animation="wave" width="40%" />}
        </Typography>
      </Box>

      {/* Product description */}
      <Box sx={{ mb: 2 }}>
        {isReady ? (
          <>
            <Typography variant="subtitle2">Description</Typography>
            <Typography>{product.description}</Typography>
          </>
        ) : (
          <>
            <Skeleton animation="wave" width="70%" />
            <Skeleton animation="wave" width="60%" />
          </>
        )}
      </Box>

      {/* Product category  */}
      {isReady ? <Typography>Category : {product.category.name}</Typography> : <Skeleton animation="wave" />}
    </Box>
  )
}

const MemoizedProductDetail = memo(ProductDetail)
export default MemoizedProductDetail
