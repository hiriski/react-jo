import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';
import LocalPhoneOutlinedIcon from '@mui/icons-material/LocalPhoneOutlined';
import LocationOnOutlinedIcon from '@mui/icons-material/LocationOnOutlined';
import WhatsappOutlinedIcon from '@mui/icons-material/WhatsappOutlined';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import {
  deleteCustomer,
  setDialogDetaiLCustomer,
  setDrawerAddEditCustomer,
} from '@src/store/customer/customer-actions';
import { TCustomer } from '@src/types/customer';
import { colors } from '@src/utils/colors';
import { getInitialsName } from '@src/utils/misc';
import { createWhatsappPhoneNumber } from '@src/utils/phone-number';
import { FC, memo } from 'react';
import { useDispatch } from 'react-redux';

type Props = {
  data: TCustomer;
};

const CustomerDetail: FC<Props> = ({ data }: Props) => {
  const dispatch = useDispatch();

  // const customer = useMemo(() => item, [item])

  const handleEdit = (customerId: string): void => {
    dispatch(setDrawerAddEditCustomer(true, customerId));
    dispatch(setDialogDetaiLCustomer(false, null));
  };

  const handleDelete = (): void => {
    dispatch(deleteCustomer(data.id));
  };

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        position: 'relative',
      }}
    >
      <IconButton
        color="inherit"
        sx={{ position: 'absolute', top: 10, right: 10 }}
        size="small"
        onClick={() => handleEdit(data.id)}
      >
        <EditOutlinedIcon sx={{ fontSize: 18, color: 'text.disabled' }} />
      </IconButton>
      <Box sx={{ mb: 2 }}>
        <Avatar
          src={data.photo_url ?? null}
          sx={{
            // bgcolor: colors[Math.floor(Math.random() * colors.length)],
            backgroundColor: data.avatar_text_color ?? colors[Math.floor(Math.random() * colors.length)],
            color: 'primary.contrastText',
            width: 64,
            height: 64,
            fontSize: 16,
            letterSpacing: 1,
          }}
          alt={data.name}
        >
          {getInitialsName(data.name)}
        </Avatar>
      </Box>
      <Box>
        <Typography sx={{ mb: 1.5, mt: 0.6, fontSize: 20 }} variant="h5" component="h4">
          {data.name}
        </Typography>

        <List sx={{ width: '100%' }} disablePadding>
          <ListItem disableGutters disablePadding>
            <ListItemIcon sx={{ minWidth: 32 }}>
              <LocalPhoneOutlinedIcon sx={{ fontSize: 18 }} />
            </ListItemIcon>

            <ListItemText primary={data.phone_number ?? '-'} />
          </ListItem>

          <ListItem disableGutters disablePadding>
            <ListItemIcon sx={{ minWidth: 32 }}>
              <EmailOutlinedIcon sx={{ fontSize: 18 }} />
            </ListItemIcon>
            <ListItemText primary={data.email ?? '-'} />
          </ListItem>

          <ListItem disableGutters disablePadding>
            <ListItemIcon sx={{ minWidth: 32 }}>
              <LocationOnOutlinedIcon sx={{ fontSize: 18 }} />
            </ListItemIcon>
            {/* <ListItemText primary={data.address ?? '-'} /> */}
          </ListItem>
        </List>
        {/* CTA */}
        <Box sx={{ mt: 6, textAlign: 'center' }}>
          {data.phone_number && (
            <Stack
              direction="row"
              spacing={3}
              sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}
            >
              <Button
                size="small"
                component="a"
                target="_blank"
                href={`https://api.whatsapp.com/send?phone=${createWhatsappPhoneNumber(data.phone_number)}&text=`}
                startIcon={<WhatsappOutlinedIcon />}
                variant="contained"
                disableElevation
                sx={{ backgroundColor: '#00a248' }}
              >
                Chat Pelanggan
              </Button>
              <Button
                size="small"
                color="error"
                onClick={handleDelete}
                startIcon={<DeleteOutlinedIcon />}
                variant="outlined"
                disableElevation
              >
                Delete
              </Button>
            </Stack>
          )}
        </Box>
      </Box>
    </Box>
  );
};

const MemoizedCustomerDetail = memo(CustomerDetail);

export default MemoizedCustomerDetail;
