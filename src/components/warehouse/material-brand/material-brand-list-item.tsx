import { Box } from '@mui/material';
import { FC } from 'react';
import { MaterialBrandItem } from './material-brand-item';
import ReactIDSwiper from 'react-id-swiper';

// Interfaces.
import { IMaterialBrand } from '@/interfaces/warehouse';

interface Props {
  items: IMaterialBrand[];
}

export const MaterialBrandListItem: FC<Props> = ({ items }) => {
  const swiperConfig = {
    slidesPerView: 'auto' as number | 'auto',
    spaceBetween: 16,
    pagination: {
      el: '.swiper-pagination',
      // type: 'progressbar',
      clickable: true,
      renderBullet: (index, className) => {
        return '<span class="' + className + '">' + (index + 1) + '</span>';
      },
    },
  };

  return (
    <Box>
      <ReactIDSwiper {...swiperConfig}>
        {items.map((item) => (
          <MaterialBrandItem key={item.id} item={item} />
        ))}
      </ReactIDSwiper>
    </Box>
  );
};
