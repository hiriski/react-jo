export const RoleIdConstants = {
  SuperAdmin: 1,
  Admin: 2,
  WarehouseTeam: 3,
  ProductionTeam: 4,
  Owner: 5,
  Management: 6,
  Cashier: 7,
  CustomerService: 8,
  Maintainer: 9,
};
