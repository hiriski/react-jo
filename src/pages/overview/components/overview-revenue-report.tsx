import { FC, MouseEvent, useEffect, useMemo, useState } from 'react';
import { BaseAnalyticsWidget, BaseCard, BaseToggleButton } from '@/components/base';
import { green, blue, indigo, orange } from '@mui/material/colors';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

// Apex chart.
import ReactApexChart from 'react-apexcharts';
import { ApexOptions } from 'apexcharts';

import { baseOptionsChart } from '@/components/chart/base-options-chart';
import useTheme from '@mui/material/styles/useTheme';
import { dropdownJobOrderPeriod } from '@/lib/dropdown';
import { getRandomArbitrary, getRandomInt } from '@/utils/common';
import { formatRupiah } from '@/utils/currency';

type TPeriod = 'last_week' | 'this_week' | 'last_month' | 'this_month' | 'this_year';

const toggleList = [
  {
    label: 'Minggu Lalu',
    value: 'last_week',
  },
  {
    label: 'Minggu ini',
    value: 'this_week',
  },
  {
    label: 'Bulan Lalu',
    value: 'last_month',
  },
  {
    label: 'Bulan ini',
    value: 'this_month',
  },
  {
    label: 'Tahun ini',
    value: 'this_year',
  },
];

const randomArrNumber = (min: number, max: number, arrLength: number): Array<number> => {
  //Use below if final number doesn't need to be whole number
  return Array.from({ length: arrLength }, () => Math.floor(Math.random() * (max - min) + min));
};

function nFormatter(num: number): number | string {
  if (num >= 1000000000) {
    return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'M';
  }
  if (num >= 1000000) {
    return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'Jt';
  }
  if (num >= 1000) {
    return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
  }
  return num;
}

export const OverviewRevenueReport: FC = () => {
  const theme = useTheme();
  const [selectedPeriod, setSelectedPeriod] = useState<TPeriod>('this_week');
  const [unavailableChartData, setUnavailableChartData] = useState<boolean>(false);

  const CHART_DATA_lastWeek = [
    {
      name: 'Print UV',
      data: randomArrNumber(750000, 5000000, 7),
    },
    {
      name: 'Print Stiker',
      data: randomArrNumber(750000, 5000000, 7),
    },
    {
      name: 'Laser Cutting',
      data: randomArrNumber(750000, 5000000, 7),
    },
    {
      name: 'Other',
      data: randomArrNumber(750000, 5000000, 7),
    },
  ];

  const CHART_DATA_thisWeek = [
    {
      name: 'Print UV',
      data: randomArrNumber(400000, 5000000, 4),
    },
    {
      name: 'Print Stiker',
      data: randomArrNumber(400000, 5000000, 4),
    },
    {
      name: 'Laser Cutting',
      data: randomArrNumber(400000, 5000000, 4),
    },
    {
      name: 'Other',
      data: randomArrNumber(400000, 5000000, 4),
    },
  ];

  const CHART_DATA_lastMonth = [
    {
      name: 'Print UV',
      data: randomArrNumber(750000, 10000000, 30),
    },
    {
      name: 'Print Stiker',
      data: randomArrNumber(750000, 10000000, 30),
    },
    {
      name: 'Laser Cutting',
      data: randomArrNumber(750000, 10000000, 30),
    },
    {
      name: 'Other',
      data: randomArrNumber(750000, 10000000, 30),
    },
  ];

  const CHART_DATA_thisMonth = [
    {
      name: 'Print UV',
      data: randomArrNumber(750000, 7000000, 18),
    },
    {
      name: 'Print Stiker',
      data: randomArrNumber(750000, 7000000, 18),
    },
    {
      name: 'Laser Cutting',
      data: randomArrNumber(750000, 7000000, 18),
    },
    {
      name: 'Other',
      data: randomArrNumber(750000, 7000000, 18),
    },
  ];

  const CHART_DATA_thisYear = [
    {
      name: 'Print UV',
      data: randomArrNumber(40000000, 200000000, 7),
    },
    {
      name: 'Print Stiker',
      data: randomArrNumber(40000000, 200000000, 7),
    },
    {
      name: 'Laser Cutting',
      data: randomArrNumber(40000000, 200000000, 7),
    },
    {
      name: 'Other',
      data: randomArrNumber(40000000, 200000000, 7),
    },
  ];

  const LABEL_DATA_lastWeek = [
    '07/11/2022',
    '07/12/2022',
    '07/13/2022',
    '07/14/2022',
    '07/15/2022',
    '07/16/2022',
    '07/17/2022',
  ];
  const LABEL_DATA_thisWeek = ['07/18/2022', '07/19/2022', '07/20/2022', '07/21/2022'];
  const LABEL_DATA_lastMonth = [
    '06/01/2022',
    '06/02/2022',
    '06/03/2022',
    '06/04/2022',
    '06/05/2022',
    '06/06/2022',
    '06/07/2022',
    '06/08/2022',
    '06/09/2022',
    '06/10/2022',
    '06/11/2022',
    '06/12/2022',
    '06/13/2022',
    '06/14/2022',
    '06/15/2022',
    '06/16/2022',
    '06/17/2022',
    '06/18/2022',
    '06/19/2022',
    '06/20/2022',
    '06/21/2022',
    '06/22/2022',
    '06/23/2022',
    '06/24/2022',
    '06/25/2022',
    '06/26/2022',
    '06/27/2022',
    '06/28/2022',
    '06/29/2022',
    '06/30/2022',
  ];
  const LABEL_DATA_thisMonth = [
    '07/01/2022',
    '07/02/2022',
    '07/03/2022',
    '07/04/2022',
    '07/05/2022',
    '07/06/2022',
    '07/07/2022',
    '07/08/2022',
    '07/09/2022',
    '07/10/2022',
    '07/11/2022',
    '07/12/2022',
    '07/13/2022',
    '07/14/2022',
    '07/15/2022',
    '07/16/2022',
    '07/17/2022',
    '07/18/2022',
  ];
  const LABEL_DATA_thisYear = [
    '01/01/2022',
    '02/01/2022',
    '03/01/2022',
    '04/01/2022',
    '05/01/2022',
    '06/01/2022',
    '07/01/2022',
  ];

  const chartData = useMemo(() => {
    switch (selectedPeriod) {
      case 'last_week':
        return CHART_DATA_lastWeek;
        break;
      case 'this_week':
        return CHART_DATA_thisWeek;
        break;
      case 'last_month':
        return CHART_DATA_lastMonth;
        break;
      case 'this_month':
        return CHART_DATA_thisMonth;
        break;
      case 'this_year':
        return CHART_DATA_thisYear;
        break;
      default:
        return CHART_DATA_lastWeek;
        break;
    }
  }, [selectedPeriod]);

  const labelData = useMemo(() => {
    switch (selectedPeriod) {
      case 'last_week':
        return LABEL_DATA_lastWeek;
        break;
      case 'this_week':
        return LABEL_DATA_thisWeek;
        break;
      case 'last_month':
        return LABEL_DATA_lastMonth;
        break;
      case 'this_month':
        return LABEL_DATA_thisMonth;
        break;
      case 'this_year':
        return LABEL_DATA_thisYear;
        break;
      default:
        return LABEL_DATA_lastWeek;
        break;
    }
  }, [selectedPeriod]);

  useEffect(() => {
    if (!chartData) {
      setUnavailableChartData(true);
    }
  }, [chartData]);

  const chartOptions: ApexOptions = {
    chart: {
      toolbar: { show: false },
      zoom: { enabled: false },
      foreColor: theme.palette.text.disabled,
      fontFamily: theme.typography.fontFamily,
      animations: { enabled: true },
    },

    stroke: { width: 2, curve: 'smooth' },

    // Markers
    markers: {
      size: 0,
      strokeColors: theme.palette.background.paper,
    },

    xaxis: {
      type: 'category',
      axisBorder: { show: false },
      axisTicks: { show: false },
    },

    yaxis: {
      // show: false,
      labels: {
        style: {
          fontWeight: 'bold',
        },
        formatter: (number): string => {
          return nFormatter(number) as string;
        },
      },
    },

    grid: {
      strokeDashArray: 3,
      borderColor: theme.palette.divider,
    },

    fill: {
      type: 'gradient',
      gradient: {
        type: 'vertical',
        shadeIntensity: 0,
        opacityFrom: 0.4,
        opacityTo: 0,
        stops: [0, 100],
      },
    },

    // Legend
    legend: {
      show: true,
      fontSize: '12px',
      position: 'top',
      horizontalAlign: 'right',
      markers: {
        radius: 12,
      },
      fontWeight: 500,
      itemMargin: { horizontal: 12 },
      labels: {
        colors: theme.palette.text.primary,
      },
    },

    // Datalabels
    dataLabels: { enabled: false },

    labels: labelData,

    // Responsive
    responsive: [
      {
        // sm
        breakpoint: theme.breakpoints.values.sm,
        options: {
          plotOptions: { bar: { columnWidth: '40%' } },
        },
      },
      {
        // md
        breakpoint: theme.breakpoints.values.md,
        options: {
          plotOptions: { bar: { columnWidth: '32%' } },
        },
      },
    ],

    tooltip: {
      shared: true,
      intersect: false,
      y: {
        formatter: (y) => {
          if (typeof y !== 'undefined') {
            return formatRupiah(y);
          }
          return y;
        },
      },
    },
  };

  const handleChangeToggleButton = (event: MouseEvent<HTMLElement>, value: TPeriod): void => {
    setSelectedPeriod(value);
  };

  return (
    <BaseCard
      sx={{ position: 'relative' }}
      title="Laporan Pendapatan"
      subtitle="Statistik Laporan Pendapatan Berdasarkan Kategori Job Order"
      rightContent={
        <Box>
          <BaseToggleButton exclusive value={selectedPeriod} onChange={handleChangeToggleButton} data={toggleList} />
        </Box>
      }
    >
      {unavailableChartData ? (
        <Box>
          <Typography>Belum ada data yang bisa ditampilkan saat ini</Typography>
        </Box>
      ) : (
        <ReactApexChart type="area" series={chartData} options={chartOptions as ApexOptions} height={320} />
      )}
    </BaseCard>
  );
};
