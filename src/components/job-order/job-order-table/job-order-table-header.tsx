import { RoleIdConstants } from '@/constants/role-id-constants';
import { alpha } from '@mui/material';
import Box from '@mui/material/Box';
import Checkbox from '@mui/material/Checkbox';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Typography from '@mui/material/Typography';
import { visuallyHidden } from '@mui/utils';
import { ChangeEvent, FC, MouseEvent } from 'react';

type Order = 'asc' | 'desc';

interface HeadCell {
  disablePadding: boolean;
  id: string /* keyof Data */;
  label: string;
  numeric: boolean;
  enableSort: boolean;
  width?: number;
  align?: 'inherit' | 'left' | 'center' | 'right' | 'justify';
  roles?: 'all' | Array<number>;
}

const headCells: Array<HeadCell> = [
  {
    id: 'orderNumber',
    numeric: false,
    disablePadding: true,
    label: 'Job Order',
    enableSort: false,
    width: 100,
    roles: 'all',
  },
  {
    id: 'orderDate',
    numeric: false,
    disablePadding: false,
    label: 'Tanggal',
    enableSort: false,
    width: 90,
    roles: 'all',
  },
  {
    id: 'dueDate',
    numeric: false,
    disablePadding: false,
    label: 'Duedate',
    enableSort: false,
    width: 90,
    roles: 'all',
  },
  {
    id: 'category',
    numeric: false,
    disablePadding: false,
    label: 'Kategori',
    enableSort: false,
    width: 100,
    roles: 'all',
  },
  {
    id: 'customer',
    numeric: false,
    disablePadding: false,
    label: 'Customer',
    enableSort: false,
    width: 160,
    roles: [
      RoleIdConstants.SuperAdmin,
      RoleIdConstants.Admin,
      RoleIdConstants.Cashier,
      RoleIdConstants.CustomerService,
      RoleIdConstants.Owner,
      RoleIdConstants.Management,
    ],
  },
  {
    id: 'paymentStatus',
    numeric: false,
    disablePadding: false,
    label: 'Pembayaran',
    enableSort: false,
    width: 120,
    roles: [
      RoleIdConstants.SuperAdmin,
      RoleIdConstants.Admin,
      RoleIdConstants.Cashier,
      RoleIdConstants.CustomerService,
      RoleIdConstants.Owner,
      RoleIdConstants.Management,
    ],
  },
  // {
  //   id: 'total',
  //   numeric: true,
  //   disablePadding: false,
  //   label: 'Total (Rp)',
  //   enableSort: false,
  //   width: 120,
  //   roles: [
  //     RoleIdConstants.SuperAdmin,
  //     RoleIdConstants.Admin,
  //     RoleIdConstants.Cashier,
  //     RoleIdConstants.CustomerService,
  //     RoleIdConstants.Owner,
  //     RoleIdConstants.Management,
  //   ],
  // },
  {
    id: 'productionStatus',
    numeric: false,
    disablePadding: false,
    label: 'Status',
    enableSort: false,
    width: 100,
    roles: 'all',
  },
  // {
  //   id: 'action',
  //   numeric: false,
  //   disablePadding: false,
  //   label: 'Actions',
  //   enableSort: false,
  //   width: 40,
  //   roles: 'all',
  // },
];

interface Props {
  numSelected: number;
  onRequestSort: (event: MouseEvent<unknown>, property: string /* keyof Data */) => void;
  onSelectAllClick: (event: ChangeEvent<HTMLInputElement>) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
}

export const JobOrderTableHeader: FC<Props> = (props: Props) => {
  const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
  const createSortHandler = (property: string /* keyof Data */) => (event: MouseEvent<unknown>) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead
      sx={{
        '& *': {
          lineHeight: '1.4 !important',
        },
        '& .MuiTableCell-sizeMedium': {
          padding: '12px 6px !important',
          backgroundColor: 'background.paper',
          // borderBottom: '1px solid rgb(183 183 183)',
          borderBottom: (theme) => `1px solid ${alpha(theme.palette.text.primary, 0.1)}`,
        },
        '& .MuiTableCell-paddingCheckbox': {
          padding: '12px 2px !important',
        },
      }}
    >
      <TableRow>
        <TableCell padding="checkbox" sx={{ width: 10 }}>
          <Checkbox
            size="small"
            color="primary"
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{
              'aria-label': 'select all desserts',
            }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.align ?? 'left'}
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
            sx={{ maxWidth: headCell.width ?? 'auto' }}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              <Typography variant="subtitle1" sx={{ color: 'text.secondary' }}>
                {headCell.label}
              </Typography>
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell padding="checkbox" sx={{ width: 50 }}>
          <Typography variant="subtitle1" sx={{ color: 'text.secondary' }}>
            Actions
          </Typography>
        </TableCell>
      </TableRow>
    </TableHead>
  );
};
