import { SIDEBAR_DRAWER_WIDTH } from '@/utils/constants';

export const AppConfig = {
  DrawerAddEditJobOrderWidth: 1000,
  SidebarDrawerWidth: SIDEBAR_DRAWER_WIDTH,
  DatabaseFormatTimestamps: 'YYYY-MM-DD HH:mm:ss',
};
