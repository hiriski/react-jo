import { ICustomer } from '@/interfaces/customer';
import { IUser } from './user';
import { IMaterial } from './warehouse';

interface IDefaultValueMasterData {
  id: number;
  name: string;
}

export type IMasterDataObjectNameValue = {
  object_name:
    | 'product_categories'
    | 'job_order_categories'
    | 'materials'
    | 'material_brands'
    | 'material_categories'
    | 'material_types'
    | 'customers'
    | 'payment_methods'
    | 'payment_statuses'
    | 'bank_accounts'
    | 'users';
};

export type TMasterDataCustomer = ICustomer;

export interface IMasterData {
  product_categories: Array<IDefaultValueMasterData>;
  job_order_categories: Array<IDefaultValueMasterData>;
  materials: Array<IMaterial>;
  material_brands: Array<IDefaultValueMasterData>;
  material_categories: Array<IDefaultValueMasterData>;
  material_types: Array<IDefaultValueMasterData>;
  customers: Array<ICustomer>;
  payment_methods: Array<IDefaultValueMasterData & { image: string }>;
  payment_statuses: Array<IDefaultValueMasterData>;
  bank_accounts: Array<IDefaultValueMasterData>;
  users: Array<IUser>;
}

export type TRequestMasterData = Array<IMasterDataObjectNameValue>;
export type RequestBodyMasterData = Array<{ object_name: string }>;
