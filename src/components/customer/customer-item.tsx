import { ROUTES } from '@/utils/constants';
import { Card } from '@components/ui';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';
import LocalPhoneOutlinedIcon from '@mui/icons-material/LocalPhoneOutlined';
import LocationOnOutlinedIcon from '@mui/icons-material/LocationOnOutlined';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import { setDialogDetaiLCustomer, setDrawerAddEditCustomer } from '@src/store/customer/customer-actions';
import { TCustomer } from '@src/types/customer';
import { colors } from '@src/utils/colors';
import { getInitialsName } from '@src/utils/misc';
import { FC, memo } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';

type Props = {
  item: TCustomer;
};
const CustomerItem: FC<Props> = ({ item }: Props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // const customer = useMemo(() => item, [item])

  const handleEdit = (customerId: string): void => {
    // dispatch(setDrawerAddEditCustomer(true, customerId));
    navigate(`${ROUTES.EDIT_CUSTOMER}/${customerId}`);
  };

  const handleDetailClick = (customerId: string): void => {
    dispatch(setDialogDetaiLCustomer(true, customerId));
  };

  return (
    <Card>
      <Box
        sx={{
          display: 'flex',
          position: 'relative',
          px: 2.8,
          py: 2.4,
        }}
      >
        <IconButton
          color="inherit"
          sx={{ position: 'absolute', top: 10, right: 10 }}
          size="small"
          onClick={() => handleEdit(item.id)}
        >
          <EditOutlinedIcon sx={{ fontSize: 18, color: 'text.disabled' }} />
        </IconButton>
        <Box sx={{ mr: 2 }} onClick={() => handleDetailClick(item.id)}>
          <Avatar
            sx={{
              bgcolor: colors[Math.floor(Math.random() * colors.length)],
              color: 'primary.contrastText',
              width: 38,
              height: 38,
              fontSize: 14,
              letterSpacing: 1,
            }}
            alt={item.name}
          >
            {getInitialsName(item.name)}
          </Avatar>
        </Box>
        <Box sx={{ flex: 1 }} onClick={() => handleDetailClick(item.id)}>
          <Typography sx={{ mb: 1.5, mt: 0.6, fontSize: 18 }} variant="h5" component="h4">
            {item.name}
          </Typography>

          <List sx={{ width: '100%' }} disablePadding>
            <ListItem disableGutters disablePadding>
              <ListItemIcon sx={{ minWidth: 32 }}>
                <LocalPhoneOutlinedIcon sx={{ fontSize: 18 }} />
              </ListItemIcon>

              <ListItemText
                sx={{
                  '* ': {
                    fontSize: 15,
                  },
                }}
                primary={item.phone_number ?? '-'}
              />
            </ListItem>

            <ListItem disableGutters disablePadding>
              <ListItemIcon sx={{ minWidth: 32 }}>
                <EmailOutlinedIcon sx={{ fontSize: 18 }} />
              </ListItemIcon>
              <ListItemText
                sx={{
                  '* ': {
                    fontSize: 15,
                  },
                }}
                primary={item.email ?? '-'}
              />
            </ListItem>

            <ListItem disableGutters disablePadding>
              <ListItemIcon sx={{ minWidth: 32 }}>
                <LocationOnOutlinedIcon sx={{ fontSize: 18 }} />
              </ListItemIcon>
              <ListItemText
                sx={{
                  height: 20,
                  textOverflow: 'ellipsis',
                  overflow: 'hidden',
                  '* ': {
                    fontSize: 15,
                  },
                }}
                // primary={item.address ?? '-'}
              />
            </ListItem>
          </List>
        </Box>
      </Box>
    </Card>
  );
};

const MemoizedCustomerItem = memo(CustomerItem);

export default MemoizedCustomerItem;
