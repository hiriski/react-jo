import AddOutlinedIcon from '@mui/icons-material/AddOutlined'
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutline'
import EditOutlinedIcon from '@mui/icons-material/EditOutlined'
import ReceiptLongOutlinedIcon from '@mui/icons-material/ReceiptLongOutlined'
import ShowChartOutlinedIcon from '@mui/icons-material/ShowChartOutlined'
import Box from '@mui/material/Box'
import { green, red } from '@mui/material/colors'
import IconButton from '@mui/material/IconButton'
import { Theme } from '@mui/material/styles'
import Typography from '@mui/material/Typography'
import {
  setDialogAddEditInventory,
  setDialogAddStockMaterialPaper,
  setDrawerDetailInventory,
} from '@src/store/inventory/actions'
import { TMaterialPaper } from '@src/types/inventory'
import { cmToMeters } from '@src/utils/unit'
import { FC } from 'react'
import { useDispatch } from 'react-redux'

type Props = {
  item: TMaterialPaper
}
const BrandWithMaterialPaperItem: FC<Props> = ({ item }: Props) => {
  const dispatch = useDispatch()

  const onEdit = (phraseId: number): void => {
    // dispatch(setDrawerAddEditPhrase(true, phraseId))
  }

  const handleEdit = (): void => {
    dispatch(setDialogAddEditInventory({ open: true, id: item.id, type: 'material_paper', brand_id: item.brand.id }))
  }

  const onClick = (paramId: number): void => {
    null
  }

  const handleClickAddStock = (): void => {
    dispatch(setDialogAddStockMaterialPaper({ open: true, id: item.id, current_stock: item.stock_length }))
  }

  return (
    <Box
      onClick={() => onClick(item.id)}
      sx={{
        ml: 10,
        position: 'relative',
        borderBottom: (theme: Theme) => `1px solid ${theme.palette.divider}`,
      }}
    >
      {/* Inner */}
      <Box sx={{ display: 'flex', pr: 3, py: 1.2 }}>
        {/* Icon */}
        <Box sx={{ width: 30, mt: 0.2 }}>
          <ReceiptLongOutlinedIcon sx={{ color: 'text.secondary', fontSize: 20 }} />
        </Box>
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            flex: 1,
          }}
        >
          <Box>
            <Typography component="h6" variant="h4" sx={{ fontSize: 16, mb: 0.3 }}>
              {item.name}
            </Typography>
            <Typography variant="subtitle2" color="text.secondary">
              {item.brand && item.brand.name}, {item.material_width}cm
            </Typography>
          </Box>

          <Box sx={{ ml: 'auto', display: 'flex', alignItems: 'center' }}>
            {/* Actions button */}
            <Box sx={{ mr: 4 }}>
              <IconButton onClick={handleClickAddStock} size="medium" color="primary">
                <AddOutlinedIcon sx={{ fontSize: 20 }} />
              </IconButton>
              <IconButton size="medium" onClick={handleEdit}>
                <EditOutlinedIcon sx={{ fontSize: 20 }} />
              </IconButton>
              <IconButton size="medium" color="error">
                <DeleteOutlinedIcon sx={{ fontSize: 20 }} />
              </IconButton>
            </Box>

            {/* Status */}
            <Box sx={{ minWidth: 90 }}>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <ShowChartOutlinedIcon sx={{ fontSize: 18 }} />
                <Typography
                  variant="h6"
                  sx={{
                    color: item.stock_length >= 5000 ? green['500'] : red['500'],
                    ml: 1,
                  }}
                >
                  {cmToMeters(item.stock_length) + ' m'}
                </Typography>
              </Box>
              <Typography variant="subtitle2" color="text.disabled">
                {item.status}
              </Typography>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  )
}

export default BrandWithMaterialPaperItem
