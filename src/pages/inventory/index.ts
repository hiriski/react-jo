import { lazy } from 'react'

const InventoryPage = lazy(() => import('./inventory-v2'))

export default InventoryPage
