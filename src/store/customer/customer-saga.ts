import type { Effect, SagaIterator } from '@redux-saga/types';
import { TCreateCustomer } from '@src/types/customer';
import { TRequestMasterData } from '@/interfaces/master-data';
import { httpResponseCreated, httpResponseOK } from '@src/utils/http';
import { scrollToTop } from '@src/utils/ui';
import { SagaReturnType, call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import Swal from 'sweetalert2';

import {
  createCustomerFailure,
  createCustomerLoading,
  createCustomerSuccess,
  deleteCustomerFailure,
  deleteCustomerLoading,
  deleteCustomerSuccess,
  fetchCustomerList as fetchListCustomerAction,
  customer_fetchCustomerFailure,
  fetchingCustomerListSuccess,
  customer_fetchCustomerLoading,
  customer_fetchCustomerSuccess,
  setDialogDetaiLCustomer,
  setDrawerAddEditCustomer,
  customer_updateCustomerFailure,
  customer_updateCustomerSuccess,
  customer_fetchCustomer,
  customer_updateCustomerLoading,
} from './customer-actions';
import { CUSTOMER_ACTION_TYPES as ActionTypes } from './customer-action-types.enum';
import CustomerApiService from './customer-api-service';
import { app_setAlert } from '../app/app-actions';

// Type definitions of return of result.
type APIResponseCustomerList = SagaReturnType<typeof CustomerApiService.findAll>;
type APIResponseCustomer = SagaReturnType<typeof CustomerApiService.find>;
type APIResponseCreateCustomer = SagaReturnType<typeof CustomerApiService.create>;
type APIResponseUpdateCustomer = SagaReturnType<typeof CustomerApiService.update>;
type APIResponseDeleteCustomer = SagaReturnType<typeof CustomerApiService.delete>;

function* setDrawerAddEditCustomerSaga({ payload }: Effect<{ open: boolean; id: number | null }>): SagaIterator {
  if (payload.id) {
    // re-fetch customer list
    yield put(customer_fetchCustomer(payload.id));
  }
  yield put({
    type: ActionTypes.SET_DRAWER_ADD_EDIT_CUSTOMER,
    payload,
  });
}

function* saga_fetchCustomerList({ payload }: Effect<TRequestMasterData>): SagaIterator {
  yield put({ type: ActionTypes.FETCHING_CUSTOMER_LIST_LOADING });
  try {
    const response: APIResponseCustomerList = yield call(CustomerApiService.findAll, payload);
    if (httpResponseOK(response.status)) {
      const { data } = response;
      yield put(fetchingCustomerListSuccess(data));
      scrollToTop();
    }
  } catch (error) {
    yield put({ type: ActionTypes.FETCHING_CUSTOMER_LIST_FAILURE });
  }
}

function* saga_fetchCustomerDetails({ payload }: Effect<{ id: number }>): SagaIterator {
  yield put(customer_fetchCustomerLoading());
  try {
    const response: APIResponseCustomer = yield call(CustomerApiService.find, payload);
    if (httpResponseOK(response.status)) {
      const { data } = response;
      yield put(customer_fetchCustomerSuccess(data));
    }
  } catch (error) {
    yield put(customer_fetchCustomerFailure());
  }
}

function* saga_storeCustomer({ payload }: Effect<TCreateCustomer>): SagaIterator {
  yield put(createCustomerLoading());
  try {
    const response: APIResponseCreateCustomer = yield call(CustomerApiService.create, payload);
    if (httpResponseCreated(response.status)) {
      const { data } = response;
      yield put(createCustomerSuccess(data));

      // re-fetch customer list
      yield put(fetchListCustomerAction());

      // close drawer
      yield put(setDrawerAddEditCustomer(false, null));

      // fire swall.
      Swal.fire({
        icon: 'success',
        title: 'Okay...',
        text: 'Customer berhasil disimpan',
      });
    }
  } catch (error) {
    yield put(app_setAlert({ show: true, severity: 'error', messages: 'Buat data customer gagal!' }));
    yield put(createCustomerFailure());
  }
}

function* sagas_updateCustomer({ payload }: Effect<{ id: number } & TCreateCustomer>): SagaIterator {
  yield put(customer_updateCustomerLoading());
  try {
    const response: APIResponseUpdateCustomer = yield call(CustomerApiService.update, payload.id, payload.body);
    if (httpResponseOK(response.status)) {
      const { data } = response;
      yield put(customer_updateCustomerSuccess(data));

      // re-fetch customer list
      yield put(fetchListCustomerAction());

      // close drawer
      yield put(setDrawerAddEditCustomer(false, null));

      // fire swall.
      Swal.fire({
        icon: 'success',
        title: 'Okay...',
        text: 'Customer berhasil di perbaharui',
      });
    }
  } catch (error) {
    yield put(app_setAlert({ show: true, severity: 'error', messages: 'Update data customer gagal!' }));
    yield put(customer_updateCustomerFailure());
  }
}

function* deleteCustomer({ payload }: Effect): SagaIterator {
  yield put(deleteCustomerLoading());
  const customerId = payload;
  try {
    const response: APIResponseDeleteCustomer = yield call(
      CustomerApiService.delete,
      customerId /* payload is a number (customer id) */,
    );
    if (httpResponseOK(response.status)) {
      yield put(deleteCustomerSuccess());

      // re-fetch customer list
      yield put(fetchListCustomerAction());

      // close dialog detail customer.
      yield put(setDialogDetaiLCustomer(false, null));

      // fire swall. I will replace with toast later.
      Swal.fire({
        icon: 'success',
        title: 'Okay!',
        text: 'Customer berhasil dihapus',
      });
    }
  } catch (error) {
    yield put(deleteCustomerFailure());
  }
}

export default function* customerSaga(): SagaIterator {
  yield takeEvery(ActionTypes.SET_DRAWER_ADD_EDIT_CUSTOMER_REQUESTED, setDrawerAddEditCustomerSaga);
  yield takeEvery(ActionTypes.FETCHING_CUSTOMER_LIST_REQUESTED, saga_fetchCustomerList);
  yield takeLatest(ActionTypes.CREATE_CUSTOMER_REQUESTED, saga_storeCustomer);
  yield takeLatest(ActionTypes.UPDATE_CUSTOMER_REQUESTED, sagas_updateCustomer);
  yield takeEvery(ActionTypes.FETCH_CUSTOMER_REQUESTED, saga_fetchCustomerDetails);
  yield takeLatest(ActionTypes.DELETE_CUSTOMER_REQUESTED, deleteCustomer);
}
