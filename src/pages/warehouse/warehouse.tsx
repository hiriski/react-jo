import { BaseCard, BaseTabPanel, BaseTabs } from '@/components/base';
// import TabContext from '@mui/lab/TabContext';

import { Box, Button, Typography, styled, Tabs } from '@mui/material';
import { BrandMaterialPaperList, DrawerDetailInventory, MaterialPaperList } from '@src/components/inventory';
import { MainLayout } from '@src/components/layouts';
import { PageTitle, StyledButton } from '@src/components/ui';
import { useAppSelector } from '@src/store/hook';
import {
  fetchListBrandMaterialPaper,
  fetchListMaterialPaper,
  setDialogAddEditInventory,
} from '@src/store/inventory/actions';
import { DRAWER_DETAIL_INVENTORY_WIDTH } from '@src/utils/constants';
import { FC, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';

// Assets.
import InventoryIcon from '@/assets/images/icons/inventory1.png';
import BackgroundImage from '@/assets/images/4.png';
import { TRequestMasterData } from '@/interfaces/master-data';
import { masterData_fetchMasterData } from '@/store/master-data/master-data-actions';
import { masterData_rootSelector } from '@/store/master-data/master-data-selectors';
import { ITab } from '@/interfaces/tab';

// Components.
import {
  warehouse_fetchMaterialBrandList,
  warehouse_fetchMaterialCategoryList,
  warehouse_fetchMaterialList,
  warehouse_setDialogAddEditMaterial,
} from '@/store/warehouse/warehouse-actions';
import { warehouse_rootSelector } from '@/store/warehouse/warehouse-selectors';
import { MaterialListItem } from '@/components/warehouse/material';
import { useNavigate } from 'react-router';
import { ROUTES } from '@/constants/routes';
import { MaterialBrandListItem } from '@/components/warehouse';
import { MaterialTable } from '@/components/warehouse/material/material-table';

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })<{
  open?: boolean;
}>(({ theme, open }) => ({
  flexGrow: 1,
  // backgroundColor: theme.palette.background.paper,
  transition: theme.transitions.create('margin', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  // marginLeft: `-${SIDEBAR_DRAWER_WIDTH}px`,
  ...(open && {
    marginRight: `${DRAWER_DETAIL_INVENTORY_WIDTH}px`,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

// const tabs = [
//   {
//     label: 'Bahan Kertas',
//     value: 'material-paper',
//   },
//   {
//     label: 'Akrilik',
//     value: 'acrylic',
//   },
// ];

const WarehousePage: FC = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { material_categories } = useAppSelector((state) => masterData_rootSelector(state).data);
  const { data: materials, isFetching: isFetchingMaterials } = useAppSelector(
    (state) => warehouse_rootSelector(state).listMaterial,
  );
  const { data: materialBrands } = useAppSelector((state) => warehouse_rootSelector(state).listMaterialBrand);
  const { data: materialCategories } = useAppSelector((state) => warehouse_rootSelector(state).listMaterialCategory);

  /** Tab */
  const [activeTab, setActiveTab] = useState<string>('1');

  /** Tabs */
  const tabs = useMemo<ITab[]>(() => {
    if (material_categories.length) return material_categories.map((x) => ({ label: x.name, value: x.id }));
    return [];
  }, [material_categories]);

  const { listBrandMaterialPaper, listMaterialPaper, drawerDetailInventory } = useAppSelector(
    (state) => state.inventory,
  );

  useEffect(() => {
    dispatch(masterData_fetchMasterData([{ object_name: 'material_categories' }]));
    dispatch(warehouse_fetchMaterialList());
    dispatch(warehouse_fetchMaterialBrandList());
    dispatch(warehouse_fetchMaterialCategoryList());
  }, []);

  const handleClickAddMaterial = (): void => {
    navigate(ROUTES.WAREHOUSE_ADD_MATERIAL);
    // dispatch(warehouse_setDialogAddEditMaterial(true, null));
  };

  // console.log('activeTab', activeTab);

  return (
    <MainLayout>
      <BaseCard
        padding={0}
        sx={{
          backgroundImage: `url(${BackgroundImage})`,
          backgroundPositionX: '100%',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'auto calc(100% + 8rem)',
          mb: 3,
        }}
      >
        <Box>
          <Box sx={{ display: 'flex', alignItems: 'center', mb: 2, px: 5, pt: 4 }}>
            <Box sx={{ width: 62, height: 62, mr: 3 }}>
              <Box component="img" sx={{ width: '100%', height: 'auto' }} src={InventoryIcon} alt="inventory icon" />
            </Box>
            <Box>
              <Typography sx={{ mb: 1 }} variant="h3" component="h2">
                Stock Bahan Baku
              </Typography>
              <Typography variant="body2">Bahan Kertas, Laminasi, Aktilik dll..</Typography>
            </Box>
          </Box>
          <Box sx={{ px: 5, pb: 3 }}>
            <StyledButton disableHoverEffect onClick={handleClickAddMaterial}>
              Tambah Bahan Baku
            </StyledButton>
          </Box>
          {/* <BaseTabs
            data={tabs}
            value={activeTab}
            onChange={(e, value) => setActiveTab(value)}
            variant="scrollable"
            scrollButtons={false}
            allowScrollButtonsMobile
          /> */}
        </Box>
      </BaseCard>
      <Main open={false}>
        {/* {tabs.map((tab) => (
          <BaseTabPanel key={tab.value} value={tab.value}>
            <Typography>{tab.label}</Typography>
            <BrandMaterialPaperList items={listBrandMaterialPaper} />
          </BaseTabPanel>
        ))} */}

        <Box sx={{ mb: 3 }}>
          <MaterialBrandListItem items={materialBrands} />
        </Box>

        <Box sx={{ mb: 3 }}>
          <MaterialListItem isLoading={isFetchingMaterials} items={materials} />
          {/* <MaterialTable isLoading={isFetchingMaterials} data={materials} /> */}
        </Box>
      </Main>
    </MainLayout>
  );
};

export default WarehousePage;
