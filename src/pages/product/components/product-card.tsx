import { Card } from '@/components/ui'
import { setDialogDetailProduct } from '@/store/product/product-actions'
import { IProduct } from '@/types/product'
import { ROUTES } from '@/utils/constants'
import Box from '@mui/material/Box'
import Paper from '@mui/material/Paper'
import Typography from '@mui/material/Typography'
import noImage from '@src/assets/images/placeholder-1920x1080.png'
import { FC } from 'react'
import { useNavigate } from 'react-router'

interface Props { 
  product: IProduct
}

const ProductCard: FC<Props> = ({ product }) => {
  const navigate = useNavigate()

  const handleClickProduct = (uuid: string): void => {
    // dispatch(setDialogDetailProduct(true, uuid))
    navigate(`${ROUTES.PRODUCT}/${uuid}`)
  }

  return (
    <Card>
      <Box onClick={() => handleClickProduct(product.id)}>
        <Box sx={{ height: 168, overflow: 'hidden', '& img': { width: '100%', height: 'auto' } }}>
          {product.images.length > 0 ? (
            <img src={product.images[0].image_sm} alt={product.title} />
          ) : (
            <img src={noImage} alt="No image" />
          )}
        </Box>
        <Box sx={{ py: 2, px: 1.5 }}>
          <Typography variant="body1" component="h2">
            {product.title}
          </Typography>
          <Typography variant="subtitle2">{product.description}</Typography>
        </Box>
      </Box>
    </Card>
  )
}

export default ProductCard
