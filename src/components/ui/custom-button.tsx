import Button, { ButtonProps } from '@mui/material/Button'
import { FC, ReactNode } from 'react'

type Props = Pick<ButtonProps, 'onClick' | 'startIcon' | 'endIcon' | 'disableElevation' | 'size' | 'sx'> & {
  children: ReactNode
  backgroundColor: string
  color: string
}

const CustomButton: FC<Props> = ({
  onClick,
  children,
  disableElevation,
  startIcon,
  endIcon,
  backgroundColor,
  color,
  size,
  sx,
}: Props) => (
  <Button
    onClick={onClick}
    startIcon={startIcon}
    endIcon={endIcon}
    variant="contained"
    color="inherit"
    disableElevation={disableElevation}
    size={size}
    sx={{
      backgroundColor,
      color,
      '&:hover': {
        backgroundColor,
        color,
      },
      ...sx,
    }}
  >
    {children}
  </Button>
)

CustomButton.defaultProps = {
  disableElevation: true,
  startIcon: null,
  endIcon: null,
  sx: {},
}

export default CustomButton
