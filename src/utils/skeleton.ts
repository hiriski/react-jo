/**
 * create skeleton.
 *
 * @param count number of skeleton
 * @returns Array<string>
 */
export const createArraySkeleton = (count?: number): Array<string> => {
  const defaultCount = 12
  return new Array(count ?? defaultCount).fill('skeleton')
}
