import { PageTitle } from '@/components/page-title';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { MainLayout } from '@src/components/layouts';
import { FC } from 'react';

const SupportCenterPage: FC = () => (
  <MainLayout>
    <PageTitle title="Pusat Bantuan" />
    <Box
      sx={{
        height: 400,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Typography component="h1" variant="h5">
        Screen Bantuan / Dokumentasi cara penggunaan aplikasi
      </Typography>
    </Box>
  </MainLayout>
);

export default SupportCenterPage;
