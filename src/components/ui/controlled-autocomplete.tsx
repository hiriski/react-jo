import MuiAutocomplete from '@mui/material/Autocomplete'
import TextField from '@mui/material/TextField'
import { FC, ReactElement } from 'react'
import { Control, Controller, FieldErrors } from 'react-hook-form'

type Props<T> = {
  // eslint-disable-next-line
  control: Control<any>
  options: ReadonlyArray<T>
  errors: FieldErrors
  name: string
  label: string
  helperText?: string
  size?: 'small' | 'medium'
  margin?: 'dense' | 'normal' | 'none'
  disabled?: boolean
  getOptionLabel: (x: any) => string
}

const ControlledAutocomplete = <TOptions extends object>({
  control,
  options,
  errors,
  name,
  label,
  helperText,
  size,
  margin,
  disabled,
  getOptionLabel,
}): ReactElement => (
  <Controller
    control={control}
    name={name}
    render={({ field: { onChange, value } }) => (
      <MuiAutocomplete
        onChange={(event, item): void => {
          onChange(item)
        }}
        value={value}
        disabled={disabled}
        options={options}
        getOptionLabel={getOptionLabel}
        // getOptionSelected={(option: TOptions, val) => val === undefined || val === '' || option.id === val.id}
        size={size}
        renderInput={(field) => (
          <TextField
            {...field}
            label={label}
            margin={margin}
            variant="outlined"
            error={Boolean(errors[name]?.message)}
            helperText={
              errors[name]?.message ? errors[name]?.message : helperText
            }
          />
        )}
      />
    )}
  />
)

ControlledAutocomplete.defaultProps = {
  helperText: '',
  size: 'small',
  margin: 'none',
  disabled: false,
}

export default ControlledAutocomplete
