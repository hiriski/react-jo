import PeopleOutlinedIcon from '@mui/icons-material/PeopleOutlined'
import ReceiptOutlinedIcon from '@mui/icons-material/ReceiptOutlined'
import TaskOutlinedIcon from '@mui/icons-material/TaskOutlined'
import { ReactElement } from 'react'

interface SearchType {
  value: string
  label: string
  icon: ReactElement
}

export const searchTypes: Array<SearchType> = [
  { value: 'job_order', label: 'Job Order', icon: <TaskOutlinedIcon sx={{ fontSize: 16 }} /> },
  { value: 'invoice', label: 'Invoice', icon: <ReceiptOutlinedIcon sx={{ fontSize: 16 }} /> },
  { value: 'customer', label: 'Customer', icon: <PeopleOutlinedIcon sx={{ fontSize: 16 }} /> },
]
