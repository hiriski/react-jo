import Dialog from '@mui/material/Dialog'
import Zoom, { ZoomProps } from '@mui/material/Zoom'
import { useAppSelector } from '@src/store/hook'
import { setDialogAddEditBrandMaterial } from '@src/store/inventory/actions'
import { FC, ReactElement, forwardRef, useEffect } from 'react'
import { useDispatch } from 'react-redux'

import { DialogContent, DialogTitle } from '../ui'
import { FormMaterialPaper } from '.'

const Transition = forwardRef<unknown, ZoomProps>(
  (props, ref): ReactElement => (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Zoom ref={ref} {...props} />
  )
)
const DialogAddEditBrandMaterial: FC = () => {
  const dispatch = useDispatch()
  const { dialogAddEditBrandMaterial } = useAppSelector((state) => state.inventory)
  const { open, id } = dialogAddEditBrandMaterial

  const handleClose = (): void => {
    // dispatch(setDialogAddEditInventory({ status: false, type: null, id: null }))
    dispatch(setDialogAddEditBrandMaterial(false, null))
  }

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleClose}
      aria-describedby="dialog-add-edit-inventory"
      PaperProps={{ elevation: 1 }}
      maxWidth="sm"
      fullWidth
    >
      <DialogContent>
        <p>Dialog add edit brand material content</p>
      </DialogContent>
    </Dialog>
  )
}

export default DialogAddEditBrandMaterial
