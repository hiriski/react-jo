export interface IStateFailure {
  status: boolean
  message?: string | null
  messages?: string[] | null
}

export interface IApiResponseError {
  success: boolean
  message?: string
  messages?: string[] | null
}

export interface DefaultActionPayloadDialog {
  open: boolean
  id?: number | null
}

export type ActionPayloadDialog = DefaultActionPayloadDialog

export type TRowsPerPage = 10 | 25 | 50
