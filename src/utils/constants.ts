// eslint-disable-next-line no-underscore-dangle
export const __DEV__ = process.env.NODE_ENV === 'development';

export const API_BASE_URL = process.env.API_BASE_URL;

export const APP_NAME = process.env.REACT_APP_NAME || 'React Jo';
export const ACCESS_TOKEN = '@accessToken';
export const LIMIT = 24;
export const PREFIX_APP_VERSION = process.env.REACT_APP_PREFIX_APP_VERSION;
export const GOOGLE_AUTH_CLIENT_ID = process.env.REACT_APP_GOOGLE_AUTH_CLIENT_ID;
export const GOOGLE_AUTH_CLIENT_SECRET = process.env.REACT_APP_GOOGLE_AUTH_CLIENT_SECRET;
export const SIDEBAR_DRAWER_WIDTH = 280;
export const SIDEBAR_COLLAPSED_WIDTH = 90;
export const DRAWER_DETAIL_INVENTORY_WIDTH = 360;
export const DRAWER_ADD_EDIT_JO_WIDTH = 800;
export const DRAWER_ADD_EDIT_CUSTOMER_WIDTH = 396;
export const DRAWER_EDITABLE_INVOICE_WIDTH = 400;
export const DB_FORMAT_TIMESTAMPS = 'YYYY-MM-DD HH:mm:ss';
export const ROWS_PER_PAGE = [12, 25, 50];

export const PRODUCTION_STATUSES = {
  WAITING: {
    name: 'Waiting',
    id: 1,
  },
  IN_PROGRESS: {
    id: 2,
    name: 'In Progress',
  },
  DONE: {
    id: 3,
    name: 'DONE',
  },
};

export const createPath = (path: string): string => {
  if (!PREFIX_APP_VERSION) return `/${path}`;
  return PREFIX_APP_VERSION.substr(-1) === '/' ? PREFIX_APP_VERSION + path : `${PREFIX_APP_VERSION}/${path}`;
};

export const ROUTES = {
  HOME: createPath(''),
  APP: createPath('app'),
  SIGNIN: createPath('auth/signin'),
  REGISTER: createPath('auth/register'),
  FORGOT_PASSWORD: createPath('forgot-password'),
  RESET_PASSWORD: createPath('reset-password'),
  WAREHOUSE: createPath('warehouse'),
  MATERIAL: createPath('warehouse/material'),
  WAREHOUSE_ADD_MATERIAL: createPath('warehouse/material/add'),
  ANALYTICS: createPath('analytics'),
  JO: createPath('job-order'),
  JOB_ORDER: createPath('job-order'),
  ADD_JOB_ORDER: createPath('job-order/add'),
  EDIT_JOB_ORDER: createPath('job-order/edit'),
  USER: createPath('user'),
  ROLE: createPath('role'),
  INVOICE: createPath('invoice'),
  SETTING: createPath('settings'),
  ACCOUNT: createPath('account'),
  SUPPORT_CENTER: createPath('support-center'),
  CUSTOMER: createPath('customer'),
  ADD_CUSTOMER: createPath('customer/add'),
  EDIT_CUSTOMER: createPath('customer/edit'),
  INVOICE_GENERATOR: createPath('invoice-generator'),
  PRODUCT: createPath('product'),
};
