import { BoxSpinner } from '@components/ui';
import CloseIcon from '@mui/icons-material/Close';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import { jobOrder_fetchDetail, setDrawerAddEditJo } from '@/store/job-order/job-order-actions';
import { masterData_fetchMasterData } from '@/store/master-data/master-data-actions';
import { FC, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useWindowSize } from 'react-use';

import { useAppSelector } from '../../store/hook';
import { JobOrderForm } from '@/components/job-order';

// Swall
import Swall from 'sweetalert2';

// Base components.
import { BaseLoader } from '@/components/base';
import { AppConfig } from '@/config/theme/app/app-config';
import { jobOrder_rootSelector } from '@/store/job-order/job-order-selectors';

const requestBodyMasterData = [
  {
    object_name: 'job_order_categories',
  },
  {
    object_name: 'materials',
  },
  {
    object_name: 'customers',
  },
  {
    object_name: 'material_brands',
  },
  {
    object_name: 'material_categories',
  },
  {
    object_name: 'material_types',
  },
  {
    object_name: 'payment_methods',
  },
  {
    object_name: 'payment_statuses',
  },
  {
    object_name: 'bank_accounts',
  },
];

export const DrawerAddEditJobOrder: FC = () => {
  const { drawerAddEditJo, createLoading, updateLoading, isFormHasChanges } = useAppSelector((state) =>
    jobOrder_rootSelector(state),
  );
  const { isFetching: isFetchingMasterData } = useAppSelector((state) => state.masterData);
  const dispatch = useDispatch();
  const isEditMode = Boolean(drawerAddEditJo.id);
  const { height: windowHeight } = useWindowSize();

  /**
   * Handle close drawer.
   */
  const handleCloseDrawer = (): void => {
    if (!isFormHasChanges) {
      dispatch(setDrawerAddEditJo(false, null));
    } else {
      // Show dialog confirmation if form has changes.
      Swall.fire({
        title: 'Hey! Ada perubahan yang belum kamu simpan!',
        icon: 'warning',
        html: '<span>Apakah kamu yakin ingin keluar ?<span><br /> <span>Perubahan yang kamu isi sebelumnya akan dihapus!</span>',
        showCancelButton: true,
        confirmButtonText: 'Ya, Keluar Aja',
        cancelButtonText: 'Tetap Disini',
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(setDrawerAddEditJo(false, null));
        }
      });
    }
  };

  useEffect(() => {
    if (drawerAddEditJo.id) {
      dispatch(jobOrder_fetchDetail(drawerAddEditJo.id));
    }
    if (drawerAddEditJo.status) {
      dispatch(masterData_fetchMasterData(requestBodyMasterData));
    }
  }, [drawerAddEditJo.status, drawerAddEditJo.id]);

  return (
    <Drawer
      elevation={2}
      anchor="right"
      open={drawerAddEditJo.status}
      onClose={handleCloseDrawer}
      PaperProps={{
        sx: {
          width: { xs: '95%', md: `calc(100% - ${AppConfig.SidebarDrawerWidth}px)` },
          // overflowY: 'hidden !important',
        },
      }}
    >
      <Box role="presentation" sx={{ position: 'relative' }}>
        {createLoading || updateLoading || (isFetchingMasterData && <BaseLoader sx={{ height: '100vh' }} />)}
        <IconButton sx={{ position: 'absolute', top: 12, right: 12 }} onClick={handleCloseDrawer} size="small">
          <CloseIcon sx={{ fontSize: 20 }} />
        </IconButton>
        <JobOrderForm displayOnDrawer={true} id={drawerAddEditJo.id} sx={{ mt: 5 }} />
      </Box>
    </Drawer>
  );
};
