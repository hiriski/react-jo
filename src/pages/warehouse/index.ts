import { lazy } from 'react';

const WarehousePage = lazy(() => import('./warehouse'));

export default WarehousePage;
