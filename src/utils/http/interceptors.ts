import { ACCESS_TOKEN, ROUTES } from '@src/utils/constants';
import { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import Cookies from 'js-cookie';
import Swal from 'sweetalert2';

import { httpResponseUnauthorized } from '.';

const onRequest = (config: Partial<AxiosRequestConfig>): AxiosRequestConfig => {
  const accessToken = Cookies.get(ACCESS_TOKEN);
  if (accessToken) {
    /* eslint-disable no-param-reassign */
    config.headers.Authorization = `Bearer ${accessToken}`;
  }
  return config;
};

const onRequestError = (error: AxiosError): Promise<AxiosError> => {
  if (process.env.NODE_ENV === 'development') {
    // eslint-disable-next-line no-console
    // console.error(`❌❌❌ Request error -> ${JSON.stringify(error)}`)
  }
  return Promise.reject(error);
};

const onResponse = (response: AxiosResponse): AxiosResponse => {
  if (process.env.NODE_ENV === 'development') {
    // eslint-disable-next-line no-console
    // console.log('💚💚💚 Response success ->', response);
  }
  return response;
};

const onResponseError = (error: AxiosError): Promise<AxiosError> => {
  if (process.env.NODE_ENV === 'development') {
    // eslint-disable-next-line no-console
    // console.error(`❌❌❌ Response error -> ${JSON.stringify(error)}`)
    if (httpResponseUnauthorized(error.response.status)) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Token Anda sudah kadaluarsa. Silahkan login kembali!',
        // footer: 'Why do I have this issue?',
      }).then((res) => {
        if (res.isConfirmed) {
          // Remove token
          Cookies.remove(ACCESS_TOKEN);
          window.location.href = ROUTES.SIGNIN;
        } else {
          Cookies.remove(ACCESS_TOKEN);
          window.location.href = ROUTES.SIGNIN;
        }
      });
    }
  }
  // Check if network disconnected
  if (error.code === 'ECONNABORTED') {
    // eslint-disable-next-line no-console
    // console.error('❌❌❌ Response error ->', 'Something when wrong with your connection', error);
  }
  return Promise.reject(error);
};

export function setupInterceptorsTo(axiosInstance: AxiosInstance): AxiosInstance {
  axiosInstance.interceptors.request.use(onRequest, onRequestError);
  axiosInstance.interceptors.response.use(onResponse, onResponseError);
  return axiosInstance;
}
