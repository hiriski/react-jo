import { IMasterData, RequestBodyMasterData } from '@/interfaces/master-data';
import { AnyAction } from 'redux';

import { MASTER_DATA_ACTION_TYPES as ActionTypes } from './master-data-action-types.enum';

// Actions definitions.
interface IFetchMasterDataRequested {
  type: typeof ActionTypes.FETCH_MASTER_DATA_REQUESTED;
  payload: RequestBodyMasterData;
}
interface IFetchMasterDataLoading {
  type: typeof ActionTypes.FETCH_MASTER_DATA_LOADING;
}
interface IFetchMasterDataFailure {
  type: typeof ActionTypes.FETCH_MASTER_DATA_FAILURE;
}
interface IFetchMasterDataSuccess {
  type: typeof ActionTypes.FETCH_MASTER_DATA_SUCCESS;
  payload: IMasterData;
}

// Union action types
export type MasterDataActions =
  | IFetchMasterDataRequested
  | IFetchMasterDataLoading
  | IFetchMasterDataFailure
  | IFetchMasterDataSuccess;

// Actions creator.
export const masterData_fetchMasterData = (body: RequestBodyMasterData): IFetchMasterDataRequested | AnyAction => ({
  type: ActionTypes.FETCH_MASTER_DATA_REQUESTED,
  payload: body,
});

export const masterData_fetchMasterDataSuccess = (data: IMasterData): IFetchMasterDataSuccess => ({
  type: ActionTypes.FETCH_MASTER_DATA_SUCCESS,
  payload: data,
});
