import { IResponseErrorValidation } from '@/interfaces/validation';

export const mapResponseErrorValidation = (responseError: IResponseErrorValidation): Array<string> => {
  const errorsMessages: Array<string> = [];
  Object.values(responseError.errors).map((m) => {
    if (m.length > 1) {
      return m.map((x) => errorsMessages.push(x));
    } else {
      return errorsMessages.push(m[0]);
    }
  });

  return errorsMessages;
};
