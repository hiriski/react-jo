import { JobOrderProductionStatusIdConstants } from '@/constants';
import { IDropdown } from '@/interfaces/dropdown';
import { materialUnits } from './warehouse';

export const dropdownGenders: IDropdown<string>[] = [
  {
    value: 'male',
    label: 'Laki-laki',
  },
  {
    value: 'female',
    label: 'Perempuan',
  },
];

export const dropdownCustomerType: IDropdown<string>[] = [
  {
    value: 'individual',
    label: 'Individu',
  },
  {
    value: 'company',
    label: 'Perusahaan',
  },
];

export const dropdownJobOrderProductionStatus: IDropdown<number>[] = [
  {
    label: 'Waiting',
    value: JobOrderProductionStatusIdConstants.Waiting,
  },
  {
    label: 'In Progress',
    value: JobOrderProductionStatusIdConstants.InProgress,
  },
  {
    label: 'Selesai',
    value: JobOrderProductionStatusIdConstants.Done,
  },
  // {
  //   label: 'Ditahan',
  //   value: JobOrderProductionStatusIdConstants.Hold,
  // },
  // {
  //   label: 'Ditolak',
  //   value: JobOrderProductionStatusIdConstants.Rejected,
  // },
];

export const dropdownJobOrderPeriod: IDropdown<string>[] = [
  {
    label: 'Overtime',
    value: 'overtime',
  },
  {
    label: 'Hari ini',
    value: 'today',
  },
  {
    label: 'Minggu ini',
    value: 'week',
  },
  {
    label: 'Bulan ini',
    value: 'month',
  },
  {
    label: 'Tahun ini',
    value: 'year',
  },
];

export const dropdownDiscountType: Array<IDropdown<string | null>> = [
  {
    label: 'No Discount',
    value: null,
  },
  {
    label: 'Percentage (%)',
    value: 'percentage',
  },
  {
    label: 'Fixed Price (Rp)',
    value: 'fixed_price',
  },
];

export const dropdownMaterialUnits = materialUnits.map((x) => ({ label: x.toUpperCase(), value: x }));
