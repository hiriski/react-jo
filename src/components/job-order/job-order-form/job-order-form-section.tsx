import { FC, ReactNode } from 'react';

// Mui components.
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';
import { SxProps } from '@mui/material';

interface Props {
  children: ReactNode;
  title?: string;
  sx?: SxProps;
  disableLastDivider?: boolean;
}

export const JobOrderFormSection: FC<Props> = ({ children, title, disableLastDivider, sx }) => (
  <>
    <Box
      component="section"
      sx={{
        px: { xs: 4, md: 14 },
        py: { xs: 3, md: 5 },
        ...sx,
      }}
    >
      {title && (
        <Box sx={{ mb: 3 }}>
          <Typography component="h5" variant="h5" sx={{ color: 'text.secondary' }}>
            {title}
          </Typography>
        </Box>
      )}
      {children}
    </Box>
    {!disableLastDivider && <Divider />}
  </>
);

JobOrderFormSection.defaultProps = { title: '', disableLastDivider: false, sx: {} };
