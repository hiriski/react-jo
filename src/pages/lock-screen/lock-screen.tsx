import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { FC, useEffect, useState } from 'react';

import { APP_NAME } from '@src/utils/constants';
import SampleLogo from '@/assets/images/sample-logo.png';
import { alpha } from '@mui/material';

import { LockScreenForm } from './component';

import LockIcon from '@/assets/images/icons/lock.png';

import LockScreenBackground1 from '@/assets/images/lawrence-kayku-frWOcVisp8U-unsplash.jpg';
import LockScreenBackground2 from '@/assets/images/chuttersnap-nAFIWWesro8-unsplash.jpg';
import LockScreenBackground3 from '@/assets/images/leon-macapagal-GCOJ1fdfjaM-unsplash.jpg';
import LockScreenBackground4 from '@/assets/images/wallpaperflare.com_wallpaper.jpg';

const arrayBackgroundImages = [
  LockScreenBackground1,
  LockScreenBackground2,
  LockScreenBackground3,
  LockScreenBackground4,
];

export const LockScreen: FC = () => {
  const [backgroundImage, setBackgroundImage] = useState(
    arrayBackgroundImages[Math.floor(Math.random() * arrayBackgroundImages.length)],
  );

  // useEffect(() => {
  //   const changeBackgroundImage = setInterval(() => {
  //     setBackgroundImage(Math.floor(Math.random() * arrayBackgroundImages.length));
  //   }, 2000);
  //   changeBackgroundImage();
  //   return () => {
  //     clearInterval(changeBackgroundImage());
  //   };
  // }, []);

  return (
    <Box sx={{ height: '100vh' }}>
      <Box
        sx={{
          width: '100%',
          height: '100vh',
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'bottom left',
          backgroundImage: `url("${backgroundImage}")`,
          position: 'fixed',
          top: 0,
          left: 0,
        }}
      />
      <Box
        sx={{
          width: '100%',
          height: '100vh',
          backgroundColor: alpha('#000', 0.4),
          position: 'fixed',
          top: 0,
          left: 0,
        }}
      />

      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100vh',
          width: {
            xs: '90%',
            sm: 400,
          },
          margin: '0 auto',
        }}
      >
        <Box
          sx={{
            width: '100%',
            px: 8,
            py: 6,
            borderRadius: 2,
            backgroundColor: 'rgba(255, 255, 255, 0.92)',
            backdropFilter: 'blur(5px)',
          }}
        >
          <Box
            sx={{
              width: '100%',
              mb: 3,
            }}
          >
            <Box sx={{ textAlign: 'center' }}>
              <Box sx={{ width: 72, height: 'auto', mb: 2 }} component="img" src={LockIcon} alt="Lock icon" />
              <Typography sx={{ mb: 1 }} variant="h4">
                Aplikasi Dikunci
              </Typography>
              <Typography component="p" variant="body1">
                Masukan password untuk membuka kunci.
              </Typography>
            </Box>
          </Box>

          {/* Lock screen form */}
          <LockScreenForm />
        </Box>
      </Box>
    </Box>
  );
};
