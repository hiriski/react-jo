import { IDropdown } from '@/interfaces/dropdown';
import { MenuItem } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import { FC, ReactElement, ReactNode } from 'react';
import { Control, Controller, FieldErrors } from 'react-hook-form';

type Props = {
  // eslint-disable-next-line
  control: Control<any>;
  errors: FieldErrors;
  name: string;
  label: string;
  helperText?: string;
  size?: 'small' | 'medium';
  margin?: 'dense' | 'normal' | 'none';
  disabled?: boolean;
  fullWidth?: boolean;
  data: IDropdown<string | number>[];
};

export const ControlledSelect: FC<Props> = ({
  control,
  errors,
  name,
  label,
  helperText,
  size,
  margin,
  disabled,
  data,
  fullWidth,
}: Props): ReactElement => (
  <Controller
    name={name}
    control={control}
    render={({ field }) => (
      <FormControl error={Boolean(errors[name]?.message)} margin={margin} size={size} fullWidth={fullWidth}>
        <InputLabel>{label}</InputLabel>
        <Select {...field} label={label} disabled={disabled}>
          {data.map(({ label, value }) => (
            <MenuItem key={String(value)} value={value}>
              {label}
            </MenuItem>
          ))}
        </Select>
        <FormHelperText error={Boolean(errors[name]?.message)}>
          {errors[name]?.message ? errors[name]?.message : helperText}
        </FormHelperText>
      </FormControl>
    )}
  />
);

ControlledSelect.defaultProps = {
  helperText: '',
  size: 'small',
  margin: 'dense',
  disabled: false,
  fullWidth: true,
};
