import { WhatsApp } from '@mui/icons-material';
import EventAvailableIcon from '@mui/icons-material/EventAvailable';
import EventBusyIcon from '@mui/icons-material/EventBusy';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import MailIcon from '@mui/icons-material/Mail';
import PersonIcon from '@mui/icons-material/Person';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import Slider from '@mui/material/Slider';
import { styled } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import {
  JoLabelPaymentStatus,
  // JoLabelProductionStatus,
  JobOrderNotes,
  ToggleUpdateProductionStatus,
} from '@src/components/jo';
import { useAppSelector } from '@src/store/hook';
import { updatePaymentStatusJo, updatePercentageProgress } from '@/store/job-order/job-order-actions';
import { TJo } from '@src/types/jo';
import { DB_FORMAT_TIMESTAMPS, PRODUCTION_STATUSES } from '@src/utils/constants';
import { createWhatsappPhoneNumber } from '@src/utils/phone-number';
import { debounce } from 'lodash';
import moment from 'moment';
import { FC, useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';

import ToggleUpdatePaymentStatus from './toggle-update-payment-status';

type Props = {
  data: TJo;
};

function valuetext(value: number): string {
  return `${value}%`;
}

const PrettoSlider = styled(Slider)({
  color: '#52af77',
  height: 4,
  '& .MuiSlider-track': {
    border: 'none',
  },
  '& .MuiSlider-thumb': {
    height: 20,
    width: 20,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    '&:focus, &:hover, &.Mui-active, &.Mui-focusVisible': {
      boxShadow: 'inherit',
    },
    '&:before': {
      display: 'none',
    },
  },
  '& .MuiSlider-valueLabel': {
    lineHeight: 1.2,
    fontSize: 12,
    background: 'unset',
    padding: 0,
    width: 32,
    height: 32,
    borderRadius: '50% 50% 50% 0',
    backgroundColor: '#52af77',
    transformOrigin: 'bottom left',
    transform: 'translate(50%, -100%) rotate(-45deg) scale(0)',
    '&:before': { display: 'none' },
    '&.MuiSlider-valueLabelOpen': {
      transform: 'translate(50%, -100%) rotate(-45deg) scale(1)',
    },
    '& > *': {
      transform: 'rotate(45deg)',
    },
  },
});

const DetailJobOrder: FC<Props> = ({ data }: Props) => {
  const dispatch = useDispatch();
  const [progressValue, setProgressValue] = useState(data?.percentage_progress ?? 0);

  const { data: paymentStatusList } = useAppSelector((state) => state.paymentStatus);

  const updatePercentage = (val: number): void => {
    dispatch(updatePercentageProgress(data.id, val));
  };

  const handleSearch = useCallback(debounce(updatePercentage, 500), []);
  // const handleSearch = useCallback(debounce(() => updatePercentage(25), 750), [])

  const handleSliderChange = (event: Event, newValue: number | Array<number>): void => {
    if (data.production_status.id !== PRODUCTION_STATUSES.WAITING.id) {
      setProgressValue(Number(newValue));
      handleSearch(Number(newValue));
    } else {
      setProgressValue(0);
      alert('Tidak bisa update progress untuk jo dengan status menunggu');
    }
  };

  if (data)
    return (
      <Box sx={{ minHeight: 300 }}>
        <Grid container spacing={2}>
          {/* Left grid */}
          <Grid item xs={12} md={8}>
            <Box
              sx={{
                mb: 2,
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <Box
                sx={{
                  px: 1.2,
                  py: 0.6,
                  maxWidth: 150,
                  borderRadius: 1,
                  textAlign: 'center',
                  backgroundColor: 'primary.main',
                  mr: 2,
                }}
              >
                <Typography sx={{ color: 'primary.contrastText' }} variant="subtitle2">
                  {data.order_number}
                </Typography>
              </Box>
              <Typography variant="subtitle2">{data.category.name}</Typography>
            </Box>

            <Box sx={{ mb: 2 }}>
              <Typography component="h2" variant="h4">
                {data.title}
              </Typography>
            </Box>

            <Divider sx={{ width: '95%', mb: 2 }} />

            {/* Production status progress */}
            <Box sx={{ mb: 2, width: '95%' }}>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                {/* <JoLabelProductionStatus
                  status={data.production_status.name}
                  sx={{ textAlign: 'center', minWidth: 160 }}
                /> */}
                <PrettoSlider
                  // disabled={!data.percentage_progress}
                  aria-label="Job order production progress"
                  defaultValue={progressValue}
                  value={progressValue}
                  getAriaValueText={valuetext}
                  valueLabelDisplay="auto"
                  onChange={handleSliderChange}
                  size="small"
                  step={10}
                  min={0}
                  max={100}
                  sx={{ ml: 4 }}
                />
              </Box>
            </Box>

            {/* Preview */}
            <Box sx={{ mb: 2 }}>
              <Box
                sx={{
                  width: '95%',
                  '& img': {
                    width: '100%',
                    borderRadius: 1,
                  },
                }}
              >
                <Typography variant="subtitle2" sx={{ mb: 0.65 }}>
                  Preview file
                </Typography>
                {/* <img src="/static/images/sample-img.jpg" alt="Sample" /> */}
                {Array.isArray(data.images) && data.images.length > 0 && (
                  <img src={data.images[0].image_lg} alt="Sample" />
                )}
              </Box>
            </Box>

            <Box sx={{ mb: 2, width: '95%' }}>
              <Typography variant="h6" sx={{ mb: 1 }}>
                Notes
              </Typography>
              <JobOrderNotes data={data.notes} joId={data.id} />
            </Box>
          </Grid>

          {/* Right grid */}
          <Grid item xs={12} md={4}>
            <Box sx={{ mb: 3 }}>
              <Typography variant="h6" sx={{ mb: 1 }}>
                Status Pembayaran
              </Typography>
              <JoLabelPaymentStatus
                status={data.transaction.payment_status.name}
                statusId={data.transaction.payment_status.id}
                description={data.transaction.payment_status.description}
                // override default style
                sx={{ py: 0.6, px: 2, mb: 2 }}
                typographyVariant="body1"
              />
            </Box>
            <Typography variant="h6" sx={{ mb: 1 }}>
              Tanggal Order & Deadline
            </Typography>
            {/* Date */}
            <Box sx={{ mb: 2 }}>
              <List sx={{ width: '100%' }} disablePadding>
                <ListItem disableGutters disablePadding>
                  <ListItemAvatar>
                    <Avatar>
                      <EventAvailableIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary={moment(data.order_date, DB_FORMAT_TIMESTAMPS).format('DD MMM YYYY - HH:mm')}
                    secondary="Tanggal Order"
                  />
                </ListItem>
                <ListItem disableGutters disablePadding>
                  <ListItemAvatar>
                    <Avatar>
                      <EventBusyIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary={moment(data.due_date, DB_FORMAT_TIMESTAMPS).format('DD MMM YYYY - HH:mm')}
                    secondary="Due Date"
                  />
                </ListItem>
              </List>
            </Box>

            <Divider />

            {/* Customer information */}
            {data.customer && (
              <Box
                sx={{
                  mt: 2,
                }}
              >
                <Typography variant="h6" sx={{ mb: 1 }}>
                  Informasi Pelanggan
                </Typography>
                <List sx={{ width: '100%' }} disablePadding>
                  <ListItem disableGutters disablePadding>
                    <ListItemAvatar>
                      <Avatar>
                        <PersonIcon />
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={data.customer.name} secondary="Nama Pelanggan" />
                  </ListItem>
                  {data.customer.phone_number && (
                    <ListItem disableGutters disablePadding>
                      <ListItemAvatar>
                        <Avatar>
                          <LocalPhoneIcon />
                        </Avatar>
                      </ListItemAvatar>
                      <ListItemText primary={data.customer.phone_number} secondary="Telp/HP" />
                    </ListItem>
                  )}
                  {data.customer.email && (
                    <ListItem disableGutters disablePadding>
                      <ListItemAvatar>
                        <Avatar>
                          <MailIcon />
                        </Avatar>
                      </ListItemAvatar>
                      <ListItemText primary={data.customer.email} secondary="Telp/HP" />
                    </ListItem>
                  )}
                  {data.customer.addresses && (
                    <ListItem disableGutters disablePadding>
                      <ListItemAvatar>
                        <Avatar>
                          <LocationOnIcon />
                        </Avatar>
                      </ListItemAvatar>
                      {/* <ListItemText primary={data.customer.address} secondary="Telp/HP" /> */}
                    </ListItem>
                  )}
                </List>

                {/* CTA */}
                {data.customer.phone_number && (
                  <Box sx={{ mt: 1.6, textAlign: 'center' }}>
                    <Button
                      component="a"
                      target="_blank"
                      href={`https://api.whatsapp.com/send?phone=${createWhatsappPhoneNumber(
                        data.customer.phone_number,
                      )}&text=`}
                      startIcon={<WhatsApp />}
                      variant="contained"
                      disableElevation
                      sx={{ backgroundColor: '#00a248' }}
                    >
                      Chat Pelanggan
                    </Button>
                  </Box>
                )}
              </Box>
            )}
          </Grid>

          {/* Center grid bottom */}
          <Grid item xs={12}>
            <Box sx={{ mb: 3, textAlign: 'center' }}>
              <Typography variant="h6" sx={{ mb: 1 }}>
                Update Status Pembayaran
              </Typography>
              <ToggleUpdatePaymentStatus joId={data.id} selectedId={data.transaction.payment_status_id} />
            </Box>
            <Box sx={{ mb: 2, textAlign: 'center' }}>
              <Typography variant="h6" sx={{ mb: 1 }}>
                Update Status Produksi
              </Typography>
              <ToggleUpdateProductionStatus joId={data.id} selectedId={data.production_status_id} />
            </Box>
          </Grid>
        </Grid>
      </Box>
    );
  return null;
};

export default DetailJobOrder;
