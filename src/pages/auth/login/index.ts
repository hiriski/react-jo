import { lazy } from 'react'

const LoginPage = lazy(() => import('./login-page'))

export default LoginPage
