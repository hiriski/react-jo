import type { SagaIterator } from '@redux-saga/types';
import { spawn } from 'redux-saga/effects';

// Import sagas.
import authSaga from './auth/saga';
import customerSaga from './customer/customer-saga';
import inventorySaga from './inventory/saga';
import jobOrderSaga from './job-order/job-order-saga';
import masterDataSaga from './master-data/master-data-saga';
import paymentMethodSaga from './payment-method/saga';
import paymentStatusSaga from './payment-status/saga';
import productSaga from './product/product-saga';
import productionStatusSaga from './production-status/saga';
import roleSaga from './role/saga';
import userSaga from './user/saga';
import analyticsSaga from './analytics/analytics-saga';
import warehouseSaga from './warehouse/warehouse-saga';
import machineSaga from './machine/machine-saga';
import dashboardSaga from './dashboard/dashboard-saga';

// Export the root saga
export default function* rootSaga(): SagaIterator {
  yield spawn(authSaga);
  yield spawn(inventorySaga);
  yield spawn(roleSaga);
  yield spawn(masterDataSaga);
  yield spawn(jobOrderSaga);
  yield spawn(paymentStatusSaga);
  yield spawn(productionStatusSaga);
  yield spawn(customerSaga);
  yield spawn(userSaga);
  yield spawn(paymentMethodSaga);
  yield spawn(productSaga);
  yield spawn(analyticsSaga);
  yield spawn(warehouseSaga);
  yield spawn(machineSaga);
  yield spawn(dashboardSaga);
}
