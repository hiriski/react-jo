import { blueGrey, indigo } from '@mui/material/colors';
import { alpha, Components } from '@mui/material/styles';
import { THEME_SPACING } from '.';

import { fontFamily } from './typography';

const components: Components = {
  // Name of the component
  MuiButton: {
    defaultProps: {
      size: 'small',
    },
    styleOverrides: {
      // Name of the slot
      // root: {
      //   // Some CSS
      //   fontFamily,
      // },
    },
  },
  MuiListItem: {
    styleOverrides: {},
  },
  MuiListItemText: {
    styleOverrides: {
      primary: {
        fontFamily,
        fontWeight: 600,
        fontSize: '0.85rem',
      },
    },
  },
  MuiAppBar: {
    styleOverrides: {
      root: {
        '& .MuiToolbar-root': {
          minHeight: 54,
        },
      },
    },
  },

  // Mui container
  MuiContainer: {
    styleOverrides: {
      root: {
        paddingLeft: THEME_SPACING * 4,
        paddingRight: THEME_SPACING * 4,
      },
    },
  },

  MuiDialog: {
    styleOverrides: {
      root: {
        // backgroundColor: alpha(indigo[800], 0.2),
        backgroundColor: 'rgb(59 68 123 / 45%)',
      },
    },
  },
  MuiAlertTitle: {
    styleOverrides: {
      root: {
        fontSize: '1rem',
      },
    },
  },
};

export default components;
