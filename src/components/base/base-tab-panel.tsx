import { FC, ReactNode } from 'react';
import Box from '@mui/material/Box';

interface Props {
  value: number;
  index: number;
  children: ReactNode;
}

export const BaseTabPanel: FC<Props> = ({ value, index, children }) => {
  if (index === value) {
    return <Box>{children}</Box>;
  }

  return null;
};
