import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import { MainLayout } from '@src/components/layouts'
import { FC } from 'react'

const AccountPage: FC = () => (
  <MainLayout>
    <Box
      sx={{
        height: 400,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Typography component="h1" variant="h5">
        Account Screen
      </Typography>
    </Box>
  </MainLayout>
)

export default AccountPage
