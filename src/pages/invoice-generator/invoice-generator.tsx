// import '@src/components/invoice-generator/scss/main.scss'

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
// import { InvoiceGenerator } from '@src/components/invoice-generator'
// import { DrawerEditableInvoice } from '@src/components/invoice-generator'
import { BoxSpinner } from '@src/components/ui';
import { useAppSelector } from '@src/store/hook';
import { setInvoiceGeneratorData, setInvoicePdfMode } from '@src/store/invoice-generator/actions';
import { fetchInvoiceData } from '@/store/job-order/job-order-actions';
import { DRAWER_EDITABLE_INVOICE_WIDTH } from '@src/utils/constants';
import { useQueryParams } from '@src/utils/url-params';
import { FC, MouseEvent, useEffect } from 'react';
import { useDispatch } from 'react-redux';

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })<{
  open?: boolean;
}>(({ theme, open }) => ({
  flexGrow: 1,
  backgroundColor: theme.palette.background.paper,
  transition: theme.transitions.create('margin', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginRight: `${DRAWER_EDITABLE_INVOICE_WIDTH}px`,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const InvoicePage: FC = () => {
  const { pdfMode, drawerEditable, invoice } = useAppSelector((state) => state.invoiceGenerator);
  const { invoiceData: invoiceJo } = useAppSelector((state) => state.jobOrder);
  const dispatch = useDispatch();
  const query = useQueryParams();
  const queryJobOrderIds: string = query.get('JobOrderIds');

  useEffect(() => {
    if (queryJobOrderIds) dispatch(fetchInvoiceData({ jo_ids: queryJobOrderIds.split(',') }));

    // Clean up and set invoice to null
    return () => {
      dispatch(setInvoiceGeneratorData(null));
    };
  }, [queryJobOrderIds]);

  const handleToggleMode = (event: MouseEvent<unknown>): void => {
    dispatch(setInvoicePdfMode(!pdfMode));
  };

  return (
    <Main open={drawerEditable}>
      <Box
        sx={{
          minHeight: '100vh',
          width: '100%',
          backgroundColor: 'background.default',
        }}
      >
        <Button variant="outlined" size="small" onClick={handleToggleMode}>
          Toggle Mode
        </Button>
        {/* {invoiceJo.isFetching ? (
          <BoxSpinner height={500} />
        ) : (
          invoice && <InvoiceGenerator data={invoice} pdfMode={pdfMode} />
        )} */}
      </Box>
      {/* <DrawerEditableInvoice /> */}
    </Main>
  );
};

export default InvoicePage;
