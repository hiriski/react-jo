import { TRole } from '@src/types/role'

import { RoleAction } from './actions'
import * as ActionTypes from './constants'

export interface RoleState {
  isFetching: boolean
  isError: boolean
  listRole: Array<TRole>
}

const initialState: RoleState = {
  isFetching: false,
  isError: false,
  listRole: [],
}

const roleReducer = (
  state: RoleState = initialState,
  action: RoleAction
): RoleState => {
  switch (action.type) {
    case ActionTypes.FETCHING_LIST_ROLE_LOADING:
      return {
        ...state,
        isFetching: true,
        isError: false,
        listRole: [],
      }
    case ActionTypes.FETCHING_LIST_ROLE_FAILURE:
      return {
        ...state,
        isFetching: false,
        isError: true,
        listRole: [],
      }
    case ActionTypes.FETCHING_LIST_ROLE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        listRole: action.payload,
      }

    default:
      return state
  }
}

export default roleReducer
