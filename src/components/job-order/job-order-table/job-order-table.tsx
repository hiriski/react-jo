import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableContainer from '@mui/material/TableContainer';
import Typography from '@mui/material/Typography';
import { BoxSpinner } from '@src/components/ui';
import { useAppSelector } from '@src/store/hook';
import { fetchListJo, jobOrder_setFilters } from '@/store/job-order/job-order-actions';
import { ROWS_PER_PAGE } from '@src/utils/constants';
import React, { ChangeEvent, FC, MouseEvent, memo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useWindowSize } from 'react-use';

// Components.
import { JobOrderTableBody } from './job-order-table-body';
import { JobOrderTableHeader } from './job-order-table-header';
import { JobOrderTablePagination } from './job-order-table-pagination';
import { JobOrderTableToolbar } from './job-order-table-toolbar';

// Interfaces.
import { IJobOrder } from '@/interfaces/job-order';
import { UUID } from '@/interfaces/common';
import { jobOrder_rootSelector } from '@/store/job-order/job-order-selectors';
import { BaseEmptyContent } from '@/components/base';

type Order = 'asc' | 'desc';

type JoTableProps = {
  data: Array<IJobOrder>;
  isFetching: boolean;
};

export const JobOrderTable: FC<JoTableProps> = ({ data, isFetching }: JoTableProps) => {
  const { height: windowHeight } = useWindowSize();
  const dispatch = useDispatch();
  const { listJo, filters } = useAppSelector((state) => jobOrder_rootSelector(state));
  const { visibleSidebarDrawer } = useAppSelector((state) => state.common);
  const { meta } = listJo;
  const spaces =
    52 /* <- toolbar */ + 64 /* <- table header */ + 76 /* <- main padding */ + 32; /* <- exta padding bottom */
  const [order, setOrder] = useState<Order>('asc');
  const [orderBy, setOrderBy] = useState<string /* keyof Data */>('calories');
  const [selected, setSelected] = useState<Array<UUID>>([]);

  // const fetchData = (params?: Record<string, string | number>): void => {
  //   dispatch(fetchListJo());
  // };
  const handleRequestSort = (event: MouseEvent<unknown>, property: string /* keyof Data */): void => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event: ChangeEvent<HTMLInputElement>): void => {
    if (event.target.checked) {
      const newSelecteds = data.map((n) => n.id);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleChangePage = (event: unknown, newPage: number): void => {
    dispatch(jobOrder_setFilters('page', newPage + 1));
  };

  /**
   * Handle change rows per page.
   * @param event
   */
  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const newValueRowsPerPage = Number(event.target.value);
    dispatch(jobOrder_setFilters('page', 1)); // Reset page to 1
    dispatch(jobOrder_setFilters('per_page', newValueRowsPerPage));
  };

  return (
    <Paper
      sx={{
        width: '100%',
      }}
      elevation={2}
    >
      <JobOrderTableToolbar numSelected={selected.length} selected={selected} />
      <TableContainer
        sx={
          {
            /* maxHeight: windowHeight - spaces */
          }
        }
      >
        <Table
          stickyHeader
          sx={{ minWidth: visibleSidebarDrawer ? 1000 : 1200 }}
          aria-labelledby="tableTitle"
          size="medium"
        >
          {!isFetching && Array.isArray(data) && data.length === 0 && (
            <caption
              style={{
                // height: windowHeight - spaces - /* extra spaces -> */ 32,
                height: 300,
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  flex: 1,
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <BaseEmptyContent title="No data" />
              </Box>
            </caption>
          )}
          {(listJo.isFetching && data.length === 0) || listJo.isLoading ? (
            <caption
              style={{
                height: 400,
              }}
            >
              <BoxSpinner height={windowHeight - spaces} />
            </caption>
          ) : (
            <>
              <JobOrderTableHeader
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={data.length}
              />
              <JobOrderTableBody
                rows={data}
                rowsPerPage={filters.per_page}
                order={order}
                page={meta ? meta.current_page : 0}
                orderBy={orderBy}
                selected={selected}
                setSelected={setSelected}
              />
            </>
          )}
        </Table>
      </TableContainer>
      <JobOrderTablePagination
        rowsPerPageOptions={ROWS_PER_PAGE}
        count={meta ? meta.total : data.length}
        rowsPerPage={filters.per_page}
        page={meta ? meta.current_page - 1 : 0}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
};
