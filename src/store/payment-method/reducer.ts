import { IPaymentMethod } from '@src/types/jo'

import { PaymentMethodAction } from './actions'
import * as ActionTypes from './constants'

export interface PaymentMethodState {
  isFetching: boolean
  isError: boolean
  data: Array<IPaymentMethod>
}

const initialState: PaymentMethodState = {
  isFetching: false,
  isError: false,
  data: [],
}

const paymentMethodReducer = (
  state: PaymentMethodState = initialState,
  action: PaymentMethodAction,
): PaymentMethodState => {
  switch (action.type) {
    case ActionTypes.FETCHING_LIST_PAYMENT_METHOD_LOADING:
      return {
        ...state,
        isFetching: true,
        isError: false,
        data: [],
      }
    case ActionTypes.FETCHING_LIST_PAYMENT_METHOD_FAILURE:
      return {
        ...state,
        isFetching: false,
        isError: true,
        data: [],
      }
    case ActionTypes.FETCHING_LIST_PAYMENT_METHOD_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        data: action.payload,
      }

    default:
      return state
  }
}

export default paymentMethodReducer
