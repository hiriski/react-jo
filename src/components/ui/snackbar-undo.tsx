import React, { FC } from 'react'
import Button from '@mui/material/Button'
import MuiSnackbar from '@mui/material/Snackbar'
import IconButton from '@mui/material/IconButton'
import CloseIcon from '@mui/icons-material/Close'

type Props = {
  handleClose: () => void
  handleUndo: (id: number) => void
}

const SnackBar: FC = () => {
  const [open, setOpen] = React.useState(false)

  const handleClick = (): void => {
    setOpen(true)
  }

  const handleClose = (event: React.SyntheticEvent | Event, reason?: string): void => {
    if (reason === 'clickaway') {
      return
    }

    setOpen(false)
  }

  const action = (
    <>
      <Button color="secondary" size="small" onClick={handleClose}>
        UNDO
      </Button>
      <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
        <CloseIcon fontSize="small" />
      </IconButton>
    </>
  )

  return (
    <div>
      <Button onClick={handleClick}>Open simple snackbar</Button>
      <MuiSnackbar open={open} autoHideDuration={6000} onClose={handleClose} message="Note archived" action={action} />
    </div>
  )
}

export default SnackBar
