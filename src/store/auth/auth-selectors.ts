// Interfaces.
import type { AppState } from '@src/store/root-reducer';
import { AuthState } from './reducer';

export const getAuthRootState = (state: AppState): AuthState => state.auth;

export const auth_rootSelector = (state: AppState): AuthState => state.auth;
