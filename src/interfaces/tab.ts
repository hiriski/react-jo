export interface ITab {
  label: string;
  value: number;
}
