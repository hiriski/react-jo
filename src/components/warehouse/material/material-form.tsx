import { FC, MouseEvent, useState } from 'react';

// Mui components.
import { Box, Grid } from '@mui/material';
import { IMasterData } from '@/interfaces/master-data';
import { SubmitHandler, useForm } from 'react-hook-form';
import { RequestBodyMaterial } from '@/interfaces/warehouse';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { dropdownMaterialUnits } from '@/lib';
import { ControlledSelect, ControlledTextField, ControlledRadioGroup } from '@/components/base/input-base';
import { StyledButton } from '@/components/ui';
import { useDispatch } from 'react-redux';
import { warehouse_createMaterial } from '@/store/warehouse/warehouse-actions';
import { useAppSelector } from '@/store/hook';
import { warehouse_rootSelector } from '@/store/warehouse/warehouse-selectors';

interface Props {
  id?: number;
  sources: IMasterData;
}

type Inputs = RequestBodyMaterial;

// Initial values
const initialValues = {
  name: '',
  unit: '',
  display_unit: '',
  material_brand_id: 1,
  material_category_id: 1,
  material_type_id: 1,
  stock_length: undefined,
  stock_width: undefined,
  stock_pcs: undefined,
  width_per_roll: undefined,
  length_per_roll: undefined,
  price: undefined,
  description: undefined,
  status: 'active',
  image: undefined,
};

const schema = yup.object().shape({
  name: yup
    .string()
    .required('Nama bahan tidak boleh kosong')
    .min(3, 'Nama bahan minimal 3 karakter')
    .max(255, 'Nama bahan maksimal 255 karakter'),
  unit: yup.string().required('Satuan presisi wajib di isi'),
  display_unit: yup.string().required('Dispplay satuan presisi wajib di isi'),
  material_brand_id: yup.number().required('Brand bahan wajib di isi'),
  material_category_id: yup.number().required('Kategori bahan wajib di isi'),
  material_type_id: yup.number().required('Tipe bahan  wajib di isi'),
  stock_length: yup.number(),
  stock_width: yup.number(),
  stock_pcs: yup.number(),
  width_per_roll: yup.number(),
  length_per_roll: yup.number(),
  price: yup.number(),
  description: yup.string(),
});

export const MaterialForm: FC<Props> = ({ id, sources }) => {
  const [images, setImages] = useState<string>();
  const { createMaterial } = useAppSelector((state) => warehouse_rootSelector(state));
  const dispatch = useDispatch();

  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
    watch,
    reset,
  } = useForm({
    defaultValues: initialValues,
    resolver: yupResolver(schema),
  });

  console.log('❌ errors', errors);

  /**
   * On form submit
   */
  const handleFormSubmit = (values): void => {
    console.log('values', values);
    dispatch(warehouse_createMaterial(values));
  };

  /**
   * Handle form reset
   */
  const handleReset = (e: MouseEvent<HTMLButtonElement>): void => {
    e.preventDefault();
    reset(initialValues);
  };

  return (
    <Box component="form" onSubmit={handleSubmit(handleFormSubmit)}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <ControlledTextField
            name="name"
            control={control}
            fullWidth
            size="small"
            label="Nama Bahan"
            errors={errors}
            helperText="Isi nama bahan baku"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <ControlledSelect
            control={control}
            errors={errors}
            name="unit"
            label="Satuan Presisi"
            helperText="Pilih satuan presisi"
            size="small"
            fullWidth
            data={dropdownMaterialUnits}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <ControlledSelect
            control={control}
            errors={errors}
            name="display_unit"
            label="Display Satuan"
            helperText="Pilih display satuan"
            size="small"
            fullWidth
            data={dropdownMaterialUnits}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <ControlledSelect
            control={control}
            errors={errors}
            name="material_category_id"
            label="Kategori Bahan"
            helperText="Pilih kategori bahan"
            size="small"
            fullWidth
            data={sources.material_categories.map((x) => ({
              label: x.name,
              value: x.id,
            }))}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <ControlledSelect
            control={control}
            errors={errors}
            name="material_type_id"
            label="Tipe bahan"
            helperText="Pilih tipe bahan"
            size="small"
            fullWidth
            data={sources.material_types.map((x) => ({
              label: x.name,
              value: x.id,
            }))}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <ControlledSelect
            control={control}
            errors={errors}
            name="material_brand_id"
            label="Brand"
            helperText="Pilih brand"
            size="small"
            fullWidth
            data={sources.material_brands.map((x) => ({
              label: x.name,
              value: x.id,
            }))}
          />
        </Grid>

        <Grid item xs={12} md={6}>
          <ControlledTextField
            name="stock_length"
            control={control}
            fullWidth
            size="small"
            label="Panjang Stock"
            errors={errors}
            helperText="Isi panjang stock yang tersedia"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <ControlledTextField
            name="stock_width"
            control={control}
            fullWidth
            size="small"
            label="Lebar Stock"
            errors={errors}
            helperText="Isi lebar stock yang tersedia"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <ControlledTextField
            name="stock_pcs"
            control={control}
            fullWidth
            size="small"
            label="Jumlah Stock (pcs)"
            errors={errors}
            helperText="Isi jumlah stock dalam pcs"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <ControlledTextField
            name="width_per_roll"
            control={control}
            fullWidth
            size="small"
            label="Lebar per roll"
            errors={errors}
            helperText="Isi lebar per roll"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <ControlledTextField
            name="length_per_roll"
            control={control}
            fullWidth
            size="small"
            label="Panjang per roll"
            errors={errors}
            helperText="Isi panjang per roll"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <ControlledTextField
            name="price"
            control={control}
            fullWidth
            size="small"
            label="Harga"
            errors={errors}
            helperText="Isi harga bahan baku"
          />
        </Grid>
        <Grid item xs={12}>
          <ControlledTextField
            name="description"
            control={control}
            fullWidth
            multiline
            minRows={2}
            maxRows={4}
            size="small"
            label="Deskripsi"
            errors={errors}
            helperText="Deskripsi (isi jika perlu)"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <ControlledRadioGroup
            row
            control={control}
            errors={errors}
            name="status"
            helperText="Status (aktif/tidak aktif)"
            data={[
              { value: 'active', label: 'Active' },
              { value: 'inactive', label: 'Tidak aktif' },
            ]}
          />
        </Grid>
        <Grid item xs={12}>
          <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', mt: 3 }}>
            <StyledButton size="large" variant="outlined" disableHoverEffect onClick={handleReset}>
              Reset Form
            </StyledButton>
            <StyledButton isLoading={createMaterial.isLoading} size="large" disableHoverEffect type="submit">
              Submit
            </StyledButton>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};
