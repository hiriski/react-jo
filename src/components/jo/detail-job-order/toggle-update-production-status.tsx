import BookmarkIcon from '@mui/icons-material/Bookmark';
import BookmarkAddedIcon from '@mui/icons-material/BookmarkAdded';
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import { Typography, Box } from '@mui/material';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import { useAppSelector } from '@src/store/hook';
import { updatePaymentStatusJo, updateProductionStatus } from '@/store/job-order/job-order-actions';
import { FC, MouseEvent, useState } from 'react';
import { useDispatch } from 'react-redux';

type Props = {
  joId: string;
  selectedId: number;
};

const StyledToggleButtonGroup = styled(ToggleButtonGroup)(({ theme }) => ({
  '& .MuiToggleButtonGroup-grouped': {
    margin: theme.spacing(0.5),
    border: 0,
    '&.Mui-disabled': {
      border: 0,
    },
    '&:not(:first-of-type)': {
      borderRadius: theme.shape.borderRadius,
    },
    '&:first-of-type': {
      borderRadius: theme.shape.borderRadius,
    },
  },
}));

const StyledToggleButton = styled(ToggleButton)(({ theme }) => ({
  '&.MuiToggleButton-root': {
    padding: '4px 10px',
  },
}));

const ToggleUpdateProductionStatus: FC<Props> = ({ joId, selectedId }: Props) => {
  const dispatch = useDispatch();
  const [alignment, setAlignment] = useState<string | undefined>(undefined);
  const { data } = useAppSelector((state) => state.productionStatus);

  const handleChange = (event: MouseEvent<HTMLElement>, newAlignment: string): void => {
    setAlignment(newAlignment);
  };

  const handleUpdatePaymentMethod = (statusId: number): void => {
    dispatch(updateProductionStatus(joId, statusId));
  };

  return (
    <Paper
      elevation={0}
      sx={{
        display: 'flex',
        border: (theme) => `1px solid ${theme.palette.divider}`,
        flexWrap: 'wrap',
      }}
    >
      <StyledToggleButtonGroup size="small" color="primary" value={alignment} exclusive onChange={handleChange}>
        {data.map((item) => (
          <StyledToggleButton key={item.id} value={item.id} onClick={() => handleUpdatePaymentMethod(item.id)}>
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              {item.id === selectedId ? <BookmarkAddedIcon /> : <BookmarkBorderIcon />}
              <Typography sx={{ textTransform: 'capitalize', ml: 0.5 }}>{item.name}</Typography>
            </Box>
          </StyledToggleButton>
        ))}
      </StyledToggleButtonGroup>
    </Paper>
  );
};

export default ToggleUpdateProductionStatus;
