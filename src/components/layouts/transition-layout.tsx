import { FC, ReactNode, useRef, memo } from 'react';
import { useLocation } from 'react-router';
import { Transition, TransitionGroup, TransitionStatus } from 'react-transition-group';

const transitionDuration = 3000;

const getTransitionStyles: TransitionStatus | any = {
  // exited (ketika page ditinggalkan)
  exited: {
    opacity: 0.3,
    transform: 'translateY(0)',
    transition: `opacity ${transitionDuration}ms ease-in-out, transform ${transitionDuration}ms ease-in-out`,
  },

  // Ketika page baru di load ini juga ketika pertama kali di load
  entered: {
    transition: `opacity ${transitionDuration / 2}ms ease-in-out, transform ${transitionDuration}ms ease-in-out`,
    opacity: 1,
    transform: 'translateY(0)', // it's should be unset!
    // transform: `translate(0)`, // it's should be unset!
  },

  // Dijalankan setelah entered
  entering: {
    opacity: 0,
    transform: 'translateY(100px)',
    transition: `opacity ${transitionDuration}ms ease-in-out, transform ${transitionDuration}ms ease-in-out`,
  },

  // dijalankan setelah entering
  exiting: {
    transition: `opacity ${transitionDuration}ms ease-in-out, transform ${transitionDuration}ms ease-in-out`,
    opacity: 'translateY(100px)',
    transform: 'scale(0)',
  },
};

interface Props {
  children: ReactNode;
  pathname: string;
}

const TransitionLayout: FC<Props> = ({ children, pathname }) => {
  const nodeRef = useRef(null);

  console.log('pathname', pathname);

  return (
    <TransitionGroup component={null}>
      <Transition
        // in={Boolean(location.pathname)}
        // ---
        // Fix Warning: findDOMNode is deprecated in StrictMode.
        // findDOMNode was passed an instance of Transition which is inside StrictMode.
        nodeRef={nodeRef}
        // ---
        key={pathname}
        in={Boolean(pathname)}
        timeout={{
          enter: transitionDuration,
          exit: transitionDuration,
        }}
      >
        {(status) => {
          console.log('status', status);
          return <div style={{ ...getTransitionStyles[status] }}>{children}</div>;
        }}
      </Transition>
    </TransitionGroup>
  );
};

export default TransitionLayout;
