import { ChangeEvent, FC, MouseEvent, ReactNode, SyntheticEvent, useEffect, useMemo, useState } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';

import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';
import InputAdornment from '@mui/material/InputAdornment';
import Typography from '@mui/material/Typography';
import { Divider, FormGroup, Switch, SxProps } from '@mui/material';
import { useAppSelector } from '@src/store/hook';
import { initDataMasterDataState } from '@/store/master-data/master-data-reducer';
import { TCreateJo, TCustomer, TJoCategory } from '@src/types/jo';
import { IMasterData } from '@/interfaces/master-data';
import { DB_FORMAT_TIMESTAMPS } from '@src/utils/constants';
import moment from 'moment';
import { Controller, SubmitHandler, useFieldArray, useForm } from 'react-hook-form';
import ImageUploading, { ImageListType } from 'react-images-uploading';
import { useDispatch } from 'react-redux';
// import Swal from 'sweetalert2'
import { colors } from '@/utils/colors';
import * as yup from 'yup';

import { ICustomer, IRequestBodyCustomer } from '@/interfaces/customer';

// Mui components.

// Mui icons.
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';

// Config,
import { AppConfig } from '@/config/theme/app/app-config';

// Validation schema.
import { customerFormSchema } from './customer-form.schema';

// Interfaces.
import { UUID } from '@/interfaces/common';

// Base components.
import { BaseSectionTitle } from '@/components/base/base-section-title';
import { IRequestBodyJobOrder } from '@/interfaces/job-order';
import {
  ControlledCheckbox,
  ControlledTextField,
  ControlledSelect,
  ControlledRadioGroup,
  ControlledAutocomplete,
} from '@/components/base/input-base';
import { BaseButton, BaseUploadUserPhoto } from '@/components/base';
import { dropdownCustomerType, dropdownGenders } from '@/lib/dropdown';
import { jobOrder_rootSelector } from '@/store/job-order/job-order-selectors';

// Lib wilayah administratif.
import { provinsi, kabupaten } from 'daftar-wilayah-indonesia';
import {
  createCustomer,
  customer_setFormIsHasChanges,
  customer_updateCustomer,
} from '@/store/customer/customer-actions';
import { customer_createFormValues } from './customer-form.helper';

type CustomerOptions = TCustomer;

type Inputs = IRequestBodyCustomer;

type Props = {
  sx?: SxProps;
  data?: ICustomer;
  id?: UUID | null;
};

const initialValues: IRequestBodyCustomer = {
  customer_type: 'individual',
  name: '',
  phone_number: '',
  email: '',
  gender: 'male',
  notes: '',
  avatar_text_color: colors[Math.floor(Math.random() * colors.length)],
  addresses: [
    {
      label: '',
      address_details: '',
      district: '',
      city: '',
      province: '',
      postcode: '',
      country: 'Indonesia',
    },
  ],
};

export const CustomerForm: FC<Props> = ({ id, data, sx }: Props) => {
  const dispatch = useDispatch();
  const {
    createLoading,
    updateLoading,
    isFormHasChanges: currentIsFormHasChanges,
  } = useAppSelector((state) => jobOrder_rootSelector(state));
  const { isFetching, data: masterData } = useAppSelector((state) => state.masterData);
  const [photo, setPhoto] = useState<string>('');

  const [isNullableCustomerAddress, setIsNullableCustomerAddress] = useState<boolean>(false);

  const handleChangeIsNullableCustomer = (e: SyntheticEvent, checked: boolean): void => {
    setIsNullableCustomerAddress(checked);
  };

  // Is edit mode
  const isEdit = useMemo<boolean>(() => {
    if (id) return true;
    else return false;
  }, [id]);

  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
    watch,
    reset,
  } = useForm({
    defaultValues: id && data ? customer_createFormValues(data) : initialValues,
    resolver: yupResolver(customerFormSchema),
  });

  // if (id && data) console.log('customer_createFormValues(data)', customer_createFormValues(data));

  // Array field customer.addresses.
  const {
    fields: fieldsCustomerAddresses,
    append,
    remove,
  } = useFieldArray({
    control,
    name: 'addresses',
  });

  const isFormError = (): boolean =>
    Object.keys(errors).length !== 0; /* && Object.getPrototypeOf(errors) !== Object.prototype */

  /**
   * Handle form submit.
   *
   * @param {Inputs} values
   * @return {SubmitHandler<Inputs>} void
   */
  const onSubmit: SubmitHandler<Inputs> = (values) => {
    if (id && data) dispatch(customer_updateCustomer(id, values));
    else dispatch(createCustomer({ photo: photo !== '' ? photo : null, ...values }));
    console.log('⚡⚡⚡ values', values);
  };

  const handleChangePhoto = (newPhoto: string): void => {
    setPhoto(newPhoto);
  };

  /**
   * Handle reset form.
   *
   * @param {MouseEvent<HTMLButtonElement>} event
   */
  const handleResetForm = (event: MouseEvent<HTMLButtonElement>): void => {
    event.preventDefault();
    reset(initialValues);
  };

  const provinces = useMemo<Array<string>>(() => {
    return provinsi().map((x) => x.nama);
  }, []);

  const cities = useMemo<Array<string>>(() => {
    return kabupaten().map((x) => x.nama);
  }, []);

  useEffect(() => {
    const _arrAddress = watch('addresses');
    if (_arrAddress.length > 1) {
      setValue(`addresses.${0}.label`, `Alamat utama`);
    }
  }, [watch('addresses')]);

  // const districts = useMemo<Array<string>>(() => {
  //   return kecamatan().map((x) => x.nama);
  // }, []);

  // Effect to set form has changes.
  useEffect(() => {
    if (getValues('name') !== '' || getValues('phone_number') || getValues('email') || watch('notes') !== '') {
      if (!currentIsFormHasChanges) {
        dispatch(customer_setFormIsHasChanges(true));
      }
    } else {
      dispatch(customer_setFormIsHasChanges(false));
    }
  }, [watch('name'), watch('phone_number'), watch('email'), watch('notes')]);

  // Set values for edit customer
  // useEffect(() => {
  //   if(id && data) )
  // }, [id, data])

  console.log('❌❌ errors', errors);

  return (
    <Box component="form" onSubmit={handleSubmit(onSubmit)}>
      <Box sx={{ display: 'flex', justifyContent: 'center', mb: 6 }}>
        <BaseUploadUserPhoto photo={photo} onChange={handleChangePhoto} />
      </Box>

      <Box sx={{ mb: 3 }}>
        <Typography sx={{ textTransform: 'uppercase' }} component="h5" variant="subtitle2">
          Info Customer
        </Typography>
      </Box>

      <Grid container spacing={3} sx={{ mb: 6 }}>
        <Grid item xs={12}>
          <ControlledRadioGroup
            row
            control={control}
            errors={errors}
            label="Kategori pelanggan"
            data={dropdownCustomerType}
            name="customer_type"
          />
        </Grid>

        <Grid item xs={12} sm={6}>
          <ControlledTextField
            control={control}
            errors={errors}
            fullWidth
            name="name"
            label={watch('customer_type') === 'company' ? 'Nama Perusahaan' : 'Nama Customer'}
            helperText={watch('customer_type') === 'company' ? 'Nama Perusahaan' : 'Nama Customer'}
            margin="none"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <ControlledTextField
            control={control}
            errors={errors}
            fullWidth
            name="phone_number"
            label="HP/Wa"
            helperText="HP/WA Customer"
            margin="none"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <ControlledTextField
            control={control}
            errors={errors}
            fullWidth
            name="email"
            label="Email"
            helperText="Email Customer"
            margin="none"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <Collapse in={watch('customer_type') === 'individual'}>
            {watch('customer_type') === 'individual' && (
              <ControlledRadioGroup
                row
                control={control}
                errors={errors}
                // label="Jenis Kelamin"
                data={dropdownGenders}
                name="gender"
              />
            )}
          </Collapse>
        </Grid>
      </Grid>

      <Box sx={{ mb: 2 }}>
        <Typography sx={{ textTransform: 'uppercase' }} component="h5" variant="subtitle2">
          Alamat Customer
        </Typography>
      </Box>

      <Box sx={{ mb: 3 }}>
        <FormGroup>
          <FormControlLabel
            control={<Switch value={isNullableCustomerAddress} />}
            label="Saya tidak tahu alamat pelanggan"
            onChange={handleChangeIsNullableCustomer}
          />
        </FormGroup>
      </Box>

      <Collapse in={!isNullableCustomerAddress}>
        {fieldsCustomerAddresses.map((x, index) => (
          <Grid key={String(index)} container spacing={3} sx={{ mb: 2 }}>
            {index !== 0 && (
              <Grid item xs={12}>
                <Divider sx={{ mt: 1 }} />
              </Grid>
            )}

            {fieldsCustomerAddresses.length > 1 && (
              <Grid item xs={12}>
                <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                  <Typography variant="subtitle1">Alamat {index + 1}</Typography>

                  <BaseButton
                    type="button"
                    size="small"
                    variant="outlined"
                    color="error"
                    startIcon={<DeleteOutlineIcon />}
                    onClick={() => remove(index)}
                  />
                </Box>
              </Grid>
            )}

            <Grid item xs={12} sm={6} md={4}>
              <ControlledAutocomplete
                control={control}
                errors={errors}
                name={`addresses.${index}.province`}
                label="Provinsi"
                helperText="Provinsi"
                fullWidth
                data={provinces}
                margin="none"
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <ControlledAutocomplete
                control={control}
                errors={errors}
                name={`addresses.${index}.city`}
                label="Kabupaten/Kota"
                helperText="Kabupaten/Kota"
                data={cities}
                // disabled={!selectedProviceId}
                margin="none"
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <ControlledTextField
                control={control}
                errors={errors}
                fullWidth
                name={`addresses.${index}.district`}
                label="Kecamatan"
                helperText="Kecamatan"
                margin="none"
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <ControlledTextField
                control={control}
                errors={errors}
                fullWidth
                name={`addresses.${index}.address_details`}
                multiline
                minRows={2}
                maxRows={3}
                label="Detail alamat"
                helperText="Nama gedung, jalan atau komplek"
                margin="none"
              />
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ControlledTextField
                control={control}
                errors={errors}
                fullWidth
                name={`addresses.${index}.postcode`}
                label="Kode Pos"
                helperText="Kode Pos"
                margin="none"
              />
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ControlledTextField
                control={control}
                errors={errors}
                fullWidth
                name={`addresses.${index}.country`}
                label="Negara"
                helperText="Negara"
                disabled={true}
                margin="none"
              />
            </Grid>
          </Grid>
        ))}

        <Box sx={{ display: 'flex', justifyContent: 'center', mb: 6 }}>
          <BaseButton
            variant="outlined"
            color="primary"
            onClick={() =>
              append({
                label: '',
                address_details: '',
                district: '',
                city: '',
                province: '',
                postcode: '',
                country: 'Indonesia',
              })
            }
          >
            Tambah Alamat ke {fieldsCustomerAddresses.length + 1}
          </BaseButton>
        </Box>
      </Collapse>

      <Grid container spacing={3} sx={{ mb: 8 }}>
        <Grid item xs={12}>
          <ControlledTextField
            control={control}
            errors={errors}
            fullWidth
            multiline
            minRows={2}
            maxRows={3}
            name="notes"
            label="Catatan"
            helperText="Tulis catatan untuk mengingat nama pelanggan"
            margin="none"
          />
        </Grid>
      </Grid>

      {/* Begin submit actions */}

      <Box sx={{ display: 'flex', aligItems: 'center', justifyContent: 'center' }}>
        <Box sx={{ mr: 5 }}>
          <BaseButton
            onClick={handleResetForm}
            disabled={isFetching || createLoading || updateLoading}
            variant="outlined"
            size="large"
            color="secondary"
            disableHoverEffect
          >
            Reset Form
          </BaseButton>
        </Box>
        <BaseButton
          disabled={isFetching || createLoading || updateLoading}
          type="submit"
          variant="contained"
          size="large"
          disableHoverEffect
        >
          Simpan
        </BaseButton>
      </Box>

      <Box></Box>

      {/* End of submit actions */}
    </Box>
  );
};
