import { TRole } from '@src/types/role'
import api from '@src/utils/http'
import { AxiosResponse } from 'axios'

const RoleAPI = {
  findAll: async (): Promise<AxiosResponse<Array<TRole>>> => api.get('/role'),
}

export default RoleAPI
