import Box from '@mui/material/Box';
import Dialog from '@mui/material/Dialog';
import Zoom, { ZoomProps } from '@mui/material/Zoom';
import { DialogContent } from '@src/components/ui';
import { useAppSelector } from '@src/store/hook';
import { jobOrder_fetchDetail, jobOrder_setDialogQr, setDialogDetailJo } from '@/store/job-order/job-order-actions';
import { FC, ReactElement, forwardRef, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useWindowSize } from 'react-use';
import { jobOrder_rootSelector } from '@/store/job-order/job-order-selectors';

const Transition = forwardRef<unknown, ZoomProps>(
  (props, ref): ReactElement => (
    // eslint-disable-next-line react/jsx-props-no-spreading
    // <Slide direction="down" ref={ref} {...props} />
    // <Grow style={{ transformOrigin: '0 0 0' }} ref={ref} {...props} />
    <Zoom ref={ref} {...props} />
  ),
);

export const JobOrderDialogQr: FC = () => {
  const dispatch = useDispatch();
  const { dialogQr } = useAppSelector((state) => jobOrder_rootSelector(state));
  const { open, qr } = dialogQr;

  const handleClose = (): void => {
    dispatch(jobOrder_setDialogQr(false, null));
  };

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleClose}
      aria-describedby="dialog-job-order-qr"
      PaperProps={{ elevation: 0 }}
      maxWidth="md"
      // fullWidth
    >
      <DialogContent sx={{ p: 5 }}>
        <Box component="img" src={qr} sx={{ height: 300, width: 300 }} />
      </DialogContent>
    </Dialog>
  );
};
