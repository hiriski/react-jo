import { TRole } from '@src/types/role'

import * as ActionTypes from './constants'

// Actions definiton.
interface FetchingListRoleRequested {
  type: typeof ActionTypes.FETCHING_LIST_ROLE_REQUESTED
}
interface FetchingListRoleLoading {
  type: typeof ActionTypes.FETCHING_LIST_ROLE_LOADING
}
interface FetchingListRoleFailure {
  type: typeof ActionTypes.FETCHING_LIST_ROLE_FAILURE
}
interface FetchingListRoleSuccess {
  type: typeof ActionTypes.FETCHING_LIST_ROLE_SUCCESS
  payload: Array<TRole>
}

// Union actions type
export type RoleAction =
  | FetchingListRoleRequested
  | FetchingListRoleLoading
  | FetchingListRoleFailure
  | FetchingListRoleSuccess

// Actions creator.
export const fetchListRole = (): FetchingListRoleRequested => ({
  type: ActionTypes.FETCHING_LIST_ROLE_REQUESTED,
})

export const fetchingListRoleSuccess = (
  payload: Array<TRole>
): FetchingListRoleSuccess => ({
  type: ActionTypes.FETCHING_LIST_ROLE_SUCCESS,
  payload,
})
