import { FC, ReactElement } from 'react';
import { Box, Typography, SxProps } from '@mui/material';

type Props = {
  title: string;
  sx?: SxProps;
};

export const BaseSectionTitle: FC<Props> = ({ title, sx }: Props): ReactElement => (
  <Box sx={{ mb: 3, ...sx }}>
    <Typography sx={{ mb: 2 }} component="h3" variant="h4">
      {title}
    </Typography>
  </Box>
);

BaseSectionTitle.defaultProps = {
  sx: {},
};
