// Axios instance
import api from '@/utils/http';

// Interfaces.
import { IAnalyticsTotalJobOrder, JobOrderTotalPeriod } from './analytics.interface';
import { AxiosResponse } from 'axios';

// Api endpoints.
import { AnalyticsApiEndpoints } from './analytics-api-endpoints';

const AnalyticsApiService = {
  getTotalJobOrder: async (
    period?: JobOrderTotalPeriod,
    date?: string,
  ): Promise<AxiosResponse<IAnalyticsTotalJobOrder>> =>
    api.get(`${AnalyticsApiEndpoints.getTotalJobOrder}`, { params: { period, date } }),
};

export default AnalyticsApiService;
