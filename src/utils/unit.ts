import convert from 'convert-units'

export const cmToMeters = (valueInCm: number): number => {
  if (!valueInCm) return 0
  return convert(valueInCm).from('cm').to('m')
}
