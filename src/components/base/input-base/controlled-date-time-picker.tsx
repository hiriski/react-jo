import { FC, useMemo, useRef, useState } from 'react';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import { DateTimePicker, StaticDateTimePicker, DateTimePickerProps } from '@mui/x-date-pickers';
import { Control, Controller, FieldErrors } from 'react-hook-form';
import moment from 'moment';

// Config.
import { AppConfig } from '@/config/theme/app/app-config';
import { StaticDatePickerSlotsComponentsProps } from '@mui/x-date-pickers/StaticDatePicker/StaticDatePicker';

interface Props
  extends Partial<DateTimePickerProps<string, string>>,
    Pick<TextFieldProps, 'size' | 'fullWidth' | 'variant' | 'margin'> {
  // eslint-disable-next-line
  control: Control<any>;
  errors: FieldErrors;
  name: string;
  label: string;
  helperText?: string;
  inputFormat?: string;
  isStatic?: boolean;
  componentsProps?: Partial<StaticDatePickerSlotsComponentsProps>;
}

export const ControlledDateTimePicker: FC<Props> = (props) => {
  const {
    control,
    errors,
    name,
    label,
    size,
    fullWidth,
    margin,
    helperText,
    inputFormat,
    isStatic,
    componentsProps,
    ...rest
  } = props;

  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange: _onChange, value: _value } }) =>
        isStatic ? (
          <StaticDateTimePicker
            label={label}
            inputFormat={inputFormat ?? 'MM/dd/yyyy'}
            value={_value || null}
            onChange={(newValue: string | null): void => {
              _onChange(moment(newValue).format(AppConfig.DatabaseFormatTimestamps));
            }}
            componentsProps={componentsProps}
            renderInput={(params) => (
              <TextField
                {...params}
                error={Boolean(errors[name]?.message)}
                helperText={errors[name]?.message ?? helperText}
                size={size}
                margin={margin}
                fullWidth={fullWidth}
              />
            )}
          />
        ) : (
          <DateTimePicker
            label={label}
            inputFormat={inputFormat ?? 'MM/dd/yyyy'}
            value={_value || null}
            onChange={(newValue: string | null): void => {
              _onChange(moment(newValue).format(AppConfig.DatabaseFormatTimestamps));
            }}
            componentsProps={componentsProps}
            PaperProps={{
              sx: {
                boxShadow: 'var(--shadow-small)',
              },
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                error={Boolean(errors[name]?.message)}
                helperText={errors[name]?.message ?? helperText}
                size={size}
                margin={margin}
                fullWidth={fullWidth}
              />
            )}
          />
        )
      }
    />
  );
};

ControlledDateTimePicker.defaultProps = {
  size: 'small',
  margin: 'dense',
  isStatic: false,
};
