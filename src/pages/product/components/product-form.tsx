import AddImage from '@/assets/images/add-image.png';
import ChooseImage from '@/assets/images/upload-image.png';
import { StyledButton } from '@/components/ui';
import { ControlledSelect } from '@/components/ui';
import { useAppSelector } from '@/store/hook';
import { createProduct, setFormIsDirty } from '@/store/product/product-actions';
import { IProduct } from '@/types/product';
import { yupResolver } from '@hookform/resolvers/yup';
import CheckIcon from '@mui/icons-material/Check';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import DriveFolderUploadIcon from '@mui/icons-material/DriveFolderUpload';
import RefreshIcon from '@mui/icons-material/Refresh';
import { styled, useMediaQuery } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import MenuItem from '@mui/material/MenuItem';
import { alpha, useTheme } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { FC, MouseEvent, useEffect, useMemo, useState } from 'react';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import ImageUploading, { ImageListType, ImageType } from 'react-images-uploading';
import { useDispatch, useSelector } from 'react-redux';
import * as yup from 'yup';

interface Props {
  id?: string;
  data?: IProduct;
}

interface Inputs {
  title: string;
  description: string;
  category_id: number;
  images: string[];
}

const schema = yup.object().shape({
  title: yup
    .string()
    .required('Judul produk tidak boleh kosong')
    .min(3, 'Judul minimal 3 karakter')
    .max(255, 'Judul maksimal 255 karakter'),
  description: yup.string(),
  // images: yup.string(),
  category_id: yup.string().required('Pilih kategori produk'),
});

// Styled product image.
const StyledProductImage = styled(Box)(({ theme }) => ({
  width: '100%',
  position: 'relative',
  borderRadius: Number(theme.shape.borderRadius) * 2,
  lineHeight: 0,
  height: 172,
  overflow: 'hidden',
  '& img': {
    height: '100%',
    width: 'auto',
    borderRadius: Number(theme.shape.borderRadius) * 2,
  },

  '&:hover': {
    '& .MuiBox-root': {
      bottom: '0 !important',
    },
  },
}));
const StyledImageActions = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'flex-end',
  justifyContent: 'center',
  position: 'absolute',
  width: '100%',
  lineHeight: 1.2,
  bottom: -120,
  backgroundColor: theme.palette.background.paper,
  background: 'linear-gradient(180deg, rgba(255,255,255,0) 0%, rgba(255,255,255,0.6) 25%, rgba(255,255,255,1) 100%)',
  left: 0,
  height: 120,
  paddingBottom: theme.spacing(2),
  transition: theme.transitions.create('bottom', {
    easing: theme.transitions.easing.easeInOut,
    duration: theme.transitions.duration.enteringScreen,
  }),
}));

interface IStyledUploadPlaceholder {
  isDragging: boolean;
}
const StyledUploadPlaceholder = styled(Box, {
  shouldForwardProp: (prop) => prop !== 'isDragging',
})<IStyledUploadPlaceholder>(({ theme, isDragging }) => ({
  position: 'relative',
  height: 172,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  padding: theme.spacing(2, 1.4),
  cursor: 'pointer',
  borderWidth: 1,
  border: 'solid',
  borderStyle: 'dotted',
  borderRadius: Number(theme.shape.borderRadius) * 1.5,
  textAlign: 'center',
  borderColor: isDragging ? theme.palette.primary.main : `${alpha(theme.palette.primary.main, 0.5)}`,
  backgroundColor: isDragging ? alpha(theme.palette.primary.main, 0.2) : alpha(theme.palette.primary.main, 0.08),

  '&:hover': {
    backgroundColor: alpha(theme.palette.primary.main, 0.12),
    '& > .MuiBox-root': {
      opacity: 1,
    },
  },

  '& > .MuiBox-root': {
    opacity: 0.7,
  },
}));

const ProductForm: FC<Props> = ({ id, data }) => {
  const dispatch = useDispatch();
  const theme = useTheme();
  const matchMobileView = useMediaQuery(theme.breakpoints.down('md'));
  const { formIsDirty: currentFormIsDirty, createProduct: createState } = useAppSelector((state) => state.product);

  /**
   * default values.
   */
  const defaultValues = {
    title: '',
    description: '',
    body: '',
    category_id: undefined,
  } as unknown as Inputs;

  /**
   * Hook form.
   */
  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
    watch,
    reset,
  } = useForm({
    defaultValues,
    resolver: yupResolver(schema),
  });

  // Local states.
  const [images, setImages] = useState<ImageType[]>([]);

  // Effect to set form is dirty
  useEffect(() => {
    if (images.length > 0 || getValues('title') !== '' || getValues('description') !== '') {
      if (!currentFormIsDirty) {
        dispatch(setFormIsDirty(true));
      }
    } else {
      dispatch(setFormIsDirty(false));
    }
  }, [images.length, watch('title'), watch('description')]);

  // Master data.
  const sources = useAppSelector((state) => state.masterData.data);

  /**
   * Hook form submit handler.
   * @param values
   */
  const onSubmit: SubmitHandler<Inputs> = (values) => {
    let formValues = values;
    if (Array.isArray(images) && images.length > 0) {
      const imagesMapped = images.map((img) => img.dataURL);
      formValues = { ...values, images: imagesMapped };
      console.log('⚡⚡ formValues', formValues);
      dispatch(createProduct(formValues));
    }
    // if (!id) dispatch(createProduct(formValues))
    // else dispatch(updateJo(id, values))
  };

  /**
   *
   * Handle change upload image.
   * @param imageList
   * @param addUpdateIndex
   */
  const onChangeImage = (imageList: ImageListType, addUpdateIndex: number[] | undefined): void => {
    // data for submit
    console.log('imageList', imageList, addUpdateIndex);
    setImages(imageList as never[]);
  };

  // Create image grid base on images length.
  const imageGrid = useMemo<number>(() => {
    switch (images.length) {
      case 0:
        return 6;
      case 1:
        return 6;
      case 2:
        return 4;
      default:
        return 3;
    }
  }, [images.length]);

  /**
   * Handle reset form.
   */
  const handleResetForm = (e: MouseEvent<HTMLButtonElement>): void => {
    e.preventDefault();
    reset(defaultValues);
    // setValue('category_id', undefined)
  };

  return (
    <>
      <Box
        component="form"
        sx={{
          px: 14,
          py: 6,
          ...(matchMobileView && {
            px: 4,
            py: 4,
          }),
        }}
        onSubmit={handleSubmit(onSubmit)}
      >
        {/* Drawer title */}
        <Box sx={{ mb: 3 }}>
          <Typography component="h2" variant="h4">
            {id ? 'Edit Produk' : 'Tambah Produk'}
          </Typography>
        </Box>

        {/* --- Product image --- */}
        <Grid container spacing={3}>
          <Grid item xs={12} sx={{ mb: 3 }}>
            <Box
              sx={{
                backgroundColor: 'background.default',
                p: 2,
                borderRadius: 1,
                border: (theme) => `1px solid ${theme.palette.divider}`,
              }}
            >
              <ImageUploading multiple value={images} onChange={onChangeImage} maxNumber={10}>
                {({
                  imageList,
                  onImageUpload,
                  onImageRemoveAll,
                  onImageUpdate,
                  onImageRemove,
                  isDragging,
                  dragProps,
                }) => (
                  <Grid container spacing={2}>
                    {/* {imageList.length === 0 ? (
                    <Grid item xs={12} md={imageGrid}>
                      
                    </Grid>
                  ) : (
                    <Box onClick={onImageUpload}>foo</Box>
                  )} */}

                    {/* <button type="button" onClick={onImageRemoveAll}>
                        Remove all images
                      </button> */}

                    {/* --- List of Product Images --- */}
                    {imageList.map((image, index) => (
                      <Grid key={String(index)} item xs={6} md={4} lg={3}>
                        <StyledProductImage key={`index-${String(index)}`}>
                          <img src={image.dataURL} alt={'Jo Image ' + index} />
                          <StyledImageActions>
                            <IconButton sx={{ mr: 2 }} onClick={() => onImageUpdate(index)}>
                              <DriveFolderUploadIcon />
                            </IconButton>
                            <IconButton color="secondary" onClick={() => onImageRemove(index)}>
                              <DeleteOutlineIcon />
                            </IconButton>
                          </StyledImageActions>
                        </StyledProductImage>
                      </Grid>
                    ))}
                    {images.length > 0 ? (
                      <Grid item xs={12} md={3}>
                        <StyledUploadPlaceholder isDragging={isDragging} onClick={onImageUpload} {...dragProps}>
                          <Box
                            sx={{
                              width: 50,
                              margin: '0 auto',
                              '& img': { width: '100%' },
                            }}
                          >
                            <img src={AddImage} alt="add-image" />
                          </Box>
                          <Typography component="h6" variant="h6">
                            <Typography>
                              <span>Add more image</span>
                            </Typography>
                          </Typography>
                        </StyledUploadPlaceholder>
                      </Grid>
                    ) : (
                      <Grid item xs={12}>
                        <StyledUploadPlaceholder isDragging={isDragging} onClick={onImageUpload} {...dragProps}>
                          <Box
                            sx={{
                              width: 50,
                              margin: '0 auto',
                              '& img': { width: '100%' },
                            }}
                          >
                            <img src={ChooseImage} alt="upload-image" />
                          </Box>

                          <Typography component="h6" variant="h6">
                            <Typography>
                              <Typography component="span" sx={{ color: 'blue' }}>
                                Browse
                              </Typography>
                              <span>or Drag image here</span>
                            </Typography>
                          </Typography>
                          <Typography component="p" variant="subtitle2" sx={{ color: 'text.disabled' }}>
                            Support JPG, PNG, GIF atau WEBP
                          </Typography>
                        </StyledUploadPlaceholder>
                      </Grid>
                    )}
                  </Grid>
                )}
              </ImageUploading>
            </Box>
          </Grid>

          {/* --- TextField Product Title --- */}
          <Grid item xs={12}>
            <Controller
              name="title"
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  fullWidth
                  margin="none"
                  size="small"
                  label="Nama Produk"
                  error={Boolean(errors.title?.message)}
                  helperText={errors.title?.message ? errors.title?.message : 'Isi nama produk'}
                />
              )}
            />
          </Grid>

          {/* --- TextField Product Description --- */}
          <Grid item xs={12}>
            <Controller
              name="description"
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  fullWidth
                  multiline
                  rows={3}
                  margin="none"
                  size="small"
                  label="Dekripsi"
                  error={Boolean(errors.description?.message)}
                  helperText={errors.description?.message ? errors.description?.message : 'Deskripsi produk.'}
                />
              )}
            />
          </Grid>

          {/* --- Select Product Category ----*/}
          <Grid item xs={12}>
            <Grid item xs={12} md={6}>
              <ControlledSelect
                control={control}
                name="category_id"
                errors={errors}
                helperText="Pilih kategori produk"
                label="Kategori produk"
              >
                {sources.product_categories.length > 0 &&
                  sources.product_categories.map((category) => (
                    <MenuItem key={String(category.id)} value={category.id}>
                      {category.name}
                    </MenuItem>
                  ))}
              </ControlledSelect>
            </Grid>
          </Grid>

          {/* Form buttons */}
          <Grid item xs={12} md={10} lg={8} sx={{ ml: 'auto', mt: 4 }}>
            <Grid container spacing={3}>
              <Grid item xs={5}>
                <StyledButton
                  fullWidth
                  disableHoverEffect
                  onClick={handleResetForm}
                  color="primary"
                  variant="outlined"
                  startIcon={<RefreshIcon />}
                  size="large"
                >
                  Reset Form
                </StyledButton>
              </Grid>
              <Grid item xs={7}>
                <StyledButton
                  fullWidth
                  disableHoverEffect
                  onClick={() => handleSubmit(onSubmit)}
                  type="submit"
                  variant="contained"
                  color="primary"
                  isLoading={createState.isLoading}
                  startIcon={<CheckIcon />}
                  size="large"
                >
                  Submit
                </StyledButton>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default ProductForm;
