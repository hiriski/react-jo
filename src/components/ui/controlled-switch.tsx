import FormControlLabel from '@mui/material/FormControlLabel'
import InputLabel from '@mui/material/InputLabel'
import Switch from '@mui/material/Switch'
import Typography from '@mui/material/Typography'
import { FC, ReactElement, ReactNode } from 'react'
import { Control, Controller, FieldErrors } from 'react-hook-form'

type Props = {
  // eslint-disable-next-line
  control: Control<any>
  errors: FieldErrors
  name: string
  label: string
  helperText?: string
  size?: 'small' | 'medium'
  margin?: 'dense' | 'normal' | 'none'
  disabled?: boolean
}

const ControlledSwitch: FC<Props> = ({
  control,
  errors,
  name,
  label,
  helperText,
  size,
  margin,
  disabled,
}: Props): ReactElement => (
  <Controller
    name={name}
    control={control}
    render={({ field }) => (
      <FormControlLabel
        sx={{ mb: 1, ml: 0 }}
        labelPlacement="end"
        control={<Switch {...field} size={size} />}
        label={<Typography variant="subtitle2">{label}</Typography>}
      />
      // <FormControl
      //   error={Boolean(errors[name]?.message)}
      //   margin={margin}
      //   size={size}
      //   fullWidth
      // >
      //   <InputLabel>{label}</InputLabel>
      //   <Select {...field} label={label} disabled={disabled}>
      //     {children}
      //   </Select>
      //   <FormHelperText error={Boolean(errors[name]?.message)}>
      //     {errors[name]?.message ? errors[name]?.message : helperText}
      //   </FormHelperText>
      // </FormControl>
    )}
  />
)

ControlledSwitch.defaultProps = {
  helperText: '',
  size: 'small',
  margin: 'dense',
  disabled: false,
}

export default ControlledSwitch
