import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { useAppSelector } from '@src/store/hook';
import { addNotesJo } from '@/store/job-order/job-order-actions';
import { colors, lightColors } from '@src/utils/colors';
import moment from 'moment';
import { FC, useState } from 'react';
import { useDispatch } from 'react-redux';

import { BoxSpinner } from '../ui';
import { UUID } from '@/interfaces/common';
import { INote } from '@/interfaces/note';

type Props = {
  data: Array<INote>;
  joId: UUID;
};

const JobOrderNotes: FC<Props> = ({ data, joId }: Props) => {
  const dispatch = useDispatch();
  const [notes, setNotes] = useState<string>('');
  const [isVisibleInputNotes, setIsVisibleInputNotes] = useState<boolean>(false);

  const { addNotes: stateAddNotes } = useAppSelector((state) => state.jobOrder);

  const handleClickAddNote = (): void => {
    // console.log('notes')
    setIsVisibleInputNotes(true);
  };

  const handleSubmitNotes = (): void => {
    if (notes) {
      dispatch(addNotesJo(joId, notes));
      setNotes('');
      setIsVisibleInputNotes(false);
    }
  };

  return (
    <Box>
      {Array.isArray(data) && data.length > 0 ? (
        data.map((item) => (
          <Box
            sx={{
              px: 3,
              py: 2,
              mb: 2,
              borderRadius: 1,
              backgroundColor: lightColors[Math.floor(Math.random() * lightColors.length)],
            }}
            key={item.id}
          >
            <Box>
              <Box sx={{ mb: 1, display: 'flex', alignItems: 'center' }}>
                <Avatar
                  sx={{
                    bgcolor: colors[Math.floor(Math.random() * colors.length)],
                    width: 36,
                    height: 36,
                  }}
                  alt={item.user.name}
                  src={item.user.photo_url}
                >
                  {item.user.name.charAt(0)}
                </Avatar>
                <Box sx={{ ml: 2 }}>
                  <Typography sx={{ fontSize: 15, fontWeight: 'bold' }}>{item.user.name}</Typography>
                  <Typography sx={{ color: 'text.secondary', fontSize: 12 }}>
                    {/* {moment(item.created_at).format('DD MMM YYYY - HH:mm')} */}
                    {moment(item.created_at).fromNow()}
                  </Typography>
                </Box>
              </Box>
              <Typography sx={{ fontSize: 16 }}>{item.body}</Typography>
            </Box>
          </Box>
        ))
      ) : (
        <Box>Tidak ada catatan</Box>
      )}

      {stateAddNotes.isLoading && <BoxSpinner height={80} />}

      {isVisibleInputNotes ? (
        <Box sx={{ textAlign: 'center' }}>
          <TextField
            onChange={(e) => setNotes(e.target.value)}
            name="notes"
            multiline
            rows={2}
            fullWidth
            placeholder="Input notes.."
            helperText="Tambahkan catatan ke jo ini"
          />
          <Button onClick={handleSubmitNotes} sx={{ mt: 1 }} variant="contained" disableElevation>
            Simpan Catatan
          </Button>
        </Box>
      ) : (
        <Box sx={{ textAlign: 'center' }}>
          <Button onClick={handleClickAddNote}>Tambah Catatan</Button>
        </Box>
      )}
    </Box>
  );
};

export default JobOrderNotes;
