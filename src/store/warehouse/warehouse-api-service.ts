// Axios instance.
import api from '@src/utils/http';

// Api endpoints.
import { WarehouseApiEndpoints } from './warehouse-api-endpoint';

// Interfaces.
import { AxiosResponse } from 'axios';
import {
  IMaterial,
  IMaterialBrand,
  IMaterialCategory,
  IMaterialType,
  RequestBodyMaterial,
} from '@/interfaces/warehouse';

export const WarehouseApiService = {
  getMaterialList: async (params?: Record<string, string | number>): Promise<AxiosResponse<IMaterial[]>> =>
    api.get(WarehouseApiEndpoints.material, { params }),

  create: async (body: RequestBodyMaterial): Promise<AxiosResponse<IMaterial>> =>
    api.post(WarehouseApiEndpoints.material, body),

  getMaterialBrandList: async (params?: Record<string, string | number>): Promise<AxiosResponse<IMaterialBrand[]>> =>
    api.get(WarehouseApiEndpoints.materialBrand, { params }),

  getMaterialCategoryList: async (
    params?: Record<string, string | number>,
  ): Promise<AxiosResponse<IMaterialCategory[]>> => api.get(WarehouseApiEndpoints.materialCategory, { params }),

  getMaterialTypeList: async (params?: Record<string, string | number>): Promise<AxiosResponse<IMaterialType[]>> =>
    api.get(WarehouseApiEndpoints.materialType, { params }),
};
