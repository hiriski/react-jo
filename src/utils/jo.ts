import { JobOrderProductionStatusIdConstants, PaymentStatusConstants } from '@/constants';
import { amber } from '@mui/material/colors';
import theme from '@src/config/theme';

export const jobOrder_createColorLabelProductionStatus = (statusId: number): string => {
  const DEFAULT_COLOR = theme.palette.primary.main;

  let color = DEFAULT_COLOR;

  switch (statusId) {
    case JobOrderProductionStatusIdConstants.Waiting:
      color = amber[700];
      break;
    case JobOrderProductionStatusIdConstants.InProgress:
      color = '#01b260';
      break;
    case JobOrderProductionStatusIdConstants.Done:
      color = '#0079ff';
      break;
    case JobOrderProductionStatusIdConstants.Hold:
      color = '#8e009a';
      break;
    case JobOrderProductionStatusIdConstants.Rejected:
      color = '#e01212';
      break;
    default:
      color = DEFAULT_COLOR;
  }

  return color;
};

export const createLabelJoPaymentStatus = (statusId: number): string => {
  const DEFAULT_COLOR = theme.palette.primary.main;

  if (!statusId) return DEFAULT_COLOR;

  switch (statusId) {
    case PaymentStatusConstants.Unpaid:
      return '#fd3c3c';
    case PaymentStatusConstants.DP:
      return '#f58517';
    case PaymentStatusConstants.Paid:
      return '#27a72c';
    default:
      return DEFAULT_COLOR;
  }
};
