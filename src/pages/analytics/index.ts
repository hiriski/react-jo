import { lazy } from 'react'

const AnalyticsPage = lazy(() => import('./analytics'))

export default AnalyticsPage
