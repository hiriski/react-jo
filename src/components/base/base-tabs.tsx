import { FC } from 'react';

// Mui components.
import Tabs, { TabsProps } from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { styled } from '@mui/material';

// Interfaces.
import { ITab } from '@/interfaces/tab';

interface StyledTabProps {
  buttonVariant?: 'contained' | 'default';
}

/** Styled tabs */
const StyledTabs = styled(Tabs)(({ theme }) => ({
  minHeight: 30,
}));

/** Styled tab */
const StyledTab = styled(Tab, { shouldForwardProp: (prop) => prop !== 'buttonVariant' })<StyledTabProps>(
  ({ theme, buttonVariant }) => ({
    textTransform: 'capitalize',
    minHeight: 30,

    ...(buttonVariant === 'contained' && {
      color: theme.palette.primary.main,
      backgroundColor: 'transparent',
      borderRadius: '6px 6px 0px 0px',
      '&.Mui-selected': {
        color: theme.palette.primary.contrastText,
        backgroundColor: theme.palette.primary.main,
      },
    }),
  }),
);

interface Props extends TabsProps, StyledTabProps {
  data: ITab[];
}

export const BaseTabs: FC<Props> = (props) => {
  const { data, buttonVariant, ...rest } = props;
  return (
    <StyledTabs {...rest}>
      {data.map((x) => (
        <StyledTab buttonVariant={buttonVariant} key={x.value} value={x.value} label={x.label} />
      ))}
    </StyledTabs>
  );
};

BaseTabs.defaultProps = {
  centered: false,
  buttonVariant: 'default',
};
