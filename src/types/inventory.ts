export type TBrandMaterialPaper = {
  id: number
  name: string
  available_sizes?: string
  material_counts: number
}

export type TCreateBrandMaterialPaper = {
  title: string
  name: string
  available_sizes: string
}

export type TUpdateBrandMaterialPaper = {
  id: number
  body: TCreateBrandMaterialPaper
}

export type TMaterialPaper = {
  id: number
  name: string
  brand?: TBrandMaterialPaper
  stock_length: number
  material_width: number
  length_per_roll?: number
  price?: number
  description?: string
  status: string
}

export type TCreateMaterialPaper = {
  name: string
  brand_id?: number | undefined | null
  stock_length: number
  material_width: number
  length_per_roll?: number
  price?: number
  description?: string
}

export type TUpdateMaterialPaper = {
  id: number
  body: TCreateMaterialPaper
}

export type TBrandWithMaterialPaper = TBrandMaterialPaper & {
  materials: Array<TMaterialPaper>
}
