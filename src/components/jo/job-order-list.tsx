import { Box, Grid } from '@mui/material'
import { useAppSelector } from '@src/store/hook'
import { TJo } from '@src/types/jo'
import { FC } from 'react'

import { JobOrderEmpty, JobOrderItem } from '.'

type Props = {
  items: Array<TJo>
  isFetching: boolean
}

const JobOrderList: FC<Props> = ({ items, isFetching }: Props) => (
  <Grid container spacing={0}>
    <Grid item xs={12} md={12}>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
        }}
      >
        <Grid container spacing={1}>
          {isFetching
            ? 'Empty'
            : items.map((item) => (
                <Grid key={String(item.id)} item xs={12}>
                  <JobOrderItem item={item} />
                </Grid>
              ))}
        </Grid>
      </Box>
      {!isFetching && items.length === 0 && <JobOrderEmpty text="Empty :(" />}
    </Grid>
  </Grid>
)

export default JobOrderList
