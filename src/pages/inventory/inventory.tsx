import { Box, Button } from '@mui/material'
import { styled } from '@mui/material/styles'
import { BrandMaterialPaperList, DrawerDetailInventory, MaterialPaperList } from '@src/components/inventory'
import { MainLayout } from '@src/components/layouts'
import { PageTitle } from '@src/components/ui'
import { useAppSelector } from '@src/store/hook'
import {
  fetchListBrandMaterialPaper,
  fetchListMaterialPaper,
  setDialogAddEditInventory,
} from '@src/store/inventory/actions'
import { DRAWER_DETAIL_INVENTORY_WIDTH } from '@src/utils/constants'
import { FC, useEffect } from 'react'
import { useDispatch } from 'react-redux'

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })<{
  open?: boolean
}>(({ theme, open }) => ({
  flexGrow: 1,
  backgroundColor: theme.palette.background.paper,
  transition: theme.transitions.create('margin', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  // marginLeft: `-${SIDEBAR_DRAWER_WIDTH}px`,
  ...(open && {
    marginRight: `${DRAWER_DETAIL_INVENTORY_WIDTH}px`,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}))

const InventoryPage: FC = () => {
  const dispatch = useDispatch()
  const { listBrandMaterialPaper, listMaterialPaper, drawerDetailInventory } = useAppSelector(
    (state) => state.inventory,
  )

  useEffect(() => {
    dispatch(fetchListBrandMaterialPaper({ hello: 'world' }))
    dispatch(fetchListMaterialPaper({ hello: 'world' }))
  }, [])

  const handleAddMaterial = (): void => {
    dispatch(
      setDialogAddEditInventory({
        open: true,
        type: 'material_paper',
        id: null,
      }),
    )
  }

  return (
    <MainLayout>
      <Main open={drawerDetailInventory.open}>
        <DrawerDetailInventory />
        <Box
          sx={{
            display: 'flex',
            marginBottom: 3,
            flexDirection: 'column',
          }}
        >
          <BrandMaterialPaperList items={listBrandMaterialPaper} />
          <MaterialPaperList items={listMaterialPaper} />
          <Button onClick={handleAddMaterial}>Add inventory</Button>
        </Box>
      </Main>
    </MainLayout>
  )
}

export default InventoryPage
