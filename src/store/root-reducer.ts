import { combineReducers } from 'redux';

import alertReducer, { AlertState } from './alert/reducer';
import analyticsReducer, { AnalyticsState } from './analytics/analytics-reducer';
import appReducer, { IAppState } from './app/app-reducer';
import authReducer, { AuthState } from './auth/reducer';
import commonReducer, { CommonState } from './common/reducer';
import customerReducer, { CustomerState } from './customer/customer-reducer';
import dashboardReducer, { DashboardState } from './dashboard/dashboard-reducer';
import inventoryReducer, { InventoryState } from './inventory/reducer';
import invoiceGeneratorReducer, { InvoiceGeneratorState } from './invoice-generator/reducer';
import jobOrderReducer, { JobOrderState } from './job-order/job-order-reducer';
import machineReducer, { MachineState } from './machine/machine-reducer';
import masterDataReducer, { MasterDataState } from './master-data/master-data-reducer';
import paymentMethodReducer, { PaymentMethodState } from './payment-method/reducer';
import paymentStatusReducer, { PaymentStatusState } from './payment-status/reducer';
import productReducer, { ProductState } from './product/product-reducer';
import productionStatusReducer, { ProductionStatusState } from './production-status/reducer';
import roleReducer, { RoleState } from './role/reducer';
import userReducer, { UserState } from './user/reducer';
import warehouseReducer, { WarehouseState } from './warehouse/warehouse-reducer';

export interface RootState {
  app: IAppState;
  common: CommonState;
  alert: AlertState;
  auth: AuthState;
  inventory: InventoryState;
  role: RoleState;
  masterData: MasterDataState;
  paymentStatus: PaymentStatusState;
  productionStatus: ProductionStatusState;
  customer: CustomerState;
  user: UserState;
  invoiceGenerator: InvoiceGeneratorState;
  paymentMethod: PaymentMethodState;
  product: ProductState;
  jobOrder: JobOrderState;
  analytics: AnalyticsState;
  warehouse: WarehouseState;
  machine: MachineState;
  dashboard: DashboardState;
}

export type AppState = RootState;

export default combineReducers<RootState>({
  app: appReducer,
  common: commonReducer,
  alert: alertReducer,
  auth: authReducer,
  inventory: inventoryReducer,
  role: roleReducer,
  masterData: masterDataReducer,
  paymentStatus: paymentStatusReducer,
  productionStatus: productionStatusReducer,
  customer: customerReducer,
  user: userReducer,
  invoiceGenerator: invoiceGeneratorReducer,
  paymentMethod: paymentMethodReducer,
  product: productReducer,
  jobOrder: jobOrderReducer,
  analytics: analyticsReducer,
  warehouse: warehouseReducer,
  machine: machineReducer,
  dashboard: dashboardReducer,
});
