import { ICustomer } from '@/interfaces/customer';
import type { LaravelPaginationLinks, LaravelPaginationMeta } from './laravel-pagination';

export type TCustomer = ICustomer;

export type TResponseListCustomer = {
  data: Array<TCustomer>;
  meta?: LaravelPaginationMeta;
  links: LaravelPaginationLinks;
};

export type TCreateCustomer = Omit<TCustomer, 'id'>;
