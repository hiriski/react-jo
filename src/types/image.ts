export type TImage = {
  id: number
  image_xs: string
  image_sm: string
  image_md: string
  image_lg: string
  image_original: string
}
