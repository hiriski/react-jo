// Pusher
import Pusher from 'pusher-js';

const pusher = new Pusher(process.env.PUSHER_APP_KEY, {
  cluster: process.env.PUSHER_APP_CLUSTER,
});

// Enable pusher logging - don't include this in production
Pusher.logToConsole = IS_DEV;

export default pusher;
