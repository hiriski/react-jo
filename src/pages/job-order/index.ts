import { lazy } from 'react';

export const JobOrderPage = lazy(() => import('./job-order-page'));
export const AddEditJobOrderPage = lazy(() => import('./add-edit-job-order'));
