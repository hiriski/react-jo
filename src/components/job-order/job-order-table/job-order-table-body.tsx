import { alpha, Button, IconButton, ListItemIcon, Menu, MenuItem, Stack } from '@mui/material';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Popover from '@mui/material/Popover';
import Checkbox from '@mui/material/Checkbox';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import { setDialogDetaiLCustomer } from '@src/store/customer/customer-actions';
import {
  jobOrder_finishProduction,
  jobOrder_setDialogDelete,
  jobOrder_setDialogQr,
  jobOrder_setDialogStartProduction,
  setDialogDetailJo,
} from '@/store/job-order/job-order-actions';
import { colors } from '@src/utils/colors';
import { DB_FORMAT_TIMESTAMPS, ROUTES } from '@src/utils/constants';
import { convertToRupiah } from '@src/utils/currency';
import { getInitialsName } from '@src/utils/misc';
import moment from 'moment';
import { Dispatch, FC, MouseEvent, SetStateAction, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';

import { UUID } from '@/interfaces/common';
import { JobOrderLabelPaymentStatus } from '../job-order-label-payment-status';
import { JobOrderLabelProductionStatus } from '../job-order-label-production-status';
import { IJobOrder } from '@/interfaces/job-order';

// Mui icons
import MoreVertIcon from '@mui/icons-material/MoreVert';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import QrCodeScannerIcon from '@mui/icons-material/QrCodeScanner';
import CallIcon from '@mui/icons-material/Call';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import StartIcon from '@mui/icons-material/Start';
import DoneAllIcon from '@mui/icons-material/DoneAll';
import DoNotTouchIcon from '@mui/icons-material/DoNotTouch';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import { useAppSelector } from '@/store';
import { auth_rootSelector } from '@/store/auth/auth-selectors';
import { JobOrderProductionStatusIdConstants, RoleIdConstants } from '@/constants';
import { useNavigate } from 'react-router';
import { isProductionTeam } from '@/utils/role';
import Swall from 'sweetalert2';
import { green, orange, red } from '@mui/material/colors';
import { showAlertFeatureUnavailable } from '@/lib/alerts';

import NoPhotoIcon from '@/assets/images/icons/photo.png';
import PdfIcon from '@/assets/images/icons/pdf-64.png';
import PdfIconDisabled from '@/assets/images/icons/pdf-64-disabled.png';

type Order = 'asc' | 'desc';

function descendingComparator<T>(a: T, b: T, orderBy: keyof T): number {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key,
): (a: { [key in Key]: number | string }, b: { [key in Key]: number | string }) => number {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// This method is created for cross-browser compatibility, if you don't
// need to support IE11, you can use Array.prototype.sort() directly
// function stableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number): any {
//   const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
//   stabilizedThis.sort((a, b) => {
//     const order = comparator(a[0], b[0]);
//     if (order !== 0) {
//       return order;
//     }
//     return a[1] - b[1];
//   });
//   return stabilizedThis.map((el) => el[0]);
// }

interface JoTableBodyProps {
  rows: Array<IJobOrder>;
  rowsPerPage: number;
  order: Order;
  page: number;
  orderBy: string;
  selected: Array<UUID>;
  setSelected: Dispatch<SetStateAction<Array<UUID>>>;
}

export const JobOrderTableBody: FC<JoTableBodyProps> = ({
  rows,
  rowsPerPage,
  order,
  page,
  orderBy,
  selected,
  setSelected,
}: JoTableBodyProps) => {
  const dispatch = useDispatch();

  const navigate = useNavigate();

  // Selectors
  const user = useAppSelector((state) => auth_rootSelector(state).authenticatedUser);

  // Action menu
  const [anchorElActionMenu, setAnchorElActionMenu] = useState<null | HTMLElement>(null);
  const [anchorElBarcode, setAnchorElBarcode] = useState<null | HTMLElement>(null);
  const [jobOrderQr, setJobOrderQr] = useState<string | null>(null);
  const [selectedMenuJobOrder, setSelectedMenuJobOrder] = useState<IJobOrder | null>(null);

  const isCanSeeCustomerInfo = useMemo(() => {
    if (user.role_id === RoleIdConstants.ProductionTeam) return false;
    return true;
  }, [user]);

  // const hasInvoices = useMemo(() => {
  //   if (row.invoice.length > 0) {
  //     return true;
  //   }
  //   return false;
  // }, []);

  const handleCheck = (event: MouseEvent<unknown>, name: UUID): void => {
    null;
    // const selectedIndex = selected.indexOf(name);
    // let newSelected: Array<number> = [];
    // if (selectedIndex === -1) {
    //   newSelected = newSelected.concat(selected, name);
    // } else if (selectedIndex === 0) {
    //   newSelected = newSelected.concat(selected.slice(1));
    // } else if (selectedIndex === selected.length - 1) {
    //   newSelected = newSelected.concat(selected.slice(0, -1));
    // } else if (selectedIndex > 0) {
    //   newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
    // }
    // setSelected(newSelected);
  };

  const isSelected = (name: UUID): boolean => selected.indexOf(name) !== -1;

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleClickDetail = (jobOrderId: UUID): void => {
    dispatch(setDialogDetailJo(true, jobOrderId));
  };

  const handleClickDetailCustomer = (customerId: string): void => {
    dispatch(setDialogDetaiLCustomer(true, customerId));
  };

  const handleClickActionMenu = (event: MouseEvent<HTMLElement>, jobOrder: IJobOrder): void => {
    setAnchorElActionMenu(event.currentTarget);
    setSelectedMenuJobOrder(jobOrder);
  };

  const handleClickQr = (event: MouseEvent<HTMLElement>, qr: string): void => {
    setJobOrderQr(qr);
    setAnchorElBarcode(event.currentTarget);
    // if (qr) dispatch(jobOrder_setDialogQr(true, qr));
  };

  const handleClickDelete = (): void => {
    if (selectedMenuJobOrder) dispatch(jobOrder_setDialogDelete(true, selectedMenuJobOrder.id));
    setAnchorElActionMenu(null);
  };

  const handleClickEdit = (): void => {
    navigate(`${ROUTES.EDIT_JOB_ORDER}/${selectedMenuJobOrder.id}`);
    setAnchorElActionMenu(null);
  };

  const handleClickStartProduction = (): void => {
    dispatch(jobOrder_setDialogStartProduction(true, selectedMenuJobOrder.id));
    setAnchorElActionMenu(null);
  };

  const handleClickFinishProduction = (): void => {
    // dispatch(jobOrder_setDialogStartProduction(true, selectedMenuJobOrder.id));
    Swall.fire({
      title: 'Yakin ingin menyelesaikan status produksi ?',
      icon: 'warning',
      text: 'Job Order yang telah di selesaikan tidak dapat dikembalikan',
      showCancelButton: true,
      confirmButtonText: 'Ya, Selesaikan',
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(jobOrder_finishProduction({ job_order_id: selectedMenuJobOrder.id }));
      }
    });
    setAnchorElActionMenu(null);
  };

  const handleClickHold = (): void => {
    showAlertFeatureUnavailable();
    setAnchorElActionMenu(null);
  };

  const handleClickReject = (): void => {
    showAlertFeatureUnavailable();
    setAnchorElActionMenu(null);
  };

  return (
    <TableBody>
      {rows.map((row, index) => {
        const isItemSelected = isSelected(row.id);
        const labelId = `enhanced-table-checkbox-${index}`;
        const hasInvoices = row.invoices.length > 0;
        return (
          <TableRow
            hover
            role="checkbox"
            aria-checked={isItemSelected}
            tabIndex={-1}
            key={row.id}
            selected={isItemSelected}
            sx={{
              '& *': {
                lineHeight: '1.4 !important',
              },
              '& .MuiTableCell-sizeMedium': {
                padding: '12px 6px !important',
                borderBottom: (theme) => `1px solid ${alpha(theme.palette.text.primary, 0.065)}`,
              },
              '& .MuiTableCell-paddingCheckbox': {
                padding: '12px 2px !important',
              },
            }}
          >
            <TableCell padding="checkbox" onClick={(event) => handleCheck(event, row.id)}>
              <Checkbox
                size="small"
                color="primary"
                checked={isItemSelected}
                inputProps={{
                  'aria-labelledby': labelId,
                }}
              />
            </TableCell>

            <TableCell component="th" id={labelId} scope="row" padding="none">
              <Box sx={{ display: 'flex', alingItems: 'center' }} onClick={() => handleClickDetail(row.id)}>
                {/* <IconButton edge="start" size="small" onClick={(e) => handleClickQr(e, row.qr)} sx={{ mr: 1 }}>
                  <QrCodeIcon />
                </IconButton> */}

                {/* Job Order images */}
                <Box sx={{ width: 34, height: 34, overflow: 'hidden', borderRadius: 1, mr: 2 }}>
                  <Box
                    sx={{ width: 34, height: 'auto' }}
                    component="img"
                    src={row.images.length ? row.images[0].image_xs : NoPhotoIcon}
                    alt={row.title}
                  />
                </Box>

                <Box sx={{ maxWidth: 120 }}>
                  <Typography component="p" variant="body2" sx={{ color: 'primary.main', fontWeight: 'bold' }}>
                    {row.order_number}
                  </Typography>
                  <Typography
                    component="p"
                    variant="subtitle2"
                    sx={{ overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }}
                  >
                    {row.title}
                  </Typography>
                </Box>
              </Box>
            </TableCell>

            {/* Created at */}
            <TableCell align="left" onClick={() => handleClickDetail(row.id)}>
              <Typography variant="subtitle1" sx={{ fontWeight: 700 }}>
                {moment(row.due_date, DB_FORMAT_TIMESTAMPS).format('DD MMM YYYY')}
              </Typography>
            </TableCell>

            {/* Due date */}
            <TableCell align="left" onClick={() => handleClickDetail(row.id)}>
              <Tooltip title={row.due_date}>
                <>
                  <Typography variant="subtitle1" sx={{ fontWeight: 700 }}>
                    {moment(row.due_date).format('DD MMM YYYY')}
                  </Typography>
                  <Typography variant="subtitle2" color="text.secondary">
                    {moment(row.due_date).fromNow()}
                  </Typography>
                </>
              </Tooltip>
            </TableCell>

            <TableCell align="left">
              <Typography variant="subtitle1">{row.category.name}</Typography>
            </TableCell>

            {/* Customer */}
            <TableCell align="left">
              {row.customer ? (
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                    maxWidth: 140,
                    ...(!isCanSeeCustomerInfo && {
                      cursor: 'not-allowed',
                    }),
                  }}
                  onClick={() => (isCanSeeCustomerInfo ? handleClickDetailCustomer(row.customer.id) : () => null)}
                >
                  <Box sx={{ mr: 1.4 }}>
                    <Avatar
                      src={row.customer.photo_url ?? null}
                      sx={{
                        backgroundColor:
                          row.customer.avatar_text_color ?? colors[Math.floor(Math.random() * colors.length)],
                        color: 'primary.contrastText',
                        width: 28,
                        height: 28,
                        fontSize: 10,
                        letterSpacing: 1,
                      }}
                      alt={row.customer.name}
                    >
                      {getInitialsName(row.customer.name, true)}
                    </Avatar>
                  </Box>
                  <Box sx={{ maxWidth: 110 }}>
                    <Typography
                      variant="subtitle1"
                      sx={{ overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis', mb: 0.4 }}
                    >
                      {row.customer.name}
                    </Typography>
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                      <CallIcon sx={{ fontSize: 12, mr: 1, color: 'text.disabled' }} />
                      <Typography
                        variant="subtitle2"
                        sx={{ overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }}
                      >
                        {row.customer.phone_number}
                      </Typography>
                    </Box>
                  </Box>
                </Box>
              ) : (
                '-'
              )}
            </TableCell>
            <TableCell align="left">
              <Stack direction="row" alignItems="center" sx={{ width: '100%' }}>
                <Tooltip title={hasInvoices ? 'Invoice' : 'Tidak ada invoice'}>
                  <IconButton disabled={!hasInvoices} size="small" sx={{ mr: 1.4 }}>
                    <Box
                      component="img"
                      src={hasInvoices ? PdfIcon : PdfIconDisabled}
                      alt="Pdf icon"
                      sx={{ width: 18, height: 'auto' }}
                    />
                  </IconButton>
                </Tooltip>
                <JobOrderLabelPaymentStatus
                  status={row.transaction.payment_status.name}
                  statusId={row.transaction.payment_status.id}
                  description={row.transaction.payment_status.description}
                  sx={{ flex: 1 }}
                />
              </Stack>
            </TableCell>
            {/* <TableCell align="left">{convertToRupiah(row.total)}</TableCell> */}
            <TableCell align="left">
              <JobOrderLabelProductionStatus status={row.production_status} sx={{ minWidth: 110 }} />
            </TableCell>
            <TableCell align="center">
              <IconButton size="small" onClick={(e) => handleClickActionMenu(e, row)}>
                <MoreVertIcon />
              </IconButton>
            </TableCell>
          </TableRow>
        );
      })}

      {/* Actions menu */}
      <Menu
        id={`job-order-action-menu`}
        anchorEl={anchorElActionMenu}
        open={Boolean(anchorElActionMenu)}
        onClose={() => setAnchorElActionMenu(null)}
        PaperProps={{
          elevation: 4,
        }}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <MenuItem
          onClick={() => {
            handleClickDetail(selectedMenuJobOrder.id);
            setAnchorElActionMenu(null);
          }}
        >
          <ListItemIcon sx={{ minWidth: '26px !important' }}>
            <RemoveRedEyeIcon sx={{ fontSize: 18 }} />
          </ListItemIcon>
          <Typography variant="subtitle1">Lihat Detail</Typography>
        </MenuItem>
        {!isProductionTeam(user) && (
          <MenuItem onClick={handleClickEdit}>
            <ListItemIcon sx={{ minWidth: '26px !important' }}>
              <EditIcon sx={{ fontSize: 18 }} />
            </ListItemIcon>
            <Typography variant="subtitle1">Edit</Typography>
          </MenuItem>
        )}
        <MenuItem onClick={(e) => handleClickQr(e, selectedMenuJobOrder.qr)}>
          <ListItemIcon sx={{ minWidth: '26px !important' }}>
            <QrCodeScannerIcon sx={{ fontSize: 18 }} />
          </ListItemIcon>
          <Typography variant="subtitle1">Scan QR</Typography>
        </MenuItem>
        {!isProductionTeam(user) && (
          <MenuItem onClick={handleClickDelete}>
            <ListItemIcon sx={{ minWidth: '26px !important' }}>
              <DeleteOutlineIcon sx={{ fontSize: 18, color: red[600] }} />
            </ListItemIcon>
            <Typography variant="subtitle1" sx={{ color: red[600] }}>
              Hapus JO
            </Typography>
          </MenuItem>
        )}

        {isProductionTeam(user) && selectedMenuJobOrder && (
          <>
            {selectedMenuJobOrder.production_status.id === JobOrderProductionStatusIdConstants.Waiting && (
              <>
                <MenuItem onClick={handleClickStartProduction}>
                  <ListItemIcon sx={{ minWidth: '26px !important' }}>
                    <StartIcon sx={{ fontSize: 18, color: 'primary.main' }} />
                  </ListItemIcon>
                  <Typography variant="subtitle1" sx={{ color: 'primary.main' }}>
                    Mulai Produksi
                  </Typography>
                </MenuItem>
                <MenuItem onClick={handleClickHold}>
                  <ListItemIcon sx={{ minWidth: '26px !important' }}>
                    <DoNotTouchIcon sx={{ fontSize: 18, color: orange[600] }} />
                  </ListItemIcon>
                  <Typography variant="subtitle1" sx={{ color: orange[600] }}>
                    Tahan
                  </Typography>
                </MenuItem>
                <MenuItem onClick={handleClickReject}>
                  <ListItemIcon sx={{ minWidth: '26px !important' }}>
                    <HighlightOffIcon sx={{ fontSize: 18, color: red[600] }} />
                  </ListItemIcon>
                  <Typography variant="subtitle1" sx={{ color: red[600] }}>
                    Tolak
                  </Typography>
                </MenuItem>
              </>
            )}
            {selectedMenuJobOrder.production_status.id === JobOrderProductionStatusIdConstants.InProgress && (
              <MenuItem onClick={handleClickFinishProduction}>
                <ListItemIcon sx={{ minWidth: '26px !important' }}>
                  <DoneAllIcon sx={{ fontSize: 18, color: green[600] }} />
                </ListItemIcon>
                <Typography variant="subtitle1" sx={{ color: green[600] }}>
                  Selesaikan Produksi
                </Typography>
              </MenuItem>
            )}
          </>
        )}
      </Menu>

      {/* Popover Qr */}
      <Popover
        BackdropProps={{
          sx: {
            backgroundColor: 'rgb(49 70 99 / 25%)',
          },
        }}
        open={Boolean(anchorElBarcode)}
        onClose={() => setAnchorElBarcode(null)}
        anchorEl={anchorElBarcode}
        transformOrigin={{
          vertical: 'center',
          horizontal: 'left',
        }}
      >
        <Box sx={{ boxShadow: 5, p: 4, position: 'relative', backgroundColor: 'background.paper' }}>
          <Box component="img" src={jobOrderQr} sx={{ height: 240, width: 240 }} />.
        </Box>
      </Popover>

      {/* {emptyRows > 0 && (
        <TableRow
          style={{  
            height: 53 * emptyRows,
          }}
        >
          <TableCell colSpan={6} />
        </TableRow>
      )} */}
    </TableBody>
  );
};
