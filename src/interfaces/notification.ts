export interface INotification<T> {
  title: string;
  summary?: string;
  priority: number;
  data: T;
}
