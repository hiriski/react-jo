// Enum
import { UUID } from '@/interfaces/common';
import { IMaterial, IMaterialBrand, IMaterialCategory, RequestBodyMaterial } from '@/interfaces/warehouse';
import { WAREHOUSE_ACTION_TYPES as ActionTypes } from './warehouse-action-types.enum';

// Actions definitions.
interface ISetDialogAddEditMaterial {
  type: typeof ActionTypes.SET_DIALOG_ADD_EDIT_MATERIAL;
  payload: {
    open: boolean;
    id?: number;
  };
}

export interface IFetchMaterialListRequested {
  type: typeof ActionTypes.FETCH_MATERIAL_LIST_REQUESTED;
  payload?: Record<string, string | number>;
}

interface IFetchMaterialListLoading {
  type: typeof ActionTypes.FETCH_MATERIAL_LIST_LOADING;
}

interface IFetchMaterialListFailure {
  type: typeof ActionTypes.FETCH_MATERIAL_LIST_FAILURE;
}

interface IFetchMaterialListSuccess {
  type: typeof ActionTypes.FETCH_MATERIAL_LIST_SUCCESS;
  payload: Array<IMaterial>;
}

export interface ICreateMaterialRequested {
  type: typeof ActionTypes.CREATE_MATERIAL_REQUESTED;
  payload: RequestBodyMaterial;
}

interface ICreateMaterialLoading {
  type: typeof ActionTypes.CREATE_MATERIAL_LOADING;
}

interface ICreateMaterialFailure {
  type: typeof ActionTypes.CREATE_MATERIAL_FAILURE;
  payload?: Array<string>;
}

interface ICreateMaterialSuccess {
  type: typeof ActionTypes.CREATE_MATERIAL_SUCCESS;
  payload: IMaterial;
}
interface IResetCreateMaterialState {
  type: typeof ActionTypes.RESET_CREATE_MATERIAL_STATE;
}

export interface IFetchMaterialBrandListRequested {
  type: typeof ActionTypes.FETCH_MATERIAL_BRAND_LIST_REQUESTED;
  payload?: Record<string, string | number>;
}

interface IFetchMaterialBrandListLoading {
  type: typeof ActionTypes.FETCH_MATERIAL_BRAND_LIST_LOADING;
}

interface IFetchMaterialBrandListFailure {
  type: typeof ActionTypes.FETCH_MATERIAL_BRAND_LIST_FAILURE;
}

interface IFetchMaterialBrandListSuccess {
  type: typeof ActionTypes.FETCH_MATERIAL_BRAND_LIST_SUCCESS;
  payload: Array<IMaterialBrand>;
}

export interface IFetchMaterialCategoryListRequested {
  type: typeof ActionTypes.FETCH_MATERIAL_CATEGORY_LIST_REQUESTED;
  payload?: Record<string, string | number>;
}

interface IFetchMaterialCategoryListLoading {
  type: typeof ActionTypes.FETCH_MATERIAL_CATEGORY_LIST_LOADING;
}

interface IFetchMaterialCategoryListFailure {
  type: typeof ActionTypes.FETCH_MATERIAL_CATEGORY_LIST_FAILURE;
}

interface IFetchMaterialCategoryListSuccess {
  type: typeof ActionTypes.FETCH_MATERIAL_CATEGORY_LIST_SUCCESS;
  payload: Array<IMaterialCategory>;
}

interface ISetDialogAddStockMaterial {
  type: typeof ActionTypes.SET_DIALOG_ADD_STOCK_MATERIAL;
  payload: { open: boolean; material: IMaterial | null };
}

// Union action types
export type WarehouseActions =
  | ISetDialogAddEditMaterial
  | IFetchMaterialListRequested
  | IFetchMaterialListLoading
  | IFetchMaterialListFailure
  | IFetchMaterialListSuccess
  | ICreateMaterialRequested
  | ICreateMaterialLoading
  | ICreateMaterialFailure
  | ICreateMaterialSuccess
  | IResetCreateMaterialState
  | IFetchMaterialBrandListRequested
  | IFetchMaterialBrandListLoading
  | IFetchMaterialBrandListFailure
  | IFetchMaterialBrandListSuccess
  | IFetchMaterialCategoryListRequested
  | IFetchMaterialCategoryListLoading
  | IFetchMaterialCategoryListFailure
  | IFetchMaterialCategoryListSuccess
  | ISetDialogAddStockMaterial;

// Actions creator.

export const warehouse_setDialogAddEditMaterial = (open: boolean, id?: number): ISetDialogAddEditMaterial => ({
  type: ActionTypes.SET_DIALOG_ADD_EDIT_MATERIAL,
  payload: { open, id },
});

export const warehouse_fetchMaterialList = (
  payload?: Record<string, string | number>,
): IFetchMaterialListRequested => ({
  type: ActionTypes.FETCH_MATERIAL_LIST_REQUESTED,
  payload,
});

export const warehouse_fetchMaterialListLoading = (): IFetchMaterialListLoading => ({
  type: ActionTypes.FETCH_MATERIAL_LIST_LOADING,
});

export const warehouse_fetchMaterialListFailure = (): IFetchMaterialListFailure => ({
  type: ActionTypes.FETCH_MATERIAL_LIST_FAILURE,
});

export const warehouse_fetchMaterialListSuccess = (payload: Array<IMaterial>): IFetchMaterialListSuccess => ({
  type: ActionTypes.FETCH_MATERIAL_LIST_SUCCESS,
  payload,
});

export const warehouse_createMaterial = (payload: RequestBodyMaterial): ICreateMaterialRequested => ({
  type: ActionTypes.CREATE_MATERIAL_REQUESTED,
  payload,
});

export const warehouse_createMaterialLoading = (): ICreateMaterialLoading => ({
  type: ActionTypes.CREATE_MATERIAL_LOADING,
});

export const warehouse_createMaterialFailure = (payload?: Array<string>): ICreateMaterialFailure => ({
  type: ActionTypes.CREATE_MATERIAL_FAILURE,
  payload,
});

export const warehouse_createMaterialSuccess = (payload: IMaterial): ICreateMaterialSuccess => ({
  type: ActionTypes.CREATE_MATERIAL_SUCCESS,
  payload,
});

export const warehouse_resetCreateMaterialState = (): IResetCreateMaterialState => ({
  type: ActionTypes.RESET_CREATE_MATERIAL_STATE,
});

export const warehouse_fetchMaterialBrandList = (
  payload?: Record<string, string | number>,
): IFetchMaterialBrandListRequested => ({
  type: ActionTypes.FETCH_MATERIAL_BRAND_LIST_REQUESTED,
  payload,
});

export const warehouse_fetchMaterialBrandListLoading = (): IFetchMaterialBrandListLoading => ({
  type: ActionTypes.FETCH_MATERIAL_BRAND_LIST_LOADING,
});

export const warehouse_fetchMaterialBrandListFailure = (): IFetchMaterialBrandListFailure => ({
  type: ActionTypes.FETCH_MATERIAL_BRAND_LIST_FAILURE,
});

export const warehouse_fetchMaterialBrandListSuccess = (
  payload: Array<IMaterialBrand>,
): IFetchMaterialBrandListSuccess => ({
  type: ActionTypes.FETCH_MATERIAL_BRAND_LIST_SUCCESS,
  payload,
});

export const warehouse_fetchMaterialCategoryList = (
  payload?: Record<string, string | number>,
): IFetchMaterialCategoryListRequested => ({
  type: ActionTypes.FETCH_MATERIAL_CATEGORY_LIST_REQUESTED,
  payload,
});

export const warehouse_fetchMaterialCategoryListLoading = (): IFetchMaterialCategoryListLoading => ({
  type: ActionTypes.FETCH_MATERIAL_CATEGORY_LIST_LOADING,
});

export const warehouse_fetchMaterialCategoryListFailure = (): IFetchMaterialCategoryListFailure => ({
  type: ActionTypes.FETCH_MATERIAL_CATEGORY_LIST_FAILURE,
});

export const warehouse_fetchMaterialCategoryListSuccess = (
  payload: Array<IMaterialCategory>,
): IFetchMaterialCategoryListSuccess => ({
  type: ActionTypes.FETCH_MATERIAL_CATEGORY_LIST_SUCCESS,
  payload,
});

// prettier-ignore
export const warehouse_setDialogAddStockMaterial = (open: boolean, material: IMaterial | null): ISetDialogAddStockMaterial => ({
  type: ActionTypes.SET_DIALOG_ADD_STOCK_MATERIAL,
  payload: { open, material },
});
