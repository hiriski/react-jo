import type { Effect, SagaIterator } from '@redux-saga/types'
import { httpResponseOK } from '@src/utils/http'
import { SagaReturnType, call, put, takeEvery } from 'redux-saga/effects'

import {
  fetchingListProductionStatusFailure,
  fetchingListProductionStatusLoading,
  fetchingListProductionStatusSuccess,
} from './actions'
import * as ActionTypes from './constants'
import ProductionStatusAPI from './service'

type APIResponseProductionStatusList = SagaReturnType<
  typeof ProductionStatusAPI.findAll
>

function* fetchListProductionStatus(): SagaIterator {
  yield put(fetchingListProductionStatusLoading())
  try {
    const response: APIResponseProductionStatusList = yield call(
      ProductionStatusAPI.findAll
    )
    if (httpResponseOK(response.status)) {
      const { data } = response
      yield put(fetchingListProductionStatusSuccess(data))
    }
  } catch (e) {
    yield put(fetchingListProductionStatusFailure())
  }
}

export default function* productionStatusSaga(): SagaIterator {
  yield takeEvery(
    ActionTypes.FETCHING_LIST_PRODUCTION_STATUS_REQUESTED,
    fetchListProductionStatus
  )
}
