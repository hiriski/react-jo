import { BaseAnalyticsWidget } from '@/components/base';
import { Grid, Box } from '@mui/material';
import { green, blue, indigo, orange } from '@mui/material/colors';
import { FC } from 'react';
import { AlbumsOutline, FolderOpenOutline, SyncOutline, TimeOutline } from 'react-ionicons';

export const OverviewTodoContainer: FC = () => {
  return (
    <Box sx={{ pb: 3 }}>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6} md={3}>
          <BaseAnalyticsWidget
            icon={<AlbumsOutline />}
            value={43}
            title="JO Hari Ini"
            color={indigo[500]}
            subtitle="Toal Jo hari ini"
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <BaseAnalyticsWidget
            value={12}
            icon={<FolderOpenOutline />}
            title="Selesai"
            color={green[500]}
            subtitle="Toal jo hari selesai hari ini"
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <BaseAnalyticsWidget
            value={11}
            icon={<SyncOutline rotate />}
            title="In Progress"
            color={blue[500]}
            subtitle="Toal jo in progress"
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <BaseAnalyticsWidget
            value={5}
            icon={<TimeOutline rotate />}
            title="Waiting List"
            color={orange[400]}
            subtitle="Toal jo in progress"
          />
        </Grid>
      </Grid>
    </Box>
  );
};
