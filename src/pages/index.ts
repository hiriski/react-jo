export { default as HomePage } from './home';
export { default as LoginPage } from './auth/login';
export { default as PageNotFound } from './errors/404';
export { default as OverviewPage } from './overview';
export { default as AnalyticsPage } from './analytics';
export { default as InventoryPage } from './inventory';
export { default as UserPage } from './user';
export { default as RolePage } from './role';
export { default as InvoicePage } from './invoice';
export { default as SettingPage } from './setting';
export { default as AccountPage } from './account';
export { default as InvoiceGeneratorPage } from './invoice-generator';
export { default as SupportCenterPage } from './support-center';
export { default as WarehousePage } from './warehouse';
export { default as AddEditMaterialPage } from './warehouse/add-edit-material-page';

export * from './job-order';
