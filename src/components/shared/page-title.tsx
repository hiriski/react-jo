import { FC, ReactElement, ReactNode } from 'react';
import { Box, Typography, SxProps } from '@mui/material';

type Props = {
  title: string;
  subtitle?: string;
  rightContent?: ReactNode;
  sx?: SxProps;
};

export const PageTitle: FC<Props> = ({ title, subtitle, rightContent, sx }: Props): ReactElement => (
  <Box
    sx={{
      mb: 2,
      display: 'flex',
      flexDirection: { xs: 'column', sm: 'row' },
      alignItems: 'center',
      justifyContent: 'space-between',
      ...sx,
    }}
  >
    <Box>
      <Typography sx={{ mb: 0.5, color: 'inherit' }} component="h2" variant="h3">
        {title}
      </Typography>
      {subtitle && <Typography sx={{ color: 'inherit' }}>{subtitle}</Typography>}
    </Box>
    {rightContent && <Box>{rightContent}</Box>}
  </Box>
);

PageTitle.defaultProps = {
  sx: {},
  subtitle: undefined,
};
