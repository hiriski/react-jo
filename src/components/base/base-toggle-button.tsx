import { FC } from 'react';

import {
  ToggleButtonGroup,
  ToggleButton,
  styled,
  alpha,
  ToggleButtonProps,
  ToggleButtonGroupProps,
} from '@mui/material';
import { IDropdown } from '@/interfaces/dropdown';

// Interfaces

const StyledToggleButtonGroup = styled(ToggleButtonGroup)(({ theme }) => ({
  display: 'flex',
}));

const StyledToggleButton = styled(ToggleButton)(({ theme }) => ({
  color: theme.palette.primary.main,
  textTransform: 'capitalize',
  backgroundColor: 'transparent',
  border: 'none !important',
  padding: theme.spacing(0.4, 1.4),
  borderRadius: Number(theme.shape.borderRadius) + 'px !important',
  margin: theme.spacing(0, 0.4),
  fontSize: '0.85rem',
  '&.Mui-selected': {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    boxShadow: 'var(--shadow-small)',
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.primary.contrastText,
    },
  },
  '&:hover': {
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.primary.main,
  },
}));

interface Props extends ToggleButtonGroupProps {
  data: Array<IDropdown<number | string>>;
}

export const BaseToggleButton: FC<Props> = (props) => {
  const { data, ...rest } = props;
  return (
    <StyledToggleButtonGroup {...rest}>
      {data.map((x) => (
        <StyledToggleButton disableRipple disableFocusRipple key={x.value} value={x.value}>
          {x.label}
        </StyledToggleButton>
      ))}
    </StyledToggleButtonGroup>
  );
};
