import { LaravelPaginationLinks, LaravelPaginationMeta } from './laravel-pagination'
import { TRole } from './role'

export type TUser = {
  id: number
  name: string
  email: string
  photo_url?: string
  role?: TRole
  gender?: string
  phone_number?: string
  about?: string
  status: string
  created_at: Date | string
}

export type TCreateUser = Omit<TUser, 'id' | 'created_at' | 'role' | 'status'> & { role_id: number; password: string }

export type TResponseListUser = {
  data: TUser[]
  meta?: LaravelPaginationMeta
  links: LaravelPaginationLinks
}
