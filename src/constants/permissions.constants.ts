/**
 *
 * Permissions constants.
 */

export const PermissionConstants = {
  // Job Order
  CreateJobOrder: 'CREATE_JO',
  UpdateJobOrder: 'UPDATE_JO',
  DeleteJobOrder: 'DELETE_JO',
  UpdatePaymentStatusJobOrder: 'UPDATE_PAYMENT_STATUS_JO',
  UpdateProductionStatusJobOrder: 'UPDATE_PRODUCTION_STATUS_JO',
  RejectJobOrder: 'REJECT_JO',
  RejectProductionJobOrder: 'REJECT_PRODUCTION_JO',
  HoldUpJobOrder: 'HOLD_PRODUCTION_JO',

  // Customer
  CreateCustomer: 'CREATE_CUSTOMER',
  UpdateCustomer: 'UPDATE_CUSTOMER',
  DeleteCustomer: 'DELETE_CUSTOMER',

  // Warehouse
  CreateMaterial: 'CREATE_MATERIAL',
  UpdateMaterial: 'UPDATE_MATERIAL',
  DeleteMaterial: 'DELETE_MATERIAL', // It's really danger!
  UpdateMaterialStock: 'UPDATE_MATERIAL_STOCK',
};
