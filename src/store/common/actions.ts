import * as ActionTypes from './constants';

// Actions definiition
interface SetSidebarDrawer {
  type: typeof ActionTypes.SET_SIDEBAR_DRAWER;
  payload: boolean;
}

interface SetPopUpSearchBox {
  type: typeof ActionTypes.SET_POPUP_SEARCH_BOX;
  payload: boolean;
}

// Union action types
export type CommonAction = SetSidebarDrawer | SetPopUpSearchBox;

// Actions creator.
export const setSidebarDrawer = (payload: boolean): SetSidebarDrawer => ({
  type: ActionTypes.SET_SIDEBAR_DRAWER,
  payload,
});

export const setPopUpSearchBox = (payload: boolean): SetPopUpSearchBox => ({
  type: ActionTypes.SET_POPUP_SEARCH_BOX,
  payload,
});
