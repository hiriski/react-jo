import AvatarMan from '@/assets/images/avatars/man.png';
import AvatarMan1 from '@/assets/images/avatars/man1.png';
import AvatarMan2 from '@/assets/images/avatars/man2.png';
import AvatarWoman from '@/assets/images/avatars/woman.png';
import AvatarWoman1 from '@/assets/images/avatars/woman1.png';
import AvatarDefault from '@/assets/images/avatars/avatar_square_blue_512dp.png';

// Constants.
import { RoleIdConstants } from '@/constants/role-id-constants';
import { IUser } from '@/interfaces/user';

export const getUserAvatarFallback = (user: IUser): string => {
  switch (user.role_id) {
    case RoleIdConstants.SuperAdmin:
      return AvatarMan;
      break;
    case RoleIdConstants.Admin:
      return AvatarMan1;
      break;
    case RoleIdConstants.WarehouseTeam:
      return AvatarMan2;
      break;
    case RoleIdConstants.ProductionTeam:
      return AvatarMan1;
      break;
    case RoleIdConstants.Owner:
      return AvatarMan;
      break;
    case RoleIdConstants.Cashier:
      return AvatarWoman1;
      break;
    case RoleIdConstants.CustomerService:
      return AvatarWoman;
      break;
    default:
      return AvatarDefault;
      break;
  }
};

export const displayGender = (gender: 'male' | 'female' | 'none'): string => {
  switch (gender) {
    case 'male':
      return 'Laki-laki';
      break;
    case 'female':
      return 'Perempuan';
      break;
    default:
      return 'None';
      break;
  }
};

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
export function getRandomArbitrary(min: number, max: number): number {
  return Math.random() * (max - min) + min;
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 */
export function getRandomInt(min: number, max: number): number {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
