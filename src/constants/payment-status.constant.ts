export const PaymentStatusConstants = {
  Unpaid: 1,
  DP: 2,
  Paid: 3,
};
