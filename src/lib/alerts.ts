import Swall from 'sweetalert2';

export const showAlertFeatureUnavailable = (customTitle?: string, customerText?: string): Promise<void> => {
  return Swall.fire({
    title: customTitle ?? 'Opss.. Fitur ini belum tersedia',
    icon: 'error',
    text: customerText ?? 'Fitur ini akan segera hadir di versi stabil selanjutnya',
    showCancelButton: false,
    confirmButtonText: 'Oh, Okay',
  }).then((result) => {
    if (result.isConfirmed) {
      console.log('confirmed');
    }
  });
};
