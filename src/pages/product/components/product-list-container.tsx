import { BaseEmptyContent } from '@/components/base'
import { useAppSelector } from '@/store/hook'
import { IProduct } from '@/types/product'
import { createArraySkeleton } from '@/utils/skeleton'
import Grid from '@mui/material/Grid'
import React, { FC, memo } from 'react'

import ProductCard from './product-card'
import ProductCardSkeleton from './product-card.skeleton'

interface Props {
  products: Array<IProduct>
  isFetching: boolean
}

const ProductListContainer: FC<Props> = ({ products, isFetching }) => {
  const arrSkeleton = createArraySkeleton()

  return isFetching ? (
    <Grid container spacing={3}>
      {arrSkeleton.map((skeleton, index) => (
        <Grid key={String(skeleton + index)} item xs={6} md={4} lg={3}>
          <ProductCardSkeleton />
        </Grid>
      ))}
    </Grid>
  ) : (
    products.length > 0 ? (
      <Grid container spacing={3}>
        {products.map((product) => (
          <Grid item key={product.id} xs={6} md={4} lg={3}>
            <ProductCard product={product} />
          </Grid>
        ))}
      </Grid>
    ) : (
      <BaseEmptyContent subtitle='Tidak ada produk' />
    )
  )
}

const MemoizedProductListContainer = memo(ProductListContainer)
export default MemoizedProductListContainer
