import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
// import Slide, { SlideProps } from '@mui/material/Slide'
// import Grow, { GrowProps } from '@mui/material/Grow'
import Zoom, { ZoomProps } from '@mui/material/Zoom';
import { BoxSpinner, DialogContent } from '@src/components/ui';
import { useAppSelector } from '@src/store/hook';
import { jobOrder_fetchDetail, setDialogDetailJo } from '@/store/job-order/job-order-actions';
import { FC, ReactElement, forwardRef, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useWindowSize } from 'react-use';

// Mui icons.
import CloseIcon from '@mui/icons-material/Close';

// Components.
import { JobOrderDetail } from './job-order-detail';
import { JobOrderConfig } from '@/config';
import { IconButton } from '@mui/material';

const Transition = forwardRef<unknown, ZoomProps>(
  (props, ref): ReactElement => (
    // eslint-disable-next-line react/jsx-props-no-spreading
    // <Slide direction="down" ref={ref} {...props} />
    // <Grow style={{ transformOrigin: '0 0 0' }} ref={ref} {...props} />
    <Zoom ref={ref} {...props} />
  ),
);

export const JobOrderDialogDetail: FC = () => {
  const dispatch = useDispatch();
  const { height: windowHeight } = useWindowSize();
  const { dialogDetailJo, detail } = useAppSelector((state) => state.jobOrder);
  const { open, id } = dialogDetailJo;
  const { isLoading, isError, data } = detail;

  const handleClose = (): void => {
    dispatch(setDialogDetailJo(false, null));
  };

  const getJobOrder = (shouldLoading: boolean): void => {
    dispatch(jobOrder_fetchDetail(id, shouldLoading));
  };

  useEffect(() => {
    if (open && id) {
      getJobOrder(true);
    }
  }, [open]);

  useEffect(() => {
    if (open && id) {
      const intervalFetchJobOrder = setInterval(() => {
        getJobOrder(false);
      }, JobOrderConfig.FetchInterval);
      return () => {
        clearInterval(intervalFetchJobOrder);
      };
    }
    return undefined;
  }, [open, id]);

  return (
    <Dialog
      open={open}
      // TransitionComponent={Transition}
      keepMounted
      onClose={handleClose}
      aria-describedby="dialog-job-order-detail"
      PaperProps={{ elevation: 1, sx: { position: 'relative' } }}
      maxWidth="md"
      fullWidth
      BackdropProps={{
        sx: { backgroundColor: 'rgb(32 35 56 / 45%)' },
      }}
    >
      <IconButton
        onClick={handleClose}
        sx={{ position: 'absolute', top: (theme) => theme.spacing(2.6), right: (theme) => theme.spacing(3) }}
      >
        <CloseIcon sx={{ fontSize: 22 }} />
      </IconButton>
      <DialogContent
        sx={{
          pt: 4,
          height: isLoading ? 300 : windowHeight - 100,
          transition: (theme) => theme.transitions.create(['height']),
        }}
      >
        {isLoading ? <BoxSpinner height={300} /> : <JobOrderDetail data={data} />}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Close</Button>
      </DialogActions>
    </Dialog>
  );
};
