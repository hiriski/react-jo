// Interfaces.
import type { AppState } from '@src/store/root-reducer';
import { DashboardState } from './dashboard-reducer';

export const dashboard_rootSelector = (state: AppState): DashboardState => state.dashboard;
