import { CssBaseline } from '@mui/material'
import { ThemeProvider as MUIThemeProvider } from '@mui/material/styles'
import theme from '@src/config/theme'
import { FC, ReactNode } from 'react'

type Props = {
  children: ReactNode
}

const ThemeProvider: FC<Props> = ({ children }: Props) => (
  <MUIThemeProvider theme={theme}>
    <CssBaseline>
      {children}
    </CssBaseline>
  </MUIThemeProvider>
)

export default ThemeProvider
