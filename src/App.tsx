import { ThemeProvider, MuiDateAdapterProvider } from '@src/providers';
import { FC, useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';

import RootRoutes from './root-routes';

import { useAppSelector } from '@/store/hook';

// Components.
import { Snackbar } from '@components/snackbar';
import { useDispatch } from 'react-redux';
import { auth_fetchAuthenticatedUser } from './store/auth/actions';
import { masterData_fetchMasterData } from './store/master-data/master-data-actions';
import { RequestBodyMasterData } from './interfaces/master-data';
import { getToken } from './utils/tokens';

// Pusher
// import { subscribePusherChannels } from './pusher-channels';

// Sound
import useSound from 'use-sound';
import soundTone3D from '@/assets/sounds/tone_3d.mp3';

// Pusher
import Pusher from 'pusher-js';

import { RoleIdConstants } from './constants';
import { INotification } from './interfaces/notification';
import { IJobOrder } from './interfaces/job-order';

// Enable pusher logging - don't include this in production
Pusher.logToConsole = IS_DEV;

const requestBodyMasterData: RequestBodyMasterData = [
  {
    object_name: 'product_categories',
  },
  {
    object_name: 'job_order_categories',
  },
  {
    object_name: 'materials',
  },
  {
    object_name: 'material_brands',
  },
  {
    object_name: 'material_categories',
  },
  {
    object_name: 'material_types',
  },
  {
    object_name: 'customers',
  },
  {
    object_name: 'payment_methods',
  },
  {
    object_name: 'payment_statuses',
  },
  {
    object_name: 'bank_accounts',
  },
  {
    object_name: 'machines',
  },
  {
    object_name: 'machine_brands',
  },
  {
    object_name: 'machine_categories',
  },
  {
    object_name: 'users',
  },
];

const App: FC = () => {
  const { isAuthenticated, authenticatedUser: user } = useAppSelector((state) => state.auth);
  const dispatch = useDispatch();

  const [playOn] = useSound(soundTone3D, { volume: 0.85 });

  useEffect(() => {
    const token = getToken();
    const getAuthenticatedUser = async (): Promise<void> => {
      dispatch(auth_fetchAuthenticatedUser());
    };

    const getInitializeData = async (): Promise<void> => {
      dispatch(masterData_fetchMasterData(requestBodyMasterData));
    };

    if (isAuthenticated && token) {
      getAuthenticatedUser();
      getInitializeData();
    }
  }, []);

  useEffect(() => {
    if (isAuthenticated && user) {
      const pusher = new Pusher(process.env.PUSHER_APP_KEY, {
        cluster: process.env.PUSHER_APP_CLUSTER,
      });
      const jobOrderChannel = pusher.subscribe('job-order-channel');

      if (user.role_id === RoleIdConstants.ProductionTeam) {
        jobOrderChannel.bind('job-order-created', (messages: INotification<IJobOrder>) => {
          playOn();
          console.log('⚡⚡⚡ Pusher', messages);
        });
      } else if (user.role_id !== RoleIdConstants.ProductionTeam) {
        jobOrderChannel.bind('job-order-production-was-started', (messages: INotification<IJobOrder>) => {
          playOn();
          console.log('⚡⚡⚡ Pusher', messages);
        });
      }
    }
  }, []);

  return (
    <ThemeProvider>
      <MuiDateAdapterProvider>
        <BrowserRouter>
          <RootRoutes />
          <Snackbar />
        </BrowserRouter>
      </MuiDateAdapterProvider>
    </ThemeProvider>
  );
};

export default App;
