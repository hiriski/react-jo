import { IUser } from '@/interfaces/user';
import { TRolePermissions } from '@src/types/role';
import { TCreateUser, TResponseListUser, TUser } from '@src/types/user';
import { AnyAction } from 'redux';

import * as ActionTypes from './constants';

// Actions definiition

interface SetDialogAddEditUserRequested {
  type: typeof ActionTypes.SET_DIALOG_ADD_EDIT_USER_REQUESTED;
  payload: {
    open: boolean;
    id: number | null;
  };
}
interface SetDrawerAddEditUser {
  type: typeof ActionTypes.SET_DIALOG_ADD_EDIT_USER;
  payload: {
    open: boolean;
    id: number | null;
  };
}
interface SetDialogDetailUser {
  type: typeof ActionTypes.SET_DIALOG_DETAIL_USER;
  payload: {
    open: boolean;
    id: number | null;
  };
}
interface SetDialogPermissionsUser {
  type: typeof ActionTypes.SET_DIALOG_PERMISSIONS_USER;
  payload: {
    open: boolean;
    userName: string | null;
    permissions: TRolePermissions[] | null;
  };
}
interface FetchingUserList {
  type: typeof ActionTypes.FETCHING_USER_LIST_REQUESTED;
  payload?: Record<string, string>;
}
interface FetchingUserListLoading {
  type: typeof ActionTypes.FETCHING_USER_LIST_LOADING;
}
interface FetchingUserListFailure {
  type: typeof ActionTypes.FETCHING_USER_LIST_FAILURE;
}
interface FetchingUserListSuccess {
  type: typeof ActionTypes.FETCHING_USER_LIST_SUCCESS;
  payload: TResponseListUser;
}

interface FetchingUser {
  type: typeof ActionTypes.FETCHING_USER_REQUESTED;
  payload: number;
}
interface FetchingUserLoading {
  type: typeof ActionTypes.FETCHING_USER_LOADING;
}
interface FetchingUserFailure {
  type: typeof ActionTypes.FETCHING_USER_FAILURE;
}
interface FetchingUserSuccess {
  type: typeof ActionTypes.FETCHING_USER_SUCCESS;
  payload: TUser;
}

interface CreateUser {
  type: typeof ActionTypes.CREATE_USER_REQUESTED;
  payload: TCreateUser;
}
interface CreateUserLoading {
  type: typeof ActionTypes.CREATE_USER_LOADING;
}
interface CreateUserSuccesss {
  type: typeof ActionTypes.CREATE_USER_SUCCESS;
  payload: TUser;
}
interface CreateUserFailure {
  type: typeof ActionTypes.CREATE_USER_FAILURE;
}

interface UpdateUser {
  type: typeof ActionTypes.UPDATE_USER_REQUESTED;
  payload: {
    id: number;
    body: TCreateUser;
  };
}
interface UpdateUserLoading {
  type: typeof ActionTypes.UPDATE_USER_LOADING;
}
interface UpdateUserSuccess {
  type: typeof ActionTypes.UPDATE_USER_SUCCESS;
  payload: TUser;
}
interface UpdateUserFailure {
  type: typeof ActionTypes.UPDATE_USER_FAILURE;
}

interface DeleteUser {
  type: typeof ActionTypes.DELETE_USER_REQUESTED;
  payload: number;
}
interface DeleteUserLoading {
  type: typeof ActionTypes.DELETE_USER_LOADING;
}
interface DeleteUserFailure {
  type: typeof ActionTypes.DELETE_USER_FAILURE;
}
interface DeleteUserSuccess {
  type: typeof ActionTypes.DELETE_USER_SUCCESS;
}

// Union action types
export type UserAction =
  | SetDialogAddEditUserRequested
  | SetDrawerAddEditUser
  | SetDialogDetailUser
  | SetDialogPermissionsUser
  | FetchingUserList
  | FetchingUserListLoading
  | FetchingUserListFailure
  | FetchingUserListSuccess
  | FetchingUser
  | FetchingUserLoading
  | FetchingUserFailure
  | FetchingUserSuccess
  | CreateUser
  | CreateUserLoading
  | CreateUserSuccesss
  | CreateUserFailure
  | UpdateUser
  | UpdateUserLoading
  | UpdateUserSuccess
  | UpdateUserFailure
  | DeleteUser
  | DeleteUserLoading
  | DeleteUserSuccess
  | DeleteUserFailure;

// Actions creator.

export const setDialogAddEditUser = (open: boolean, id: number | null): SetDialogAddEditUserRequested => ({
  type: ActionTypes.SET_DIALOG_ADD_EDIT_USER_REQUESTED,
  payload: { open, id },
});

export const setDialogDetailUser = (open: boolean, id: number | null): SetDialogDetailUser => ({
  type: ActionTypes.SET_DIALOG_DETAIL_USER,
  payload: { open, id },
});

export const setDialogPermissionsUser = (
  open: boolean,
  userName: string | null,
  permissions: TRolePermissions[] | null,
): SetDialogPermissionsUser => ({
  type: ActionTypes.SET_DIALOG_PERMISSIONS_USER,
  payload: { open, userName, permissions },
});

/** fetch user list */
export const fetchUserList = (payload?: Record<string, string>): FetchingUserList | AnyAction => ({
  type: ActionTypes.FETCHING_USER_LIST_REQUESTED,
  payload,
});
export const fetchingUserListLoading = (): FetchingUserListLoading => ({
  type: ActionTypes.FETCHING_USER_LIST_LOADING,
});
export const fetchingUserListFailure = (): FetchingUserListFailure => ({
  type: ActionTypes.FETCHING_USER_LIST_FAILURE,
});
export const fetchingUserListSuccess = (payload: TResponseListUser): FetchingUserListSuccess => ({
  type: ActionTypes.FETCHING_USER_LIST_SUCCESS,
  payload,
});

/** fetch user by id */
export const fetchUser = (payload: number): FetchingUser => ({
  type: ActionTypes.FETCHING_USER_REQUESTED,
  payload,
});
export const fetchingUserLoading = (): FetchingUserLoading => ({
  type: ActionTypes.FETCHING_USER_LOADING,
});
export const fetchingUserFailure = (): FetchingUserFailure => ({
  type: ActionTypes.FETCHING_USER_FAILURE,
});
export const fetchingUserSuccess = (payload: TUser): FetchingUserSuccess => ({
  type: ActionTypes.FETCHING_USER_SUCCESS,
  payload,
});

/** create user */
export const createUser = (payload: TCreateUser): CreateUser => ({
  type: ActionTypes.CREATE_USER_REQUESTED,
  payload,
});
export const createUserLoading = (): CreateUserLoading => ({
  type: ActionTypes.CREATE_USER_LOADING,
});
export const createUserFailure = (): CreateUserFailure => ({
  type: ActionTypes.CREATE_USER_FAILURE,
});
export const createUserSuccess = (payload: TUser): CreateUserSuccesss => ({
  type: ActionTypes.CREATE_USER_SUCCESS,
  payload,
});

/** update user */
export const updateUser = (id: number, body: TCreateUser): UpdateUser => ({
  type: ActionTypes.UPDATE_USER_REQUESTED,
  payload: { id, body },
});
export const updateUserLoading = (): UpdateUserLoading => ({
  type: ActionTypes.UPDATE_USER_LOADING,
});
export const updateUserFailure = (): UpdateUserFailure => ({
  type: ActionTypes.UPDATE_USER_FAILURE,
});
export const updateUserSuccess = (payload: TUser): UpdateUserSuccess => ({
  type: ActionTypes.UPDATE_USER_SUCCESS,
  payload,
});

/** delete user */
export const deleteUser = (payload: number): DeleteUser => ({
  type: ActionTypes.DELETE_USER_REQUESTED,
  payload,
});
export const deleteUserLoading = (): DeleteUserLoading => ({
  type: ActionTypes.DELETE_USER_LOADING,
});
export const deleteUserFailure = (): DeleteUserFailure => ({
  type: ActionTypes.DELETE_USER_FAILURE,
});
export const deleteUserSuccess = (): DeleteUserSuccess => ({
  type: ActionTypes.DELETE_USER_SUCCESS,
});
