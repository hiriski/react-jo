import { UUID } from '@/interfaces/common';
import { JobOrderFilterPeriod, JobOrderFilterProductionStatusId } from '@/interfaces/job-order';

export interface IJobOrderPayloadUpdatePaymentStatus {
  job_order_id: UUID;
  payment_status_id: number;
  down_payment?: number | null;
}

export interface IPayloadJobOrderStartProduction {
  job_order_id: UUID;
  machine_ids: Array<number>;
  progress_by_user_id: UUID;
}
export interface IJobOrderFilters {
  period: JobOrderFilterPeriod;
  production_status_id: JobOrderFilterProductionStatusId;
  per_page: number;
  page: number;
}

export interface IPayloadJobOrderDelete {
  job_order_id: UUID;
  reason_deleted: string;
}
