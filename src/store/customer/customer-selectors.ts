// Interfaces.
import type { AppState } from '@src/store/root-reducer';
import { CustomerState } from './customer-reducer';

export const customer_rootSelector = (state: AppState): CustomerState => state.customer;
