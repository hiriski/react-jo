import { BaseButton } from '@/components/base';
import { ROUTES } from '@/utils/constants';
import { MainLayout } from '@components/layouts';
import { Pagination } from '@components/ui';
import { CustomerList, DialogDetailCustomer, DrawerAddEditCustomer, FabCustomer } from '@src/components/customer';
import { fetchCustomerList } from '@src/store/customer/customer-actions';
import { useAppSelector } from '@src/store/hook';
import { ChangeEvent, FC, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import Box from '@mui/material/Box';

// Router
import { useNavigate } from 'react-router-dom';

const CustomerPage: FC = () => {
  const dispatch = useDispatch();
  const { listCustomer } = useAppSelector((state) => state.customer);
  const { isFetching, meta } = listCustomer;
  const navigate = useNavigate();

  const fetchData = (params?: Record<string, string>): void => {
    dispatch(fetchCustomerList(params));
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handlePaginationChange = (event: ChangeEvent<unknown>, page: number): void => {
    fetchData({ page: String(page) });
  };

  const handleAddCustomer = (): void => {
    navigate(ROUTES.ADD_CUSTOMER);
  };

  return (
    <MainLayout>
      <Box>
        <BaseButton onClick={handleAddCustomer}>Add Customer</BaseButton>
      </Box>
      <CustomerList />
      <Pagination
        fixed={true}
        isFetching={isFetching}
        onChange={handlePaginationChange}
        count={meta ? meta.last_page : 0}
        page={meta ? meta.current_page : 1}
        color="primary"
      />
      <FabCustomer />
      <DrawerAddEditCustomer />
      <DialogDetailCustomer />
    </MainLayout>
  );
};
export default CustomerPage;
