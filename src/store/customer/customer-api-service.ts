import { IRequestBodyCustomer } from '@/interfaces/customer';
import { TCustomer, TResponseListCustomer } from '@src/types/customer';
import api from '@src/utils/http';
import { AxiosResponse } from 'axios';

// Api endpoints.
import { CustomerApiEndpoints } from './customer-api-endpoints';

const CustomerApiService = {
  findAll: async (params?: Record<string, string>): Promise<AxiosResponse<TResponseListCustomer>> =>
    api.get(CustomerApiEndpoints.index, { params }),
  find: async (id: number): Promise<AxiosResponse<TCustomer>> => api.get(`${CustomerApiEndpoints.index}/${id}`),
  create: async (body: IRequestBodyCustomer): Promise<AxiosResponse<TCustomer>> =>
    api.post(CustomerApiEndpoints.index, body),
  update: async (id: number, body: IRequestBodyCustomer): Promise<AxiosResponse<TCustomer>> =>
    api.put(`${CustomerApiEndpoints.index}/${id}`, body),
  delete: async (id: number): Promise<AxiosResponse> => api.delete(`${CustomerApiEndpoints.index}${id}`),
};
export default CustomerApiService;
