import AddIcon from '@mui/icons-material/Add';
import LayersClearIcon from '@mui/icons-material/LayersClear';
import { Box, Button, Container, Typography } from '@mui/material';
import { useAppSelector } from '@src/store/hook';
import { setDialogAddEditInventory } from '@src/store/inventory/actions';
import { setDrawerAddEditJo } from '@/store/job-order/job-order-actions';
import { ROUTES } from '@src/utils/constants';
import { FC } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

type Props = {
  text?: string;
};

const MaterialPaperEmpty: FC<Props> = ({ text }: Props) => {
  const dispatch = useDispatch();
  const { isAuthenticated } = useAppSelector((state) => state.auth);
  const navigate = useNavigate();

  const onAdd = (): void => {
    dispatch(setDrawerAddEditJo(true, null));
  };

  return (
    <Container>
      <Box
        sx={{
          display: 'flex',
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
          minHeight: 420,
        }}
      >
        <LayersClearIcon sx={{ fontSize: 120, mb: 2, color: 'text.disabled' }} />
        <Typography variant="h5" color="text.disabled">
          {text}
        </Typography>
        <Box sx={{ mt: 4 }}>
          <Button startIcon={<AddIcon />} onClick={onAdd} variant="text" size="small" color="secondary">
            Add New
          </Button>
        </Box>
      </Box>
    </Container>
  );
};

MaterialPaperEmpty.defaultProps = {
  text: 'Nothing to see here..',
};

export default MaterialPaperEmpty;
