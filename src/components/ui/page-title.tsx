import { FC, ReactElement } from 'react'
import { Box, Typography } from '@mui/material'

type Props = {
  title: string
}

const PageTitle: FC<Props> = ({ title }: Props): ReactElement => (
  <Box
    sx={{
      mb: 3,
    }}
  >
    <Typography sx={{ color: 'text.secondary', fontWeight: '300' }} component="h1" variant="h4">
      {title}
    </Typography>
  </Box>
)

export default PageTitle
