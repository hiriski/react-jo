import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { FC } from 'react';

import LoginForm from '@src/components/auth/login/login-form';
import { AuthLayout } from '@src/components/layouts';
import { APP_NAME } from '@src/utils/constants';
import SampleLogo from '@/assets/images/sample-logo.png';
import { useTheme } from '@mui/material';

const LoginPage: FC = () => {
  const theme = useTheme();
  return (
    <AuthLayout>
      <Box
        sx={{
          display: 'flex',
          position: 'relative',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100vh',
          width: {
            xs: '90%',
            sm: 460,
          },
          margin: '0 auto',
        }}
      >
        <Box
          sx={{
            width: '100%',
            px: 8,
            py: 6,
            borderRadius: 1,
            backgroundColor: 'rgba(255, 255, 255, 0.92)',
            backdropFilter: 'blur(2px)',
          }}
        >
          <Box
            sx={{
              width: '100%',
              mb: 3,
            }}
          >
            <Typography sx={{ mb: 2 }} component="h1" variant="h2">
              Login
            </Typography>
            <Typography component="p" variant="body1">
              Silahkan login untuk memulai aktifitas.
            </Typography>
          </Box>

          {/* Login form */}
          <LoginForm />

          {/* Logo */}
          <Box
            sx={{
              mt: 4,
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Box sx={{ width: 46, height: 'auto', mr: 3, ml: -5 }} component="img" src={SampleLogo} alt="Sample logo" />
            <Box>
              <Typography
                component="h1"
                sx={{
                  fontWeight: 'bold',
                  fontSize: 17,
                  color: 'text.primary',
                }}
              >
                {APP_NAME}
              </Typography>
              <Box
                sx={{
                  px: 1.6,
                  py: 0.4,
                  fontSize: '0.74rem',
                  borderRadius: 5,
                  textAlign: 'center',
                  display: 'inline-block',
                  backgroundColor: '#04a572',
                  color: 'primary.contrastText',
                }}
              >
                Development Version
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </AuthLayout>
  );
};

export default LoginPage;
