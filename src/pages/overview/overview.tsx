import { fetchListPaymentMethod } from '@/store/payment-method/actions';
import Typography from '@mui/material/Typography';
import { MainLayout } from '@src/components/layouts';
// import { DashboardGreeting } from '@src/components/overview'
import { fetchListPaymentStatus } from '@src/store/payment-status/actions';
import { fetchListProductionStatus } from '@src/store/production-status/actions';
import { fetchListRole } from '@src/store/role/actions';
import { FC, useEffect } from 'react';
import { batch, useDispatch } from 'react-redux';
import {
  OverviewAnalyticsJobOrderByCategory,
  OverviewAnalyticsTotalJobOrder,
  OverviewCustomer,
  OverviewGreeting,
  OverviewJobOrderContainer,
  OverviewJobOrderInProgress,
  OverviewJobOrderLatest,
  OverviewReportMaterialStock,
  OverviewRevenueReport,
  OverviewTableMaterial,
} from '@/pages/overview/components';
import { fetchListJo } from '@/store/job-order/job-order-actions';
import { useAppSelector } from '@/store/hook';
import { PageTitle } from '@/components/shared';

import HeaderTopBackgroundImage from '@/assets/images/wallpaperflare.com_wallpaper.jpg';

// Mui components.
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import { fetchTotalJobOrder } from '@/store/analytics/analytics-actions';
import { SIDEBAR_DRAWER_WIDTH } from '@/utils/constants';

import { alpha } from '@mui/material';
import {
  dashboard_fetchListJobOrderInProgress,
  dashboard_fetchListJobOrderLatest,
} from '@/store/dashboard/dashboard-actions';
import { dashboard_rootSelector } from '@/store/dashboard/dashboard-selectors';
import { warehouse_fetchMaterialList } from '@/store/warehouse/warehouse-actions';
import { warehouse_rootSelector } from '@/store/warehouse/warehouse-selectors';

const OverviewPage: FC = () => {
  const dispatch = useDispatch();
  const { listJo } = useAppSelector((state) => state.jobOrder);
  const { visibleSidebarDrawer } = useAppSelector((state) => state.common);
  const { listJobOrderInProgress, listJobOrderLatest } = useAppSelector((state) => dashboard_rootSelector(state));

  const { listMaterial } = useAppSelector((s) => warehouse_rootSelector(s));

  const fetchData = async (): Promise<void> => {
    batch(() => {
      dispatch(dashboard_fetchListJobOrderInProgress());
      dispatch(dashboard_fetchListJobOrderLatest());
      dispatch(warehouse_fetchMaterialList());
    });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <MainLayout>
      <PageTitle
        title="Dashboard"
        subtitle="Ringkasan Laporan Order, Pendapatan dan Stock Bahan Baku"
        rightContent={
          <Box>
            <Typography>Test Right content</Typography>
          </Box>
        }
      />

      {/* Greeting and Total Job Order */}
      <Box sx={{ position: 'relative', overflow: 'hidden', borderRadius: 1 }}>
        <Box
          sx={{
            position: 'absolute',
            top: 0,
            right: 0,
            width: '100%',
            height: 250,
            backgroundImage: `url(${HeaderTopBackgroundImage})`,
            backgroundPosition: 'center right',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
          }}
        />
        <Box
          sx={{
            position: 'absolute',
            backgroundColor: alpha('#fff', 0.85),
            width: '100%',
            height: 250,
            top: 0,
            right: 0,
          }}
        />
        <Box sx={{ px: 5, pt: 4 }}>
          <OverviewGreeting />
          <OverviewAnalyticsTotalJobOrder />
        </Box>
      </Box>

      {/* Just a spacer */}
      <Box sx={{ pb: 4 }} />

      {/* Revenue Report */}
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <OverviewRevenueReport />
        </Grid>
      </Grid>

      {/* Just a spacer */}
      <Box sx={{ pb: 4 }} />

      {/* Report Material Stock */}
      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <OverviewTableMaterial data={listMaterial.data} />
        </Grid>
        <Grid item xs={12} md={6}>
          <OverviewReportMaterialStock />
        </Grid>
      </Grid>

      {/* Just a spacer */}
      <Box sx={{ pb: 4 }} />

      <Grid container spacing={3}>
        <Grid item xs={12} md={6} lg={5}>
          <OverviewJobOrderInProgress
            data={listJobOrderInProgress.data}
            isFetching={listJobOrderInProgress.isFetching}
          />
        </Grid>
        <Grid item xs={12} md={6} lg={7}>
          <OverviewAnalyticsJobOrderByCategory />
        </Grid>
      </Grid>

      <Grid container spacing={3}>
        <Grid item xs={12}>
          <OverviewJobOrderLatest data={listJobOrderLatest.data} isFetching={listJobOrderLatest.isFetching} />
        </Grid>
      </Grid>

      {/* Just a spacer */}
      <Box sx={{ pb: 4 }} />

      <Grid container spacing={4}>
        <Grid item xs={12} sm={6} md={4}>
          <OverviewCustomer />
        </Grid>
      </Grid>

      {/* Just a spacer */}
      <Box sx={{ pb: 4 }} />

      <Grid container spacing={4}>
        <Grid item xs={12}>
          <OverviewJobOrderContainer isFetching={listJo.isFetching} items={listJo.data} />
        </Grid>
      </Grid>
    </MainLayout>
  );
};

export default OverviewPage;
