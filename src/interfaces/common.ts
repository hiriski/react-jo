import { NavigateFunction } from 'react-router';

export type UUID = string;

export interface SagaEffectWithNavigateFunction<T = any, P = any> {
  type: T;
  payload: P;
  navigate: NavigateFunction;
}
