import { TJoInvoiceData } from './jo'

export type TInvoiceItem = {
  id: number
  description: string
  notes?: string
  quantity: number
  price: number
  discount?: number
}

export type TInvoice = {
  id: number
  invoice_number: string
  bill_to_name: string
  bill_to_address?: string
  bill_to_phone_number?: string
  invoice_date: string | Date
  invoice_due_date: string | Date
  tax?: number
  bill_discount?: number
  notes?: string
  terms_and_conditions?: string
  created_at?: string | Date
  items: Array<TInvoiceItem>
}

export type TInvoiceGeneratorData = TJoInvoiceData & {
  logo?: string | null
  companyName: string
  companyAddress: string
  companyCountry: string
  title: string | null
  billToLabel: string
  customerName: string
  customerPhoneNumber: string
  customerEmail: string
  customerAddress: string
  invoiceNumberLabel: string
  notesLabel: string
  notes: string
  termLabel: string
  term: string
  subTotalLabel: string
  subTotal: string
  taxLabel: string
  totalLabel: string
  currency: string
}
