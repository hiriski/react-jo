import MarkEmailReadOutlinedIcon from '@mui/icons-material/MarkEmailReadOutlined'
import Avatar from '@mui/material/Avatar'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Checkbox from '@mui/material/Checkbox'
import { styled } from '@mui/material/styles'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableRow from '@mui/material/TableRow'
import Typography from '@mui/material/Typography'
import { setDialogDetailUser, setDialogPermissionsUser } from '@src/store/user/actions'
import { TRolePermissions } from '@src/types/role'
import { TUser } from '@src/types/user'
import { colors } from '@src/utils/colors'
import { DB_FORMAT_TIMESTAMPS } from '@src/utils/constants'
import { getInitialsName } from '@src/utils/misc'
import moment from 'moment'
import { Dispatch, FC, MouseEvent, SetStateAction, memo } from 'react'
import { useDispatch } from 'react-redux'

import { Order } from './types'

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '& *': {
    lineHeight: '1.4 !important',
  },
  '& .MuiTableCell-sizeMedium': {
    padding: '16px 6px !important',
  },
  // '&:nth-of-type(odd)': {
  //   backgroundColor: theme.palette.action.hover,
  // },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}))

interface Props {
  rows: TUser[]
  rowsPerPage: number
  order: Order
  page: number
  orderBy: string
  selected: Array<number>
  setSelected: Dispatch<SetStateAction<Array<number>>>
}

const UserTableBody: FC<Props> = ({ rows, rowsPerPage, order, page, orderBy, selected, setSelected }) => {
  // eslint-disable-next-line no-console
  console.log('✅✅ rows', rows)
  const dispatch = useDispatch()

  const handleCheck = (event: MouseEvent<unknown>, name: number): void => {
    const selectedIndex = selected.indexOf(name)
    let newSelected: Array<number> = []

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name)
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1))
    }

    setSelected(newSelected)
  }

  const isSelected = (name: number): boolean => selected.indexOf(name) !== -1

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0

  const handleClickDetail = (joId: number): void => {
    dispatch(setDialogDetailUser(true, joId))
  }

  const handleClickPermissions = (open: boolean, userName: string, permissions: TRolePermissions[]): void => {
    dispatch(setDialogPermissionsUser(open, userName, permissions))
  }

  return (
    <TableBody>
      {rows.map((row, index) => {
        const isItemSelected = isSelected(row.id)
        const labelId = `enhanced-table-checkbox-${index}`
        const { role } = row
        return (
          <StyledTableRow
            role="checkbox"
            aria-checked={isItemSelected}
            tabIndex={-1}
            key={String(row.id)}
            selected={isItemSelected}
          >
            <TableCell padding="checkbox" onClick={(event) => handleCheck(event, row.id)}>
              <Checkbox
                size="small"
                color="primary"
                checked={isItemSelected}
                inputProps={{
                  'aria-labelledby': labelId,
                }}
              />
            </TableCell>
            <TableCell component="th" id={labelId} scope="row" padding="none">
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Avatar
                  sx={{
                    bgcolor: colors[Math.floor(Math.random() * colors.length)],
                    color: 'primary.contrastText',
                    mr: 2,
                    width: 46,
                    height: 46,
                    fontSize: 14,
                    letterSpacing: 1,
                  }}
                  alt={row.name}
                >
                  {getInitialsName(row.name)}
                </Avatar>
                <Box>
                  <Typography variant="h5" sx={{ mb: 0.3, fontSize: 17 }}>
                    {row.name}
                  </Typography>
                  <Box sx={{ display: 'flex', alignItems: 'center' }}>
                    <MarkEmailReadOutlinedIcon sx={{ fontSize: 17, mr: 1 }} />
                    <Typography variant="subtitle2" sx={{ color: 'primary.main' }}>
                      {row.email}
                    </Typography>
                  </Box>
                </Box>
              </Box>
            </TableCell>
            <TableCell align="left">{role.name}</TableCell>
            <TableCell align="left">
              <Button
                onClick={() => handleClickPermissions(true, row.name, role.permissions)}
                variant="outlined"
                color="secondary"
                size="small"
              >
                Permissions
              </Button>
            </TableCell>
            <TableCell align="right">
              <Box
                sx={{
                  backgroundColor: '#d4fee7',
                  textAlign: 'center',
                  borderRadius: 4,
                  py: 0.6,
                  px: 1,
                  maxWidth: 86,
                }}
              >
                <Typography sx={{ color: '#009330', fontSize: 12 }} variant="subtitle2">
                  {row.status.toUpperCase()}
                </Typography>
              </Box>
            </TableCell>
            <TableCell align="left">
              <Box>
                <Typography color="text.secondary" variant="subtitle2">
                  {moment(row.created_at, DB_FORMAT_TIMESTAMPS).format('DD MMM YYYY')}
                </Typography>
              </Box>
            </TableCell>
            <TableCell align="right">
              <Button size="small" variant="outlined" onClick={() => handleClickDetail(row.id)}>
                Detail
              </Button>
            </TableCell>
          </StyledTableRow>
        )
      })}

      {emptyRows > 0 && (
        <TableRow
          style={{
            height: 53 * emptyRows,
          }}
        >
          <TableCell colSpan={6} />
        </TableRow>
      )}
    </TableBody>
  )
}

const MemoizedUserTableBody = memo(UserTableBody)

export default MemoizedUserTableBody
