import { FC } from 'react';

import { Box, Typography, Avatar } from '@mui/material';
import { MobileDatePicker } from '@mui/x-date-pickers/MobileDatePicker';

import AvatarImage from '@/assets/images/avatars/27.jpg';
import { useAppSelector } from '@/store';
import { auth_rootSelector } from '@/store/auth/auth-selectors';

import { colors } from '@/lib/colors';
import { getInitialsName } from '@/utils/misc';
import { getUserAvatarFallback } from '@/utils/common';

const now = new Date();
const time = now.getHours();
const isMorning = time >= 1 && time < 11;
const isAfternoon = time >= 11 && time < 16;
const isEvening = time >= 16 && time < 19;
const isNight = time > 19;

export const OverviewGreeting: FC = () => {
  const user = useAppSelector((state) => auth_rootSelector(state).authenticatedUser);
  return (
    <Box sx={{ display: 'flex', position: 'relative' }}>
      <Avatar
        src={user.photo_url ?? getUserAvatarFallback(user)}
        sx={{
          backgroundColor: user.avatar_text_color ?? colors[Math.floor(Math.random() * colors.length)],
          color: 'primary.contrastText',
          width: 60,
          height: 60,
          mr: 3,
        }}
        alt={user.name}
      >
        {getInitialsName(user.name, true)}
      </Avatar>
      <Box sx={{ display: 'flex', alignItems: 'flex-start', flexDirection: 'column', justifyContent: 'center' }}>
        <Typography variant="body2" sx={{ mb: 0.5 }}>
          {isMorning && 'Selamat Pagi 👋'}
          {isAfternoon && 'Selamat Siang 👋'}
          {isEvening && 'Selamat Sore 👋'}
          {isNight && 'Selamat Malam 👋'}
        </Typography>
        <Typography sx={{ fontWeight: 700, fontSize: 16 }}>{user.name}</Typography>
      </Box>
    </Box>
  );
};
