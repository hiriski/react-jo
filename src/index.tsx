// import 'react-image-crop/src/ReactCrop.scss'

import './styles/styles.scss';
import './styles/lib/swiper.scss';

import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import { ReduxProvider } from './providers';

ReactDOM.render(
  <React.StrictMode>
    <ReduxProvider>
      <App />
    </ReduxProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);
