// Interfaces.
import type { AppState } from '@src/store/root-reducer';
import { MachineState } from './machine-reducer';

export const machine_rootSelector = (state: AppState): MachineState => state.machine;
