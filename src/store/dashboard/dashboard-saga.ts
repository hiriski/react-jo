// Saga
import type { Effect, SagaIterator } from '@redux-saga/types';
import { SagaReturnType, call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';

// Enum action types.
import { DASHBOARD_ACTION_TYPES as ActionTypes } from './dashboard-action-types.enum';

// Utils
import { httpResponseOK } from '@src/utils/http';

// Actions creators.
import {
  dashboard_fetchListJobOrderInProgressFailure,
  dashboard_fetchListJobOrderInProgressLoading,
  dashboard_fetchListJobOrderInProgressSuccess,
  dashboard_fetchListJobOrderLatestFailure,
  dashboard_fetchListJobOrderLatestLoading,
  dashboard_fetchListJobOrderLatestSuccess,
} from './dashboard-actions';

// Api Services.
import JobOrderApiService from '../job-order/job-order-api-service';
import { JobOrderProductionStatusIdConstants } from '@/constants';

// Type definitions of return of result.
type ApiResponseJobOrderInProgress = SagaReturnType<typeof JobOrderApiService.findAll>;

function* saga_dashboardFetchListJobOrderInProgress({
  payload,
}: Effect<
  ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_IN_PROGRESS_REQUESTED,
  Record<string, string | number>
>): SagaIterator {
  yield put(dashboard_fetchListJobOrderInProgressLoading());

  const params = { production_status_id: JobOrderProductionStatusIdConstants.InProgress };
  try {
    const { status, data }: ApiResponseJobOrderInProgress = yield call(JobOrderApiService.findAll, params);
    if (httpResponseOK(status)) {
      yield put(dashboard_fetchListJobOrderInProgressSuccess(data.data));
    }
  } catch (e) {
    yield put(dashboard_fetchListJobOrderInProgressFailure());
  }
}

function* saga_dashboardFetchListJobOrderLatest({
  payload,
}: Effect<ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_LATEST_REQUESTED, Record<string, string | number>>): SagaIterator {
  yield put(dashboard_fetchListJobOrderLatestLoading());

  // const params = { production_status_id: JobOrderProductionStatusIdConstants.InProgress };
  try {
    const { status, data }: ApiResponseJobOrderInProgress = yield call(JobOrderApiService.findAll);
    if (httpResponseOK(status)) {
      yield put(dashboard_fetchListJobOrderLatestSuccess(data.data));
    }
  } catch (e) {
    yield put(dashboard_fetchListJobOrderLatestFailure());
  }
}

export default function* dashboardSaga(): SagaIterator {
  yield takeEvery(
    ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_IN_PROGRESS_REQUESTED,
    saga_dashboardFetchListJobOrderInProgress,
  );
  yield takeEvery(ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_LATEST_REQUESTED, saga_dashboardFetchListJobOrderLatest);
}
