// Enum action types.
import { DASHBOARD_ACTION_TYPES as ActionTypes } from './dashboard-action-types.enum';

// Interfaces.
import { IJobOrder } from '@/interfaces/job-order';

// Actions definition
interface IDashboardFetchListJobOrderInProgressRequested {
  type: typeof ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_IN_PROGRESS_REQUESTED;
  payload?: Record<string, string | number>;
}
interface IDashboardFetchListJobOrderInProgressLoading {
  type: typeof ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_IN_PROGRESS_LOADING;
}
interface IDashboardFetchListJobOrderInProgressFailure {
  type: typeof ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_IN_PROGRESS_FAILURE;
}
interface IDashboardFetchListJobOrderInProgressSuccess {
  type: typeof ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_IN_PROGRESS_SUCCESS;
  payload: Array<IJobOrder>;
}
interface IDashboardFetchListJobOrderLatestRequested {
  type: typeof ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_LATEST_REQUESTED;
  payload?: Record<string, string | number>;
}
interface IDashboardFetchListJobOrderLatestLoading {
  type: typeof ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_LATEST_LOADING;
}
interface IDashboardFetchListJobOrderLatestFailure {
  type: typeof ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_LATEST_FAILURE;
}
interface IDashboardFetchListJobOrderLatestSuccess {
  type: typeof ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_LATEST_SUCCESS;
  payload: Array<IJobOrder>;
}

// Union action types
export type DashboardActions =
  | IDashboardFetchListJobOrderInProgressRequested
  | IDashboardFetchListJobOrderInProgressLoading
  | IDashboardFetchListJobOrderInProgressFailure
  | IDashboardFetchListJobOrderInProgressSuccess
  | IDashboardFetchListJobOrderLatestRequested
  | IDashboardFetchListJobOrderLatestLoading
  | IDashboardFetchListJobOrderLatestFailure
  | IDashboardFetchListJobOrderLatestSuccess;

// Actions creators.
export const dashboard_fetchListJobOrderInProgress = (
  params?: Record<string, string | number>,
): IDashboardFetchListJobOrderInProgressRequested => ({
  type: ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_IN_PROGRESS_REQUESTED,
  payload: params,
});
export const dashboard_fetchListJobOrderInProgressLoading = (): IDashboardFetchListJobOrderInProgressLoading => ({
  type: ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_IN_PROGRESS_LOADING,
});
export const dashboard_fetchListJobOrderInProgressFailure = (): IDashboardFetchListJobOrderInProgressFailure => ({
  type: ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_IN_PROGRESS_FAILURE,
});
export const dashboard_fetchListJobOrderInProgressSuccess = (
  payload: Array<IJobOrder>,
): IDashboardFetchListJobOrderInProgressSuccess => ({
  type: ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_IN_PROGRESS_SUCCESS,
  payload,
});

export const dashboard_fetchListJobOrderLatest = (
  params?: Record<string, string | number>,
): IDashboardFetchListJobOrderLatestRequested => ({
  type: ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_LATEST_REQUESTED,
  payload: params,
});
export const dashboard_fetchListJobOrderLatestLoading = (): IDashboardFetchListJobOrderLatestLoading => ({
  type: ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_LATEST_LOADING,
});
export const dashboard_fetchListJobOrderLatestFailure = (): IDashboardFetchListJobOrderLatestFailure => ({
  type: ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_LATEST_FAILURE,
});
export const dashboard_fetchListJobOrderLatestSuccess = (
  payload: Array<IJobOrder>,
): IDashboardFetchListJobOrderLatestSuccess => ({
  type: ActionTypes.DASHBOARD_FETCH_LIST_JOB_ORDER_LATEST_SUCCESS,
  payload,
});
