import { FormCustomer } from '@components/customer';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import { setDrawerAddEditCustomer } from '@src/store/customer/customer-actions';
import { useAppSelector } from '@src/store/hook';
import { DRAWER_ADD_EDIT_CUSTOMER_WIDTH } from '@src/utils/constants';
import { FC, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { BoxSpinner } from '../ui';

// Components.
import { CustomerForm } from '@/components/customer';

const DrawerAddEditCustomer: FC = () => {
  const { drawerAddEdit: drawer, detail } = useAppSelector((state) => state.customer);
  const { data, isFetching } = detail;

  const dispatch = useDispatch();

  const onClose = (): void => {
    dispatch(setDrawerAddEditCustomer(false, null));
  };

  // useEffect(() => {
  //   if(drawer.id) dispatch()
  // }, [drawer.id])

  return (
    <Drawer
      elevation={0}
      anchor="right"
      open={drawer.open}
      onClose={onClose}
      PaperProps={{
        sx: {
          width: { xs: '100%', md: DRAWER_ADD_EDIT_CUSTOMER_WIDTH },
        },
      }}
    >
      <Box role="presentation" sx={{ position: 'relative' /* px: 8, pt: 3, pb: 5 */ }}>
        {isFetching ? (
          <BoxSpinner height={500} />
        ) : !drawer.id ? (
          <CustomerForm />
        ) : (
          detail.data && <FormCustomer id={drawer.id} data={detail.data} />
        )}
      </Box>
    </Drawer>
  );
};

export default DrawerAddEditCustomer;
