import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Slider from '@mui/material/Slider';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import { JoLabelPaymentStatus, ToggleUpdateProductionStatus } from '@src/components/jo';
import { useAppSelector } from '@src/store/hook';
import {
  jobOrder_finishProduction,
  jobOrder_setDialogStartProduction,
  updatePercentageProgress,
  updateProductionStatus,
} from '@/store/job-order/job-order-actions';
import { TJo } from '@src/types/jo';
import { DB_FORMAT_TIMESTAMPS, PRODUCTION_STATUSES } from '@src/utils/constants';
import { createWhatsappPhoneNumber } from '@src/utils/phone-number';
import { debounce } from 'lodash';
import moment from 'moment';
import { FC, useCallback, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import Swall from 'sweetalert2';
import { BaseButton, BaseListItemText } from '@/components/base';
import { UUID } from '@/interfaces/common';
import { JobOrderLabelProductionStatus } from '../job-order-label-production-status';
import { JobOrderProductionStatusIdConstants } from '@/constants';

// Mui icons
import WhatsApp from '@mui/icons-material/WhatsApp';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import EventAvailableIcon from '@mui/icons-material/EventAvailable';
import EventBusyIcon from '@mui/icons-material/EventBusy';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import MailIcon from '@mui/icons-material/Mail';
import PersonIcon from '@mui/icons-material/Person';
import DoneIcon from '@mui/icons-material/Done';
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet';
import CallIcon from '@mui/icons-material/Call';

import BookmarkIcon from '@mui/icons-material/Bookmark';
import ReactIDSwiper from 'react-id-swiper';

// React Image Lightbox
import Lightbox from 'react-image-lightbox';

import { JobOrderNotes } from './job-order-notes';
import { isProductionTeam } from '@/utils/role';
import { auth_rootSelector } from '@/store/auth/auth-selectors';
import { JobOrderDetailPaymentStatus } from './job-order-detail-payment-status';
import { getInitialsName } from '@/utils/misc';
import { colors } from '@/utils/colors';
import { green } from '@mui/material/colors';
import PrintingMachine from '@/assets/images/printing-machine.png';

type Props = {
  data: TJo;
};

function valuetext(value: number): string {
  return `${value}%`;
}

const PrettoSlider = styled(Slider)({
  color: '#52af77',
  height: 4,
  '& .MuiSlider-track': {
    border: 'none',
  },
  '& .MuiSlider-thumb': {
    height: 20,
    width: 20,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    '&:focus, &:hover, &.Mui-active, &.Mui-focusVisible': {
      boxShadow: 'inherit',
    },
    '&:before': {
      display: 'none',
    },
  },
  '& .MuiSlider-valueLabel': {
    lineHeight: 1.2,
    fontSize: 12,
    background: 'unset',
    padding: 0,
    width: 32,
    height: 32,
    borderRadius: '50% 50% 50% 0',
    backgroundColor: '#52af77',
    transformOrigin: 'bottom left',
    transform: 'translate(50%, -100%) rotate(-45deg) scale(0)',
    '&:before': { display: 'none' },
    '&.MuiSlider-valueLabelOpen': {
      transform: 'translate(50%, -100%) rotate(-45deg) scale(1)',
    },
    '& > *': {
      transform: 'rotate(45deg)',
    },
  },
});

export const JobOrderDetail: FC<Props> = ({ data }: Props) => {
  const dispatch = useDispatch();

  const user = useAppSelector((s) => auth_rootSelector(s).authenticatedUser);

  const [progressValue, setProgressValue] = useState(data?.percentage_progress ?? 0);
  const [openImage, setOpenImage] = useState<boolean>(false);
  const [clickedIndexImage, setClickedImageIndex] = useState<number>(0);

  const { data: paymentStatusList } = useAppSelector((state) => state.paymentStatus);

  const swiperConfig = {
    slidesPerView: 'auto' as number | 'auto',
    spaceBetween: 16,
    pagination: {
      el: '.swiper-pagination',
      // type: 'progressbar',
      clickable: true,
      renderBullet: (index, className) => {
        return '<span class="' + className + '">' + (index + 1) + '</span>';
      },
    },
  };

  const updatePercentage = (val: number): void => {
    dispatch(updatePercentageProgress(data.id, val));
  };

  const handleSearch = useCallback(debounce(updatePercentage, 500), []);
  // const handleSearch = useCallback(debounce(() => updatePercentage(25), 750), [])

  const handleSliderChange = (event: Event, newValue: number | Array<number>): void => {
    if (data.production_status.id !== PRODUCTION_STATUSES.WAITING.id) {
      setProgressValue(Number(newValue));
      handleSearch(Number(newValue));
    } else {
      setProgressValue(0);
      alert('Tidak bisa update progress untuk jo dengan status menunggu');
    }
  };

  /**
   * Handle start production
   * @param {UUID} job_order_id
   */
  const handleStartProduction = (job_order_id: UUID): void => {
    dispatch(jobOrder_setDialogStartProduction(true, job_order_id));
  };

  /**
   * Handle finish production.
   */
  const handleFinishProduction = (): void => {
    Swall.fire({
      title: 'Yakin ingin menyelesaikan status produksi ?',
      icon: 'warning',
      text: 'Job Order yang telah di selesaikan tidak dapat dikembalikan',
      showCancelButton: true,
      confirmButtonText: 'Ya, Selesaikan',
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(jobOrder_finishProduction({ job_order_id: data.id }));
      }
    });
  };

  const handleClickImage = (clickedIndex: number): void => {
    setClickedImageIndex(clickedIndex);
    setOpenImage(true);
  };

  if (data)
    return (
      <>
        {openImage && (
          <Lightbox
            mainSrc={data.images[clickedIndexImage].image_lg ?? ''}
            nextSrc={data.images[(clickedIndexImage + 1) % data.images.length].image_lg}
            prevSrc={data.images[(clickedIndexImage + data.images.length - 1) % data.images.length].image_lg}
            onCloseRequest={() => setOpenImage(false)}
            onMovePrevRequest={() =>
              setClickedImageIndex((clickedIndexImage + data.images.length - 1) % data.images.length)
            }
            onMoveNextRequest={() => setClickedImageIndex((clickedIndexImage + 1) % data.images.length)}
          />
        )}
        <Box sx={{ minHeight: 300 }}>
          <Grid container spacing={2}>
            {/* Left grid */}
            <Grid item xs={12} md={8}>
              <Box
                sx={{
                  mb: 2,
                  display: 'flex',
                  alignItems: 'center',
                }}
              >
                <Box
                  sx={{
                    px: 1.2,
                    py: 0.6,
                    maxWidth: 150,
                    borderRadius: 1,
                    textAlign: 'center',
                    backgroundColor: 'primary.main',
                    mr: 2,
                  }}
                >
                  <Typography sx={{ color: 'primary.contrastText', fontWeight: 700 }}>{data.order_number}</Typography>
                </Box>
                <Typography variant="subtitle2">{data.category.name}</Typography>
              </Box>

              <Box sx={{ mb: 2, overflow: 'hidden', maxHeight: 52 }}>
                <Typography component="h2" variant="h4">
                  {data.title}
                </Typography>
              </Box>

              <Divider sx={{ width: '95%', mb: 2 }} />

              {/* Production status progress */}
              <Box sx={{ mb: 2 }}>
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                    '& button': {
                      mr: 2,
                    },
                  }}
                >
                  {/* Start production button */}
                  {data.production_status.id === JobOrderProductionStatusIdConstants.Waiting && isProductionTeam(user) && (
                    <BaseButton
                      disableHoverEffect
                      onClick={() => handleStartProduction(data.id)}
                      variant="contained"
                      startIcon={<PlayArrowIcon />}
                    >
                      Mulai Produksi
                    </BaseButton>
                  )}

                  {/* Finish production button */}
                  {data.production_status.id === JobOrderProductionStatusIdConstants.InProgress &&
                    isProductionTeam(user) && (
                      <BaseButton
                        disableHoverEffect
                        onClick={handleFinishProduction}
                        variant="contained"
                        startIcon={<DoneIcon />}
                      >
                        Selesaikan
                      </BaseButton>
                    )}

                  {/* Label production status */}
                  <JobOrderLabelProductionStatus
                    status={data.production_status}
                    sx={{ textAlign: 'center', minWidth: 100 }}
                  />
                </Box>

                {data.production_status.id === JobOrderProductionStatusIdConstants.InProgress && (
                  <Box sx={{ width: 400 }}>
                    <PrettoSlider
                      // disabled={!data.percentage_progress}
                      aria-label="Job order production progress"
                      defaultValue={progressValue}
                      value={progressValue}
                      getAriaValueText={valuetext}
                      valueLabelDisplay="auto"
                      onChange={handleSliderChange}
                      size="small"
                      step={10}
                      min={0}
                      max={100}
                    />
                  </Box>
                )}
              </Box>

              {/* Preview */}
              <Box sx={{ mb: 4 }}>
                <Box
                  sx={{
                    width: '95%',
                    overflow: 'hidden',
                    '& img': {
                      width: '100%',
                      borderRadius: 1,
                    },
                  }}
                >
                  <Typography variant="subtitle2" sx={{ mb: 0.65 }}>
                    Preview file
                  </Typography>

                  {Array.isArray(data.images) &&
                    data.images.length > 0 &&
                    (data.images.length > 1 ? (
                      <ReactIDSwiper {...swiperConfig}>
                        {data.images.map((img, index) => (
                          <Box
                            key={img.id}
                            onClick={() => handleClickImage(index)}
                            sx={{
                              backgroundColor: '#eee',
                              width: 310,
                              height: 180,
                              overflow: 'hidden',
                              borderRadius: 1,
                              '& img': { width: '100%', height: 'auto' },
                            }}
                          >
                            <img src={img.image_sm} alt={data.title + '- image ' + index} />
                          </Box>
                        ))}
                      </ReactIDSwiper>
                    ) : (
                      <img onClick={() => handleClickImage(0)} src={data.images[0].image_lg} alt="Sample" />
                    ))}
                </Box>
              </Box>

              <Box sx={{ mb: 4, width: '95%' }}>
                <JobOrderNotes data={data.notes} joId={data.id} />
              </Box>
            </Grid>

            {/* Right grid */}
            <Grid item xs={12} md={4}>
              <Box sx={{ mb: 3 }}>
                {!isProductionTeam(user) && (
                  <JobOrderDetailPaymentStatus
                    currentPaymentStatus={data.transaction.payment_status}
                    jobOrderId={data.id}
                    totalOrder={data.total_order}
                    downPayment={data.transaction.down_payment}
                  />
                )}

                {/* Payment Method */}
                {data.transaction.payment_method && (
                  <Box sx={{ textAlign: 'center', '& img': { height: 26, width: 'auto' } }}>
                    <ListItem disableGutters disablePadding>
                      <ListItemAvatar>
                        <Avatar sx={{ backgroundColor: green[600], color: 'common.white' }}>
                          <AccountBalanceWalletIcon />
                        </Avatar>
                      </ListItemAvatar>
                      <BaseListItemText primary={data.transaction.payment_method.name} secondary="Metode Pembayaran" />
                    </ListItem>
                  </Box>
                )}
              </Box>

              {/* Date  */}
              <Box sx={{ mb: 3 }}>
                <Typography variant="body1" sx={{ mb: 1, color: 'text.secondary' }}>
                  Tanggal
                </Typography>
                {/* Date */}
                <Box sx={{ mb: 2 }}>
                  <List sx={{ width: '100%' }} disablePadding>
                    <ListItem disableGutters disablePadding>
                      <ListItemAvatar>
                        <Avatar>
                          <EventAvailableIcon />
                        </Avatar>
                      </ListItemAvatar>
                      <BaseListItemText
                        primary={moment(data.order_date, DB_FORMAT_TIMESTAMPS).format('DD MMM YYYY - HH:mm')}
                        secondary="Tanggal Order"
                      />
                    </ListItem>
                    <ListItem disableGutters disablePadding>
                      <ListItemAvatar>
                        <Avatar>
                          <EventBusyIcon />
                        </Avatar>
                      </ListItemAvatar>
                      <BaseListItemText
                        primary={moment(data.due_date, DB_FORMAT_TIMESTAMPS).format('DD MMM YYYY - HH:mm')}
                        secondary="Tanggal Deadline"
                      />
                    </ListItem>
                  </List>
                </Box>
              </Box>

              <Divider />

              <Box sx={{ my: 3 }}>
                <Typography variant="body1" sx={{ mb: 1, color: 'text.secondary' }}>
                  Bahan Baku Produksi
                </Typography>

                {data.materials.map((material, index) => (
                  <List key={String(index)} sx={{ width: '100%' }} disablePadding>
                    <ListItem disableGutters disablePadding>
                      <ListItemAvatar>
                        <Avatar>
                          <BookmarkIcon />
                        </Avatar>
                      </ListItemAvatar>
                      <BaseListItemText
                        primary={`${Number(Math.floor(material.usage.dimension_material_length))} ${
                          material.unit
                        } x ${Number(Math.floor(material.usage.dimension_material_width))} ${material.unit}`}
                        secondary="Ukuran ( P x L )"
                      />
                    </ListItem>

                    <ListItem disableGutters disablePadding>
                      <ListItemAvatar>
                        <Avatar>
                          <PersonIcon />
                        </Avatar>
                      </ListItemAvatar>
                      <BaseListItemText primary={material.name} secondary="Jenis Kertas" />
                    </ListItem>

                    <ListItem disableGutters disablePadding>
                      <ListItemAvatar>
                        <Avatar>
                          <BookmarkIcon />
                        </Avatar>
                      </ListItemAvatar>
                      <BaseListItemText primary={data.order_quantity} secondary="Quantity" />
                    </ListItem>
                  </List>
                ))}
              </Box>

              {/* Machine */}
              {data.production_status_id === JobOrderProductionStatusIdConstants.InProgress && (
                <Box>
                  <Divider />
                  <Typography variant="subtitle1">Mesin Produksi : </Typography>
                  {data.machines.length > 0 &&
                    data.machines.map((machine) => (
                      <Box key={String(machine.id)}>
                        <Box sx={{ lineHeight: 0 }}>
                          <Box
                            component="img"
                            sx={{ width: 74, height: 'auto', mr: 2 }}
                            src={machine.image_url ?? PrintingMachine}
                            alt="Production machine"
                          />
                        </Box>
                        <Box>
                          <Typography variant="h6">{machine.name}</Typography>
                        </Box>
                      </Box>
                    ))}
                </Box>
              )}

              {/* Customer information */}
              {data.customer && (
                <>
                  <Divider />
                  <Box
                    sx={{
                      mt: 2,
                    }}
                  >
                    <Box sx={{ mb: 3 }}>
                      <Typography variant="body1" sx={{ mb: 1, color: 'text.secondary' }}>
                        Informasi Pelanggan
                      </Typography>
                      <List sx={{ width: '100%' }} disablePadding>
                        <ListItem disableGutters disablePadding>
                          <ListItemAvatar>
                            <Avatar
                              src={data.customer.photo_url ?? null}
                              sx={{
                                backgroundColor:
                                  data.customer.avatar_text_color ?? colors[Math.floor(Math.random() * colors.length)],
                                color: 'primary.contrastText',
                              }}
                              alt={data.customer.name}
                            >
                              {getInitialsName(data.customer.name, true)}
                            </Avatar>
                          </ListItemAvatar>
                          <BaseListItemText primary={data.customer.name} secondary="Nama Pelanggan" />
                        </ListItem>
                        {data.customer.phone_number && (
                          <ListItem disableGutters disablePadding>
                            <ListItemAvatar>
                              <Avatar>
                                <LocalPhoneIcon />
                              </Avatar>
                            </ListItemAvatar>
                            <BaseListItemText primary={data.customer.phone_number} secondary="HP/Whatsapp" />
                          </ListItem>
                        )}
                        {/* {data.customer.addresses.length > 0 &&
                          data.customer.addresses.map((customerAddress) => (
                            <ListItem key={customerAddress.id} disableGutters disablePadding>
                              <ListItemAvatar>
                                <Avatar>
                                  <LocationOnIcon />
                                </Avatar>
                              </ListItemAvatar>
                              <BaseListItemText
                                primary={`${customerAddress.address_details} ${customerAddress.district} ${customerAddress.city} ${customerAddress.province} ${customerAddress.postcode}`}
                                secondary="Alamat"
                              />
                            </ListItem>
                          ))} */}
                      </List>
                    </Box>
                  </Box>
                </>
              )}

              {/* CTA */}
              {/* {!isProductionTeam(user) && data.customer && (
                <>
                  {data.customer.phone_number !== null && (
                    <Box sx={{ mt: 1.6, textAlign: 'center' }}>
                      <Button
                        component="a"
                        target="_blank"
                        href={`https://api.whatsapp.com/send?phone=${createWhatsappPhoneNumber(
                          data.customer.phone_number,
                        )}&text=`}
                        startIcon={<WhatsApp />}
                        variant="contained"
                        disableElevation
                        sx={{ backgroundColor: '#00a248' }}
                      >
                        Chat Pelanggan
                      </Button>
                    </Box>
                  )}
                </>
              )} */}
            </Grid>

            {/* Center grid bottom */}
            <Grid item xs={12}>
              {/* <Box sx={{ mb: 3, textAlign: 'center' }}>
                <Typography variant="body1" sx={{ mb: 1, color: 'text.secondary' }}>
                  Update Status Pembayaran
                </Typography>
              </Box>
              <Box sx={{ mb: 2, textAlign: 'center' }}>
                <Typography variant="body1" sx={{ mb: 1, color: 'text.secondary' }}>
                  Update Status Produksi
                </Typography>
                <ToggleUpdateProductionStatus joId={data.id} selectedId={data.production_status.id} />
              </Box> */}
            </Grid>
          </Grid>
        </Box>
        {/* Dialog input down payment */}
        {/* <DialogInputDownPayment /> */}
      </>
    );
  return null;
};
