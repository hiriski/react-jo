import { Box, Grid } from '@mui/material'
import { SectionTitle } from '@src/components/ui'
import { useAppSelector } from '@src/store/hook'
import { TMaterialPaper } from '@src/types/inventory'
import { FC } from 'react'

import {
  MaterialPaperEmpty,
  MaterialPaperItem,
  MaterialPaperItemSkeleton,
} from '../index'

type Props = {
  items: Array<TMaterialPaper>
}

const MaterialPaperList: FC<Props> = ({ items }: Props) => {
  const { isFetching } = useAppSelector((state) => state.inventory)
  return (
    <>
      <SectionTitle title="Material Kertas" />
      <Grid container spacing={0}>
        <Grid item xs={12} md={12}>
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'column',
            }}
          >
            <Grid container spacing={1}>
              {isFetching
                ? new Array(4).fill('.').map((item, index) => (
                    <Grid item xs={12} key={String(item + index)}>
                      <MaterialPaperItemSkeleton />
                    </Grid>
                  ))
                : items.map((item) => (
                    <Grid key={String(item.id)} item xs={12}>
                      <MaterialPaperItem item={item} />
                    </Grid>
                  ))}
            </Grid>
          </Box>
          {!isFetching && items.length === 0 && (
            <MaterialPaperEmpty text="Empty :(" />
          )}
        </Grid>
      </Grid>
    </>
  )
}

export default MaterialPaperList
