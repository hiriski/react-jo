export const MaterialTypeIdConstant = {
  MaterialSticker: 1,
  MaterialBanner: 2,
  MaterialPaperArtPaper: 3,
  MaterialAcrylic: 4,
  Lamination: 5,
};

export const MaterialUnitConstants = {
  mm: 'mm',
  cm: 'cm',
  m: 'm',
  inch: 'inch',
  liter: 'liter',
  gram: 'gram',
  kg: 'kg',
  pcs: 'pcs',
  sheet: 'sheet',
  ream: 'ream',
  roll: 'roll',
};
