export type TPayloadDialogAddEditInventory = {
  open: boolean
  brand_id?: number | null
  type: 'brand_material_paper' | 'material_paper' | null
  id?: number
}

export type TPayloadDrawerDetailInventory = Omit<TPayloadDialogAddEditInventory, 'brand_id'>

export type PayloadAddStockMaterialPaper = {
  id: number
  add_stock_length: number
}
