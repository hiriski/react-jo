import { lazy } from 'react';

const SupportCenterPage = lazy(() => import('./support-center'));

export default SupportCenterPage;
