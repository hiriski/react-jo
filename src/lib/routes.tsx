import ProductPage from '@/pages/product/product-page';
import {
  AccountPage,
  AnalyticsPage,
  InventoryPage,
  InvoiceGeneratorPage,
  InvoicePage,
  JobOrderPage,
  LoginPage,
  OverviewPage,
  PageNotFound,
  RolePage,
  SettingPage,
  WarehousePage,
  SupportCenterPage,
  UserPage,
  AddEditMaterialPage,
  AddEditJobOrderPage,
} from '@src/pages';

import { CustomerPage, AddEditCustomerPage } from '@/pages/customer';

import { Navigate, Outlet, RouteObject } from 'react-router-dom';

// Constants.
import { ROUTES } from '@/constants/routes';
import { LockScreen } from '@/pages/lock-screen';

const routes = (isLoggedIn: boolean, appIsLocked: boolean): Array<RouteObject> => [
  {
    path: '/auth',
    element: <Outlet />,
    children: [
      {
        path: ROUTES.SIGNIN,
        element: <LoginPage />,
      },
    ],
  },
  {
    path: '/',
    element: !isLoggedIn ? <Navigate to={ROUTES.SIGNIN} /> : appIsLocked ? <LockScreen /> : <Outlet />,
    children: [
      {
        path: 'analytics',
        element: <AnalyticsPage />,
      },
      {
        path: 'inventory',
        element: <InventoryPage />,
      },
      {
        path: 'job-order',
        element: <Outlet />,
        children: [
          {
            path: 'add',
            element: <AddEditJobOrderPage />,
          },
          {
            path: 'edit/:id',
            element: <AddEditJobOrderPage />,
          },
          {
            path: '',
            element: <JobOrderPage />,
          },
        ],
      },
      {
        path: 'user',
        element: <UserPage />,
      },
      {
        path: 'role',
        element: <RolePage />,
      },
      {
        path: 'invoice',
        element: <InvoicePage />,
      },
      {
        path: 'warehouse',
        element: <Outlet />,
        children: [
          {
            path: 'material/add',
            element: <AddEditMaterialPage />,
          },
          {
            path: '',
            element: <WarehousePage />,
          },
        ],
      },
      {
        path: 'settings',
        element: <SettingPage />,
      },
      {
        path: 'account',
        element: <AccountPage />,
      },
      {
        path: 'support-center',
        element: <SupportCenterPage />,
      },
      {
        path: 'customer',
        element: <Outlet />,
        children: [
          {
            path: 'add',
            element: <AddEditCustomerPage />,
          },
          {
            path: 'edit/:id',
            element: <AddEditCustomerPage />,
          },
          {
            path: '',
            element: <CustomerPage />,
          },
        ],
      },
      {
        path: 'invoice-generator',
        element: <InvoiceGeneratorPage />,
      },
      {
        path: 'product',
        element: <ProductPage />,
        children: [{ path: ':uuid', element: <ProductPage /> }],
      },
      {
        path: '/',
        element: isLoggedIn ? <OverviewPage /> : <Navigate to={ROUTES.SIGNIN} />,
      },
      { path: '*', element: <PageNotFound /> },
    ],
  },
];

export default routes;
