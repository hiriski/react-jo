export interface IAnalyticsTotalJobOrder {
  total: number;
  waiting: number;
  in_progress: number;
  done: number;
}
export type JobOrderTotalPeriod = 'overtime' | 'today' | 'week' | 'month' | 'year';
