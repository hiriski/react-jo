import { FC, MouseEvent, useState } from 'react';
import { BaseAnalyticsWidget, BaseCard } from '@/components/base';
import { green, blue, indigo, orange } from '@mui/material/colors';
import { AlbumsOutline, FolderOpenOutline, SyncOutline, TimeOutline } from 'react-ionicons';

import { Grid, Box, Typography, Avatar, ToggleButtonGroup, ToggleButton, styled, alpha } from '@mui/material';

// Apex chart.
import ReactApexChart from 'react-apexcharts';
import { ApexOptions } from 'apexcharts';

import merge from 'lodash/merge';
import { baseOptionsChart } from '@/components/chart/base-options-chart';

const CHART_DATA = [
  {
    name: 'Print Stiker',
    type: 'column',
    data: [23, 11, 22, 27, 13, 22, 37, 21, 44, 22, 30],
  },
  {
    name: 'Laser Cutting',
    type: 'area',
    data: [44, 55, 41, 67, 22, 43, 21, 41, 56, 27, 43],
  },
  {
    name: 'Others',
    type: 'line',
    data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39],
  },
];

export const OverviewAnalyticsJobOrderByCategory: FC = () => {
  const chartOptions = merge(baseOptionsChart, {
    stroke: { width: [0, 2, 3] },
    plotOptions: { bar: { columnWidth: '14%' } },
    fill: { type: ['solid', 'gradient', 'solid'] },
    labels: [
      '01/01/2003',
      '02/01/2003',
      '03/01/2003',
      '04/01/2003',
      '05/01/2003',
      '06/01/2003',
      '07/01/2003',
      '08/01/2003',
      '09/01/2003',
      '10/01/2003',
      '11/01/2003',
    ],
    xaxis: { type: 'datetime' },
    tooltip: {
      shared: true,
      intersect: false,
      y: {
        formatter: (y) => {
          if (typeof y !== 'undefined') {
            return `${y.toFixed(0)} visits`;
          }
          return y;
        },
      },
    },
  });
  return (
    <BaseCard sx={{ position: 'relative' }}>
      <ReactApexChart type="line" series={CHART_DATA} options={chartOptions as ApexOptions} height={364} />
    </BaseCard>
  );
};
