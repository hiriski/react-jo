import { FC } from 'react';
import ListItemText, { ListItemTextProps } from '@mui/material/ListItemText';

export const BaseListItemText: FC<ListItemTextProps> = (props) => {
  return (
    <ListItemText
      sx={{
        display: 'flex',
        flexDirection: 'column-reverse',
        '& .MuiListItemText-secondary': { fontSize: '0.8rem', mb: 0.4 },
      }}
      {...props}
    />
  );
};
