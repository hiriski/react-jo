import { blueGrey, common } from '@mui/material/colors';

export default {
  background: {
    dark: '#F4F6F8',
    default: '#F9FAFB',
    paper: common.white,
  },
  primary: {
    light: '#f1faff',
    main: '#4790FF',
    dark: '#2352B7',
    contrastText: '#fbfbfb',
  },
  secondary: {
    light: '#FFDDAA',
    main: '#ff872c',
    dark: '#DB6620',
    contrastText: '#fbfbfb',
  },
  blue: {
    light: '#c0ceff',
    main: '#4d73ff',
    dark: '#253d92',
    contrastText: '#fff',
  },
  darkBlue: {
    light: '#5b6b9a',
    main: '#142042',
    dark: '#0d1223',
    contrastText: '#fbfbfb',
  },
  green: {
    light: '#51ef89',
    main: '#00b13f',
    dark: '#008630',
    contrastText: '#fff',
  },
  text: {
    primary: blueGrey[900],
    secondary: blueGrey[600],
  },
};
