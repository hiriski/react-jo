import { httpResponseOK } from '@/utils/http';
import { SagaIterator } from 'redux-saga';
import { SagaReturnType, SimpleEffect, call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';

import { setAlert } from '../alert/actions';
import { ANALYTICS_ACTION_TYPES as ActionTypes } from './analytics-action-types.enum';
// Action types
import { fetchTotalJobOrderFailure, fetchTotalJobOrderLoading, fetchTotalJobOrderSuccess } from './analytics-actions';

// Api service
import AnalyticsApiService from './analytics-api-service';
import { JobOrderTotalPeriod } from './analytics.interface';

type APIResponseProductList = SagaReturnType<typeof AnalyticsApiService.getTotalJobOrder>;

function* fetchTotalJobOrder({
  payload,
}: SimpleEffect<
  typeof ActionTypes.FETCH_TOTAL_JOB_ORDER_REQUESTED,
  { period?: JobOrderTotalPeriod; date?: string }
>): SagaIterator {
  yield put(fetchTotalJobOrderLoading());
  try {
    const { status, data }: APIResponseProductList = yield call(
      AnalyticsApiService.getTotalJobOrder,
      payload.period,
      payload.date,
    );
    if (httpResponseOK(status)) {
      yield put(fetchTotalJobOrderSuccess(data, payload.period, payload.date));
    }
  } catch (e) {
    yield put(fetchTotalJobOrderFailure());
  }
}

export default function* analyticsSaga(): SagaIterator {
  yield takeEvery(ActionTypes.FETCH_TOTAL_JOB_ORDER_REQUESTED, fetchTotalJobOrder);
}
