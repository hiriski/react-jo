import {
  TBrandMaterialPaper,
  TBrandWithMaterialPaper,
  TCreateBrandMaterialPaper,
  TCreateMaterialPaper,
  TMaterialPaper,
  TUpdateMaterialPaper,
} from '@src/types/inventory'
import api from '@src/utils/http'
import { AxiosResponse } from 'axios'

const InventoryAPI = {
  findBrandMaterialPaper: async (id: number): Promise<AxiosResponse<TBrandMaterialPaper>> =>
    api.get('/inventory/material-paper/brand'),

  findAllBrandMaterialPaper: async (params: Record<string, string>): Promise<AxiosResponse<TBrandMaterialPaper[]>> =>
    api.get(`/inventory/material-paper/brand`, { params }),

  createBrandMaterialPaper: async (body: TCreateBrandMaterialPaper): Promise<AxiosResponse<TBrandMaterialPaper>> =>
    api.post(`/inventory/material-paper/brand`, body),

  findMaterialPaper: async (id: number): Promise<AxiosResponse<TMaterialPaper>> =>
    api.get(`/inventory/material-paper/${id}`),

  findAllMaterialPaper: async (params: Record<string, string>): Promise<AxiosResponse<TMaterialPaper[]>> =>
    api.get(`/inventory/material-paper`, { params }),

  createMaterialPaper: async (body: TCreateMaterialPaper): Promise<AxiosResponse<TMaterialPaper>> =>
    api.post(`/inventory/material-paper`, body),

  addStockLengthMaterialPaper: async (id: number, add_stock_length: number): Promise<AxiosResponse<TMaterialPaper>> =>
    api.put(`/inventory/material-paper/add-stock-length/${id}`, {
      add_stock_length,
    }),

  updateMaterialPaper: async (id: number, body: TUpdateMaterialPaper): Promise<AxiosResponse<TMaterialPaper>> =>
    api.put(`/inventory/material-paper/${id}`, body),

  findAllV2: async (): Promise<AxiosResponse<Array<TBrandWithMaterialPaper>>> =>
    api.get('/inventory/material-paper-v2'),
}

export default InventoryAPI
