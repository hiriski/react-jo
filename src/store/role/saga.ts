import type { Effect, SagaIterator } from '@redux-saga/types';
import { httpResponseOK } from '@src/utils/http';
import { SagaReturnType, call, put, takeEvery } from 'redux-saga/effects';

import { fetchingListRoleSuccess } from './actions';
import * as ActionTypes from './constants';
import RoleAPI from './service';

type APIResponseListRole = SagaReturnType<typeof RoleAPI.findAll>;

function* fetchListRole({ payload }: Effect<string>): SagaIterator {
  // eslint-disable-next-line no-console
  yield put({ type: ActionTypes.FETCHING_LIST_ROLE_LOADING });
  try {
    const response: APIResponseListRole = yield call(RoleAPI.findAll);
    if (httpResponseOK(response.status)) {
      const { data } = response;
      yield put(fetchingListRoleSuccess(data));
    }
  } catch (e) {
    yield put({ type: ActionTypes.FETCHING_LIST_ROLE_FAILURE });
  }
}

export default function* roleSaga(): SagaIterator {
  yield takeEvery(ActionTypes.FETCHING_LIST_ROLE_REQUESTED, fetchListRole);
}
