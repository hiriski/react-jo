import { Box, Container, useMediaQuery, Typography } from '@mui/material';
import { alpha, styled, useTheme } from '@mui/material/styles';
import AppAppBar from '@src/components/appbar';
import PopUpSearchBox from '@src/components/popup-search-box';
import { Sidebar } from '@src/components/sidebar';
import { useAppSelector } from '@src/store/hook';
import { fetchListRole } from '@src/store/role/actions';
import { SIDEBAR_DRAWER_WIDTH } from '@src/utils/constants';
import { FC, ReactElement, ReactNode, useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
// import { TransitionLayout } from '@/components/layouts';

import { DialogAddEditInventory } from '../inventory';
// import BlueBackgroundHeader from '@/assets/images/blue_layout_background.jpg';
// import LayoutHeaderImage from '@/assets/images/spacejoy-7fX2YfJIrOQ-unsplash.jpg';

// Components.
import {
  DrawerAddEditJobOrder,
  JobOrderDialogDelete,
  JobOrderDialogDetail,
  JobOrderDialogQr,
  JobOrderDialogStartProduction,
} from '@/components/job-order';
import { WarehouseDialogAddEditMaterial, WarehouseDialogAddStockMaterial } from '@/components/warehouse';
import { setSidebarDrawer } from '@/store/common/actions';
import { DialogDetailCustomer } from '../customer';
import { app_rootSelector } from '@/store/app/app-selectors';

// Components.
import Spinner from '@atlaskit/spinner';

// App config.
import { AppConfig } from '@/config';

const Main = styled('main')(({ theme }) => ({
  display: 'flex',
  flexWrap: 'wrap',
}));

type Props = {
  children: ReactNode;
};

const MainLayout: FC<Props> = ({ children }: Props) => {
  const { pathname } = useLocation();
  const dispatch = useDispatch();
  const { visibleSidebarDrawer } = useAppSelector((state) => state.common);
  const { app_isLoading } = useAppSelector((state) => app_rootSelector(state));

  const { listRole } = useAppSelector((state) => state.role);

  // Mui hooks.
  const theme = useTheme();
  const matchSmallScreen = useMediaQuery(theme.breakpoints.down('md'));

  // useEffect(() => {
  //   if (Array.isArray(listRole) && listRole.length === 0) {
  //     dispatch(fetchListRole())
  //   }
  // }, [])

  const renderSpacer: ReactElement = <Box sx={{ height: (theme) => theme.spacing(3) }} />;

  const initialize = async (): Promise<void | boolean> => {
    dispatch(fetchListRole());
  };

  /**
   * Handle user key press
   */
  // const handleUseKeyPress = useCallback((event: KeyboardEvent) => {
  //   const { keyCode } = event;
  //   if (keyCode === 32 /** Space key */) {
  //     dispatch(setSidebarDrawer(!visibleSidebarDrawer));
  //   }
  // }, []);

  // useEffect(() => {
  //   window.addEventListener('keydown', handleUseKeyPress);
  //   return () => {
  //     window.removeEventListener('keydown', handleUseKeyPress);
  //   };
  // }, []);

  return (
    <>
      <AppAppBar />
      <Sidebar />
      <PopUpSearchBox />
      {/* <TransitionLayout pathname={pathname}> */}

      <Main>
        <Box
          sx={{
            width: visibleSidebarDrawer ? SIDEBAR_DRAWER_WIDTH : 0,
            height: '100vh',
            transition: theme.transitions.create(['width'], {
              easing: theme.transitions.easing.easeOut,
              duration: theme.transitions.duration.enteringScreen,
            }),

            ...(matchSmallScreen && {
              display: 'none',
            }),
          }}
        />
        <Box
          sx={{
            position: 'relative',
            width: visibleSidebarDrawer ? `calc(100% - ${SIDEBAR_DRAWER_WIDTH}px)` : '100%',
            transition: theme.transitions.create(['width'], {
              easing: theme.transitions.easing.easeOut,
              duration: theme.transitions.duration.enteringScreen,
            }),

            ...(app_isLoading && {
              overflow: 'hidden',
              maxHeight: '100vh',
            }),

            ...(matchSmallScreen && {
              width: '100% !important',
            }),
          }}
        >
          {/* Layout loading */}
          {app_isLoading && (
            <Box
              sx={{
                position: 'absolute',
                zIndex: (theme) => Number(theme.zIndex.snackbar) + 1,
                top: 0,
                right: 0,
                width: '100%',
                display: 'flex',
                height: '100vh',
                alignItems: 'center',
                flexDirection: 'column',
                justifyContent: 'center',
                backgroundColor: (theme) => alpha(theme.palette.background.paper, 0.85),
                '& svg > circle': { stroke: (theme) => theme.palette.primary.main + ' !important' },
              }}
            >
              <Spinner size={34} appearance="inherit" />
              <Typography sx={{ color: 'text.secondary', mt: 2 }} variant="subtitle1">
                {AppConfig.LoadingText}
              </Typography>
            </Box>
          )}
          <Container maxWidth={AppConfig.ContainerFullWidth ? false : 'lg'}>
            <Box
              sx={{
                pt: '54px',
                flex: 1,
                display: 'flex',
                minHeight: '100vh',
                flexDirection: 'column',
              }}
            >
              {/* Spacer */}
              {renderSpacer}

              {/* children */}
              {children}

              {/* Spacer */}
              {renderSpacer}
            </Box>
          </Container>
        </Box>
      </Main>
      {/* </TransitionLayout> */}

      {/* Drawer add edit job order  */}
      <DrawerAddEditJobOrder />

      {/* Dilaog add edit inventory  */}
      <DialogAddEditInventory />

      {/* Dialog detail job order */}
      <JobOrderDialogDetail />

      {/* Dialog Detail customer */}
      <DialogDetailCustomer />

      {/* Dialog Job Order Qr */}
      <JobOrderDialogQr />

      {/* Dialog Start Production Job Order */}
      <JobOrderDialogStartProduction />

      {/* Dialog Delete Job Order */}
      <JobOrderDialogDelete />

      {/* Dialog add stock material */}
      <WarehouseDialogAddStockMaterial />

      {/* Dialog add edit material */}
      <WarehouseDialogAddEditMaterial />
    </>
  );
};
export default MainLayout;
