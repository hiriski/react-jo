import * as ActionTypes from './constants';
import { AuthAction } from './actions';
import { IUser } from '@/interfaces/user';
import { IStateFailure } from '../../types/common';

const initialStateError: IStateFailure = {
  status: false,
  message: null,
  messages: null,
};

export interface AuthState {
  isAuthenticated: boolean;
  loginLoading: boolean;
  loginError?: IStateFailure;
  authenticatedUser?: IUser | null;
  provider: string | null;

  registerLoading: boolean;
  registerError?: IStateFailure;
  registerSuccess: boolean;

  unlockLoading: boolean;
  unlockFailure: boolean;

  fetchAuthenticatedUserLoading: boolean;
  fetchAuthenticatedUserFailure: boolean;
}

const initialState = {
  loginLoading: false,
  loginError: initialStateError,
  isAuthenticated: false,
  authenticatedUser: null,
  provider: null,

  registerLoading: false,
  registerError: initialStateError,
  registerSuccess: false,

  unlockLoading: false,
  unlockFailure: false,

  fetchAuthenticatedUserLoading: false,
  fetchAuthenticatedUserFailure: false,
};

const authReducer = (state: AuthState = initialState, action: AuthAction): AuthState => {
  switch (action.type) {
    case ActionTypes.LOGIN_LOADING:
      return {
        ...state,
        authenticatedUser: null,
        loginLoading: true,
        // isAuthenticated: false,
      };
    case ActionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        authenticatedUser: action.payload,
        loginError: initialStateError,
        loginLoading: false,
        // isAuthenticated: true,
      };
    case ActionTypes.LOGIN_FAILURE:
      return {
        ...state,
        authenticatedUser: null,
        loginError: action.payload ?? /* coming from saga --> */ { ...state.loginError, status: true },
        loginLoading: false,
      };
    case ActionTypes.REGISTER_LOADING:
      return {
        ...state,
        authenticatedUser: null,
        registerLoading: true,
        // isAuthenticated: false,
      };
    case ActionTypes.REGISTER_SUCCESS:
      return {
        ...state,
        authenticatedUser: action.payload,
        registerError: initialStateError,
        registerLoading: false,
        // isAuthenticated: true,
      };
    case ActionTypes.REGISTER_FAILURE:
      return {
        ...state,
        authenticatedUser: null,
        registerError: action.payload ?? /* coming from saga --> */ { ...state.loginError, status: true },
        loginLoading: false,
      };

    case ActionTypes.LOGIN_WITH_SOCIAL_ACCOUNT_LOADING:
      return {
        ...state,
        authenticatedUser: null,
        loginLoading: true,
        provider: null,
        // isAuthenticated: false,
      };
    case ActionTypes.LOGIN_WITH_SOCIAL_ACCOUNT_FAILURE:
      return {
        ...state,
        authenticatedUser: null,
        loginError: action.payload ?? /* coming from saga --> */ { ...state.loginError, status: true },
        loginLoading: false,
      };
    case ActionTypes.LOGIN_WITH_SOCIAL_ACCOUNT_SUCCESS:
      return {
        ...state,
        provider: action.payload.provider,
        authenticatedUser: action.payload.user,
        // isAuthenticated: true,
      };

    case ActionTypes.UNLOCK_APP_LOADING:
      return {
        ...state,
        unlockLoading: true,
        unlockFailure: false,
      };

    case ActionTypes.UNLOCK_APP_FAILURE:
      return {
        ...state,
        unlockLoading: false,
        unlockFailure: true,
      };

    case ActionTypes.UNLOCK_APP_SUCCESS:
      return {
        ...state,
        unlockLoading: false,
        unlockFailure: false,
      };

    case ActionTypes.FETCH_AUTHENTICATED_USER_LOADING:
      return {
        ...state,
        fetchAuthenticatedUserLoading: true,
        fetchAuthenticatedUserFailure: false,
      };

    case ActionTypes.FETCH_AUTHENTICATED_USER_FAILURE:
      return {
        ...state,
        fetchAuthenticatedUserLoading: false,
        fetchAuthenticatedUserFailure: true,
      };

    case ActionTypes.FETCH_AUTHENTICATED_USER_SUCCESS:
      return {
        ...state,
        fetchAuthenticatedUserLoading: false,
        fetchAuthenticatedUserFailure: false,
        authenticatedUser: action.payload,
        // isAuthenticated: true,
      };

    case ActionTypes.SET_IS_AUTHENTICATED:
      return {
        ...state,
        isAuthenticated: action.payload,
      };

    // (reset auth state)
    case ActionTypes.RESET_AUTH_STATE:
      return initialState;
    default:
      return state;
  }
};

export default authReducer;
