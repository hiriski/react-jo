import { IRequestBodyCustomer } from '@/interfaces/customer';
import { SectionTitle } from '@components/ui';
import { yupResolver } from '@hookform/resolvers/yup';
import PeopleIcon from '@mui/icons-material/People';
import { InputAdornment, SxProps } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';
import { createCustomer, customer_updateCustomer } from '@src/store/customer/customer-actions';
import { useAppSelector } from '@src/store/hook';
import { TCreateCustomer, TCustomer } from '@src/types/customer';
import { TBrandMaterialPaper } from '@src/types/inventory';
import { FC, useEffect } from 'react';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import * as yup from 'yup';

type TInputs = TCreateCustomer;

type SourceProps = { brands: Array<TBrandMaterialPaper> };

type Props = {
  sx?: SxProps;
  id?: string | null;
  data?: TCustomer | null;
  source?: SourceProps;
};

const defaultValues = {
  name: '',
  phone_number: '',
  email: '',
  address: [],
  notes: '',
};

const schema = yup.object().shape({
  name: yup.string().required('Nama bahan tidak boleh kosong'),
});

const FormCustomer: FC<Props> = ({ id, data, sx, source }: Props) => {
  const dispatch = useDispatch();
  const { createCustomer: create, updateCustomer: update } = useAppSelector((state) => state.customer);

  const isEditMode = Boolean(id);

  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
    reset,
  } = useForm({
    defaultValues,
    resolver: yupResolver(schema),
  });

  const onSubmit: SubmitHandler<Partial<TInputs>> = (values) => {
    if (!id) dispatch(createCustomer(values as IRequestBodyCustomer));
    else dispatch(customer_updateCustomer(id, values as IRequestBodyCustomer));
  };

  useEffect(() => {
    if (data) {
      setValue('name', data.name);
      setValue('phone_number', data.phone_number);
      setValue('email', data.email);
      // setValue('address', data.address)
      setValue('notes', data.notes);
    }
  }, [id, data]);

  return (
    <Box
      sx={{
        display: 'flex',
        fontWeight: 'bold',
        flexDirection: 'column',
        width: '100%',
        px: 4,
        py: 2,
        ...sx,
      }}
    >
      <SectionTitle title={isEditMode ? 'Edit Data Pelanggan' : 'Buat Data Pelanggan Baru'} />
      <Box component="form" onSubmit={handleSubmit(onSubmit)}>
        <Box
          sx={{
            width: 88,
            height: 88,
            margin: '0 auto',
            mb: 2,
            cursor: 'pointer',
            borderWidth: 1,
            border: 'solid',
            borderStyle: 'solid',
            borderRadius: '88px', // don't use number
            textAlign: 'center',
            borderColor: 'divider',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <PeopleIcon sx={{ fontSize: 32 }} />
        </Box>

        <Controller
          name="name"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              fullWidth
              margin="dense"
              size="small"
              label="Nama Pelanggan"
              error={Boolean(errors.name?.message)}
              helperText={errors.name?.message ?? 'Nama Pelanggan'}
            />
          )}
        />
        <Controller
          name="phone_number"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              fullWidth
              margin="dense"
              size="small"
              label="HP/Telepon"
              error={Boolean(errors.phone_number?.message)}
              helperText={errors.phone_number?.message ?? 'HP/Telepon Pelanggan'}
            />
          )}
        />
        <Controller
          name="email"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              fullWidth
              margin="dense"
              size="small"
              label="E-mail"
              error={Boolean(errors.email?.message)}
              helperText={errors.email?.message ?? 'Email Pelanggan'}
            />
          )}
        />
        {/* <Controller
          name="address"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              fullWidth
              multiline
              rows={2}
              margin="dense"
              size="small"
              label="Alamat pelanggan"
              error={Boolean(errors.address?.message)}
              helperText={errors.address?.message ?? '(Opsional) Alamat pelanggan'}
            />
          )}
        /> */}
        <Controller
          name="notes"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              multiline
              fullWidth
              rows={3}
              margin="dense"
              size="small"
              label="Catatan"
              error={Boolean(errors.notes?.message)}
              helperText={
                errors.notes?.message
                  ? errors.notes?.message
                  : '(Opsional) Catatan khusus untuk mengingat data pelanggan'
              }
            />
          )}
        />

        {create.isLoading || update.isLoading ? (
          <Button disabled sx={{ mt: 1 }} type="submit" fullWidth variant="contained" disableElevation>
            <CircularProgress size={24} />
          </Button>
        ) : (
          <Button sx={{ mt: 1 }} type="submit" fullWidth variant="contained" disableElevation>
            {!isEditMode ? 'Simpan' : 'Update'}
          </Button>
        )}
      </Box>
    </Box>
  );
};

FormCustomer.defaultProps = {
  id: null,
  sx: {},
};

export default FormCustomer;
