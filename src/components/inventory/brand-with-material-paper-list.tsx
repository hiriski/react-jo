import AddOutlinedIcon from '@mui/icons-material/AddOutlined'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import FolderOutlinedIcon from '@mui/icons-material/FolderOutlined'
import TurnedInOutlinedIcon from '@mui/icons-material/TurnedInOutlined'
import Accordion from '@mui/material/Accordion'
import AccordionDetails from '@mui/material/AccordionDetails'
import AccordionSummary from '@mui/material/AccordionSummary'
import MuiAvatar from '@mui/material/Avatar'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Chip from '@mui/material/Chip'
import Divider from '@mui/material/Divider'
import Typography from '@mui/material/Typography'
import { setDialogAddEditBrandMaterial, setDialogAddEditInventory } from '@src/store/inventory/actions'
import { TBrandWithMaterialPaper } from '@src/types/inventory'
import { colors } from '@src/utils/colors'
import { FC, SyntheticEvent, memo, useState } from 'react'
import { useDispatch } from 'react-redux'

import { BrandWithMaterialPaperItem } from '.'

const Avatar = memo(() => (
  <MuiAvatar sx={{ fontSize: 22, mr: 3, backgroundColor: colors[Math.floor(Math.random() * colors.length)] }}>
    <FolderOutlinedIcon />
  </MuiAvatar>
))

type Props = {
  data: TBrandWithMaterialPaper[]
}

const BrandWithMaterialPaperList: FC<Props> = ({ data }: Props) => {
  const [expanded, setExpanded] = useState<string | false>(false)
  const dispatch = useDispatch()

  const handleAddInventory = (brand_id: number): void => {
    dispatch(setDialogAddEditInventory({ open: true, brand_id, type: 'material_paper', id: null }))
  }

  console.log(' data', data)

  const handleChange = (panel: string) => (event: SyntheticEvent, isExpanded: boolean) => {
    setExpanded(isExpanded ? panel : false)
  }

  const handleAddBrandMaterial = (): void => {
    dispatch(setDialogAddEditBrandMaterial(true, null))
  }

  return (
    <Box>
      {Array.isArray(data) && data.length > 0
        ? data.map((item) => {
            const panel: string = 'panel' + String(item.id)
            return (
              <Accordion
                key={String(item.id)}
                expanded={expanded === panel}
                onChange={handleChange(panel)}
                sx={{
                  boxShadow: 'none !important',
                  borderRadius: 1.2,
                  border: (theme) => `1px solid ${theme.palette.divider}`,
                  mb: 2,
                }}
              >
                <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1bh-content" id="panel1bh-header">
                  <Box sx={{ display: 'flex', flex: 1, alignItems: 'center', justifyContent: 'space-between' }}>
                    <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-start' }}>
                      <Avatar />
                      <Box>
                        <Box sx={{ display: 'flex', alignItems: 'center' }}>
                          <Typography sx={{ fontSize: 19 }} variant="h5" component="h4">
                            {item.name}
                          </Typography>
                          <Chip
                            sx={{ height: 22, ml: 1, backgroundColor: '#e2efff', color: '#005eb2' }}
                            label="Brand"
                          />
                        </Box>
                        <Typography variant="subtitle2" sx={{ color: 'text.disabled' }}>
                          {item.available_sizes ? item.available_sizes : '-'}
                        </Typography>
                      </Box>
                    </Box>

                    <Chip
                      sx={{ color: 'text.secondary', mr: 4 }}
                      icon={<TurnedInOutlinedIcon sx={{ fontSize: 18 }} />}
                      label={item.material_counts + ' Material'}
                    />
                  </Box>
                </AccordionSummary>
                <Divider />
                <AccordionDetails sx={{ mt: 1, p: 0 }}>
                  {Array.isArray(item.materials) && item.materials.length > 0 ? (
                    <>
                      <Box sx={{ ml: 10, mt: 2 }}>
                        <Typography variant="subtitle2" color="text.disabled">
                          List Bahan :
                        </Typography>
                      </Box>
                      {item.materials.map((mItem) => (
                        <BrandWithMaterialPaperItem key={String(mItem.id)} item={mItem} />
                      ))}
                    </>
                  ) : (
                    <Typography>Empty</Typography>
                  )}

                  <Box sx={{ textAlign: 'center', mt: 3, mb: 3 }}>
                    <Button
                      onClick={() => handleAddInventory(item.id)}
                      startIcon={<AddOutlinedIcon />}
                      variant="outlined"
                      size="small"
                    >
                      Tambah Material
                    </Button>
                  </Box>
                </AccordionDetails>
              </Accordion>
            )
          })
        : null}
      <Box
        sx={{
          mt: 3,
          px: 2,
          py: 1.6,
          cursor: 'pointer',
          borderRadius: 1.2,
          textAlign: 'center',
          border: (theme) => `2px dashed ${theme.palette.divider}`,
          '&:hover': {
            backgroundColor: 'background.default',
          },
        }}
        onClick={handleAddBrandMaterial}
      >
        <Typography variant="subtitle2" sx={{ color: 'text.secondary', textTransform: 'uppercase' }}>
          Tambah Brand Material
        </Typography>
      </Box>
    </Box>
  )
}

export default BrandWithMaterialPaperList
