export interface IResponseErrorValidation {
  message: string;
  errors: {
    [key: string]: Array<string>;
  };
}
