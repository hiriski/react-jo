import type { Effect, SagaIterator, SimpleEffect } from '@redux-saga/types';
import { TCreateMaterialPaper } from '@src/types/inventory';
import { httpResponseCreated, httpResponseOK } from '@src/utils/http';
import { SagaReturnType, call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';

import { AppState } from '../root-reducer';
import {
  addStockLengthMaterialPaperFailure,
  addStockLengthMaterialPaperLoading,
  addStockLengthMaterialPaperSuccess,
  createInventoryMaterialPaperFailure,
  createInventoryMaterialPaperLoading,
  createInventoryMaterialPaperSuccess,
  fetchMaterialPaperFailure,
  fetchMaterialPaperLoading,
  fetchMaterialPaperSuccess,
  fetchingListBrandMaterialPaperSuccess,
  fetchingListMaterialPaperSuccess,
  fetchingListMaterialPaperV2Failure,
  fetchingListMaterialPaperV2Loading,
  fetchingListMaterialPaperV2Success,
  fetchListMaterialPaperV2 as refetchListrMaterialPaperv2,
  setDialogAddEditInventory,
  setDialogAddStockMaterialPaper,
} from './actions';
import * as ActionTypes from './constants';
import { InventoryState } from './reducer';
import InventoryAPI from './service';
import { PayloadAddStockMaterialPaper } from './types';

const getRootState = (state: AppState): InventoryState => state.inventory;

type APIResponseListBrandMaterialPaper = SagaReturnType<typeof InventoryAPI.findAllBrandMaterialPaper>;
type APIResponseListMaterialPaper = SagaReturnType<typeof InventoryAPI.findAllMaterialPaper>;
type APIResponseListBrandWithMaterialPaper = SagaReturnType<typeof InventoryAPI.findAllV2>;
type APIResponseCreateInventoryMaterialPaper = SagaReturnType<typeof InventoryAPI.createMaterialPaper>;
type APIResponseMaterialPaper = SagaReturnType<typeof InventoryAPI.findMaterialPaper>;
type APIResponseAddStockMaterialPaper = SagaReturnType<typeof InventoryAPI.addStockLengthMaterialPaper>;

function* fetchListBrandMaterialPaper({ payload }: Effect<string>): SagaIterator {
  // eslint-disable-next-line no-console

  yield put({ type: ActionTypes.FETCHING_LIST_BRAND_MATERIAL_PAPER_LOADING });
  try {
    const response: APIResponseListBrandMaterialPaper = yield call(InventoryAPI.findAllBrandMaterialPaper, payload);
    if (httpResponseOK(response.status)) {
      const { data } = response;
      yield put(fetchingListBrandMaterialPaperSuccess(data));
    }
  } catch (e) {
    yield put({ type: ActionTypes.FETCHING_LIST_BRAND_MATERIAL_PAPER_FAILURE });
  }
}

function* fetchListMaterialPaper({ payload }: Effect<string>): SagaIterator {
  // eslint-disable-next-line no-console
  console.log('-> payload fetchListMaterialPaper', payload);
  yield put({ type: ActionTypes.FETCHING_LIST_MATERIAL_PAPER_LOADING });
  try {
    const response: APIResponseListMaterialPaper = yield call(InventoryAPI.findAllMaterialPaper, payload);
    if (httpResponseOK(response.status)) {
      const { data } = response;
      yield put(fetchingListMaterialPaperSuccess(data));
    }
  } catch (e) {
    yield put({ type: ActionTypes.FETCHING_LIST_MATERIAL_PAPER_LOADING });
  }
}

function* fetchListBrandWithMaterialPaper(): SagaIterator {
  yield put(fetchingListMaterialPaperV2Loading());
  try {
    const response: APIResponseListBrandWithMaterialPaper = yield call(InventoryAPI.findAllV2);
    if (httpResponseOK(response.status)) {
      const { data } = response;
      yield put(fetchingListMaterialPaperV2Success(data));
    }
  } catch (e) {
    yield put(fetchingListMaterialPaperV2Failure());
  }
}

function* createInventoryMaterialPaper({ payload }: Effect<TCreateMaterialPaper>): SagaIterator {
  yield put(createInventoryMaterialPaperLoading());
  try {
    const response: APIResponseCreateInventoryMaterialPaper = yield call(InventoryAPI.createMaterialPaper, payload);
    if (httpResponseCreated(response.status)) {
      const { data } = response;
      yield put(createInventoryMaterialPaperSuccess(data));

      // close dialog
      yield put(setDialogAddEditInventory({ open: false, brand_id: null, id: null, type: null }));

      // refetch list
      yield put(refetchListrMaterialPaperv2());
    }
  } catch (e) {
    yield put(createInventoryMaterialPaperFailure());
  }
}

function* fetchMaterialPaperById({
  payload,
}: SimpleEffect<typeof ActionTypes.FETCH_MATERIAL_PAPER_REQUESTED, number>): SagaIterator {
  yield put(fetchMaterialPaperLoading());
  try {
    const response: APIResponseMaterialPaper = yield call(InventoryAPI.findMaterialPaper, payload);
    if (httpResponseOK(response.status)) {
      const { data } = response;
      yield put(fetchMaterialPaperSuccess(data));
    }
  } catch (e) {
    yield put(fetchMaterialPaperFailure());
  }
}

type ActionAddStockMaterialPaper = SimpleEffect<
  typeof ActionTypes.ADD_STOCK_LENGTH_MATERIAL_PAPER_REQUESTED,
  PayloadAddStockMaterialPaper
>;

function* addStockMaterialPaper({ payload }: ActionAddStockMaterialPaper): SagaIterator {
  console.log('Payload', payload);
  yield put(addStockLengthMaterialPaperLoading());
  try {
    const response: APIResponseAddStockMaterialPaper = yield call(
      InventoryAPI.addStockLengthMaterialPaper,
      payload.id,
      payload.add_stock_length,
    );
    if (httpResponseOK(response.status)) {
      const { data } = response;
      yield put(addStockLengthMaterialPaperSuccess(data));

      // close dialog
      yield put(setDialogAddStockMaterialPaper({ open: false, id: null, current_stock: null }));

      // refetch list
      yield put(refetchListrMaterialPaperv2());
    }
  } catch (e) {
    yield put(addStockLengthMaterialPaperFailure());
  }
}

export default function* inventorySaga(): SagaIterator {
  yield takeEvery(ActionTypes.FETCHING_LIST_BRAND_MATERIAL_PAPER_REQUESTED, fetchListBrandMaterialPaper);
  yield takeEvery(ActionTypes.FETCHING_LIST_MATERIAL_PAPER_REQUESTED, fetchListMaterialPaper);
  yield takeEvery(ActionTypes.FETCHING_LIST_MATERIAL_PAPER_V2_REQUESTED, fetchListBrandWithMaterialPaper);
  yield takeLatest(ActionTypes.CREATE_INVENTORY_MATERIAL_PAPER_REQUESTED, createInventoryMaterialPaper);
  yield takeLatest(ActionTypes.FETCH_MATERIAL_PAPER_REQUESTED, fetchMaterialPaperById);
  yield takeLatest(ActionTypes.ADD_STOCK_LENGTH_MATERIAL_PAPER_REQUESTED, addStockMaterialPaper);
}
