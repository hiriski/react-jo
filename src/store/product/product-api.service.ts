import { IProduct, IResponseProductList } from '@/types/product'
import api from '@/utils/http'
import { AxiosResponse } from 'axios'

const ProductAPI = {
  findAll: async (params?: Record<string, string | number>): Promise<AxiosResponse<IResponseProductList>> =>
    api.get(`/product`, { params }),

  find: async (id: string): Promise<AxiosResponse<IProduct>> => api.get(`/product/${id}`),

  create: async (body: IProduct): Promise<AxiosResponse<IProduct>> => api.post(`/product`, body),
}

export default ProductAPI
