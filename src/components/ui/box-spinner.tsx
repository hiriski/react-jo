import Spinner, { Size as SpinnerSize } from '@atlaskit/spinner';
import { Box, SxProps, alpha } from '@mui/material';
import { FC } from 'react';

type Props = {
  sx?: SxProps;
  height?: number | string;
  spinnerSize?: SpinnerSize;
  opacity?: number;
};

const BoxSpinner: FC<Props> = ({ sx, height, spinnerSize, opacity }: Props) => (
  <Box
    sx={{
      display: 'flex',
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      height: '100%',
      // height,
      backgroundColor: (theme) => alpha(theme.palette.background.paper, opacity),
      '& svg > circle': { stroke: (theme) => theme.palette.primary.main + ' !important' },
      ...sx,
    }}
  >
    <Spinner size={spinnerSize} appearance="inherit" />
  </Box>
);

BoxSpinner.defaultProps = {
  sx: {},
  spinnerSize: 36,
  opacity: 0.85,
  height: '100%',
};

export default BoxSpinner;
