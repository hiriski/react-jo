import Spinner, { Size as SpinnerSize } from '@atlaskit/spinner'
import Box from '@mui/material/Box'
import { FC } from 'react'

type Props = {
  size?: SpinnerSize
}

const SuspenseFallback: FC<Props> = ({ size }: Props) => (
  <Box
    sx={{
      display: 'flex',
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      minHeight: '100vh',
      '& svg': { stroke: (theme) => theme.palette.primary.main },
    }}
  >
    <Spinner size={size} appearance="inherit" />
  </Box>
)

SuspenseFallback.defaultProps = {
  size: 'medium',
}

export default SuspenseFallback
