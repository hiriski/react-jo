import Spinner from '@atlaskit/spinner'
import MuiButton, { ButtonProps } from '@mui/material/Button'
import { FC, ReactElement } from 'react'

interface Props extends ButtonProps {
  isLoading?: boolean
}

const Button: FC<Props> = (props: Props) => {
  const { children, isLoading } = props
  const renderSpinner = (): ReactElement => <Spinner size={29} />
  return <MuiButton {...props}>{!isLoading ? children : renderSpinner}</MuiButton>
}

Button.defaultProps = {
  isLoading: false,
}

export default Button
