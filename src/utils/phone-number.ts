export const createWhatsappPhoneNumber = (phoneNumber: string): string | undefined => {
  const CODE = '62'
  if (!phoneNumber) return undefined

  const sanitized_number = phoneNumber.toString().replace(/[- )(]/g, '') // remove unnecessary chars from the number

  // add 62 before the number here 62 is country code of Indonesia
  const final_number = `${CODE}${sanitized_number.substring(sanitized_number.length - 10)}`

  return final_number
}
