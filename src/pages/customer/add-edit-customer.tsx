import { BaseCard, BaseLoader } from '@/components/base';
import { PageTitle } from '@/components/shared';
import { BoxSpinner } from '@/components/ui';
import { MaterialForm } from '@/components/warehouse';
import { RequestBodyMasterData } from '@/interfaces/master-data';
import { useAppSelector } from '@/store/hook';
import { masterData_rootSelector } from '@/store/master-data/master-data-selectors';
import { warehouse_resetCreateMaterialState } from '@/store/warehouse/warehouse-actions';
import { warehouse_rootSelector } from '@/store/warehouse/warehouse-selectors';
import { ROUTES } from '@/utils/constants';
import { Grid } from '@mui/material';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { MainLayout } from '@src/components/layouts';
import { FC, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate, useParams } from 'react-router';

import { useTheme } from '@mui/material';
import { CustomerForm } from '@/components/customer';
import { customer_rootSelector } from '@/store/customer/customer-selectors';
import { customer_fetchCustomer, customer_resetCreateMaterialState } from '@/store/customer/customer-actions';

const AddEditCustomerPage: FC = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const theme = useTheme();
  const params = useParams();

  const { data: masterData, isFetching: isFetchingMasterData } = useAppSelector((state) =>
    masterData_rootSelector(state),
  );
  const { createCustomer, updateCustomer, detail } = useAppSelector((state) => customer_rootSelector(state));

  // useEffect(() => {
  //   dispatch(masterData_fetchMasterData(masterDataPayload));
  // }, []);

  /**
   * Navigate to warehouse page when create material success
   */
  useEffect(() => {
    if (createCustomer.isSuccess || updateCustomer.isSuccess) {
      navigate(ROUTES.CUSTOMER);
      return () => {
        dispatch(customer_resetCreateMaterialState());
      };
    }
    if (params.id) dispatch(customer_fetchCustomer(params.id));
    return () => undefined;
  }, [createCustomer.isSuccess, updateCustomer.isSuccess, params.id]);

  return (
    <MainLayout>
      <Grid container justifyContent="center">
        <Grid item sm={12} md={10}>
          <PageTitle
            title="Tambah Bahan Customer"
            // subtitle=""
            rightContent={
              <Box>
                <Typography>Right content</Typography>
              </Box>
            }
          />
          <BaseCard
            sx={{
              p: {
                xs: theme.spacing(5, 7),
                md: theme.spacing(8, 10),
              },
              position: 'relative',
              minHeight: 400,
            }}
          >
            {createCustomer.isLoading || detail.isFetching || isFetchingMasterData ? (
              <BaseLoader spinnerSize={48} />
            ) : (
              <CustomerForm id={params.id} data={detail.data} />
            )}
          </BaseCard>
        </Grid>
      </Grid>
    </MainLayout>
  );
};

export default AddEditCustomerPage;
