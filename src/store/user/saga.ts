import type { Effect, SagaIterator } from '@redux-saga/types'
import { TCreateUser } from '@src/types/user'
import { httpResponseCreated, httpResponseOK } from '@src/utils/http'
import { SagaReturnType, call, put, takeEvery, takeLatest } from 'redux-saga/effects'
import Swal from 'sweetalert2'

import {
  createUserFailure,
  createUserLoading,
  createUserSuccess,
  deleteUserFailure,
  deleteUserLoading,
  deleteUserSuccess,
  fetchUser as fetchUserAction,
  fetchingUserFailure,
  fetchUserList as fetchingUserListAction,
  fetchingUserListFailure,
  fetchingUserListLoading,
  fetchingUserListSuccess,
  fetchingUserLoading,
  fetchingUserSuccess,
  setDialogAddEditUser,
  setDialogDetailUser,
  updateUserFailure,
  updateUserLoading,
  updateUserSuccess,
} from './actions'
import * as ActionTypes from './constants'
import UserAPI from './service'

// Type definitions of return of result.
type APIResponseUserList = SagaReturnType<typeof UserAPI.findAll>
type APIResponseUser = SagaReturnType<typeof UserAPI.find>
type APIResponseCreateUser = SagaReturnType<typeof UserAPI.create>
type APIResponseUpdateUser = SagaReturnType<typeof UserAPI.update>
type APIResponseDeleteUser = SagaReturnType<typeof UserAPI.delete>

function* setDialogAddEditUserSaga({ payload }: Effect<{ open: boolean; id: number | null }>): SagaIterator {
  if (payload.id) {
    // re-fetch user list
    yield put(fetchUserAction(payload.id))
  }
  yield put({
    type: ActionTypes.SET_DIALOG_ADD_EDIT_USER,
    payload,
  })
}

function* fetchUserList({ payload }: Effect<Record<string, string>>): SagaIterator {
  yield put(fetchingUserListLoading())
  try {
    const response: APIResponseUserList = yield call(UserAPI.findAll, payload)
    if (httpResponseOK(response.status)) {
      const { data } = response
      yield put(fetchingUserListSuccess(data))
    }
  } catch (error) {
    yield put(fetchingUserListFailure())
  }
}

function* fetchUser({ payload }: Effect<{ id: number }>): SagaIterator {
  yield put(fetchingUserLoading())
  try {
    const response: APIResponseUser = yield call(UserAPI.find, payload)
    if (httpResponseOK(response.status)) {
      const { data } = response
      yield put(fetchingUserSuccess(data))
    }
  } catch (error) {
    yield put(fetchingUserFailure())
  }
}

function* createUser({ payload }: Effect<TCreateUser>): SagaIterator {
  yield put(createUserLoading())
  try {
    const response: APIResponseCreateUser = yield call(UserAPI.create, payload)
    if (httpResponseCreated(response.status)) {
      const { data } = response
      yield put(createUserSuccess(data))

      // re-fetch user list
      yield put(fetchingUserListAction())

      // close drawer
      yield put(setDialogAddEditUser(false, null))

      // fire swall.
      Swal.fire({
        icon: 'success',
        title: 'Okay...',
        text: 'User berhasil disimpan',
      })
    }
  } catch (error) {
    yield put(createUserFailure())
  }
}

function* updateUser({ payload }: Effect<{ id: number } & TCreateUser>): SagaIterator {
  yield put(updateUserLoading())
  try {
    const response: APIResponseUpdateUser = yield call(UserAPI.update, payload.id, payload.body)
    if (httpResponseOK(response.status)) {
      const { data } = response
      yield put(updateUserSuccess(data))

      // re-fetch user list
      yield put(fetchingUserListAction())

      // close drawer
      yield put(setDialogAddEditUser(false, null))

      // fire swall.
      Swal.fire({
        icon: 'success',
        title: 'Okay...',
        text: 'User berhasil di perbaharui',
      })
    }
  } catch (error) {
    yield put(updateUserFailure())
  }
}

function* deleteUser({ payload }: Effect): SagaIterator {
  yield put(deleteUserLoading())
  const userId = payload
  try {
    const response: APIResponseDeleteUser = yield call(UserAPI.delete, userId /* payload is a number (user id) */)
    if (httpResponseOK(response.status)) {
      yield put(deleteUserSuccess())

      // re-fetch user list
      yield put(fetchingUserListAction())

      // close dialog detail user.
      yield put(setDialogDetailUser(false, null))

      // fire swall. I will replace with toast later.
      Swal.fire({
        icon: 'success',
        title: 'Okay!',
        text: 'User berhasil dihapus',
      })
    }
  } catch (error) {
    yield put(deleteUserFailure())
  }
}

export default function* userSaga(): SagaIterator {
  yield takeEvery(ActionTypes.SET_DIALOG_ADD_EDIT_USER_REQUESTED, setDialogAddEditUserSaga)
  yield takeEvery(ActionTypes.FETCHING_USER_LIST_REQUESTED, fetchUserList)
  yield takeLatest(ActionTypes.CREATE_USER_REQUESTED, createUser)
  yield takeLatest(ActionTypes.UPDATE_USER_REQUESTED, updateUser)
  yield takeEvery(ActionTypes.FETCHING_USER_REQUESTED, fetchUser)
  yield takeLatest(ActionTypes.DELETE_USER_REQUESTED, deleteUser)
}
