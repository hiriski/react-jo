import { ActionPayloadDialog } from '@src/types/common'
import {
  TBrandMaterialPaper,
  TBrandWithMaterialPaper,
  TCreateMaterialPaper,
  TMaterialPaper,
} from '@src/types/inventory'

import * as ActionTypes from './constants'
import { PayloadAddStockMaterialPaper, TPayloadDialogAddEditInventory, TPayloadDrawerDetailInventory } from './types'

// Actions definiton.
interface SetDialogAddEditInventory {
  type: typeof ActionTypes.SET_DIALOG_ADD_EDIT_INVENTORY
  payload: TPayloadDialogAddEditInventory
}
interface SetDialogAddEditBrandMaterial {
  type: typeof ActionTypes.SET_DIALOG_ADD_EDIT_BRAND_MATERIAL
  payload: {
    open: boolean
    id: number | null
  }
}
interface SetDrawerDetailInventory {
  type: typeof ActionTypes.SET_DRAWER_DETAIL_INVENTORY
  payload: TPayloadDrawerDetailInventory
}

interface FetchingListBrandMaterialPaperRequested {
  type: typeof ActionTypes.FETCHING_LIST_BRAND_MATERIAL_PAPER_REQUESTED
  payload: Record<string, string>
}
interface FetchingListBrandMaterialPaperLoading {
  type: typeof ActionTypes.FETCHING_LIST_BRAND_MATERIAL_PAPER_LOADING
}
interface FetchingListBrandMaterialPaperFailure {
  type: typeof ActionTypes.FETCHING_LIST_BRAND_MATERIAL_PAPER_FAILURE
}
interface FetchingListBrandMaterialPaperSuccess {
  type: typeof ActionTypes.FETCHING_LIST_BRAND_MATERIAL_PAPER_SUCCESS
  payload: Array<TBrandMaterialPaper>
}

interface FetchingListMaterialPaperRequested {
  type: typeof ActionTypes.FETCHING_LIST_MATERIAL_PAPER_REQUESTED
  payload: Record<string, string>
}
interface FetchingListMaterialPaperLoading {
  type: typeof ActionTypes.FETCHING_LIST_MATERIAL_PAPER_LOADING
}
interface FetchingListMaterialPaperFailure {
  type: typeof ActionTypes.FETCHING_LIST_MATERIAL_PAPER_FAILURE
}
interface FetchingListMaterialPaperSuccess {
  type: typeof ActionTypes.FETCHING_LIST_MATERIAL_PAPER_SUCCESS
  payload: Array<TMaterialPaper>
}

interface FetchListMaterialPaperV2 {
  type: typeof ActionTypes.FETCHING_LIST_MATERIAL_PAPER_V2_REQUESTED
}
interface FetchingListMaterialPaperV2Loading {
  type: typeof ActionTypes.FETCHING_LIST_MATERIAL_PAPER_V2_LOADING
}
interface FetchingListMaterialPaperV2Failure {
  type: typeof ActionTypes.FETCHING_LIST_MATERIAL_PAPER_V2_FAILURE
}
interface FetchingListMaterialPaperV2Success {
  type: typeof ActionTypes.FETCHING_LIST_MATERIAL_PAPER_V2_SUCCESS
  payload: Array<TBrandWithMaterialPaper>
}

interface CreateInventoryMaterialPaper {
  type: typeof ActionTypes.CREATE_INVENTORY_MATERIAL_PAPER_REQUESTED
  payload: TCreateMaterialPaper
}
interface CreateInventoryMaterialPaperLoading {
  type: typeof ActionTypes.CREATE_INVENTORY_MATERIAL_PAPER_LOADING
}
interface CreateInventoryMaterialPaperFailure {
  type: typeof ActionTypes.CREATE_INVENTORY_MATERIAL_PAPER_FAILURE
}
interface CreateInventoryMaterialPaperSuccess {
  type: typeof ActionTypes.CREATE_INVENTORY_MATERIAL_PAPER_SUCCESS
  payload: TMaterialPaper
}

interface FetchMaterialPaper {
  type: typeof ActionTypes.FETCH_MATERIAL_PAPER_REQUESTED
  payload: number
}
interface FetchMaterialPaperLoading {
  type: typeof ActionTypes.FETCH_MATERIAL_PAPER_LOADING
}
interface FetchMaterialPaperFailure {
  type: typeof ActionTypes.FETCH_MATERIAL_PAPER_FAILURE
}
interface FetchMaterialPaperSuccess {
  type: typeof ActionTypes.FETCH_MATERIAL_PAPER_SUCCESS
  payload: TMaterialPaper
}

interface AddStockLengthMaterialPaper {
  type: typeof ActionTypes.ADD_STOCK_LENGTH_MATERIAL_PAPER_REQUESTED
  payload: PayloadAddStockMaterialPaper
}
interface AddStockLengthMaterialPaperLoading {
  type: typeof ActionTypes.ADD_STOCK_LENGTH_MATERIAL_PAPER_LOADING
}
interface AddStockLengthMaterialPaperFailure {
  type: typeof ActionTypes.ADD_STOCK_LENGTH_MATERIAL_PAPER_FAILURE
}
interface AddStockLengthMaterialPaperSuccess {
  type: typeof ActionTypes.ADD_STOCK_LENGTH_MATERIAL_PAPER_SUCCESS
  payload: TMaterialPaper
}
interface SetDialogAddStockMaterialPaper {
  type: typeof ActionTypes.SET_DIALOG_ADD_STOCK_MATERIAL_PAPER
  payload: ActionPayloadDialog & { current_stock: number | null }
}
// Union actions type
export type InventoryAction =
  | SetDialogAddEditInventory
  | SetDialogAddEditBrandMaterial
  | SetDrawerDetailInventory
  | FetchingListBrandMaterialPaperRequested
  | FetchingListBrandMaterialPaperLoading
  | FetchingListBrandMaterialPaperFailure
  | FetchingListBrandMaterialPaperSuccess
  | FetchingListMaterialPaperRequested
  | FetchingListMaterialPaperLoading
  | FetchingListMaterialPaperFailure
  | FetchingListMaterialPaperSuccess
  | FetchListMaterialPaperV2
  | FetchingListMaterialPaperV2Loading
  | FetchingListMaterialPaperV2Failure
  | FetchingListMaterialPaperV2Success
  | CreateInventoryMaterialPaper
  | CreateInventoryMaterialPaperLoading
  | CreateInventoryMaterialPaperFailure
  | CreateInventoryMaterialPaperSuccess
  | FetchMaterialPaper
  | FetchMaterialPaperLoading
  | FetchMaterialPaperFailure
  | FetchMaterialPaperSuccess
  | AddStockLengthMaterialPaper
  | AddStockLengthMaterialPaperLoading
  | AddStockLengthMaterialPaperFailure
  | AddStockLengthMaterialPaperSuccess
  | SetDialogAddStockMaterialPaper

// Actions creator.
export const setDialogAddEditInventory = (payload: TPayloadDialogAddEditInventory): SetDialogAddEditInventory => ({
  type: ActionTypes.SET_DIALOG_ADD_EDIT_INVENTORY,
  payload,
})

export const setDialogAddEditBrandMaterial = (open: boolean, id?: number | null): SetDialogAddEditBrandMaterial => ({
  type: ActionTypes.SET_DIALOG_ADD_EDIT_BRAND_MATERIAL,
  payload: { open, id },
})

export const setDrawerDetailInventory = (payload: TPayloadDrawerDetailInventory): SetDrawerDetailInventory => ({
  type: ActionTypes.SET_DRAWER_DETAIL_INVENTORY,
  payload,
})

export const fetchListBrandMaterialPaper = (
  payload?: Record<string, string>
): FetchingListBrandMaterialPaperRequested => ({
  type: ActionTypes.FETCHING_LIST_BRAND_MATERIAL_PAPER_REQUESTED,
  payload,
})

export const fetchingListBrandMaterialPaperSuccess = (
  payload: Array<TBrandMaterialPaper>
): FetchingListBrandMaterialPaperSuccess => ({
  type: ActionTypes.FETCHING_LIST_BRAND_MATERIAL_PAPER_SUCCESS,
  payload,
})

export const fetchListMaterialPaper = (payload: Record<string, string>): FetchingListMaterialPaperRequested => ({
  type: ActionTypes.FETCHING_LIST_MATERIAL_PAPER_REQUESTED,
  payload,
})

export const fetchingListMaterialPaperSuccess = (payload: Array<TMaterialPaper>): FetchingListMaterialPaperSuccess => ({
  type: ActionTypes.FETCHING_LIST_MATERIAL_PAPER_SUCCESS,
  payload,
})

/** List material paper v2 */
export const fetchListMaterialPaperV2 = (): FetchListMaterialPaperV2 => ({
  type: ActionTypes.FETCHING_LIST_MATERIAL_PAPER_V2_REQUESTED,
})
export const fetchingListMaterialPaperV2Loading = (): FetchingListMaterialPaperV2Loading => ({
  type: ActionTypes.FETCHING_LIST_MATERIAL_PAPER_V2_LOADING,
})
export const fetchingListMaterialPaperV2Failure = (): FetchingListMaterialPaperV2Failure => ({
  type: ActionTypes.FETCHING_LIST_MATERIAL_PAPER_V2_FAILURE,
})
export const fetchingListMaterialPaperV2Success = (
  payload: Array<TBrandWithMaterialPaper>
): FetchingListMaterialPaperV2Success => ({
  type: ActionTypes.FETCHING_LIST_MATERIAL_PAPER_V2_SUCCESS,
  payload,
})

/** Create inventory (material paper) */
export const createInventoryMaterialPaper = (payload: TCreateMaterialPaper): CreateInventoryMaterialPaper => ({
  type: ActionTypes.CREATE_INVENTORY_MATERIAL_PAPER_REQUESTED,
  payload,
})
export const createInventoryMaterialPaperLoading = (): CreateInventoryMaterialPaperLoading => ({
  type: ActionTypes.CREATE_INVENTORY_MATERIAL_PAPER_LOADING,
})
export const createInventoryMaterialPaperFailure = (): CreateInventoryMaterialPaperFailure => ({
  type: ActionTypes.CREATE_INVENTORY_MATERIAL_PAPER_FAILURE,
})
export const createInventoryMaterialPaperSuccess = (payload: TMaterialPaper): CreateInventoryMaterialPaperSuccess => ({
  type: ActionTypes.CREATE_INVENTORY_MATERIAL_PAPER_SUCCESS,
  payload,
})

/** Fetch material paper by id */
export const fetchMaterialPaper = (payload: number): FetchMaterialPaper => ({
  type: ActionTypes.FETCH_MATERIAL_PAPER_REQUESTED,
  payload,
})
export const fetchMaterialPaperLoading = (): FetchMaterialPaperLoading => ({
  type: ActionTypes.FETCH_MATERIAL_PAPER_LOADING,
})
export const fetchMaterialPaperFailure = (): FetchMaterialPaperFailure => ({
  type: ActionTypes.FETCH_MATERIAL_PAPER_FAILURE,
})
export const fetchMaterialPaperSuccess = (payload: TMaterialPaper): FetchMaterialPaperSuccess => ({
  type: ActionTypes.FETCH_MATERIAL_PAPER_SUCCESS,
  payload,
})

/* Add stock length material paper*/
export const addStockLengthMaterialPaper = (payload: PayloadAddStockMaterialPaper): AddStockLengthMaterialPaper => ({
  type: ActionTypes.ADD_STOCK_LENGTH_MATERIAL_PAPER_REQUESTED,
  payload,
})
export const addStockLengthMaterialPaperLoading = (): AddStockLengthMaterialPaperLoading => ({
  type: ActionTypes.ADD_STOCK_LENGTH_MATERIAL_PAPER_LOADING,
})
export const addStockLengthMaterialPaperFailure = (): AddStockLengthMaterialPaperFailure => ({
  type: ActionTypes.ADD_STOCK_LENGTH_MATERIAL_PAPER_FAILURE,
})
export const addStockLengthMaterialPaperSuccess = (payload: TMaterialPaper): AddStockLengthMaterialPaperSuccess => ({
  type: ActionTypes.ADD_STOCK_LENGTH_MATERIAL_PAPER_SUCCESS,
  payload,
})
export const setDialogAddStockMaterialPaper = (
  payload: ActionPayloadDialog & { current_stock: number | null }
): SetDialogAddStockMaterialPaper => ({
  type: ActionTypes.SET_DIALOG_ADD_STOCK_MATERIAL_PAPER,
  payload,
})
