import { Typography, Box } from '@mui/material';
import { useAppSelector } from '@src/store/hook';
import { FC } from 'react';

const AVATAR_SIZE = 60;

const DashboardGreeting: FC = () => {
  const { authenticatedUser: user } = useAppSelector((state) => state.auth);
  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}
    >
      <Box
        sx={{
          mr: 2,
          overflow: 'hidden',
          width: AVATAR_SIZE,
          height: AVATAR_SIZE,
          borderRadius: AVATAR_SIZE,
          '& img': { width: '100%' },
        }}
      >
        <img src="/static/images/avatar_square_blue_512dp.png" alt="Sample avatar" />
      </Box>
      <Box>
        <Typography component="span" variant="subtitle2">
          Welcome Back,
        </Typography>
        <Typography component="h5" variant="h5">
          {user.name}
        </Typography>
      </Box>
    </Box>
  );
};

export default DashboardGreeting;
