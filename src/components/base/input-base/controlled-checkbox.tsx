import { FC } from 'react';
import Typography from '@mui/material/Typography';
import { Control, Controller, FieldErrors } from 'react-hook-form';
import { FormControlLabel, FormHelperText, Checkbox, FormGroup } from '@mui/material';

interface Props {
  // eslint-disable-next-line
  control: Control<any>;
  name: string;
  label: string;
  errors: FieldErrors;
  helperText?: string;
  size?: 'small' | 'medium';
  disabled?: boolean;
}

export const ControlledCheckbox: FC<Props> = ({ control, name, label, errors, helperText }) => (
  <Controller
    name={name}
    control={control}
    render={({ field }) => {
      return (
        <>
          <FormGroup>
            <FormControlLabel {...field} control={<Checkbox />} label={label} />
          </FormGroup>
          <FormHelperText error={Boolean(errors[name]?.message)}>
            {errors[name]?.message ? errors[name]?.message : helperText}
          </FormHelperText>
        </>
      );
    }}
  />
);

ControlledCheckbox.defaultProps = {
  helperText: '',
  size: 'small',
  disabled: false,
};
