import { BoxSpinner, DialogContent } from '@components/ui';
import Dialog from '@mui/material/Dialog';
import Zoom, { ZoomProps } from '@mui/material/Zoom';
import { useAppSelector } from '@src/store/hook';
import { fetchMaterialPaper, setDialogAddEditInventory } from '@src/store/inventory/actions';
import { masterData_fetchMasterData } from '@/store/master-data/master-data-actions';
import { FC, ReactElement, forwardRef, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { FormMaterialPaper } from '.';

const Transition = forwardRef<unknown, ZoomProps>(
  (props, ref): ReactElement => (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Zoom ref={ref} {...props} />
  ),
);
const DialogAddEditInventory: FC = () => {
  const dispatch = useDispatch();
  const { dialogAddEditInventory, detail } = useAppSelector((state) => state.inventory);
  const { open, id, type, brand_id } = dialogAddEditInventory;
  const { isFetching: isFetchingMasterData } = useAppSelector((state) => state.masterData);
  const { isFetching: isFetchingDetail, data } = detail;

  const handleClose = (): void => {
    dispatch(setDialogAddEditInventory({ open: false, type: null, id: null }));
  };

  useEffect(() => {
    if (open) {
      if (type === 'material_paper')
        dispatch(
          masterData_fetchMasterData([
            {
              object_name: 'material_brands',
            },
          ]),
        );
      if (id) dispatch(fetchMaterialPaper(id));
    }
  }, [open]);

  return (
    <div>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="dialog-add-edit-inventory"
        PaperProps={{ elevation: 0 }}
        maxWidth="sm"
        fullWidth
      >
        <DialogContent>
          {isFetchingMasterData || isFetchingDetail ? (
            <BoxSpinner height={360} />
          ) : (
            type === 'material_paper' && <FormMaterialPaper id={id} brand_id={brand_id} defaultData={data} />
          )}
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default DialogAddEditInventory;
