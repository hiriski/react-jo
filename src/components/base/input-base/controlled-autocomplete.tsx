import { IDropdown } from '@/interfaces/dropdown';
import { MenuItem } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import { FC, ReactElement, ReactNode } from 'react';
import { Control, Controller, FieldErrors } from 'react-hook-form';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';

type Props = {
  // eslint-disable-next-line
  control: Control<any>;
  errors: FieldErrors;
  name: string;
  label: string;
  helperText?: string;
  size?: 'small' | 'medium';
  margin?: 'dense' | 'normal' | 'none';
  disabled?: boolean;
  fullWidth?: boolean;
  data: IDropdown<string | number>[] | Array<string>;
};

export const ControlledAutocomplete: FC<Props> = ({
  control,
  errors,
  name,
  label,
  helperText,
  size,
  margin,
  disabled,
  data,
  fullWidth,
}: Props): ReactElement => (
  <Controller
    name={name}
    control={control}
    render={({ field: { onChange, value } }) => (
      <Autocomplete
        onChange={(event, item): void => {
          onChange(item);
        }}
        value={value}
        disabled={disabled}
        options={data}
        // getOptionLabel={getOptionLabel}
        // getOptionSelected={(option: TOptions, val) => val === undefined || val === '' || option.id === val.id}
        size={size}
        renderInput={(field) => (
          <TextField
            {...field}
            label={label}
            error={Boolean(errors[name]?.message)}
            helperText={errors[name]?.message ?? helperText}
            size={size}
            margin={margin}
            fullWidth={fullWidth}
          />
        )}
      />
    )}
  />
);

ControlledAutocomplete.defaultProps = {
  helperText: '',
  size: 'small',
  margin: 'dense',
  disabled: false,
  fullWidth: true,
};
