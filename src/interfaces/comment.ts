import { IUser } from './user';

export interface IComment {
  id: string;
  body: string;
  user: IUser;
}
