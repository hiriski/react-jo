import FormControlLabel from '@mui/material/FormControlLabel';
import FormHelperText from '@mui/material/FormHelperText';
import InputLabel from '@mui/material/InputLabel';
import RadioGroup, { RadioGroupProps } from '@mui/material/RadioGroup';
import Typography from '@mui/material/Typography';
import { FC, ReactElement, ReactNode } from 'react';
import { Control, Controller, FieldErrors } from 'react-hook-form';

interface Props extends RadioGroupProps {
  control: Control<any>;
  errors: FieldErrors;
  name: string;
  helperText?: string;
  size?: 'small' | 'medium';
  margin?: 'dense' | 'normal' | 'none';
  disabled?: boolean;
  row?: boolean;
}

const ControlledRadio: FC<Props> = ({ children, control, errors, name, helperText, row }) => (
  <Controller
    name={name}
    control={control}
    render={({ field }) => {
      return (
        <>
          <RadioGroup row={row} name="payment_status_id" {...field}>
            {children}
          </RadioGroup>
          {helperText && <FormHelperText>{helperText}</FormHelperText>}
        </>
      );
    }}
  />
);

ControlledRadio.defaultProps = {
  helperText: '',
  size: 'small',
  margin: 'dense',
  disabled: false,
  row: false,
};

export default ControlledRadio;
