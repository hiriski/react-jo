import { UUID } from '@/interfaces/common';
import { useAppSelector } from '@/store/hook';
import { setDialogInputDownPayment, updatePaymentStatusJo } from '@/store/job-order/job-order-actions';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import EditIcon from '@mui/icons-material/Edit';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import MoneyOffIcon from '@mui/icons-material/MoneyOff';
import SellIcon from '@mui/icons-material/Sell';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import Menu, { MenuProps } from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { alpha, styled } from '@mui/material/styles';
import React, { FC } from 'react';
import { useDispatch } from 'react-redux';

const StyledMenu = styled((props: MenuProps) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    borderRadius: 6,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color: theme.palette.mode === 'light' ? 'rgb(55, 65, 81)' : theme.palette.grey[300],
    boxShadow:
      'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
    '& .MuiMenu-list': {
      padding: '4px 0',
    },
    '& .MuiMenuItem-root': {
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      '&:active': {
        backgroundColor: alpha(theme.palette.primary.main, theme.palette.action.selectedOpacity),
      },
    },
  },
}));

interface Props {
  joId: UUID;
  currentPaymentStatus: string;
  totalOrder: number;
}

const MenuUpdatePaymentJo: FC<Props> = ({ joId, currentPaymentStatus, totalOrder }) => {
  const { data: listOfPaymentStatus } = useAppSelector((state) => state.paymentStatus);
  const dispatch = useDispatch();

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLElement>): void => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const handleChange = (id: number): void => {
    if (id === 2 /** <-- it's down payment */) {
      dispatch(setDialogInputDownPayment(true, joId, totalOrder));
    }
    dispatch(dispatch(updatePaymentStatusJo({ job_order_id: joId, payment_status_id: id })));
  };

  return (
    <div>
      <Button
        id="demo-customized-button"
        aria-controls={open ? 'demo-customized-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        variant="contained"
        disableElevation
        onClick={handleClick}
        endIcon={<KeyboardArrowDownIcon />}
      >
        {currentPaymentStatus ?? 'Status pembayaran'}
      </Button>
      <StyledMenu
        id="demo-customized-menu"
        MenuListProps={{
          'aria-labelledby': 'demo-customized-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >
        {listOfPaymentStatus.map((item) => (
          <MenuItem key={item.id} onClick={() => handleChange(item.id)} disableRipple>
            {item.id === 1 && <MoneyOffIcon sx={{ mr: 1, fontSize: 16 }} />}
            {item.id === 2 && <AttachMoneyIcon sx={{ mr: 1, fontSize: 16 }} />}
            {item.id === 3 && <SellIcon sx={{ mr: 1, fontSize: 16 }} />}
            {item.name}
          </MenuItem>
        ))}
      </StyledMenu>
    </div>
  );
};

export default MenuUpdatePaymentJo;
