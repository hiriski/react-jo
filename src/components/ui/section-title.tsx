import { FC, ReactElement } from 'react'
import { Box, Divider, Typography, SxProps } from '@mui/material'

type Props = {
  title: string
  sx?: SxProps
}

const SectionTitle: FC<Props> = ({ title, sx }: Props): ReactElement => (
  <Box sx={{ mb: 3, ...sx }}>
    <Typography sx={{ mb: 1.4 }} component="h4" variant="h5">
      {title}
    </Typography>
    <Divider sx={{ width: 100 }} />
  </Box>
)

SectionTitle.defaultProps = {
  sx: {},
}

export default SectionTitle
