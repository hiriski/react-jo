import { PermissionConstants } from '@/constants/permissions.constants';
import { IUser } from '@/interfaces/user';

export type ValueOf<T> = T[keyof T];

/** ------ Permissions helper ------- */

/**
 * Base helper check user ability.
 *
 * @param {IUser} user
 * @param {string} ability
 * @return {boolean}
 */
export const permissions_can = (user: IUser, ability: string): boolean => {
  return Boolean(user.role.permissions.find((x) => x.code === ability));
};

export const permissions_canCreateJobOrder = (user: IUser): boolean => {
  return permissions_can(user, PermissionConstants.CreateJobOrder);
};
