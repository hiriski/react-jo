import { IMasterData } from '@/interfaces/master-data';

import { MasterDataActions } from './master-data-actions';
import { MASTER_DATA_ACTION_TYPES as ActionTypes } from './master-data-action-types.enum';

export interface MasterDataState {
  isFetching: boolean;
  isError: boolean;
  data: IMasterData;
}

export const initDataMasterDataState: IMasterData = {
  product_categories: [],
  job_order_categories: [],
  materials: [],
  material_brands: [],
  material_categories: [],
  material_types: [],
  customers: [],
  payment_methods: [],
  payment_statuses: [],
  bank_accounts: [],
  users: [],
};

const initialState: MasterDataState = {
  isFetching: false,
  isError: false,
  data: initDataMasterDataState,
};

const masterDataReducer = (state: MasterDataState = initialState, action: MasterDataActions): MasterDataState => {
  switch (action.type) {
    case ActionTypes.FETCH_MASTER_DATA_LOADING:
      return {
        ...state,
        isError: false,
        isFetching: true,
      };
    case ActionTypes.FETCH_MASTER_DATA_FAILURE:
      return {
        ...state,
        isError: true,
        isFetching: false,
      };
    case ActionTypes.FETCH_MASTER_DATA_SUCCESS:
      return {
        ...state,
        isError: false,
        isFetching: false,
        data: { ...state.data, ...action.payload },
      };

    default:
      return state;
  }
};

export default masterDataReducer;
