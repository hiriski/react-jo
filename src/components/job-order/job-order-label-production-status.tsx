import { JobOrderProductionStatusIdConstants } from '@/constants';
import { IJobOrderProductionStatus } from '@/interfaces/job-order';
import Box from '@mui/material/Box';
import { alpha, SxProps } from '@mui/material/styles';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import { jobOrder_createColorLabelProductionStatus } from '@src/utils/jo';
import { FC } from 'react';
import { SyncOutline, CheckmarkOutline, AlertCircleOutline } from 'react-ionicons';

type Props = {
  status: IJobOrderProductionStatus;
  sx?: SxProps;
};

export const JobOrderLabelProductionStatus: FC<Props> = ({ status, sx }: Props) => {
  const color = jobOrder_createColorLabelProductionStatus(status.id);
  return (
    <Tooltip title={status.description || ''}>
      <Box
        sx={{
          color,
          backgroundColor: alpha(color, 0.15),
          textAlign: 'center',
          // display: 'inline-block',
          borderRadius: 4,
          display: 'inline-flex',
          alignItems: 'center',
          justifyContent: 'center',
          py: 0.6,
          px: 1,

          // Icon
          '& span': {
            height: 14,
          },

          '& svg': {
            mr: 1,
          },

          ...sx,
        }}
      >
        {status.id === JobOrderProductionStatusIdConstants.Waiting && (
          <AlertCircleOutline color={color} height="14px" width="14px" />
        )}
        {status.id === JobOrderProductionStatusIdConstants.InProgress && (
          <SyncOutline rotate color={color} height="14px" width="14px" />
        )}
        {status.id === JobOrderProductionStatusIdConstants.Done && (
          <CheckmarkOutline color={color} height="14px" width="14px" />
        )}
        <Typography sx={{}} variant="subtitle2" color="inherit">
          {status.name}
        </Typography>
      </Box>
    </Tooltip>
  );
};
