import { FC } from 'react';
import { StyledButton } from '@/components/ui';
import { Box, TextField } from '@mui/material';
import { auth_unlockApp } from '@src/store/auth/actions';
import { SubmitHandler, useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { ControlledTextField } from '@/components/base/input-base';
import { useAppSelector } from '@/store';
import { auth_rootSelector } from '@/store/auth/auth-selectors';

type Inputs = {
  password: string;
};

export const LockScreenForm: FC = () => {
  const dispatch = useDispatch();

  const { unlockLoading } = useAppSelector((state) => auth_rootSelector(state));

  const {
    watch,
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: { password: '' },
  });

  const onSubmit: SubmitHandler<Inputs> = (values) => {
    dispatch(auth_unlockApp(values.password));
  };

  return (
    <Box
      sx={{
        display: 'flex',
        fontWeight: 'bold',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        width: '100%',
      }}
      component="form"
      onSubmit={handleSubmit(onSubmit)}
    >
      <ControlledTextField
        control={control}
        name="password"
        errors={errors}
        size="small"
        label="Password"
        type="password"
        fullWidth
      />

      <Box sx={{ mb: 2 }} />

      <StyledButton
        isLoading={unlockLoading}
        disabled={watch('password').length < 4}
        size="large"
        color="primary"
        type="submit"
        disableHoverEffect
        fullWidth
      >
        Unlock
      </StyledButton>
    </Box>
  );
};
