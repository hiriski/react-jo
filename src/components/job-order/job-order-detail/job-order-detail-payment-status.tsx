import { PaymentStatusConstants } from '@/constants';
import { UUID } from '@/interfaces/common';
import { IPaymentStatus } from '@/interfaces/job-order';
import { useAppSelector } from '@/store/hook';
import { setDialogInputDownPayment, updatePaymentStatusJo } from '@/store/job-order/job-order-actions';
import { masterData_fetchMasterData } from '@/store/master-data/master-data-actions';
import { masterData_rootSelector } from '@/store/master-data/master-data-selectors';
import { createLabelJoPaymentStatus } from '@/utils/jo';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import MoneyOffIcon from '@mui/icons-material/MoneyOff';
import SellIcon from '@mui/icons-material/Sell';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import Menu, { MenuProps } from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';
import { alpha, styled } from '@mui/material/styles';
import React, { FC, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import Swall from 'sweetalert2';
import Spinner from '@atlaskit/spinner';
import { jobOrder_rootSelector } from '@/store/job-order/job-order-selectors';
import { formatRupiah } from '@/utils/currency';

const StyledMenu = styled((props: MenuProps) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    borderRadius: Number(theme.shape.borderRadius),
    marginTop: theme.spacing(1),
    minWidth: 180,
    color: theme.palette.mode === 'light' ? 'rgb(55, 65, 81)' : theme.palette.grey[300],
    // boxShadow: 3,
    '& .MuiMenu-list': {
      padding: '4px 0',
    },
    '& .MuiMenuItem-root': {
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      '&:active': {
        backgroundColor: alpha(theme.palette.primary.main, theme.palette.action.selectedOpacity),
      },
    },
  },
}));

interface Props {
  jobOrderId: UUID;
  currentPaymentStatus: IPaymentStatus;
  totalOrder: number;
  downPayment?: number;
}

export const JobOrderDetailPaymentStatus: FC<Props> = ({
  jobOrderId,
  currentPaymentStatus,
  totalOrder,
  downPayment,
}) => {
  const { payment_statuses } = useAppSelector((state) => masterData_rootSelector(state).data);
  const { isLoading, isError } = useAppSelector((s) => jobOrder_rootSelector(s).updatePaymentStatus);
  const dispatch = useDispatch();

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLElement>): void => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = (): void => {
    setAnchorEl(null);
  };

  useEffect(() => {
    if (!payment_statuses.length) dispatch(masterData_fetchMasterData([{ object_name: 'payment_statuses' }]));
  }, [payment_statuses]);

  const handleChange = (newValue: number): void => {
    if (currentPaymentStatus.id !== newValue) {
      Swall.fire({
        title: 'Yakin ingin update Status Pembayaran ?',
        icon: 'warning',
        text: '',
        showCancelButton: true,
        confirmButtonText: 'Ya, Update Status',
      }).then((result) => {
        if (result.isConfirmed) {
          setAnchorEl(null);
          if (newValue === PaymentStatusConstants.DP /** <-- it's down payment */) {
            dispatch(setDialogInputDownPayment(true, jobOrderId, totalOrder));
          } else {
            dispatch(
              updatePaymentStatusJo({ job_order_id: jobOrderId, payment_status_id: newValue, down_payment: null }),
            );
          }
        } else {
          // do something
        }
      });
    }
  };

  return (
    <Stack direction="column" spacing={2} sx={{ mb: 1.5 }}>
      <Typography>Status Pembayaran :</Typography>
      <Box sx={{ display: 'flex', alignItems: 'center' }}>
        {isLoading && (
          <Box sx={{ mr: 1.5 }}>
            <Spinner size="medium" />
          </Box>
        )}
        <Button
          id="demo-customized-button"
          aria-controls={open ? 'demo-customized-menu' : undefined}
          aria-haspopup="true"
          aria-expanded={open ? 'true' : undefined}
          variant="outlined"
          disableElevation
          onClick={handleClick}
          endIcon={<KeyboardArrowDownIcon />}
          sx={{
            color: createLabelJoPaymentStatus(currentPaymentStatus.id),
            border: `1px solid ${createLabelJoPaymentStatus(currentPaymentStatus.id)}`,
            textAlign: 'center',
          }}
        >
          {currentPaymentStatus.name ?? 'Status pembayaran'}
        </Button>
        <Menu
          id="job-order-payment-status-menu"
          MenuListProps={{
            'aria-labelledby': 'demo-customized-button',
          }}
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          PaperProps={{
            elevation: 4,
          }}
          // anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          // transformOrigin={{
          //   vertical: 'top',
          //   horizontal: 'right',
          // }}
        >
          {payment_statuses.map((item) => (
            <MenuItem key={item.id} onClick={() => handleChange(item.id)} disableRipple>
              {item.id === 1 && <MoneyOffIcon sx={{ mr: 1, fontSize: 16 }} />}
              {item.id === 2 && <AttachMoneyIcon sx={{ mr: 1, fontSize: 16 }} />}
              {item.id === 3 && <SellIcon sx={{ mr: 1, fontSize: 16 }} />}
              {item.name}
            </MenuItem>
          ))}
        </Menu>

        {/* down payment  */}
        {currentPaymentStatus.id === PaymentStatusConstants.DP && (
          <Typography sx={{ fontWeight: 700, ml: 2 }}>Rp {formatRupiah(downPayment)}</Typography>
        )}
      </Box>
    </Stack>
  );
};
