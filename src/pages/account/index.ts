import { lazy } from 'react'

const AccountPage = lazy(() => import('./account'))

export default AccountPage
