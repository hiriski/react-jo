import { SIDEBAR_DRAWER_WIDTH } from '@/utils/constants';

export const AppConfig = {
  ContainerFullWidth: true,
  DrawerAddEditJobOrderWidth: 1000,
  SidebarDrawerWidth: SIDEBAR_DRAWER_WIDTH,
  DatabaseFormatTimestamps: 'YYYY-MM-DD HH:mm:ss',
  LoadingText: 'Antosan sakedap',
};
