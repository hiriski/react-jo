import { FC } from 'react';
import { IDropdown } from '@/interfaces/dropdown';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormHelperText from '@mui/material/FormHelperText';
import RadioGroup, { RadioGroupProps } from '@mui/material/RadioGroup';
import Radio from '@mui/material/Radio';
import Typography from '@mui/material/Typography';
import { Control, Controller, FieldErrors } from 'react-hook-form';
import { FormControl, FormLabel } from '@mui/material';

interface Props extends Pick<RadioGroupProps, 'name' | 'value' | 'defaultValue'> {
  // eslint-disable-next-line
  control: Control<any>;
  errors: FieldErrors;
  helperText?: string;
  size?: 'small' | 'medium';
  disabled?: boolean;
  row?: boolean;
  label?: string;
  fullWidth?: boolean;
  data: IDropdown<string | number>[];
}

export const ControlledRadioGroup: FC<Props> = ({
  control,
  errors,
  name,
  label,
  helperText,
  row,
  data,
  size,
  fullWidth,
}) => (
  <Controller
    name={name}
    control={control}
    render={({ field }) => {
      return (
        <FormControl fullWidth={fullWidth}>
          {label && <FormLabel>{label}</FormLabel>}
          <RadioGroup row={row} name={name} {...field}>
            {data.map((x) => (
              <FormControlLabel
                key={x.value}
                sx={{ mb: 1, ml: 0 }}
                value={x.value}
                labelPlacement="end"
                control={<Radio size={size} />}
                label={x.label}
              />
            ))}
          </RadioGroup>
          <FormHelperText error={Boolean(errors[name]?.message)}>
            {errors[name]?.message ? errors[name]?.message : helperText}
          </FormHelperText>
        </FormControl>
      );
    }}
  />
);

ControlledRadioGroup.defaultProps = {
  helperText: '',
  size: 'small',
  disabled: false,
  row: false,
  label: '',
  fullWidth: false,
};
