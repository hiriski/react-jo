// Interfaces.
import type { AppState } from '@src/store/root-reducer';
import { WarehouseState } from './warehouse-reducer';

export const warehouse_rootSelector = (state: AppState): WarehouseState => state.warehouse;
