import { IMaterialBrand } from '@/interfaces/warehouse';
import { Box, Typography } from '@mui/material';
import { FC } from 'react';

import FlyersImage from '@/assets/images/icons/flyers.png';

interface Props {
  item: IMaterialBrand;
}

export const MaterialBrandItem: FC<Props> = ({ item }) => {
  return (
    <Box
      sx={{
        backgroundColor: 'background.paper',
        width: 200,
        overflow: 'hidden',
        borderRadius: 1,
        mr: 2,
        boxShadow: 1,
        px: 3,
        py: 2,
      }}
    >
      <Box sx={{ '& img': { width: 72, height: 'auto' } }}>
        {item.image_url ? <img src={item.image_url} alt={item.name} /> : <img src={FlyersImage} alt={item.name} />}
      </Box>
      <Box>
        <Typography variant="h6">{item.name}</Typography>
        <Typography variant="subtitle2">{item.material_counts} items</Typography>
      </Box>
    </Box>
  );
};
