import { FC, useState } from 'react';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Box from '@mui/material/Box';
import { alpha } from '@mui/material/styles';
import styled from '@mui/material/styles/styled';
import Typography from '@mui/material/Typography';
import ReactImagesUploading, { ImageListType, ImageType, ImageUploadingPropsType } from 'react-images-uploading';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

// Ionicons
import { CloudUploadOutline, CloseOutline } from 'react-ionicons';

// Mui icons.
import PhotoCameraIcon from '@mui/icons-material/PhotoCamera';

// colors
import { blue, red } from '@mui/material/colors';

interface IStyledUploadPhotoMarker {
  isDragging: boolean;
  isMax: boolean;
  isError: boolean;
}

const StyledUploadPhotoMarker = styled(Box, {
  shouldForwardProp: (prop) => prop !== 'isDragging',
})<IStyledUploadPhotoMarker>(({ theme, isDragging, isMax, isError }) => ({
  position: 'relative',
  height: 140,
  width: '100%',
  borderRadius: Number(theme.shape.borderRadius),
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  textAlign: 'center',
  cursor: 'pointer',
  borderWidth: 1,
  border: 'solid',
  borderStyle: 'dotted',
  borderColor: isDragging ? theme.palette.primary.main : `${alpha(theme.palette.primary.main, 0.5)}`,
  backgroundColor: isDragging ? alpha(theme.palette.primary.main, 0.2) : alpha(theme.palette.primary.main, 0.08),
  color: theme.palette.text.secondary,
  userSelect: 'none',

  overflow: 'hidden',

  opacity: 0.85,

  '& h6': {
    fontSize: 12,
    margin: '0 1rem',
    '& strong': {
      color: theme.palette.primary.main,
    },
  },

  '& svg': {
    color: `${theme.palette.primary.main} !important`,
  },

  '&:hover': {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    opacity: 1,
    '& h6': {
      '& strong': {
        color: theme.palette.primary.contrastText,
      },
    },
    '& svg': {
      color: `${theme.palette.primary.contrastText} !important`,
    },
  },

  '& > .MuiBox-root': {
    opacity: 0.7,
  },

  ...(isMax && {
    backgroundColor: alpha('#000', 0.2),
    color: theme.palette.text.primary,
    borderColor: `${alpha('#000', 0.2)}`,
    cursor: 'no-drop',
    '&:hover': {
      backgroundColor: alpha('#000', 0.2),
      color: theme.palette.text.primary,
    },
  }),

  ...(isError && {
    backgroundColor: alpha(red[500], 0.15),
    color: red[700],
    borderColor: `${alpha(red[500], 0.15)}`,
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.primary.contrastText,
      '& h6': {
        '& strong': {
          color: theme.palette.primary.contrastText,
        },
      },
      '& svg': {
        color: `${theme.palette.primary.contrastText} !important`,
      },
    },
  }),
}));

const StyledBoxImageContainer = styled('div')(({ theme }) => ({
  position: 'relative',
  paddingTop: 130,
  '&:hover': {
    '& > .image-actions ': {
      transform: 'scale(1)',
    },
  },
}));

const StyledBoxImage = styled('div')(({ theme }) => ({
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
  borderRadius: `${Number(theme.shape.borderRadius)}px`,
  border: `1px solid ${alpha('#333', 0.075)}`,
  overflow: 'hidden',
  '&:hover': {
    boxShadow: theme.shadows[2],
  },
  '& img': {
    objectFit: 'cover',
    width: '100%',
    height: '100%',
  },
}));

const ActionButtonContainer = styled('div')(({ theme }) => ({
  position: 'absolute',
  top: -10,
  right: -12,
  zIndex: 1,
  backgroundColor: theme.palette.background.paper,
  borderRadius: 40,
  padding: '2px 4px',
  boxShadow: 'var(--shadow-small)',
  transform: 'scale(0)',
  transition: theme.transitions.create(['transform']),
}));

const ActionButton = styled(IconButton)(({ theme }) => ({
  '& span': {
    lineHeight: 0,
  },
}));

interface Props extends Omit<ImageUploadingPropsType, 'value' | 'onChange'> {
  images: ImageListType;
  onChange: (imageList: ImageListType, addUpdateIndex: number[] | undefined) => void;
  isError?: boolean;
}

export const BaseUploadImages: FC<Props> = (props) => {
  const [_images, _setImages] = useState<ImageListType>([]);
  const { images, onChange, isError, ...rest } = props;

  /**
   *
   * Handle change upload image.
   * @param imageList
   * @param addUpdateIndex
   */
  const handleChange = (imageList: ImageListType, addUpdateIndex: number[] | undefined): void => {
    _setImages(imageList);
    onChange(imageList, addUpdateIndex);
  };

  // console.log('_images', _images);

  // a little function to help us with reordering the result
  const reorder = (list, startIndex, endIndex): any => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  const onDragEnd = (result): any => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const items = reorder(_images, result.source.index, result.destination.index);

    _setImages(items);
  };

  const getListStyle = (isDraggingOver: boolean): any => ({
    background: isDraggingOver ? 'lightblue' : 'lightgrey',
    display: 'flex',
    padding: 8,
    overflow: 'auto',
  });

  const getItemStyle = (isDragging: boolean, draggableStyle: any): any => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    padding: 8 * 2,
    margin: `0 ${8}px 0 0`,

    // change background colour if dragging
    background: isDragging ? 'lightgreen' : 'grey',

    // styles we need to apply on draggables
    ...draggableStyle,
  });

  return (
    <ReactImagesUploading {...rest} value={_images} onChange={handleChange}>
      {({ imageList, onImageUpload, onImageUpdate, onImageRemove, isDragging, dragProps }) => (
        <Box>
          <StyledUploadPhotoMarker
            isError={isError}
            isDragging={isDragging}
            isMax={props.maxNumber === _images.length}
            onClick={props.maxNumber === _images.length ? () => null : onImageUpload}
            {...dragProps}
          >
            <CloudUploadOutline height={'40px'} width={'40px'} />
            <Typography component="h6">
              <strong>Browse </strong>
              <span>or drag image here</span>
            </Typography>
          </StyledUploadPhotoMarker>

          <Grid container spacing={2} sx={{ mt: 1 }}>
            {_images.map((image, index) => (
              <Draggable key={String(index)} draggableId={image.id} index={index}>
                {(provided, snapshot) => (
                  <div
                    // item
                    // xs={3}
                    // md={3}
                    // lg={2}
                    style={getItemStyle(snapshot.isDragging, provided.draggableProps.style)}
                  >
                    <StyledBoxImageContainer>
                      {/* <ActionButtonContainer className="image-actions">
                                <ActionButton
                                  onClick={() => onImageUpdate(index)}
                                  size="small"
                                  sx={{ color: 'primary.main' }}
                                >
                                  <CloudUploadOutline width="20px" height="20px" color={blue[600]} />
                                </ActionButton>
                                <ActionButton onClick={() => onImageRemove(index)} size="small" sx={{ color: 'red' }}>
                                  <CloseOutline width="20px" height="20px" color={red[600]} />
                                </ActionButton>
                              </ActionButtonContainer> */}
                      <StyledBoxImage>
                        <img src={image.dataURL} alt={`Image ${index}`} />
                      </StyledBoxImage>
                    </StyledBoxImageContainer>
                  </div>
                )}
              </Draggable>
            ))}
          </Grid>

          {_images.length > 0 && (
            <Grid container spacing={2} sx={{ mt: 1 }}>
              {_images.map((image, index) => (
                <Grid key={String(index)} item xs={3} md={3} lg={2}>
                  <StyledBoxImageContainer>
                    <ActionButtonContainer className="image-actions">
                      <ActionButton onClick={() => onImageUpdate(index)} size="small" sx={{ color: 'primary.main' }}>
                        <CloudUploadOutline width="20px" height="20px" color={blue[600]} />
                      </ActionButton>
                      <ActionButton onClick={() => onImageRemove(index)} size="small" sx={{ color: 'red' }}>
                        <CloseOutline width="20px" height="20px" color={red[600]} />
                      </ActionButton>
                    </ActionButtonContainer>
                    <StyledBoxImage>
                      <img src={image.dataURL} alt={`Image ${index}`} />
                    </StyledBoxImage>
                  </StyledBoxImageContainer>
                </Grid>
              ))}
            </Grid>
          )}
        </Box>
      )}
    </ReactImagesUploading>
  );
};
