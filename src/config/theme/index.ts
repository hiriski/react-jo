import { createTheme } from '@mui/material/styles';

import components from './overrides-components';
import palette from './palette';
import shadows from './shadows';
import typography from './typography';

export const THEME_SPACING = 6;

export default createTheme({
  spacing: THEME_SPACING,
  palette,
  typography,
  components,
  shadows,
});
