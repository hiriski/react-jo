import { lazy } from 'react'

const ProductPage = lazy(() => import('./product-page'))

export default ProductPage
