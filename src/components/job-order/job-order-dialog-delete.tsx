import { FC, useEffect, useState } from 'react';

// Mui components.
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import Typography from '@mui/material/Typography';
import RadioGroup from '@mui/material/RadioGroup';
import Radio from '@mui/material/Radio';
import FormLabel from '@mui/material/FormLabel';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import FormControlLabel from '@mui/material/FormControlLabel';

// Base components.
import { BaseDialog, BaseLoader } from '@/components/base';

// Hooks
import { useAppSelector } from '@src/store/hook';
import { useDispatch } from 'react-redux';

// Redux
import { jobOrder_delete, jobOrder_setDialogDelete } from '@/store/job-order/job-order-actions';
import { jobOrder_rootSelector } from '@/store/job-order/job-order-selectors';
import { IDropdown } from '@/interfaces/dropdown';
import { SubmitHandler, useForm } from 'react-hook-form';
import { IPayloadJobOrderDelete } from '@/store/job-order/job-order.interfaces';
import { TrashOutline } from 'react-ionicons';

// Yup resolver
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { ControlledRadioGroup, ControlledTextField } from '../base/input-base';

// Type form
type FormValues = IPayloadJobOrderDelete;

const dropdownReasonList: Array<IDropdown<string>> = [
  {
    label: 'Job Order Duplikat',
    value: 'Job Order Duplikat',
  },
  {
    label: 'Salah membuat data Job Order',
    value: 'Salah membuat data Job Order',
  },
  {
    label: 'Lainnya',
    value: 'other',
  },
];

const schema = yup.object().shape({
  job_order_id: yup.string().required('Job Order cannot be null'),
  reason_deleted: yup
    .string()
    .min(3, 'Minimal 3 karakter')
    .max(500, 'Pesan alasan dihapus terlalu panjang')
    .required('Silahkan pilih alasan'),
});

export const JobOrderDialogDelete: FC = () => {
  const dispatch = useDispatch();

  const { dialogDelete, deleteJobOrder } = useAppSelector((state) => jobOrder_rootSelector(state));
  const { open, job_order_id } = dialogDelete;

  const [radioReason, setRadioReason] = useState<string | undefined>(undefined);

  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    defaultValues: {
      job_order_id,
      reason_deleted: undefined,
    },
    resolver: yupResolver(schema),
  });

  const handleFormSubmit: SubmitHandler<FormValues> = (values) => {
    console.log('values', values);
    dispatch(jobOrder_delete(values));
  };

  /**
   * Handle close dialog.
   */
  const handleClose = (): void => {
    dispatch(jobOrder_setDialogDelete(false, null));
  };

  /**
   * Handle confirm.
   */
  const handleConfirm = (): void => {
    handleSubmit(handleFormSubmit)();
  };

  // Set hook form value
  useEffect(() => {
    if (radioReason !== 'other') {
      setValue('reason_deleted', radioReason);
    } else {
      setValue('reason_deleted', '');
    }
  }, [radioReason]);

  // Set hook form value
  useEffect(() => {
    if (job_order_id) {
      setValue('job_order_id', job_order_id);
    }
  }, [job_order_id]);

  // Clean up
  useEffect(() => {
    if (!open)
      return () => {
        setRadioReason(undefined);
      };

    return undefined;
  }, [open]);

  return (
    <BaseDialog
      open={open}
      disableCancelButton
      confirmButtonText="Hapus Job Order"
      title="Hapus Job Order"
      onClose={handleClose}
      onConfirm={handleConfirm}
      maxWidth="xs"
      icon={<TrashOutline />}
      iconColor="red"
      fullWidth
    >
      {deleteJobOrder.isLoading && <BaseLoader />}

      <Typography sx={{ fontSize: 15, my: 2 }}>Silahkan berikan alasan dihapus :</Typography>

      <Box component="form" onSubmit={handleSubmit(handleFormSubmit)}>
        <FormControl fullWidth margin="none">
          <FormLabel>Pilih Alasan</FormLabel>
          <RadioGroup onChange={(e, value: string) => setRadioReason(value)} value={radioReason} name="radio_reason">
            {dropdownReasonList.map((x) => (
              <FormControlLabel
                key={x.value}
                sx={{ mb: 1, ml: 0 }}
                value={x.value}
                labelPlacement="end"
                control={<Radio size="medium" />}
                label={x.label}
              />
            ))}
          </RadioGroup>
          {radioReason !== 'other' && (
            <FormHelperText error={Boolean(errors?.reason_deleted?.message)}>
              {errors?.reason_deleted?.message ? errors?.reason_deleted?.message : ''}
            </FormHelperText>
          )}
        </FormControl>

        <Collapse in={radioReason === 'other'}>
          <ControlledTextField
            fullWidth
            control={control}
            errors={errors}
            name="reason_deleted"
            label="Berikan alasan lain"
            size="medium"
            multiline
            minRows={2}
            maxRows={5}
          />
        </Collapse>
      </Box>
    </BaseDialog>
  );
};
