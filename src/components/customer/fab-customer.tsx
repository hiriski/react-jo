import AddOutlinedIcon from '@mui/icons-material/AddOutlined';
import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import Box from '@mui/material/Box';
import Fab from '@mui/material/Fab';
import { useTheme } from '@mui/material/styles';
import Tooltip from '@mui/material/Tooltip';
import { setDrawerAddEditCustomer } from '@src/store/customer/customer-actions';
import { useAppSelector } from '@src/store/hook';
import { DRAWER_ADD_EDIT_CUSTOMER_WIDTH } from '@src/utils/constants';
import { FC } from 'react';
import { useDispatch } from 'react-redux';

const FabCustomer: FC = () => {
  const dispatch = useDispatch();
  const { drawerAddEdit: drawer } = useAppSelector((state) => state.customer);
  const theme = useTheme();

  const handleOpen = (): void => {
    dispatch(setDrawerAddEditCustomer(true, null));
  };

  const handleClose = (): void => {
    dispatch(setDrawerAddEditCustomer(false, null));
  };

  return (
    <Box
      sx={{
        zIndex: 1301,
        /* <- drawer zIndex + 1 */ position: 'fixed',
        bottom: 36, // ideal with fixed paginatio
        right: 26,
        transition: theme.transitions.create('right', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        ...(drawer.open && {
          right: DRAWER_ADD_EDIT_CUSTOMER_WIDTH + 26,
          transition: theme.transitions.create('right', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
          }),
        }),
      }}
    >
      <Tooltip title="Tambah customer">
        <Fab
          size="medium"
          onClick={!drawer.open ? handleOpen : handleClose}
          color={!drawer.open ? 'primary' : 'secondary'}
          aria-label="add"
        >
          {!drawer.open ? <AddOutlinedIcon /> : <CloseOutlinedIcon />}
        </Fab>
      </Tooltip>
    </Box>
  );
};

export default FabCustomer;
