// Axios
import api from '@src/utils/http';
import { AxiosResponse } from 'axios';

// Api endpoints.
import { MachineApiEndpoints } from './machine-api-endpoint';

// interfaces.
import { IMachine } from '@/interfaces/machine';

const MachineApiService = {
  findAll: async (params?: Record<string, string | number>): Promise<AxiosResponse<Array<IMachine>>> =>
    api.get(MachineApiEndpoints.index, { params }),
};

export default MachineApiService;
