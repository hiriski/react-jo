import api from '@src/utils/http';
import { AxiosResponse } from 'axios';
import { MasterDataApiEndpoints } from './master-data-api-endpoints';
import { IMasterData, TRequestMasterData } from '@/interfaces/master-data';

const MasterDataApiService = {
  invoke: async (body: TRequestMasterData): Promise<AxiosResponse<IMasterData>> =>
    api.post(MasterDataApiEndpoints.index, body),
};
export default MasterDataApiService;
