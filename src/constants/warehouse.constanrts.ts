/**
 * Warehouse constants.
 */

export const MaterialTypeConstants = {
  MaterialSticker: 1,
  MaterialBanner: 2,
  MaterialArtPaper: 3,
  MaterialAcrylic: 4,
  MaterialFinishingPrintSticker: 5,
};
