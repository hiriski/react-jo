import { CustomerDetail } from '@components/customer';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
// import Slide, { SlideProps } from '@mui/material/Slide'
// import Grow, { GrowProps } from '@mui/material/Grow'
import Zoom, { ZoomProps } from '@mui/material/Zoom';
import { BoxSpinner, DialogContent } from '@src/components/ui';
import { customer_fetchCustomer, setDialogDetaiLCustomer } from '@src/store/customer/customer-actions';
import { useAppSelector } from '@src/store/hook';
import { FC, ReactElement, forwardRef, useEffect } from 'react';
import { useDispatch } from 'react-redux';

const Transition = forwardRef<unknown, ZoomProps>(
  (props, ref): ReactElement => (
    // eslint-disable-next-line react/jsx-props-no-spreading
    // <Slide direction="down" ref={ref} {...props} />
    // <Grow style={{ transformOrigin: '0 0 0' }} ref={ref} {...props} />
    <Zoom unmountOnExit ref={ref} {...props} />
  ),
);
const DialogDetailCustomer: FC = () => {
  const dispatch = useDispatch();
  const { dialogDetail, detail } = useAppSelector((state) => state.customer);
  const { open, id } = dialogDetail;
  const { isFetching, data } = detail;

  const handleClose = (): void => {
    dispatch(setDialogDetaiLCustomer(false, null));
  };

  useEffect(() => {
    if (open && id) {
      dispatch(customer_fetchCustomer(id));
    }
  }, [open]);

  return (
    <div>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="dialog-detail-customer"
        PaperProps={{ elevation: 1, sx: { position: 'relative' } }}
        maxWidth="sm"
        fullWidth
      >
        {/* <DialogTitle>Detail Job Order</DialogTitle> */}
        <DialogContent sx={{ pt: 4 }}>
          {isFetching ? <BoxSpinner height={260} /> : data && <CustomerDetail data={data} />}
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default DialogDetailCustomer;
