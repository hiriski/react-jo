import pusherInstance from '@/pusher';
import { RoleIdConstants } from '@/constants/role-id-constants';
import { INotification } from './interfaces/notification';
import { IJobOrder } from './interfaces/job-order';
import { IUser } from './interfaces/user';

export const subscribePusherChannels = (user: IUser): void | undefined => {
  if (user) {
    // Only team production will be receipt notification job-order-created.
    if (user.role_id === RoleIdConstants.ProductionTeam) {
      const eventName = 'job-order-created';
      const jobOrderChannel = pusherInstance.subscribe('job-order-channel');
      jobOrderChannel.bind(eventName, (messages: INotification<IJobOrder>) => {
        console.log(`Notification ${eventName} ->`, messages);
      });
    }
  } else return undefined;
};
