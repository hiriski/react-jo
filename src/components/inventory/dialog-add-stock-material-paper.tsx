import { DialogContent, DialogTitle } from '@components/ui'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import FormControl from '@mui/material/FormControl'
import FormHelperText from '@mui/material/FormHelperText'
import InputAdornment from '@mui/material/InputAdornment'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import Select, { SelectChangeEvent } from '@mui/material/Select'
import TextField from '@mui/material/TextField'
import Typography from '@mui/material/Typography'
import Zoom, { ZoomProps } from '@mui/material/Zoom'
import { useAppSelector } from '@src/store/hook'
import { addStockLengthMaterialPaper, setDialogAddStockMaterialPaper } from '@src/store/inventory/actions'
import { ChangeEvent, FC, ReactElement, forwardRef, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'

const Transition = forwardRef<unknown, ZoomProps>((props, ref): ReactElement => <Zoom ref={ref} {...props} />)

const DialogAddStockMaterialPaper: FC = () => {
  const [value, setValue] = useState<number | null>(5000)

  const dispatch = useDispatch()
  const { dialogAddStockMaterialPaper: state } = useAppSelector((state) => state.inventory)
  const { open, id, current_stock } = state

  const handleClose = (): void => {
    dispatch(setDialogAddStockMaterialPaper({ open: false, id: null, current_stock: null }))
  }

  const handleChange = (event: SelectChangeEvent<number>): void => {
    setValue(event.target.value as number)
  }
  const handleSubmit = (): void => {
    dispatch(addStockLengthMaterialPaper({ id, add_stock_length: value }))
  }

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleClose}
      aria-describedby="dialog-add-stock-material-paper"
      PaperProps={{ elevation: 0 }}
      maxWidth="sm"
      fullWidth
    >
      <DialogContent>
        <DialogTitle>Tambah Stock Bahan Kertas</DialogTitle>
        <Box sx={{ display: 'flex', mt: 2 }}>
          <FormControl sx={{ flex: 1, mr: 2 }} variant="outlined" size="small" margin="none" fullWidth>
            <InputLabel id="select-additional-stock">Pilih Brand/Merk</InputLabel>
            <Select
              onChange={handleChange}
              value={value}
              labelId="select-additional-stock"
              id="select-additional-stock"
              label="Select Category"
            >
              <MenuItem value={5000}>1 Roll</MenuItem>
              <MenuItem value={10000}>2 Roll</MenuItem>
              <MenuItem value={15000}>3 Roll</MenuItem>
              <MenuItem value={20000}>4 Roll</MenuItem>
              <MenuItem value={25000}>5 Roll</MenuItem>
            </Select>
            {/* <FormHelperText>Masukan jumlah tambahan stok</FormHelperText> */}
          </FormControl>
          <Button onClick={handleSubmit} variant="contained" disableElevation>
            Tambah Stok
          </Button>
        </Box>
        <Box sx={{ mt: 2 }}>
          <Typography>Stok saat ini : {current_stock}</Typography>
        </Box>
      </DialogContent>
    </Dialog>
  )
}

export default DialogAddStockMaterialPaper
