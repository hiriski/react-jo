// Saga
import type { Effect, SagaIterator } from '@redux-saga/types';
import { SagaReturnType, call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';

// Enum action types.
import { MACHINE_ACTION_TYPES as ActionTypes } from './machine-action-types.enum';

// Api service
import MachineApiService from './machine-api-service';

// Utils
import { httpResponseOK } from '@src/utils/http';

// Actions creators.
import { machine_fetchListFailure, machine_fetchListLoading, machine_fetchListSuccess } from './machine-actions';

// Type definitions of return of result.
type ApiResponseListMachine = SagaReturnType<typeof MachineApiService.findAll>;

function* saga_fetchListMachine({
  payload,
}: Effect<ActionTypes.FETCH_LIST_MACHINE_REQUESTED, Record<string, string | number>>): SagaIterator {
  yield put(machine_fetchListLoading());
  try {
    const { status, data }: ApiResponseListMachine = yield call(MachineApiService.findAll, payload);
    if (httpResponseOK(status)) {
      yield put(machine_fetchListSuccess(data));
    }
  } catch (e) {
    yield put(machine_fetchListFailure());
  }
}

export default function* machineSaga(): SagaIterator {
  yield takeLatest(ActionTypes.FETCH_LIST_MACHINE_REQUESTED, saga_fetchListMachine);
}
