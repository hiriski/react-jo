import React, { FC } from 'react'
import { Box, Skeleton } from '@mui/material'

const BrandMaterialPaperItemSkeleton: FC = () => (
  <Box
    sx={{
      borderRadius: 1,
      cursor: 'pointer',
      position: 'relative',
    }}
  >
    <Skeleton variant="rectangular" animation="wave" width="100%" height={77} />
  </Box>
)

export default BrandMaterialPaperItemSkeleton
