import { lazy } from 'react';

const OverviewPage = lazy(() => import('./overview'));

export default OverviewPage;
