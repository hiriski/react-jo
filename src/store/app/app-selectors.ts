// Interfaces.
import type { AppState } from '@src/store/root-reducer';
import { IAppState } from './app-reducer';

export const app_rootSelector = (state: AppState): IAppState => state.app;
