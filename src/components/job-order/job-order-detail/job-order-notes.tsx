import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { useAppSelector } from '@src/store/hook';
import { addNotesJo } from '@/store/job-order/job-order-actions';
import { colors, lightColors } from '@src/utils/colors';
import moment from 'moment';
import { FC, useState } from 'react';
import { useDispatch } from 'react-redux';

import { BoxSpinner } from '@/components/ui';
import { UUID } from '@/interfaces/common';
import { INote } from '@/interfaces/note';
import { getUserAvatarFallback } from '@/utils/common';
import { getInitialsName } from '@/utils/misc';
import { BaseButton } from '@/components/base';
import NoteAddIcon from '@mui/icons-material/NoteAdd';

type Props = {
  data: Array<INote>;
  joId: UUID;
};

export const JobOrderNotes: FC<Props> = ({ data, joId }: Props) => {
  const dispatch = useDispatch();
  const [notes, setNotes] = useState<string>('');
  const [isVisibleInputNotes, setIsVisibleInputNotes] = useState<boolean>(false);

  const { addNotes: stateAddNotes } = useAppSelector((state) => state.jobOrder);

  const handleClickAddNote = (): void => {
    // console.log('notes')
    setIsVisibleInputNotes(true);
  };

  const handleSubmitNotes = (): void => {
    if (notes) {
      dispatch(addNotesJo(joId, notes));
      setNotes('');
      setIsVisibleInputNotes(false);
    }
  };

  return (
    <Box>
      <Typography variant="h6" sx={{ mb: 4 }}>
        Catatan :
      </Typography>
      {Array.isArray(data) &&
        data.length > 0 &&
        data
          .slice(0)
          .reverse()
          .map((item, index) => (
            <Box key={item.id}>
              <Box
                sx={{
                  width: '75%',
                  margin: data.length > 1 ? '0' : '0 auto',
                  position: 'relative',
                  textAlign: 'left',
                  mb: data.length > 1 ? 4 : 2,
                  padding: '1.5em 1em',
                  transform: data.length > 1 ? 'rotate(0deg)' : 'rotate(2deg)',

                  '&:after': {
                    display: 'block',
                    content: '""',
                    position: 'absolute',
                    width: 100,
                    height: '24px',
                    top: '-12px',
                    left: '30%',
                    border: '1px solid #fff',
                    background: 'rgba(254, 254, 254, .75)',
                    boxShadow: '0px 1px 4px rgb(0 0 0 / 15%)',
                  },

                  boxShadow: 1,
                  borderRadius: 1,

                  ...(index === 0 && {
                    backgroundColor: '#fff8c6',
                  }),
                  ...(index === 1 && {
                    backgroundColor: '#d7ebff',
                  }),
                  ...(index === 2 && {
                    backgroundColor: '#deffe9',
                  }),
                  ...(index === 3 && {
                    backgroundColor: '#ffeafa',
                  }),
                  ...(index === 4 && {
                    backgroundColor: '#fff4ed',
                  }),
                  ...(index === 5 && {
                    backgroundColor: '#f3e2ff',
                  }),
                  ...(index >= 6 && {
                    backgroundColor: '#fff8c6',
                  }),
                }}
              >
                <Box sx={{ mb: 1.5, display: 'flex', alignItems: 'center' }}>
                  <Avatar sx={{ width: 34, height: 34 }} src={item.user.photo_url || getUserAvatarFallback(item.user)}>
                    {getInitialsName(item.user.name, true)}
                  </Avatar>
                  <Box sx={{ ml: 2 }}>
                    <Typography sx={{ fontSize: 13, fontWeight: 'bold' }}>{item.user.name}</Typography>
                    <Typography variant="subtitle2" sx={{ color: 'text.secondary' }}>
                      {moment(item.created_at).fromNow()}
                    </Typography>
                  </Box>
                </Box>
                <Typography sx={{ fontFamily: " 'Mali', cursive", fontSize: 15, fontWeight: 500 }}>
                  {item.body}
                </Typography>
              </Box>
            </Box>
          ))}

      {stateAddNotes.isLoading && <BoxSpinner height={80} />}

      {isVisibleInputNotes ? (
        <Box sx={{ textAlign: 'center' }}>
          <TextField
            onChange={(e) => setNotes(e.target.value)}
            name="notes"
            multiline
            rows={2}
            fullWidth
            placeholder="Silahkan masukan catatan.."
            helperText="Tambahkan catatan ke Job Order ini"
            sx={{ mb: 3 }}
          />
          <BaseButton startIcon={<NoteAddIcon />} onClick={handleSubmitNotes} variant="contained">
            Tambah Catatan
          </BaseButton>
        </Box>
      ) : (
        <Box sx={{ textAlign: 'center' }}>
          <Button onClick={handleClickAddNote}>Tambah Catatan</Button>
        </Box>
      )}
    </Box>
  );
};
