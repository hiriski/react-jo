import { TablePagination } from '@mui/material'
import { ChangeEvent, FC } from 'react'

interface Props {
  count: number
  rowsPerPage: number
  rowsPerPageOptions: Array<number>
  page: number
  onPageChange: (event: unknown, newPage: number) => void
  onRowsPerPageChange: (event: ChangeEvent<HTMLInputElement>) => void
}

const UserTablePagination: FC<Props> = ({
  count,
  rowsPerPage,
  rowsPerPageOptions,
  page,
  onPageChange,
  onRowsPerPageChange,
}: Props) => (
  <TablePagination
    rowsPerPageOptions={rowsPerPageOptions}
    component="div"
    count={count}
    rowsPerPage={rowsPerPage}
    page={page}
    onPageChange={onPageChange}
    onRowsPerPageChange={onRowsPerPageChange}
  />
)

export default UserTablePagination
