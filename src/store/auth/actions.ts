import { AnyAction } from 'redux';
import * as ActionTypes from './constants';
import { TRequestLoginWithSocialAccount, TRequestLogin, TRequestRegister } from '../../types/auth';
import { IUser } from '@/interfaces/user';
import { IStateFailure } from '../../types/common';

// Actions definiition

interface LoginRequested {
  type: typeof ActionTypes.LOGIN_REQUESTED;
  payload: TRequestLogin;
}
interface LoginLoading {
  type: typeof ActionTypes.LOGIN_LOADING;
}
interface LoginFailure {
  type: typeof ActionTypes.LOGIN_FAILURE;
  payload: IStateFailure;
}
interface LoginSuccess {
  type: typeof ActionTypes.LOGIN_SUCCESS;
  payload: IUser | null;
}

interface RegisterRequested {
  type: typeof ActionTypes.REGISTER_REQUESTED;
  payload: TRequestRegister;
}
interface RegisterLoading {
  type: typeof ActionTypes.REGISTER_LOADING;
}
interface RegisterFailure {
  type: typeof ActionTypes.REGISTER_FAILURE;
  payload: IStateFailure;
}
interface RegisterSuccess {
  type: typeof ActionTypes.REGISTER_SUCCESS;
  payload: IUser | null;
}
interface LoginWithSocialAccountRequested {
  type: typeof ActionTypes.LOGIN_WITH_SOCIAL_ACCOUNT_REQUESTED;
  payload: TRequestLoginWithSocialAccount;
}

interface LoginWithSocialAccountLoading {
  type: typeof ActionTypes.LOGIN_WITH_SOCIAL_ACCOUNT_LOADING;
}

interface LoginWithSocialAccountFailure {
  type: typeof ActionTypes.LOGIN_WITH_SOCIAL_ACCOUNT_FAILURE;
  payload: IStateFailure;
}
interface LoginWithSocialAccountSuccess {
  type: typeof ActionTypes.LOGIN_WITH_SOCIAL_ACCOUNT_SUCCESS;
  payload: { provider: string; user: IUser };
}
interface RevokeTokenLoading {
  type: typeof ActionTypes.REVOKE_TOKEN_LOADING;
}
interface RevokeTokenFailure {
  type: typeof ActionTypes.REVOKE_TOKEN_FAILURE;
}
interface ResetAuthState {
  type: typeof ActionTypes.RESET_AUTH_STATE;
}

interface IUnlockAppRequested {
  type: typeof ActionTypes.UNLOCK_APP_REQUESTED;
  payload: string;
}
interface IUnLockAppLoading {
  type: typeof ActionTypes.UNLOCK_APP_LOADING;
}
interface IUnLockAppFailure {
  type: typeof ActionTypes.UNLOCK_APP_FAILURE;
}
interface IUnLockAppSuccess {
  type: typeof ActionTypes.UNLOCK_APP_SUCCESS;
}

interface IFetchAuthenticatedUserRequested {
  type: typeof ActionTypes.FETCH_AUTHENTICATED_USER_REQUESTED;
}
interface IFetchAuthenticatedUserLoading {
  type: typeof ActionTypes.FETCH_AUTHENTICATED_USER_LOADING;
}
interface IFetchAuthenticatedUserFailure {
  type: typeof ActionTypes.FETCH_AUTHENTICATED_USER_FAILURE;
}
interface IFetchAuthenticatedUserSuccess {
  type: typeof ActionTypes.FETCH_AUTHENTICATED_USER_SUCCESS;
  payload: IUser;
}
interface ISetIsAuthenticated {
  type: typeof ActionTypes.SET_IS_AUTHENTICATED;
  payload: boolean;
}

// Union action types
export type AuthAction =
  | LoginRequested
  | LoginLoading
  | LoginFailure
  | LoginSuccess
  | RegisterRequested
  | RegisterLoading
  | RegisterFailure
  | RegisterSuccess
  | LoginWithSocialAccountRequested
  | LoginWithSocialAccountLoading
  | LoginWithSocialAccountFailure
  | LoginWithSocialAccountSuccess
  | RevokeTokenLoading
  | RevokeTokenFailure
  | ResetAuthState
  | IUnlockAppRequested
  | IUnLockAppLoading
  | IUnLockAppFailure
  | IUnLockAppSuccess
  | IFetchAuthenticatedUserRequested
  | IFetchAuthenticatedUserLoading
  | IFetchAuthenticatedUserFailure
  | IFetchAuthenticatedUserSuccess
  | ISetIsAuthenticated;

// Actions creators.

export const login = (payload: TRequestLogin): LoginRequested => ({
  type: ActionTypes.LOGIN_REQUESTED,
  payload,
});

export const setLoginError = (payload: IStateFailure): LoginFailure => ({
  type: ActionTypes.LOGIN_FAILURE,
  payload,
});

export const register = (payload: TRequestRegister): RegisterRequested => ({
  type: ActionTypes.REGISTER_REQUESTED,
  payload,
});

export const setRegisterError = (payload: IStateFailure): RegisterFailure => ({
  type: ActionTypes.REGISTER_FAILURE,
  payload,
});

// Login with social account.
export const loginWithSocialAccount = (payload: TRequestLoginWithSocialAccount): LoginWithSocialAccountRequested => ({
  type: ActionTypes.LOGIN_WITH_SOCIAL_ACCOUNT_REQUESTED,
  payload,
});

export const setLoginWithSocialAccountError = (payload: IStateFailure): LoginWithSocialAccountFailure => ({
  type: ActionTypes.LOGIN_WITH_SOCIAL_ACCOUNT_FAILURE,
  payload,
});

export const loginWithSocialAccountSuccess = (provider: string, user: IUser): LoginWithSocialAccountSuccess => ({
  type: ActionTypes.LOGIN_WITH_SOCIAL_ACCOUNT_SUCCESS,
  payload: { provider, user },
});

export const revokeToken = (): AnyAction => ({
  type: ActionTypes.REVOKE_TOKEN_REQUESTED,
});

export const auth_unlockApp = (payload: string): IUnlockAppRequested => ({
  type: ActionTypes.UNLOCK_APP_REQUESTED,
  payload,
});

export const auth_unlockAppLoading = (): IUnLockAppLoading => ({
  type: ActionTypes.UNLOCK_APP_LOADING,
});

export const auth_unlockAppFailure = (): IUnLockAppFailure => ({
  type: ActionTypes.UNLOCK_APP_FAILURE,
});

export const auth_unlockAppSuccess = (): IUnLockAppSuccess => ({
  type: ActionTypes.UNLOCK_APP_SUCCESS,
});

export const auth_fetchAuthenticatedUser = (): IFetchAuthenticatedUserRequested => ({
  type: ActionTypes.FETCH_AUTHENTICATED_USER_REQUESTED,
});

export const auth_fetchAuthenticatedUserLoading = (): IFetchAuthenticatedUserLoading => ({
  type: ActionTypes.FETCH_AUTHENTICATED_USER_LOADING,
});

export const auth_fetchAuthenticatedUserFailure = (): IFetchAuthenticatedUserFailure => ({
  type: ActionTypes.FETCH_AUTHENTICATED_USER_FAILURE,
});

export const auth_fetchAuthenticatedUserSuccess = (payload: IUser): IFetchAuthenticatedUserSuccess => ({
  type: ActionTypes.FETCH_AUTHENTICATED_USER_SUCCESS,
  payload,
});

export const auth_resetAuthState = (): ResetAuthState => ({
  type: ActionTypes.RESET_AUTH_STATE,
});

export const auth_setIsAuthenticated = (payload: boolean): ISetIsAuthenticated => ({
  type: ActionTypes.SET_IS_AUTHENTICATED,
  payload,
});
