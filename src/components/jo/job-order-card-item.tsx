import InventoryIcon from '@mui/icons-material/Inventory';
import { Avatar, Theme } from '@mui/material';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { setDrawerDetailInventory } from '@src/store/inventory/actions';
import { FC } from 'react';
import { useDispatch } from 'react-redux';

// Interfaces.
import { IJobOrder } from '@/interfaces/job-order';
import { setDialogDetailJo } from '@/store/job-order/job-order-actions';
import { getInitialsName } from '@/utils/misc';
import { CameraAltOutlined } from '@mui/icons-material';
import { UUID } from '@/interfaces/common';

type Props = {
  item: IJobOrder;
};

export const JobOrderCardItem: FC<Props> = ({ item }: Props) => {
  const dispatch = useDispatch();

  const onEdit = (phraseId: number): void => {
    // dispatch(setDrawerAddEditPhrase(true, phraseId))
  };

  const handleClickDetail = (joId: UUID): void => {
    dispatch(setDialogDetailJo(true, joId));
  };

  return (
    <Box
      onClick={() => handleClickDetail(item.id)}
      sx={{
        borderRadius: 1,
        cursor: 'pointer',
        position: 'relative',
        backgroundColor: 'background.paper',
        border: (theme: Theme) => `1px solid ${theme.palette.divider}`,
        display: 'table',
        mb: 2,
      }}
    >
      {/* Inner */}
      <Box>
        {/* Jo Image */}
        <Box style={{ width: '10%' }}>
          <Box
            sx={{
              height: 40,
              width: 40,
              overflow: 'hidden',
              borderRadius: 2,
              '& img': {
                height: '100%',
                width: 'auto',
              },
            }}
          >
            {item.images.length > 0 ? (
              <img src={item.images[0].image_sm} alt={item.title + ' image'} />
            ) : (
              <Box
                sx={{
                  backgroundColor: 'primary.main',
                  color: 'primary.contrastText',
                  height: 40,
                  width: 40,
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <CameraAltOutlined />
              </Box>
            )}
          </Box>
          {/* <Avatar
            sx={{
              bgcolor: 'primary.main',
              color: 'primary.contrastText',
              mr: 2,
              width: 46,
              height: 46,
              fontSize: 14,
              letterSpacing: 1,
            }}
            alt={item.title}
          >
            {getInitialsName(item.title)}
          </Avatar> */}
        </Box>

        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            flex: 1,
          }}
        >
          <Box>
            <Typography component="h6" variant="h4" sx={{ fontSize: 17, mb: 0.3 }}>
              {item.title}
            </Typography>
            <Typography variant="subtitle2" color="text.secondary">
              {item.body}
            </Typography>
          </Box>
          <Box sx={{ ml: 'auto', textAlign: 'right' }}>
            <Typography component="h6" variant="h4">
              {item.due_date}
            </Typography>
            <Typography variant="subtitle2">{item.production_status.name}</Typography>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
