import { yupResolver } from '@hookform/resolvers/yup';
import { InputAdornment } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import Grid from '@mui/material/Grid';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { SxProps } from '@mui/material';
import { useAppSelector } from '@src/store/hook';
import { createInventoryMaterialPaper } from '@src/store/inventory/actions';
import { TCreateMaterialPaper, TMaterialPaper } from '@src/types/inventory';
import { FC, useEffect, useMemo } from 'react';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import * as yup from 'yup';

type TInputs = TCreateMaterialPaper;

type Props = {
  sx?: SxProps;
  id: number | null;
  brand_id?: number | null;
  defaultData?: TMaterialPaper | null;
};

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
    },
  },
};

const defaultValues = {
  name: '',
  brand_id: undefined,
  stock_length: undefined,
  material_width: undefined,
  length_per_roll: undefined,
  description: '',
  price: undefined,
};

const schema = yup.object().shape({
  name: yup.string().required('Nama bahan tidak boleh kosong'),
  stock_length: yup.number().required('Total panjang bahan tidak boleh kosong'),
  material_width: yup.number().required('Lebar bahan tidak boleh kosong'),
  length_per_roll: yup.number().required('Panjang bahan per roll tidak boleh kosong'),
});

const FormMaterialPaper: FC<Props> = ({ id, sx, brand_id, defaultData }: Props) => {
  const dispatch = useDispatch();
  const { materialPaper } = useAppSelector((state) => state.inventory);
  const { data: masterData } = useAppSelector((state) => state.masterData);

  const isEditMode = useMemo(() => (id ? true : false), [id]);

  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
    reset,
  } = useForm({
    defaultValues,
    resolver: yupResolver(schema),
  });

  if (brand_id) setValue('brand_id', brand_id);

  const onSubmit: SubmitHandler<TInputs> = (values) => {
    if (!id) dispatch(createInventoryMaterialPaper(values));
    // if (!id) dispatch(materialPape(values))
    // else dispatch(updateJo(id, values))
  };

  useEffect(() => {
    if (id && defaultData) {
      setValue('name', defaultData.name);
      setValue('brand_id', defaultData.brand.id);
      setValue('description', defaultData.description);
      setValue('material_width', defaultData.material_width);
      setValue('price', defaultData.price);
      setValue('stock_length', defaultData.stock_length);
      setValue('length_per_roll', defaultData.length_per_roll);
    }
  }, [id, defaultData]);

  return (
    <Box sx={sx}>
      <Box sx={{ mt: 3, mb: 3 }}>
        <Typography variant="h5">{isEditMode ? 'Edit Bahan Kertas' : 'Tambah Bahan Kertas Baru'}</Typography>
      </Box>
      <Box
        sx={{
          display: 'flex',
          fontWeight: 'bold',
          alignItems: 'center',
          flexDirection: 'column',
          justifyContent: 'center',
          width: '100%',
        }}
        component="form"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Controller
              name="brand_id"
              control={control}
              render={({ field }) => (
                <FormControl variant="standard" size="small" margin="none" fullWidth>
                  <InputLabel id="select-category">Pilih Brand/Merk</InputLabel>
                  <Select
                    {...field}
                    labelId="select-category"
                    id="select-category"
                    label="Select Category"
                    MenuProps={MenuProps}
                    disabled={masterData.material_brands.length < 1}
                    defaultValue={materialPaper ? (materialPaper.brand ? materialPaper.brand.id : '') : ''}
                  >
                    {masterData.material_brands.length > 0 &&
                      masterData.material_brands.map(({ id: brandId, name: brandName }) => (
                        <MenuItem key={String(brandId)} value={brandId}>
                          {brandName}
                        </MenuItem>
                      ))}
                  </Select>
                  <FormHelperText error={Boolean(errors.brand_id?.message)}>{errors.brand_id?.message}</FormHelperText>
                </FormControl>
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="name"
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  fullWidth
                  variant="standard"
                  margin="none"
                  size="small"
                  label="Nama Bahan"
                  error={Boolean(errors.name?.message)}
                  helperText={errors.name?.message ?? 'Nama Bahan'}
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Controller
              name="stock_length"
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  variant="standard"
                  margin="none"
                  size="small"
                  type="number"
                  fullWidth
                  label="Total panjang bahan"
                  error={Boolean(errors.stock_length?.message)}
                  helperText={errors.stock_length?.message ?? 'Total Panjang bahan yang tersedia saat ini'}
                  InputProps={{
                    endAdornment: <InputAdornment position="start">cm</InputAdornment>,
                  }}
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Controller
              name="material_width"
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  variant="standard"
                  margin="none"
                  size="small"
                  fullWidth
                  label="Lebar Bahan"
                  error={Boolean(errors.material_width?.message)}
                  helperText={errors.material_width?.message ?? 'Lebar bahan dalam satuan cm'}
                  InputProps={{
                    endAdornment: <InputAdornment position="start">cm</InputAdornment>,
                  }}
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Controller
              name="length_per_roll"
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  variant="standard"
                  margin="none"
                  size="small"
                  fullWidth
                  label="Panjang per roll"
                  error={Boolean(errors.length_per_roll?.message)}
                  helperText={errors.length_per_roll?.message ?? 'Panjang bahan per 1 role'}
                  InputProps={{
                    endAdornment: <InputAdornment position="start">cm</InputAdornment>,
                  }}
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Controller
              name="price"
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  fullWidth
                  variant="standard"
                  margin="none"
                  size="small"
                  label="Harga Beli"
                  error={Boolean(errors.price?.message)}
                  helperText={errors.price?.message ?? '(Opsional) Harga beli'}
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="description"
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  multiline
                  fullWidth
                  rows={3}
                  variant="standard"
                  margin="none"
                  size="small"
                  label="Deskripsi/Catatan"
                  error={Boolean(errors.description?.message)}
                  helperText={errors.description?.message ? errors.description?.message : '(Opsional) Catatan..'}
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Button sx={{ mt: 1 }} type="submit" fullWidth variant="contained" disableElevation>
              {isEditMode ? 'Update' : 'Simpan'}
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

FormMaterialPaper.defaultProps = {
  sx: {},
  brand_id: null,
  defaultData: null,
};

export default FormMaterialPaper;
