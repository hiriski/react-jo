import { AxiosResponse } from 'axios';
import api from '../../utils/http';
import {
  TLoginUser,
  TRequestLoginWithSocialAccount,
  TRegisterUser,
  TRequestLogin,
  TRequestRegister,
  TLoginWithSocialAccount,
} from '../../types/auth';
import { IUser } from '@/interfaces/user';

const AuthAPI = {
  register: (data: TRequestRegister): Promise<AxiosResponse<TRegisterUser>> => api.post('/auth/register', data),

  login: (data: TRequestLogin): Promise<AxiosResponse<TLoginUser>> => api.post('/auth/login', data),

  revokeToken: (): Promise<AxiosResponse> => api.post('/auth/revoke-token'),

  user: (): Promise<AxiosResponse> => api.get('/auth/user'),

  loginWithSocialAcccount: (data: TRequestLoginWithSocialAccount): Promise<AxiosResponse<TLoginWithSocialAccount>> =>
    api.post('/auth/social', data),

  unlock: (body: TRequestLogin): Promise<AxiosResponse<undefined>> => api.post('/auth/unlock', body),
  getAuthenticatedUser: (): Promise<AxiosResponse<IUser>> => api.get('/auth/get-authenticated-user'),
};

export default AuthAPI;
