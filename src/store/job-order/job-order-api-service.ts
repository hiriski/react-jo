import { UUID } from '@/interfaces/common';
import { IJobOrder, IRequestBodyJobOrder } from '@/interfaces/job-order';
import { TCreateJo, TJo, TJoInvoiceData, TResponseListJo } from '@src/types/jo';
import api from '@src/utils/http';
import { AxiosResponse } from 'axios';
import { JobOrderApiEndpoints } from './job-order-api-endpoint';
import {
  IJobOrderPayloadUpdatePaymentStatus,
  IPayloadJobOrderDelete,
  IPayloadJobOrderStartProduction,
} from './job-order.interfaces';

export type TPayloadGetJoInvoiceData = {
  jo_ids: Array<number | string>;
};

const JobOrderApiService = {
  findAll: async (params?: Record<string, string | number>): Promise<AxiosResponse<TResponseListJo>> =>
    api.get(JobOrderApiEndpoints.index, { params }),

  create: async (body: IRequestBodyJobOrder): Promise<AxiosResponse<TJo>> => api.post(JobOrderApiEndpoints.index, body),

  find: async (id: UUID): Promise<AxiosResponse<TJo>> => api.get(`${JobOrderApiEndpoints.index}/${id}`),

  update: async (id: number, body: TCreateJo): Promise<AxiosResponse<TJo>> =>
    api.put(`${JobOrderApiEndpoints.index}/${id}`, body),

  getInvoiceData: async (arrJoIds: TPayloadGetJoInvoiceData): Promise<AxiosResponse<TJoInvoiceData>> =>
    api.post('${JobOrderApiEndpoints.index}/get-invoice-data', arrJoIds),

  updatePaymentStatus: async (body: IJobOrderPayloadUpdatePaymentStatus): Promise<AxiosResponse<TJo>> =>
    api.put(`${JobOrderApiEndpoints.updatePaymentStatus}`, body),

  updateProductionStatus: async (joId: number, productionStatusId: number): Promise<AxiosResponse<TJo>> =>
    api.put(`${JobOrderApiEndpoints.index}/update-production-status/${joId}`, {
      production_status_id: productionStatusId,
    }),

  updatePercentageProgress: async (joId: number, value: number): Promise<AxiosResponse<TJo>> =>
    api.put(`${JobOrderApiEndpoints.index}/update-percentage-progress/${joId}`, { value }),

  addNotes: async (joId: number, notes: string): Promise<AxiosResponse<TJo>> =>
    api.put(`${JobOrderApiEndpoints.index}/add-note/${joId}`, { notes }),

  startProduction: async (body: IPayloadJobOrderStartProduction): Promise<AxiosResponse<IJobOrder>> =>
    api.post(`${JobOrderApiEndpoints.startProduction}`, body),

  finishProduction: async (body: { job_order_id: UUID }): Promise<AxiosResponse<IJobOrder>> =>
    api.post(`${JobOrderApiEndpoints.finishProduction}`, body),

  delete: async (body: IPayloadJobOrderDelete): Promise<AxiosResponse<undefined>> =>
    api.post(`${JobOrderApiEndpoints.delete}`, body),
};

export default JobOrderApiService;
