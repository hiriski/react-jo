import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography'
import { MainLayout } from '@src/components/layouts'
import { RootState } from '@src/store/root-reducer'
import { FC, useEffect } from 'react'

const HomePage: FC = () => (
  <MainLayout>
    <Box
      sx={{
        display: 'flex',
        marginBottom: 3,
        flexDirection: { xs: 'column', md: 'row' },
      }}
    >
      <Card sx={{ textAlign: 'center' }}>
        <CardContent>
          <Typography variant="h5" sx={{ marginBottom: 1 }}>
            HomePage
          </Typography>
          <Typography
            variant="body2"
            sx={{ marginBottom: 1, fontWeight: '700' }}
          >
            Test
          </Typography>
          <Button variant="contained" disableElevation>
            Click Me
          </Button>
        </CardContent>
      </Card>
    </Box>
  </MainLayout>
)

export default HomePage
