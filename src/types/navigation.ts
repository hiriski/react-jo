import { ReactElement } from 'react'

export type TNavigation = {
  label: string
  icon: ReactElement
  path: string
}
