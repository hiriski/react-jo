import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import AddIcon from '@mui/icons-material/Add';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';

import SearchIcon from '@mui/icons-material/Search';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import IconButton from '@mui/material/IconButton';
import InputBase from '@mui/material/InputBase';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Slide from '@mui/material/Slide';
import { alpha, styled } from '@mui/material/styles';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import useScrollTrigger from '@mui/material/useScrollTrigger';
import { revokeToken } from '@src/store/auth/actions';
import { setPopUpSearchBox, setSidebarDrawer } from '@src/store/common/actions';
import { useAppSelector } from '@src/store/hook';
import { setDrawerAddEditJo } from '@/store/job-order/job-order-actions';
import { APP_NAME, ROUTES, SIDEBAR_DRAWER_WIDTH } from '@src/utils/constants';
import React, { FC, ReactElement, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link as RouterLink } from 'react-router-dom';
import { useTheme } from '@mui/material';

// Mui icons.
import MenuIcon from '@mui/icons-material/Menu';
import ArrowBack from '@mui/icons-material/ArrowBack';
import { useMediaQuery } from '@mui/material';
import { permissions_canCreateJobOrder } from '@/utils/permissions';

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
  matchSmallScreen: boolean;
}

const CustomAppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open' && prop !== 'matchSmallScreen',
})<AppBarProps>(({ theme, open, matchSmallScreen }) => ({
  borderBottom: `1px solid ${theme.palette.text.disabled}`,
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${SIDEBAR_DRAWER_WIDTH}px)`,
    marginLeft: `${SIDEBAR_DRAWER_WIDTH}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    ...(matchSmallScreen && {
      marginLeft: '0 !important',
      width: '100%',
    }),
  }),
}));

/** Search box inspired by Postman search box */
const Search = styled('div')(({ theme }) => ({
  width: 295,
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.black, 0.07),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.black, 0.05),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 1, 0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  width: '100%',
  '& .MuiInputBase-input': {
    fontSize: 14,
    padding: theme.spacing(0.85),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}));

type HideOnScrollProps = {
  children: ReactElement;
};

const HideOnScroll: FC<HideOnScrollProps> = ({ children }: HideOnScrollProps) => {
  const trigger = useScrollTrigger();
  return (
    <Slide timeout={500} appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
};

const AppAppBar: FC = () => {
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const { authenticatedUser: user } = useAppSelector((state) => state.auth);
  const { visibleSidebarDrawer } = useAppSelector((state) => state.common);

  const theme = useTheme();

  const matchSmallScreen = useMediaQuery(theme.breakpoints.down('md'));

  const isMenuOpen = Boolean(anchorEl);

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>): void => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = (): void => {
    setAnchorEl(null);
  };

  const handleLogout = (): void => {
    dispatch(revokeToken());

    // Close menu
    handleMenuClose();
  };

  const handleOpenDrawerAddJo = (): void => {
    dispatch(setDrawerAddEditJo(true));
  };

  const handleOpenSidebarDrawer = (): void => {
    dispatch(setSidebarDrawer(true));
  };

  const handleCloseSidebarDrawer = (): void => {
    dispatch(setSidebarDrawer(false));
  };

  const handleSearchBox = (): void => {
    dispatch(setPopUpSearchBox(true));
  };

  // Profile menu
  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem component={RouterLink} to={ROUTES.ACCOUNT}>
        <ListItemIcon>
          <AccountCircleIcon fontSize="small" />
        </ListItemIcon>
        <ListItemText>Profile</ListItemText>
      </MenuItem>
      <MenuItem onClick={handleLogout}>
        <ListItemIcon>
          <ExitToAppIcon sx={{ color: 'red' }} fontSize="small" />
        </ListItemIcon>
        <ListItemText>Log Out</ListItemText>
      </MenuItem>
    </Menu>
  );

  return (
    <>
      {/* <HideOnScroll> */}
      <CustomAppBar
        position="fixed"
        elevation={0} /** <-- fix warning has no effect */
        variant="outlined"
        color="inherit"
        sx={{ border: 'none !important' }}
        open={visibleSidebarDrawer}
        matchSmallScreen={matchSmallScreen}
      >
        <Container maxWidth={false}>
          <Toolbar disableGutters>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={visibleSidebarDrawer ? handleCloseSidebarDrawer : handleOpenSidebarDrawer}
              sx={{ mr: 2 }}
            >
              {visibleSidebarDrawer ? (
                <ArrowBack sx={{ color: 'text.secondary', fontSize: 20 }} />
              ) : (
                <MenuIcon sx={{ color: 'text.secondary', fontSize: 20 }} />
              )}
            </IconButton>

            <Box
              component={RouterLink}
              to="/"
              sx={{
                display: 'flex',
                alignItems: 'center',
                textDecoration: 'none',
                justifyContent: 'center',
              }}
            >
              {/* <Logo width={28} /> */}
              <Typography
                component="h1"
                sx={{
                  fontWeight: 'bold',
                  fontSize: 16,
                  color: 'text.primary',
                }}
              >
                {APP_NAME}
              </Typography>
            </Box>
            <Box sx={{ flexGrow: 1 }} />
            <Box
              sx={{
                display: {
                  xs: 'none',
                  md: 'flex',
                },
              }}
            >
              <Search>
                <SearchIconWrapper>
                  <SearchIcon sx={{ fontSize: 18, color: 'text.disabled' }} />
                </SearchIconWrapper>
                <StyledInputBase
                  fullWidth
                  size="small"
                  onClick={handleSearchBox}
                  placeholder="Search…"
                  inputProps={{ 'aria-label': 'search' }}
                />
              </Search>
            </Box>
            <Box sx={{ flexGrow: 1 }} />
            <Box sx={{ display: { xs: 'flex' } }}>
              {permissions_canCreateJobOrder(user) && (
                <Button
                  size="small"
                  onClick={handleOpenDrawerAddJo}
                  color="secondary"
                  startIcon={<AddIcon />}
                  variant="contained"
                  disableElevation
                >
                  Buat Jo
                </Button>
              )}

              <Button
                aria-label="account of current user"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleProfileMenuOpen}
                color="inherit"
                sx={{
                  borderRadius: 6,
                  px: 1.6,
                  textTransform: 'none',
                  mr: -1.5 /* it's same with edge="end" */,
                }}
              >
                <Typography
                  variant="subtitle2"
                  component="h6"
                  sx={{
                    fontWeight: 'bold',
                  }}
                >
                  {user.name}
                </Typography>
              </Button>
            </Box>
          </Toolbar>
        </Container>
      </CustomAppBar>
      {/* </HideOnScroll> */}
      {renderMenu}
    </>
  );
};

export default AppAppBar;
