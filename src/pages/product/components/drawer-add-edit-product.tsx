import { BoxSpinner } from '@/components/ui';
import { masterData_fetchMasterData } from '@/store/master-data/master-data-actions';
import { fetchProduct, setDrawerAddEditProduct } from '@/store/product/product-actions';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import { alpha } from '@mui/material/styles';
import { useAppSelector } from '@src/store/hook';
import { FC, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import Swall from 'sweetalert2';

import ProductForm from './product-form';

const DrawerAddEditProduct: FC = () => {
  const { drawerAddEditProduct: drawer, detailProduct, formIsDirty } = useAppSelector((state) => state.product);
  const masterData = useAppSelector((state) => state.masterData);

  const dispatch = useDispatch();

  // handle close drawer.
  const onClose = (): void => {
    if (!formIsDirty) {
      dispatch(setDrawerAddEditProduct(false, null));
    } else {
      // Show dialog confirmation if form is dirty
      Swall.fire({
        title: 'Yakin ingin keluar ?',
        icon: 'warning',
        html: '<span>Apakah kamu yakin ingin keluar ?<span><br /> <span>Perubahan yang kamu isi akan dihapus!</span>',
        showCancelButton: true,
        confirmButtonText: 'Ya, keluar',
        cancelButtonText: 'Tetap disini',
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(setDrawerAddEditProduct(false, null));
        } else {
          console.log('cancelled');
        }
      });
    }
  };

  // Effect.
  useEffect(() => {
    if (drawer.open) {
      dispatch(
        masterData_fetchMasterData([
          {
            object_name: 'product_categories',
          },
        ]),
      );
    }

    if (drawer.id) {
      dispatch(fetchProduct(drawer.id));
    }
  }, [drawer.open]);

  return (
    <Drawer
      elevation={0}
      anchor="right"
      open={drawer.open}
      onClose={onClose}
      BackdropProps={{
        sx: {
          backgroundColor: (theme) => alpha(theme.palette.text.primary, 0.4),
        },
      }}
      PaperProps={{
        sx: {
          width: { xs: '100%', md: '80%', lg: '70%' },
          // position: 'relative',
        },
      }}
    >
      {/* <Fab size="medium" color="primary" onClick={onClose} aria-label="Add product">
        <CloseOutlinedIcon />
      </Fab> */}

      <Box role="presentation" sx={{ position: 'relative' /* px: 8, pt: 3, pb: 5 */ }}>
        {detailProduct.isFetching || masterData.isFetching ? (
          <BoxSpinner height="100%" />
        ) : !drawer.id ? (
          <ProductForm />
        ) : (
          detailProduct.data && <ProductForm data={detailProduct.data} />
        )}
      </Box>
    </Drawer>
  );
};

export default DrawerAddEditProduct;
