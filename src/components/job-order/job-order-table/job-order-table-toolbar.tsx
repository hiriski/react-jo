import FilterListIcon from '@mui/icons-material/FilterList';
import ReceiptIcon from '@mui/icons-material/Receipt';
import RefreshIcon from '@mui/icons-material/Refresh';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Toolbar from '@mui/material/Toolbar';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import { CustomButton } from '@src/components/ui';
import { fetchListJo, jobOrder_setFilters } from '@/store/job-order/job-order-actions';
import { ROUTES } from '@src/utils/constants';
import { ChangeEvent, FC, MouseEvent, MouseEventHandler, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { UUID } from '@/interfaces/common';
import {
  styled,
  alpha,
  ToggleButton,
  ToggleButtonGroup,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Grid,
  SelectChangeEvent,
  useTheme,
} from '@mui/material';
import { useAppSelector } from '@/store';
import moment from 'moment';
import { MobileDatePicker } from '@mui/x-date-pickers/MobileDatePicker';
import { IAnalyticsTotalJobOrder } from '@/store/analytics/analytics.interface';
import { SyncOutline, TimeOutline, CheckmarkCircleOutline } from 'react-ionicons';
import { JobOrderProductionStatusIdConstants } from '@/constants';
import { dropdownJobOrderPeriod, dropdownJobOrderProductionStatus } from '@/lib/dropdown';
import { JobOrderFilterPeriod } from '@/interfaces/job-order';
import { jobOrder_rootSelector } from '@/store/job-order/job-order-selectors';

interface Props {
  numSelected: number;
  selected: Array<UUID>;
}

const StyledToggleButtonGroup = styled(ToggleButtonGroup)(({ theme }) => ({
  display: 'flex',
}));
const StyledToggleButton = styled(ToggleButton)(({ theme }) => ({
  color: theme.palette.primary.main,
  // textTransform: 'capitalize',
  // backgroundColor: theme.palette.primary.light,
  border: 'none !important',
  padding: theme.spacing(0.6, 1.6),

  borderRadius: Number(theme.shape.borderRadius) + 'px !important',
  '&.Mui-selected': {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.primary.contrastText,
    },
  },
  '&:hover': {
    backgroundColor: alpha(theme.palette.primary.main, 0.15),
  },

  // Icon
  '& span': {
    height: 16,
    marginRight: 4,
  },

  '& p': { fontSize: '0.8rem' },
}));

export const JobOrderTableToolbar: FC<Props> = (props: Props) => {
  const { numSelected, selected } = props;
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const theme = useTheme();

  const { totalJobOrder } = useAppSelector((state) => state.analytics);
  const { filters, listJo } = useAppSelector((s) => jobOrder_rootSelector(s));

  const handleRefresh = (): void => {
    dispatch(fetchListJo({ shouldLoading: true }));
  };

  const handleGetInvoiceData = (): void => {
    navigate(ROUTES.INVOICE_GENERATOR + '?JobOrderIds=' + selected.toString());
  };

  const [selectedPeriod, setSelectedPeriod] = useState<JobOrderFilterPeriod>(null);
  const [customDate, setCustomDate] = useState<undefined | string>(undefined);

  const onChangeProductionStatus = (e: MouseEvent<HTMLElement>, newValue: number): void => {
    /** force to set custom date to undefined */
    setCustomDate(undefined);
    // setSelectedProductionStatusId(newValue);
    dispatch(jobOrder_setFilters('production_status_id', newValue));
  };

  const onChangePeriod = (e: SelectChangeEvent): void => {
    setCustomDate(undefined); /** force to set custom date to undefined */

    // console.log('e.target.value', e.target.value);
    // setSelectedPeriod(e.target.value as JobOrderFilterPeriod);
    dispatch(jobOrder_setFilters('period', e.target.value));
  };

  /**
   * Handle change custom date.
   * @param {string} value
   * @param {string} keyboardInputValue
   */
  const onChangeCustomDate = (value: string, keyboardInputValue?: string): void => {
    setSelectedPeriod(undefined);
    setCustomDate(moment(value).format('YYYY-MM-DD'));
  };

  return (
    <Toolbar
      sx={{
        px: { sm: 2 },
        borderBottom: `1px solid ${theme.palette.divider}`,
        ...(numSelected > 0 && {
          // bgcolor: (theme) => alpha(theme.palette.primary.main, theme.palette.action.activatedOpacity),
          backgroundColor: theme.palette.primary.main,
        }),
      }}
    >
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          flex: 1,
        }}
      >
        {numSelected > 0 ? (
          <>
            <Typography sx={{ flex: '1 1 100%' }} color="inherit" variant="subtitle1" component="div">
              {numSelected} selected
            </Typography>
            <Box sx={{ ml: 'auto' }}>
              <Tooltip title="Generate Invoice">
                <CustomButton
                  disableElevation
                  color="#000"
                  backgroundColor="#fbfbfb"
                  startIcon={<ReceiptIcon />}
                  onClick={handleGetInvoiceData}
                  size="small"
                >
                  Generate Invoice
                </CustomButton>
              </Tooltip>
            </Box>
          </>
        ) : (
          <Box
            sx={{
              flex: 1,
              display: 'flex',
              alignItems: 'center',
              flexDirection: {
                xs: 'column',
                md: 'row',
              },
            }}
          >
            <Box
              sx={{
                ml: 1,
                mr: 2,
                display: 'flex',
                alignItems: 'center',
                cursor: 'pointer',
                '& span': { lineHeight: 1 },
              }}
              onClick={handleRefresh}
            >
              <SyncOutline
                rotate={listJo.isFetching}
                color={listJo.isFetching ? 'red' : '#797979'}
                height="20px"
                width="20px"
              />
            </Box>
            <FormControl sx={{ width: 140 }}>
              <InputLabel size="small" variant="outlined">
                Periode
              </InputLabel>
              <Select size="small" value={filters.period} label="Periode" onChange={onChangePeriod}>
                {dropdownJobOrderPeriod.map((x, index) => (
                  <MenuItem key={String(index)} value={x.value}>
                    {x.label}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            {/* <MobileDatePicker
              closeOnSelect
              openTo="day"
              views={['day']}
              label="Tanggal"
              inputFormat="dd-mm-yyyy"
              value={customDate}
              onChange={onChangeCustomDate}
              renderInput={(params) => <TextField {...params} size="small" />}
            /> */}

            <Box sx={{ ml: 'auto' }}>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Typography sx={{ mr: 2 }} variant="body2">
                  Status Produksi
                </Typography>
                <Box
                  sx={{
                    px: 1.2,
                    py: 0.8,
                    borderRadius: 1.5,
                    backgroundColor: 'background.paper',
                    border: (theme) => `1px solid ${theme.palette.primary.light}`,
                  }}
                >
                  <StyledToggleButtonGroup
                    color="standard"
                    size="small"
                    value={filters.production_status_id}
                    onChange={onChangeProductionStatus}
                    exclusive
                  >
                    {dropdownJobOrderProductionStatus.map((period) => (
                      <StyledToggleButton
                        disableRipple
                        disableFocusRipple
                        key={String(period.value)}
                        value={period.value}
                      >
                        {period.value === JobOrderProductionStatusIdConstants.InProgress && (
                          <SyncOutline color="inherit" width="16px" height="16px" />
                        )}
                        {period.value === JobOrderProductionStatusIdConstants.Waiting && (
                          <TimeOutline color="inherit" width="16px" height="16px" />
                        )}
                        {period.value === JobOrderProductionStatusIdConstants.Done && (
                          <CheckmarkCircleOutline color="inherit" width="16px" height="16px" />
                        )}
                        <Typography>{period.label}</Typography>
                      </StyledToggleButton>
                    ))}
                  </StyledToggleButtonGroup>
                </Box>
              </Box>
              {/* <TableToolbarExport /> */}
            </Box>
          </Box>
        )}
      </Box>
    </Toolbar>
  );
};
