import { TJoProductionStatus } from '@src/types/jo'

import { ProductionStatusAction } from './actions'
import * as ActionTypes from './constants'

export interface ProductionStatusState {
  isFetching: boolean
  isError: boolean
  data: Array<TJoProductionStatus>
}

const initialState: ProductionStatusState = {
  isFetching: false,
  isError: false,
  data: [],
}

const productionStatusReducer = (
  state: ProductionStatusState = initialState,
  action: ProductionStatusAction
): ProductionStatusState => {
  switch (action.type) {
    case ActionTypes.FETCHING_LIST_PRODUCTION_STATUS_LOADING:
      return {
        ...state,
        isFetching: true,
        isError: false,
        data: [],
      }
    case ActionTypes.FETCHING_LIST_PRODUCTION_STATUS_FAILURE:
      return {
        ...state,
        isFetching: false,
        isError: true,
        data: [],
      }
    case ActionTypes.FETCHING_LIST_PRODUCTION_STATUS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        data: action.payload,
      }

    default:
      return state
  }
}

export default productionStatusReducer
