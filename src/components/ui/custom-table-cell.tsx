import Box from '@mui/material/Box';
import TableCell, { TableCellProps } from '@mui/material/TableCell';
import { FC, MouseEvent, ReactNode } from 'react';

type Props = Pick<TableCellProps, 'align' | 'padding' | 'size' | 'variant' | 'sx' | 'component'> & {
  children: ReactNode;
  maxWidth?: number | string;
  onClick?: (e: MouseEvent<any>) => void;
};

const OverflowTableCell: FC<Props> = (props: Props) => {
  const { children, align, padding, size, variant, component, sx, maxWidth } = props;
  return (
    <TableCell align={align} padding={padding} component={component} size={size} variant={variant} sx={{ ...sx }}>
      <Box sx={{ overflow: 'hidden', whiteSpace: 'nowrap', maxWidth, textOverflow: 'elipsis', overflowX: 'hidden' }}>
        {children}
      </Box>
    </TableCell>
  );
};

OverflowTableCell.defaultProps = {
  align: 'left',
  maxWidth: '100%',
};

export default OverflowTableCell;
