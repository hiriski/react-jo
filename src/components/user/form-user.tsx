import { ControlledSelect, SectionTitle } from '@components/ui';
import { yupResolver } from '@hookform/resolvers/yup';
import PeopleIcon from '@mui/icons-material/People';
import { InputAdornment } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import MenuItem from '@mui/material/MenuItem';
import TextField from '@mui/material/TextField';
import { SxProps } from '@mui/material';
import { useAppSelector } from '@src/store/hook';
import { createUser } from '@src/store/user/actions';
import { TCustomer } from '@src/types/customer';
import { TBrandMaterialPaper } from '@src/types/inventory';
import { TRole } from '@src/types/role';
import { TCreateUser } from '@src/types/user';
import { FC, useEffect } from 'react';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import * as yup from 'yup';

type TInputs = TCreateUser;

type SourceProps = { brands: Array<TBrandMaterialPaper> };

type Props = {
  sx?: SxProps;
  id?: number | null;
  data?: TCustomer | null;
  listRole: TRole[];
};

const defaultValues = {
  name: '',
  email: '',
  password: '',
  role_id: undefined,
};

const schema = yup.object().shape({
  name: yup.string().required('Nama tidak boleh kosong'),
  email: yup.string().required('Nama tidak boleh kosong'),
  password: yup.string().required('Nama tidak boleh kosong'),
  role_id: yup.string().required('Nama tidak boleh kosong'),
});

const FormUser: FC<Props> = ({ id, data, sx, listRole }: Props) => {
  const dispatch = useDispatch();
  const { createCustomer: create, updateCustomer: update } = useAppSelector((state) => state.customer);

  const isEditMode = Boolean(id);

  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
    reset,
  } = useForm({
    defaultValues,
    resolver: yupResolver(schema),
  });

  const onSubmit: SubmitHandler<Partial<TInputs>> = (values) => {
    console.log('values', values);
    // if (!id) dispatch(createUser(values))
    // else dispatch(updateCustomer(id, values))
  };

  // useEffect(() => {
  //   if (data) {
  //     setValue('name', data.name)
  //     setValue('email', data.email)
  //   }
  // }, [id, data])

  return (
    <Box
      sx={{
        display: 'flex',
        fontWeight: 'bold',
        flexDirection: 'column',
        width: '100%',
        ...sx,
      }}
    >
      <SectionTitle title={isEditMode ? 'Edit User' : 'Tambah User'} />
      <Box component="form" onSubmit={handleSubmit(onSubmit)}>
        <Box
          sx={{
            width: 88,
            height: 88,
            margin: '0 auto',
            mb: 2,
            cursor: 'pointer',
            borderWidth: 1,
            border: 'solid',
            borderStyle: 'solid',
            borderRadius: '88px', // don't use number
            textAlign: 'center',
            borderColor: 'divider',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <PeopleIcon sx={{ fontSize: 32 }} />
        </Box>

        <ControlledSelect control={control} name="role_id" errors={errors} helperText="Pilih Role" label="Role">
          <MenuItem value={null}>None</MenuItem>
          {listRole.length > 0 &&
            listRole.map(({ id: catId, name: catName }) => (
              <MenuItem key={String(catId)} value={catId}>
                {catName}
              </MenuItem>
            ))}
        </ControlledSelect>

        <Controller
          name="name"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              fullWidth
              margin="dense"
              size="small"
              label="Nama"
              error={Boolean(errors.name?.message)}
              helperText={errors.name?.message ?? 'Nama User'}
            />
          )}
        />
        <Controller
          name="email"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              fullWidth
              margin="dense"
              size="small"
              label="Nama"
              error={Boolean(errors.email?.message)}
              helperText={errors.email?.message ?? 'Nama User'}
            />
          )}
        />
        <Controller
          name="password"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              fullWidth
              margin="dense"
              size="small"
              label="Nama"
              error={Boolean(errors.password?.message)}
              helperText={errors.password?.message ?? 'Password'}
            />
          )}
        />

        {create.isLoading || update.isLoading ? (
          <Button disabled sx={{ mt: 1 }} type="submit" fullWidth variant="contained" disableElevation>
            <CircularProgress size={24} />
          </Button>
        ) : (
          <Button sx={{ mt: 1 }} type="submit" fullWidth variant="contained" disableElevation>
            {!isEditMode ? 'Simpan' : 'Update'}
          </Button>
        )}
      </Box>
    </Box>
  );
};

FormUser.defaultProps = {
  id: null,
  sx: {},
};

export default FormUser;
