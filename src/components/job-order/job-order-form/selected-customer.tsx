import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';
import LocalPhoneOutlinedIcon from '@mui/icons-material/LocalPhoneOutlined';
import LocationOnOutlinedIcon from '@mui/icons-material/LocationOnOutlined';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import { TCustomer } from '@src/types/customer';
import { colors } from '@src/utils/colors';
import { getInitialsName } from '@src/utils/misc';
import { FC, memo } from 'react';

type Props = {
  data: TCustomer;
};

const SelectedCustomer: FC<Props> = ({ data }: Props) => {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        position: 'relative',
        border: (theme) => ({ border: `1px solid ${theme.palette.divider}` }),
        backgroundColor: 'background.paper',
        borderRadius: 1.2,
        px: 2,
        py: 2,
      }}
    >
      <Box sx={{ mb: 1.5 }}>
        <Avatar
          sx={{
            bgcolor: colors[Math.floor(Math.random() * colors.length)],
            color: 'primary.contrastText',
            width: 54,
            height: 54,
            fontSize: 16,
            letterSpacing: 1,
          }}
          alt={data.name}
        >
          {getInitialsName(data.name)}
        </Avatar>
      </Box>
      <Box>
        <Typography sx={{ mb: 1.5, mt: 0.6, fontSize: 19 }} variant="h5" component="h4">
          {data.name}
        </Typography>

        <List sx={{ width: '100%' }} disablePadding>
          <ListItem disableGutters disablePadding>
            <ListItemIcon sx={{ minWidth: 32 }}>
              <LocalPhoneOutlinedIcon sx={{ fontSize: 18 }} />
            </ListItemIcon>

            <ListItemText
              sx={{
                '* ': {
                  fontSize: 15,
                },
              }}
              primary={data.phone_number ?? '-'}
            />
          </ListItem>

          <ListItem disableGutters disablePadding>
            <ListItemIcon sx={{ minWidth: 32 }}>
              <EmailOutlinedIcon sx={{ fontSize: 18 }} />
            </ListItemIcon>
            <ListItemText
              sx={{
                '* ': {
                  fontSize: 15,
                },
              }}
              primary={data.email ?? '-'}
            />
          </ListItem>

          <ListItem disableGutters disablePadding>
            <ListItemIcon sx={{ minWidth: 32 }}>
              <LocationOnOutlinedIcon sx={{ fontSize: 18 }} />
            </ListItemIcon>
            <ListItemText
              sx={{
                '* ': {
                  fontSize: 15,
                },
              }}
              // primary={data.address ?? '-'}
            />
          </ListItem>
        </List>
      </Box>
    </Box>
  );
};

const MemoizedSelectedCustomer = memo(SelectedCustomer);

export default MemoizedSelectedCustomer;
