import { SectionTitle } from '@components/ui'
import { Box, Grid } from '@mui/material'
import { useAppSelector } from '@src/store/hook'
import { FC } from 'react'

import { CustomerEmpty, CustomerItem, CustomerItemSkeleton } from '.'

const CustomerList: FC = () => {
  const { listCustomer } = useAppSelector((state) => state.customer)
  const { isFetching, data } = listCustomer

  return (
    <>
      <SectionTitle title="Customer List" />
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
          pb: 10, // spacing for fixed pagination
        }}
      >
        <Grid container spacing={2}>
          {isFetching
            ? new Array(24).fill('.').map((item, index) => (
                <Grid item xs={12} md={4} key={String(item + index)}>
                  <CustomerItemSkeleton />
                </Grid>
              ))
            : Array.isArray(data) &&
              data.length > 0 &&
              data.map((item) => (
                <Grid key={String(item.id)} item xs={12} md={4}>
                  <CustomerItem item={item} />
                </Grid>
              ))}
        </Grid>
      </Box>
      {!isFetching && data.length === 0 && <CustomerEmpty text="Empty :(" />}
    </>
  )
}

export default CustomerList
