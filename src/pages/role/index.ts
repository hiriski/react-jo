import { lazy } from 'react'

const RolePage = lazy(() => import('./role'))

export default RolePage
