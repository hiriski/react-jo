import { FC } from 'react';

// Mui components.
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

type Props = {
  title: string;
  subtitle?: string;
};

const PageTitle: FC<Props> = ({ title, subtitle }) => (
  <Box
    sx={{
      mb: 3,
    }}
  >
    <Typography component="h1" variant="h3">
      {title}
    </Typography>
  </Box>
);

export default PageTitle;
