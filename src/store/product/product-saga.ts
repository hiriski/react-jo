import { IProduct } from '@/types/product'
import { httpResponseCreated, httpResponseOK } from '@/utils/http'
import { scrollToTop } from '@/utils/ui'
import { SagaIterator } from 'redux-saga'
import { SagaReturnType, SimpleEffect, call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { setAlert } from '../alert/actions'
import { PRODUCT_ACTION_TYPES } from './product-action-types.enum'
// Action types
import {
  createProductFailure,
  createProductLoading,
  createProductSuccess,
  fetchProductFailure,
  fetchProductListFailure,
  fetchProductListLoading,
  fetchProductListSuccess,
  fetchProductLoading,
  fetchProductSuccess,
  fetchProductList as fetchProducts,
  setDrawerAddEditProduct,
} from './product-actions'
// Product API
import ProductAPI from './product-api.service'

type APIResponseProductList = SagaReturnType<typeof ProductAPI.findAll>
type APIResponseDetailProduct = SagaReturnType<typeof ProductAPI.find>
type APIResponseCreateProduct = SagaReturnType<typeof ProductAPI.create>

function* fetchProductList({
  payload,
}: SimpleEffect<
  typeof PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LIST_REQUESTED,
  Record<string, string | number>
>): SagaIterator {
  yield put(fetchProductListLoading())
  try {
    const { status, data }: APIResponseProductList = yield call(ProductAPI.findAll, payload)
    if (httpResponseOK(status)) {
      yield put(fetchProductListSuccess(data))
    }
  } catch (e) {
    yield put(fetchProductListFailure())
  }
}

function* fetchProductById({
  payload,
}: SimpleEffect<typeof PRODUCT_ACTION_TYPES.FETCH_PRODUCT_REQUESTED, string>): SagaIterator {
  yield put(fetchProductLoading())
  try {
    const { status, data }: APIResponseDetailProduct = yield call(ProductAPI.find, payload)
    if (httpResponseOK(status)) {
      yield put(fetchProductSuccess(data))
      yield put(setDrawerAddEditProduct(false, null))
    }
  } catch (e) {
    yield put(fetchProductFailure())
  }
}

// Create product.
type ICreateProductPayload = SimpleEffect<typeof PRODUCT_ACTION_TYPES.CREATE_PRODUCT_REQUESTED, IProduct>
function* createProduct({ payload }: ICreateProductPayload): SagaIterator {
  yield put(createProductLoading())
  try {
    const { status, data }: APIResponseCreateProduct = yield call(ProductAPI.create, payload)
    if (httpResponseCreated(status)) {
      yield put(createProductSuccess(data))

      // Close drawer add or edit produk.

      yield put(setDrawerAddEditProduct(false, null))

      const refetchParams = { limit: 12 }
      yield put(fetchProducts(refetchParams))

      // Scroll to top
      scrollToTop()

      // display alert
      yield put(setAlert({ open: true, message: 'Produk barhasil dibuat', severity: 'success' }))
    }
  } catch (e) {
    console.log(e)
    yield put(createProductFailure())
  }
}

export default function* productSaga(): SagaIterator {
  yield takeEvery(PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LIST_REQUESTED, fetchProductList)
  yield takeEvery(PRODUCT_ACTION_TYPES.FETCH_PRODUCT_REQUESTED, fetchProductById)
  yield takeEvery(PRODUCT_ACTION_TYPES.CREATE_PRODUCT_REQUESTED, createProduct)
}
