import { ANALYTICS_ACTION_TYPES as ActionTypes } from './analytics-action-types.enum';
import { AnalyticsActions } from './analytics-actions';
import { IAnalyticsTotalJobOrder, JobOrderTotalPeriod } from './analytics.interface';

export interface AnalyticsState {
  totalJobOrder: {
    isFetching: boolean;
    isError: boolean;
    data: {
      overtime: IAnalyticsTotalJobOrder;
      today: IAnalyticsTotalJobOrder;
      week: IAnalyticsTotalJobOrder;
      month: IAnalyticsTotalJobOrder;
      year: IAnalyticsTotalJobOrder;
      customDate: IAnalyticsTotalJobOrder;
    };
  };
}

const initialTotalJobOrderData = {
  total: 0,
  waiting: 0,
  in_progress: 0,
  done: 0,
};

const initialState = {
  totalJobOrder: {
    isFetching: false,
    isError: false,
    data: {
      overtime: initialTotalJobOrderData,
      today: initialTotalJobOrderData,
      week: initialTotalJobOrderData,
      month: initialTotalJobOrderData,
      year: initialTotalJobOrderData,
      customDate: initialTotalJobOrderData,
    },
  },
};

const analyticsReducer = (state: AnalyticsState = initialState, action: AnalyticsActions): AnalyticsState => {
  switch (action.type) {
    // Fetch total job order.
    case ActionTypes.FETCH_TOTAL_JOB_ORDER_LOADING:
      return {
        ...state,
        totalJobOrder: {
          ...state.totalJobOrder,
          isFetching: true,
          isError: false,
        },
      };
    case ActionTypes.FETCH_TOTAL_JOB_ORDER_FAILURE:
      return {
        ...state,
        totalJobOrder: {
          ...state.totalJobOrder,
          isFetching: false,
          isError: true,
        },
      };
    case ActionTypes.FETCH_TOTAL_JOB_ORDER_SUCCESS:
      return {
        ...state,
        totalJobOrder: {
          ...state.totalJobOrder,
          isFetching: false,
          isError: false,
          data: {
            ...state.totalJobOrder.data,
            ...(action.payload.period === 'overtime' && {
              overtime: action.payload.data,
            }),
            ...(action.payload.period === 'today' && {
              today: action.payload.data,
            }),
            ...(action.payload.period === 'week' && {
              week: action.payload.data,
            }),
            ...(action.payload.period === 'month' && {
              month: action.payload.data,
            }),
            ...(action.payload.period === 'month' && {
              year: action.payload.data,
            }),
            ...(action.payload.date && {
              customDate: action.payload.data,
            }),
          },
        },
      };

    default:
      return state;
  }
};

export default analyticsReducer;
