import { TJoProductionStatus } from '@src/types/jo'

import * as ActionTypes from './constants'

// Actions definiton.
interface FetchingListProductionStatusRequested {
  type: typeof ActionTypes.FETCHING_LIST_PRODUCTION_STATUS_REQUESTED
}
interface FetchingListProductionStatusLoading {
  type: typeof ActionTypes.FETCHING_LIST_PRODUCTION_STATUS_LOADING
}
interface FetchingListProductionStatusFailure {
  type: typeof ActionTypes.FETCHING_LIST_PRODUCTION_STATUS_FAILURE
}
interface FetchingListProductionStatusSuccess {
  type: typeof ActionTypes.FETCHING_LIST_PRODUCTION_STATUS_SUCCESS
  payload: Array<TJoProductionStatus>
}

// Union actions type
export type ProductionStatusAction =
  | FetchingListProductionStatusRequested
  | FetchingListProductionStatusLoading
  | FetchingListProductionStatusFailure
  | FetchingListProductionStatusSuccess

// Actions creator.
export const fetchListProductionStatus =
  (): FetchingListProductionStatusRequested => ({
    type: ActionTypes.FETCHING_LIST_PRODUCTION_STATUS_REQUESTED,
  })

export const fetchingListProductionStatusLoading =
  (): FetchingListProductionStatusLoading => ({
    type: ActionTypes.FETCHING_LIST_PRODUCTION_STATUS_LOADING,
  })

export const fetchingListProductionStatusFailure =
  (): FetchingListProductionStatusFailure => ({
    type: ActionTypes.FETCHING_LIST_PRODUCTION_STATUS_FAILURE,
  })

export const fetchingListProductionStatusSuccess = (
  payload: Array<TJoProductionStatus>
): FetchingListProductionStatusSuccess => ({
  type: ActionTypes.FETCHING_LIST_PRODUCTION_STATUS_SUCCESS,
  payload,
})
