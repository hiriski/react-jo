import { lazy } from 'react'

const SettingPage = lazy(() => import('./setting'))

export default SettingPage
