import { FC, useEffect, useMemo, useState } from 'react';

// Mui components.
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import DoneIcon from '@mui/icons-material/Done';

// Base components.
import { BaseDialog, BaseLoader } from '@/components/base';

// Hooks
import { useAppSelector } from '@src/store/hook';
import { useDispatch } from 'react-redux';

import PrintingMachine from '@/assets/images/printing-machine.png';

// Redux
import { jobOrder_setDialogStartProduction, jobOrder_startProduction } from '@/store/job-order/job-order-actions';
import { jobOrder_rootSelector } from '@/store/job-order/job-order-selectors';
import { masterData_rootSelector } from '@/store/master-data/master-data-selectors';
import { machine_rootSelector } from '@/store/machine/machine-selectors';
import { machine_fetchList } from '@/store/machine/machine-actions';
import { BoxSpinner } from '../ui';
import { alpha, InputLabel, MenuItem, Select } from '@mui/material';
import { masterData_fetchMasterData } from '@/store/master-data/master-data-actions';
import { UUID } from '@/interfaces/common';
import { auth_rootSelector } from '@/store/auth/auth-selectors';
import { RoleIdConstants } from '@/constants';
import { IUser } from '@/interfaces/user';
import Swall from 'sweetalert2';

export const JobOrderDialogStartProduction: FC = () => {
  const dispatch = useDispatch();
  const { dialogStartProduction, startProduction } = useAppSelector((state) => jobOrder_rootSelector(state));
  const { listMachine } = useAppSelector((state) => machine_rootSelector(state));
  const { data: masterData, isFetching: isFetchingMasterData } = useAppSelector((state) =>
    masterData_rootSelector(state),
  );
  const userId = useAppSelector((state) => auth_rootSelector(state).authenticatedUser.id);
  const { open, job_order_id } = dialogStartProduction;

  const [checkedItems, setCheckedItems] = useState<Array<{ id: number; label: string; checked: boolean }>>([]);
  const [selectedUserId, setSelectedUserId] = useState<UUID | null>(userId ?? null);

  // List of user with role Production Team
  const listOfUserWithRoleTimProduction = useMemo<Array<IUser>>(() => {
    if (masterData.users) return masterData.users.filter((x) => x.role.id === RoleIdConstants.ProductionTeam);
    else return [];
  }, [masterData.users]);

  /**
   * Handle close dialog.
   */
  const handleClose = (): void => {
    dispatch(jobOrder_setDialogStartProduction(false, null));
  };

  /**
   * Handle confirm.
   */
  const handleConfirm = (): void => {
    const machine_ids = checkedItems.filter((x) => x.checked).map((x) => x.id) || [];

    if (machine_ids.length > 1) {
      Swall.fire({
        title: `Yakin Memulai Produksi dengan ${machine_ids.length} Mesin ?`,
        icon: 'warning',
        html: `<span>Job Order ini akan di kaitkan dengan ${machine_ids.length} mesin yang Anda pilih <span>`,
        showCancelButton: true,
        confirmButtonText: 'Ya, Mulai Produksi',
        cancelButtonText: 'Tidak',
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(jobOrder_startProduction({ job_order_id, machine_ids, progress_by_user_id: selectedUserId }));
        } else {
          console.log('cancelled');
        }
      });
    } else {
      dispatch(jobOrder_startProduction({ job_order_id, machine_ids, progress_by_user_id: selectedUserId }));
    }
  };

  /**
   * Handle confirm.
   */
  const handleChange = (id: number, checked: boolean): void => {
    const updatedCheckedItems = checkedItems.map((x) => (x.id === id ? { ...x, checked: !checked } : x));
    setCheckedItems(updatedCheckedItems);
  };

  const checkedItemsLength = useMemo(() => {
    return checkedItems.filter((x) => x.checked).length;
  }, [checkedItems, listMachine.data]);

  useEffect(() => {
    if (open && job_order_id) {
      if (!listMachine.data.length) dispatch(machine_fetchList());
      if (!masterData.users.length) dispatch(masterData_fetchMasterData([{ object_name: 'users' }]));
    }
  }, [open]);

  useEffect(() => {
    if (!open) {
      // Clean up!
      return () => {
        setCheckedItems([]);
        setSelectedUserId(null);
      };
    }
    return undefined;
  }, []);

  useEffect(() => {
    if (open) {
      if (listMachine.data.length) {
        setCheckedItems(listMachine.data.map((x) => ({ id: x.id, label: x.name, checked: false })));
      }
    }
  }, [listMachine.data, open]);

  if (open) {
    return (
      <BaseDialog
        // disableDialogActions
        // disableCloseButton
        disableCancelButton
        disableConfirmButton={checkedItems.length > 0 ? checkedItems.every((x) => !x.checked) : false}
        confirmButtonText="Mulai Produksi"
        title="Mulai Produksi"
        open={open}
        onClose={handleClose}
        onConfirm={handleConfirm}
        maxWidth="md"
        fullWidth
      >
        {startProduction.isLoading && <BaseLoader />}

        <Typography sx={{ fontSize: 15, mb: 2 }}>Silahkan Pilih Mesin untuk memulai produksi</Typography>

        <FormControl sx={{ width: '100%', display: 'flex', flexDirection: 'column', mb: 3 }}>
          <Grid container spacing={2}>
            {listMachine.isFetching || isFetchingMasterData ? (
              <BoxSpinner height={100} />
            ) : (
              checkedItems.length > 0 &&
              checkedItems.map((x, index) => (
                <Grid key={String(index)} item xs={12} md={6}>
                  <FormControlLabel
                    onChange={(e, checked: boolean) => handleChange(x.id, x.checked)}
                    onClick={() => handleChange(x.id, x.checked)}
                    sx={{
                      px: 2,
                      py: 1,
                      width: '100%',
                      mr: 0,
                      ml: 0,
                      borderRadius: 1,
                      overflow: 'hidden',
                      position: 'relative',
                      backgroundColor: 'background.paper',
                      boxShadow: 1,
                      border: (theme) => (x.checked ? `1px dashed #01bb52` : `1px dashed ${alpha('#000', 0.07)}`),
                      '& .MuiFormControlLabel-label': {
                        display: 'none',
                      },
                    }}
                    control={
                      <>
                        {/* <Box
                        sx={{
                          top: 0,
                          left: 0,
                          width: '100%',
                          height: '100%',
                          position: 'absolute',
                          backgroundColor: '#f1faff',
                          transition: (theme) => theme.transitions.create(['visible']),
                        }}
                      /> */}
                        <Box
                          sx={{
                            top: 8,
                            right: x.checked ? 8 : -100,
                            width: 24,
                            height: 24,
                            position: 'absolute',
                            backgroundColor: '#01bb52',
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: '50%',

                            transition: (theme) => theme.transitions.create(['right']),
                          }}
                        >
                          <DoneIcon sx={{ color: 'primary.contrastText', fontSize: 18 }} />
                        </Box>
                        <Box sx={{ lineHeight: 0 }}>
                          <Box
                            component="img"
                            sx={{ width: 74, height: 'auto', mr: 2 }}
                            src={listMachine.data[index]?.image_url ?? PrintingMachine}
                            alt="Printing machine"
                          />
                        </Box>
                        <Box>
                          <Typography variant="h6">{x.label}</Typography>
                          <Typography variant="subtitle2">
                            Brand : {listMachine.data[index]?.brand.name ?? '-'}
                          </Typography>

                          <Typography variant="subtitle2">
                            Kategori : {listMachine.data[index]?.category.name ?? '-'}
                          </Typography>
                        </Box>
                      </>
                    }
                    label={x.label}
                  />
                </Grid>
              ))
            )}
          </Grid>
        </FormControl>

        {/* Select user */}
        <Box sx={{ mb: 2 }}>
          <FormControl fullWidth>
            <InputLabel variant="outlined" size="small">
              Dikerjakan oleh:
            </InputLabel>
            <Select
              size="small"
              onChange={(e) => setSelectedUserId(String(e.target.value))}
              value={selectedUserId}
              label="Dikerjakan oleh"
            >
              {listOfUserWithRoleTimProduction.length &&
                listOfUserWithRoleTimProduction.map((x) => (
                  <MenuItem key={x.id} value={x.id}>
                    {x.name}
                  </MenuItem>
                ))}
            </Select>
          </FormControl>
        </Box>

        <Box sx={{ textAlign: 'center', height: 20 }}>
          {checkedItemsLength > 0 && <Typography variant="subtitle1">{checkedItemsLength} mesin dipilih</Typography>}
        </Box>
      </BaseDialog>
    );
  }
  return null;
};
