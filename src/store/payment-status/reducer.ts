import { TPaymentStatus } from '@src/types/jo'

import { PaymentStatusAction } from './actions'
import * as ActionTypes from './constants'

export interface PaymentStatusState {
  isFetching: boolean
  isError: boolean
  data: Array<TPaymentStatus>
}

const initialState: PaymentStatusState = {
  isFetching: false,
  isError: false,
  data: [],
}

const paymentStatusReducer = (
  state: PaymentStatusState = initialState,
  action: PaymentStatusAction
): PaymentStatusState => {
  switch (action.type) {
    case ActionTypes.FETCHING_LIST_PAYMENT_STATUS_LOADING:
      return {
        ...state,
        isFetching: true,
        isError: false,
        data: [],
      }
    case ActionTypes.FETCHING_LIST_PAYMENT_STATUS_FAILURE:
      return {
        ...state,
        isFetching: false,
        isError: true,
        data: [],
      }
    case ActionTypes.FETCHING_LIST_PAYMENT_STATUS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        data: action.payload,
      }

    default:
      return state
  }
}

export default paymentStatusReducer
