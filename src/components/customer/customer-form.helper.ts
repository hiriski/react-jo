import { ICustomer, IRequestBodyCustomer } from '@/interfaces/customer';

export const customer_createFormValues = (data: ICustomer): IRequestBodyCustomer => {
  return {
    name: data.name,
    photo: '',
    customer_type: data.customer_type,
    avatar_text_color: data.avatar_text_color,
    email: data.email,
    phone_number: data.phone_number,
    gender: data.gender,
    notes: data.notes,
    addresses: data.addresses.map((x) => ({
      label: x.label,
      address_details: x.address_details,
      district: x.district,
      city: x.city,
      province: x.province,
      postcode: x.postcode,
      country: x.country,
    })),
  };
};
