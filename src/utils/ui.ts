import { animateScroll } from 'react-scroll'

export const scrollToTop = (): void => animateScroll.scrollToTop()
