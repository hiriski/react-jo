// Axios
import api from '@src/utils/http';
import { AxiosResponse } from 'axios';

// Api endpoints.
import { DashboardApiEndpoints } from './dashboard-api-endpoint';

const DashboardApiService = {};

export default DashboardApiService;
