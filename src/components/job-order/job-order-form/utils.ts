import { ICustomer } from '@/interfaces/customer';
import { TCreateJo } from '@src/types/jo';
import _ from 'lodash';

export const checkFields = (values: Array<TCreateJo>): boolean => {
  return true;
};

export const filterDependenciesDropdown = <T extends object>(
  arr: Array<T>,
  objDependency: number | string,
  selectedDepId: number | string,
): Array<T> => {
  return arr.filter((element) => element[objDependency] === selectedDepId);
};

export const findCustomerById = (arrCustomer: Array<ICustomer>, customerId: string): ICustomer | null => {
  if (!arrCustomer) return null;
  return arrCustomer.find((c) => c.id === customerId);
};
