import { TProductionStatus } from '@src/types/jo'
import api from '@src/utils/http'
import { AxiosResponse } from 'axios'

const ProductionStatusAPI = {
  findAll: async (): Promise<AxiosResponse<Array<TProductionStatus>>> =>
    api.get('/jo/production-status'),
}

export default ProductionStatusAPI
