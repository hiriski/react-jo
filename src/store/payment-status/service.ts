import { TPaymentStatus } from '@src/types/jo'
import api from '@src/utils/http'
import { AxiosResponse } from 'axios'

const PaymentStatusAPI = {
  findAll: async (): Promise<AxiosResponse<Array<TPaymentStatus>>> =>
    api.get('/jo/payment-status'),
}

export default PaymentStatusAPI
