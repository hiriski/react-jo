import BookmarkIcon from '@mui/icons-material/Bookmark';
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import { Typography } from '@mui/material';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import { Box } from '@mui/material';
import { useAppSelector } from '@src/store/hook';
import { updatePaymentStatusJo } from '@/store/job-order/job-order-actions';
import { FC, MouseEvent, useState } from 'react';
import { useDispatch } from 'react-redux';

type Props = {
  joId: string;
  selectedId: number;
};

const ToggleUpdatePaymentStatus: FC<Props> = ({ joId, selectedId }: Props) => {
  const dispatch = useDispatch();
  const [alignment, setAlignment] = useState<string | undefined>(undefined);
  const { data } = useAppSelector((state) => state.paymentStatus);

  const handleChange = (event: MouseEvent<HTMLElement>, newAlignment: string): void => {
    setAlignment(newAlignment);
  };

  const handleUpdateToggle = (paymentStatusId: number): void => {
    if (paymentStatusId === 2 /** It's down payment */) {
      alert('call me');
    } else {
      dispatch(updatePaymentStatusJo({ job_order_id: joId, payment_status_id: paymentStatusId }));
    }
  };

  return (
    <ToggleButtonGroup size="small" color="primary" value={alignment} exclusive onChange={handleChange}>
      {data.map((item) => (
        <ToggleButton key={item.id} value={item.id} onClick={() => handleUpdateToggle(item.id)}>
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            {item.id === selectedId ? <BookmarkIcon /> : <BookmarkBorderIcon />}
            <Typography sx={{ textTransform: 'capitalize', ml: 0.5 }}>{item.name}</Typography>
          </Box>
        </ToggleButton>
      ))}
    </ToggleButtonGroup>
  );
};

export default ToggleUpdatePaymentStatus;
