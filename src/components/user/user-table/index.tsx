import PersonAddAltOutlinedIcon from '@mui/icons-material/PersonAddAltOutlined'
import Badge from '@mui/material/Badge'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Paper from '@mui/material/Paper'
import Table from '@mui/material/Table'
import TableContainer from '@mui/material/TableContainer'
import Typography from '@mui/material/Typography'
import { BoxSpinner } from '@src/components/ui'
import { setDialogAddEditUser } from '@src/store/user/actions'
import { TUser } from '@src/types/user'
import React, { ChangeEvent, FC, MouseEvent, useState } from 'react'
import { useDispatch } from 'react-redux'
import { useWindowSize } from 'react-use'

import FilterByRole from './filter-by-role'
import UserTableBody from './table-body'
import UserTableHeader from './table-header'
import UserTablePagination from './table-pagination'
import { Order } from './types'

type Props = {
  data: TUser[]
  isFetching: boolean
}

const UserTable: FC<Props> = ({ data, isFetching }: Props) => {
  const dispatch = useDispatch()
  const { height: windowHeight } = useWindowSize()
  const spaces =
    52 /* <- toolbar */ + 64 /* <- table header */ + 76 /* <- main padding */ + 84 /* <- exta padding bottom */
  const [order, setOrder] = useState<Order>('asc')
  const [orderBy, setOrderBy] = useState<string /* keyof Data */>('calories')
  const [selected, setSelected] = useState<Array<number>>([])
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(25)

  const handleRequestSort = (event: MouseEvent<unknown>, property: string /* keyof Data */): void => {
    const isAsc = orderBy === property && order === 'asc'
    setOrder(isAsc ? 'desc' : 'asc')
    setOrderBy(property)
  }

  const handleSelectAllClick = (event: ChangeEvent<HTMLInputElement>): void => {
    if (event.target.checked) {
      const newSelecteds = data.map((n) => n.id)
      setSelected(newSelecteds)
      return
    }
    setSelected([])
  }

  const handleChangePage = (event: unknown, newPage: number): void => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  const handleClickAddUser = (): void => {
    dispatch(setDialogAddEditUser(true, null))
  }

  return (
    <Box sx={{ width: '100%' }}>
      <Box sx={{ display: 'flex', alignItems: 'center', mb: 2 }}>
        <Badge color="secondary" badgeContent={data.length} max={999}>
          <Typography component="h2" variant="h4" sx={{ pr: 2 }}>
            User
          </Typography>
        </Badge>

        <FilterByRole />

        <Button
          startIcon={<PersonAddAltOutlinedIcon />}
          onClick={handleClickAddUser}
          sx={{ ml: 'auto' }}
          variant="contained"
          color="primary"
          size="small"
          disableElevation
        >
          Add User
        </Button>
      </Box>

      <Paper
        sx={{
          width: '100%',
          mb: 2,
          // border: (theme) => `1px solid ${theme.palette.divider}`,
        }}
        elevation={0}
      >
        <TableContainer sx={{ maxHeight: windowHeight - spaces }}>
          <Table stickyHeader sx={{ minWidth: 1100 }} size="medium">
            {!isFetching && Array.isArray(data) && data.length === 0 && (
              <caption
                style={{
                  height: windowHeight - spaces - /* extra spaces -> */ 72,
                }}
              >
                <Box
                  sx={{
                    display: 'flex',
                    flex: 1,
                    height: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <Typography variant="h6">Empty</Typography>
                </Box>
              </caption>
            )}
            {isFetching ? (
              <caption
                style={{
                  height: windowHeight - spaces - /* extra spaces -> */ 72,
                }}
              >
                <BoxSpinner height={windowHeight - spaces} />
              </caption>
            ) : (
              <>
                <UserTableHeader
                  numSelected={selected.length}
                  order={order}
                  orderBy={orderBy}
                  onSelectAllClick={handleSelectAllClick}
                  onRequestSort={handleRequestSort}
                  rowCount={data.length}
                />
                <UserTableBody
                  rows={data}
                  rowsPerPage={rowsPerPage}
                  order={order}
                  page={page}
                  orderBy={orderBy}
                  selected={selected}
                  setSelected={setSelected}
                />
              </>
            )}
          </Table>
        </TableContainer>
        <UserTablePagination
          rowsPerPageOptions={[5, 10, 25]}
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </Box>
  )
}

export default UserTable
