import { BoxSpinner } from '@components/ui';
import CloseIcon from '@mui/icons-material/Close';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import { jobOrder_fetchDetail, setDrawerAddEditJo } from '@/store/job-order/job-order-actions';
import { masterData_fetchMasterData } from '@/store/master-data/master-data-actions';
import { TRequestMasterData } from '@/interfaces/master-data';
import { DRAWER_ADD_EDIT_JO_WIDTH } from '@src/utils/constants';
import { FC, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useWindowSize } from 'react-use';

import { useAppSelector } from '../../store/hook';
// import { FormJo } from '.';

const requestMasterData = [
  {
    object_name: 'job_order_categories',
  },
  {
    object_name: 'material_papers',
  },
  {
    object_name: 'customers',
  },
  {
    object_name: 'brands_material_paper',
  },
  {
    object_name: 'payment_method',
  },
];

const DrawerAddEditJo: FC = () => {
  const { drawerAddEditJo } = useAppSelector((state) => state.jobOrder);
  const { data: masterData, isFetching: isFetchingMasterData } = useAppSelector((state) => state.masterData);
  const { createLoading, updateLoading } = useAppSelector((state) => state.jobOrder);
  const dispatch = useDispatch();
  const isEditMode = Boolean(drawerAddEditJo.id);
  const { height: windowHeight } = useWindowSize();

  const onClose = (): void => {
    dispatch(setDrawerAddEditJo(false, null));
  };

  useEffect(() => {
    if (drawerAddEditJo.id) {
      dispatch(jobOrder_fetchDetail(drawerAddEditJo.id));
    }
    if (drawerAddEditJo.status) {
      dispatch(masterData_fetchMasterData(requestMasterData as TRequestMasterData));
    }
  }, [drawerAddEditJo.status, drawerAddEditJo.id]);

  return (
    <Drawer
      elevation={0}
      anchor="right"
      open={drawerAddEditJo.status}
      onClose={onClose}
      PaperProps={{
        sx: {
          width: { xs: '100%', md: DRAWER_ADD_EDIT_JO_WIDTH },
        },
      }}
    >
      {createLoading || updateLoading || (isFetchingMasterData && <BoxSpinner height={windowHeight} />)}
      <Box role="presentation" sx={{ position: 'relative' /* px: 8, pt: 3, pb: 5 */ }}>
        <IconButton sx={{ position: 'absolute', top: 12, right: 12 }} onClick={onClose} size="small">
          <CloseIcon sx={{ fontSize: 20 }} />
        </IconButton>
        {/* <FormJo sources={masterData} id={drawerAddEditJo.id} sx={{ mt: 5 }} /> */}
      </Box>
    </Drawer>
  );
};

export default DrawerAddEditJo;
