import InventoryIcon from '@mui/icons-material/Inventory'
import { Theme } from '@mui/material'
import Box from '@mui/material/Box'
import { green, red } from '@mui/material/colors'
import Typography from '@mui/material/Typography'
import { setDrawerDetailInventory } from '@src/store/inventory/actions'
import { TMaterialPaper } from '@src/types/inventory'
import { cmToMeters } from '@src/utils/unit'
import { FC } from 'react'
import { useDispatch } from 'react-redux'

type Props = {
  item: TMaterialPaper
}
const MaterialPaperItem: FC<Props> = ({ item }: Props) => {
  const dispatch = useDispatch()

  const onEdit = (phraseId: number): void => {
    // dispatch(setDrawerAddEditPhrase(true, phraseId))
  }

  const onClick = (paramId: number): void => {
    dispatch(
      setDrawerDetailInventory({
        open: true,
        type: 'material_paper',
        id: paramId,
      })
    )
  }

  return (
    <Box
      onClick={() => onClick(item.id)}
      sx={{
        borderRadius: 1,
        cursor: 'pointer',
        position: 'relative',
        backgroundColor: 'background.paper',
        border: (theme: Theme) => `1px solid ${theme.palette.divider}`,
      }}
    >
      {/* <Box sx={{ position: 'absolute', bottom: 10, right: 10 }}>
        <IconButton onClick={() => onEdit(id)} size="small">
          <EditIcon sx={{ fontSize: 16, color: 'text.disabled' }} />
        </IconButton>
      </Box> */}

      {/* Inner */}
      <Box sx={{ display: 'flex', px: 2, py: 2 }}>
        {/* Icon */}
        <Box sx={{ width: 30, mt: 0.2 }}>
          <InventoryIcon sx={{ color: 'text.disabled', fontSize: 20 }} />
        </Box>
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            flex: 1,
          }}
        >
          <Box>
            <Typography component="h6" variant="h4" sx={{ fontSize: 17, mb: 0.3 }}>
              {item.name}
            </Typography>
            <Typography variant="subtitle2" color="text.secondary">
              {item.brand && item.brand.name}, {item.material_width}cm
            </Typography>
          </Box>
          <Box sx={{ ml: 'auto', textAlign: 'right' }}>
            <Typography
              component="h6"
              variant="h4"
              sx={{
                fontSize: 17,
                mb: 0.3,
                color: item.stock_length >= 5000 ? green['500'] : red['500'],
              }}
            >
              {cmToMeters(item.stock_length)}m
            </Typography>
            <Typography
              variant="subtitle2"
              sx={{
                color: item.stock_length >= 5000 ? green['500'] : red['500'],
              }}
            >
              {item.status}
            </Typography>
          </Box>
        </Box>
      </Box>
    </Box>
  )
}

export default MaterialPaperItem
