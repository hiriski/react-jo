import AttachMoneyIcon from '@mui/icons-material/AttachMoney'
import CheckIcon from '@mui/icons-material/Check'
import MoneyOffIcon from '@mui/icons-material/MoneyOff'
import Box from '@mui/material/Box'
import { SxProps, TypographyVariant } from '@mui/material/styles'
import Tooltip from '@mui/material/Tooltip'
import Typography from '@mui/material/Typography'
import { createLabelJoPaymentStatus } from '@src/utils/jo'
import { FC } from 'react'

type Props = {
  statusId: number
  status: string
  description?: string
  sx?: SxProps
  typographyVariant?: TypographyVariant
}

const JoLabelPaymentStatus: FC<Props> = ({ statusId, status, description, typographyVariant, sx }: Props) => (
  <Tooltip title={description || ''}>
    <Box
      sx={{
        color: createLabelJoPaymentStatus(statusId),
        border: `1px solid ${createLabelJoPaymentStatus(statusId)}`,
        textAlign: 'center',
        borderRadius: 1,
        py: 0.4,
        px: 0.6,
        zIndex: 1,
        ...sx,
      }}
    >
      <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        {statusId === 1 && <MoneyOffIcon sx={{ mr: 1, fontSize: 16 }} />}
        {statusId === 2 && <AttachMoneyIcon sx={{ mr: 1, fontSize: 16 }} />}
        {statusId === 3 && <CheckIcon sx={{ mr: 1, fontSize: 18 }} />}
        <Typography variant={typographyVariant}>{status}</Typography>
      </Box>
    </Box>
  </Tooltip>
)

JoLabelPaymentStatus.defaultProps = {
  description: '',
  typographyVariant: 'subtitle2',
  sx: {},
}

export default JoLabelPaymentStatus
