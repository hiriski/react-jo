interface LaravelPaginationMetaLink {
  url: string
  label: string
  active: boolean
}

export interface LaravelPaginationLinks {
  first: string
  last: string
  next?: string | null
  prev?: string | null
}

export interface LaravelPaginationMeta {
  // eslint-disable-next-line camelcase
  current_page: number
  from: number
  // eslint-disable-next-line camelcase
  last_page: number
  links: LaravelPaginationMetaLink[]
  path: string
  per_page: number
  to: number
  total: number
}
