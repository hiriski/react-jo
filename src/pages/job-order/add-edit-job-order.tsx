import { BaseCard, BaseLoader } from '@/components/base';
import { PageTitle } from '@/components/shared';
import { BoxSpinner } from '@/components/ui';
import { MaterialForm } from '@/components/warehouse';
import { RequestBodyMasterData } from '@/interfaces/master-data';
import { useAppSelector } from '@/store/hook';
import { masterData_fetchMasterData } from '@/store/master-data/master-data-actions';
import { masterData_rootSelector } from '@/store/master-data/master-data-selectors';
import { warehouse_resetCreateMaterialState } from '@/store/warehouse/warehouse-actions';
import { warehouse_rootSelector } from '@/store/warehouse/warehouse-selectors';
import { ROUTES } from '@/utils/constants';
import { Grid } from '@mui/material';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { MainLayout } from '@src/components/layouts';
import { FC, useEffect } from 'react';
import { useDispatch } from 'react-redux';

// Components.
import { JobOrderForm } from '@/components/job-order';
import { app_setLoading } from '@/store/app/app-actions';
import { useParams } from 'react-router';
import { jobOrder_fetchDetail } from '@/store/job-order/job-order-actions';
import { jobOrder_rootSelector } from '@/store/job-order/job-order-selectors';

const requestBodyMasterData = [
  {
    object_name: 'job_order_categories',
  },
  {
    object_name: 'materials',
  },
  {
    object_name: 'customers',
  },
  {
    object_name: 'material_brands',
  },
  {
    object_name: 'material_categories',
  },
  {
    object_name: 'material_types',
  },
  {
    object_name: 'payment_methods',
  },
  {
    object_name: 'payment_statuses',
  },
  {
    object_name: 'bank_accounts',
  },
];

const AddEditJobOrderPage: FC = () => {
  const dispatch = useDispatch();
  const params = useParams();

  const { isFetching: isFetchingMasterData } = useAppSelector((state) => masterData_rootSelector(state));
  const { detail } = useAppSelector((s) => jobOrder_rootSelector(s));

  useEffect(() => {
    dispatch(masterData_fetchMasterData(requestBodyMasterData));
  }, []);

  useEffect(() => {
    if (isFetchingMasterData || detail.isFetching) dispatch(app_setLoading(true));
    else dispatch(app_setLoading(false));
  }, [isFetchingMasterData, detail.isFetching]);

  useEffect(() => {
    if (params.id) dispatch(jobOrder_fetchDetail(params.id));
  }, [params.id]);

  return (
    <MainLayout>
      <Grid container justifyContent="center">
        <Grid item sm={12}>
          <PageTitle
            title={params.id ? 'Edit Job Order' : 'Buat Job Order'}
            subtitle="Lorem ipsum dolor sit amet"
            rightContent={
              <Box>
                <Typography>Right content</Typography>
              </Box>
            }
          />
          <BaseCard
            sx={{
              p: 0,
              position: 'relative',
              minHeight: 400,
            }}
          >
            {params.id ? (
              !detail.isFetching && detail.data ? (
                <JobOrderForm data={detail.data} id={params.id} />
              ) : null
            ) : (
              <JobOrderForm />
            )}
          </BaseCard>
        </Grid>
      </Grid>
    </MainLayout>
  );
};

export default AddEditJobOrderPage;
