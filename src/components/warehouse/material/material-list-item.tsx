import { BaseCard } from '@/components/base';
import { IMaterial } from '@/interfaces/warehouse';
import { Box } from '@mui/material';
import { FC } from 'react';
import { MaterialItem } from './material-item';

interface Props {
  items: IMaterial[];
  isLoading: boolean;
}

export const MaterialListItem: FC<Props> = ({ items, isLoading }) => {
  return (
    <BaseCard>{isLoading ? 'Loading...' : items.map((item) => <MaterialItem key={item.id} item={item} />)}</BaseCard>
  );
};
