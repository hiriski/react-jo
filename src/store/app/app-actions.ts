// Enum action types.
import { AppActionTypes as Types } from './app-actions.enum';
import { IPayloadSetAlert } from './app.interfaces';

// Actions definitions.
interface IAppSetLoading {
  type: typeof Types.app_SET_LOADING;
  payload: boolean;
}

interface IAppSetAlert {
  type: typeof Types.app_SET_ALERT;
  payload: IPayloadSetAlert;
}

interface IAppResetAlert {
  type: typeof Types.app_RESET_ALERT;
  payload: undefined;
}

interface IAppSetIsLocked {
  type: typeof Types.app_SET_IS_LOCKED;
  payload: boolean;
}

// Union actions type.
export type AppActions = IAppSetLoading | IAppSetAlert | IAppResetAlert | IAppSetIsLocked;

// Actions creator.
export const app_setLoading = (payload: boolean): IAppSetLoading => ({
  type: Types.app_SET_LOADING,
  payload,
});

export const app_setAlert = (payload: IPayloadSetAlert): IAppSetAlert => ({
  type: Types.app_SET_ALERT,
  payload,
});

export const app_resetAlert = (): IAppResetAlert => ({
  type: Types.app_RESET_ALERT,
  payload: undefined,
});

export const app_setIsLocked = (payload: boolean): IAppSetIsLocked => ({
  type: Types.app_SET_IS_LOCKED,
  payload,
});
