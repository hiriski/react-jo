import { FC, MouseEvent, useState } from 'react';
import { BaseAnalyticsWidget, BaseCard } from '@/components/base';
import { green, blue, indigo, orange } from '@mui/material/colors';
import { AlbumsOutline, FolderOpenOutline, SyncOutline, TimeOutline } from 'react-ionicons';

import Box from '@mui/material/Box';

// Apex chart.
import ReactApexChart from 'react-apexcharts';
import { ApexOptions } from 'apexcharts';

import merge from 'lodash/merge';
import { baseOptionsChart } from '@/components/chart/base-options-chart';
import useTheme from '@mui/material/styles/useTheme';

export const OverviewReportMaterialStock: FC = () => {
  const theme = useTheme();

  const CHART_DATA = [
    {
      data: [21, 22, 10, 28, 16],
    },
  ];

  const chartOptions: ApexOptions = {
    chart: {
      type: 'bar',
      toolbar: { show: false },
      zoom: { enabled: false },
      foreColor: theme.palette.text.disabled,
      fontFamily: theme.typography.fontFamily,
      animations: { enabled: true },
    },

    colors: baseOptionsChart.colors,

    // Markers
    markers: {
      size: 0,
      strokeColors: theme.palette.background.paper,
    },

    xaxis: {
      categories: [['John', 'Doe'], ['Joe', 'Smith'], ['Jake', 'Williams'], 'Amber', ['Peter', 'Brown']],
      labels: {
        style: {
          colors: baseOptionsChart.colors,
          fontSize: '12px',
        },
      },
    },

    grid: {
      strokeDashArray: 3,
      borderColor: theme.palette.divider,
    },

    plotOptions: {
      bar: {
        columnWidth: '70%',
        distributed: true,
      },
    },

    legend: {
      show: true,
      fontSize: '12px',
      position: 'top',
      horizontalAlign: 'right',
      markers: {
        radius: 12,
      },
      fontWeight: 500,
      itemMargin: { horizontal: 12 },
      labels: {
        colors: theme.palette.text.primary,
      },
    },

    // Datalabels
    dataLabels: { enabled: false },

    // Responsive
    responsive: [
      {
        // sm
        breakpoint: theme.breakpoints.values.sm,
        options: {
          plotOptions: { bar: { columnWidth: '40%' } },
        },
      },
      {
        // md
        breakpoint: theme.breakpoints.values.md,
        options: {
          plotOptions: { bar: { columnWidth: '32%' } },
        },
      },
    ],

    tooltip: {
      shared: true,
      intersect: false,
      y: {
        formatter: (y) => {
          if (typeof y !== 'undefined') {
            return `${y.toFixed(0)} visits`;
          }
          return y;
        },
      },
    },
  };

  return (
    <BaseCard
      sx={{ position: 'relative' }}
      title="Laporan Stok Bahan Baku"
      subtitle="Statistik Laporan Bahan Baku Produksi"
      rightContent={<Box>Render right content</Box>}
    >
      <ReactApexChart type="bar" series={CHART_DATA} options={chartOptions as ApexOptions} height={320} />
    </BaseCard>
  );
};
