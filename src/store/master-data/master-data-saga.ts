import type { SimpleEffect, SagaIterator } from '@redux-saga/types';
import { httpResponseOK } from '@src/utils/http';
import { SagaReturnType, call, put, takeEvery } from 'redux-saga/effects';

import { masterData_fetchMasterDataSuccess } from './master-data-actions';
import { MASTER_DATA_ACTION_TYPES as ActionTypes } from './master-data-action-types.enum';
import MasterDataApiService from './master-data-api-service';
import { TRequestMasterData } from '@/interfaces/master-data';

// Type definitions of return of result.
type APIResponseMasterData = SagaReturnType<typeof MasterDataApiService.invoke>;

function* getMasterData({
  payload,
}: SimpleEffect<typeof ActionTypes.FETCH_MASTER_DATA_REQUESTED, TRequestMasterData>): SagaIterator {
  yield put({ type: ActionTypes.FETCH_MASTER_DATA_LOADING });
  try {
    const response: APIResponseMasterData = yield call(MasterDataApiService.invoke, payload);
    if (httpResponseOK(response.status)) {
      const { data } = response;
      yield put(masterData_fetchMasterDataSuccess(data));
    }
  } catch (error) {
    yield put({ type: ActionTypes.FETCH_MASTER_DATA_FAILURE });
  }
}

export default function* masterDataSaga(): SagaIterator {
  yield takeEvery(ActionTypes.FETCH_MASTER_DATA_REQUESTED, getMasterData);
}
