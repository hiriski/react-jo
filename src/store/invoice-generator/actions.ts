import { TInvoiceGeneratorData } from '@src/types/invoice'

import * as ActionTypes from './constants'

// Actions definition
interface SetInvoicePdfMode {
  type: typeof ActionTypes.SET_INVOICE_PDF_MODE
  payload: boolean
}

interface SetDrawerEditableInvoice {
  type: typeof ActionTypes.SET_DRAWER_EDITABLE_INVOICE
  payload: boolean
}

interface HandleInvoiceChange {
  type: typeof ActionTypes.HANDLE_INVOICE_CHANGE
  payload: {
    property: string
    value: string | number
  }
}

interface SetInvoiceGeneratorData {
  type: typeof ActionTypes.SET_INVOICE_GENERATOR_DATA
  payload: TInvoiceGeneratorData
}

// Union actions type
export type InvoiceAction = SetInvoicePdfMode | SetDrawerEditableInvoice | HandleInvoiceChange | SetInvoiceGeneratorData

// Actions creator.
export const setInvoicePdfMode = (payload: boolean): SetInvoicePdfMode => ({
  type: ActionTypes.SET_INVOICE_PDF_MODE,
  payload,
})

export const setDrawerEditableInvoice = (payload: boolean): SetDrawerEditableInvoice => ({
  type: ActionTypes.SET_DRAWER_EDITABLE_INVOICE,
  payload,
})

export const handleInvoiceChange = (property: string, value: string | number): HandleInvoiceChange => ({
  type: ActionTypes.HANDLE_INVOICE_CHANGE,
  payload: { property, value },
})

export const setInvoiceGeneratorData = (payload: TInvoiceGeneratorData): SetInvoiceGeneratorData => ({
  type: ActionTypes.SET_INVOICE_GENERATOR_DATA,
  payload,
})
