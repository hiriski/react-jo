export const JobOrderProductionStatusIdConstants = {
  Waiting: 1,
  InProgress: 2,
  Done: 3,
  Hold: 4,
  Rejected: 5,
};

export const JobOrderCategoryIdConstants = {
  PrintSticker: 1,
  LaserCutting: 2,
  PrintUV: 3,
  Banner: 4,
};
