import { FC, MouseEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { BaseAnalyticsWidget, BaseCard } from '@/components/base';
import { green, blue, indigo, orange } from '@mui/material/colors';
import { AlbumsOutline, CheckmarkDoneCircleOutline, SyncOutline, TimeOutline } from 'react-ionicons';

import {
  Grid,
  Box,
  Typography,
  Avatar,
  ToggleButtonGroup,
  ToggleButton,
  styled,
  alpha,
  TextField,
} from '@mui/material';
import { MobileDatePicker } from '@mui/x-date-pickers/MobileDatePicker';

import AvatarImage from '@/assets/images/avatars/27.jpg';
import { useDispatch } from 'react-redux';
import { fetchTotalJobOrder } from '@/store/analytics/analytics-actions';
import { useAppSelector } from '@/store/hook';
import moment, { Moment } from 'moment';
import { IAnalyticsTotalJobOrder } from '@/store/analytics/analytics.interface';
import { dropdownJobOrderPeriod } from '@/lib/dropdown';

// Interfaces

const StyledToggleButtonGroup = styled(ToggleButtonGroup)(({ theme }) => ({
  display: 'flex',
}));

const StyledToggleButton = styled(ToggleButton)(({ theme }) => ({
  color: theme.palette.primary.main,
  textTransform: 'capitalize',
  backgroundColor: 'transparent',
  border: 'none !important',
  padding: theme.spacing(0.4, 1.25),
  borderRadius: Number(theme.shape.borderRadius) + 'px !important',
  '&.Mui-selected': {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.primary.contrastText,
    },
  },
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.3),
  },
}));

export const OverviewAnalyticsTotalJobOrder: FC = () => {
  const dispatch = useDispatch();
  const { totalJobOrder } = useAppSelector((state) => state.analytics);
  const { isFetching: isLoading } = totalJobOrder;
  const [selectedPeriod, setSelectedPeriod] = useState<undefined | 'overtime' | 'today' | 'week' | 'month' | 'year'>(
    'overtime',
  );
  const [customDate, setCustomDate] = useState<undefined | string>(undefined);

  const onChangePeriod = (
    e: MouseEvent<HTMLElement>,
    newPeriod: undefined | 'today' | 'week' | 'month' | 'year',
  ): void => {
    /** force to set custom date to undefined */
    setCustomDate(undefined);
    setSelectedPeriod(newPeriod);
  };

  useEffect(() => {
    if (customDate) {
      dispatch(fetchTotalJobOrder(null, customDate));
    } else {
      dispatch(fetchTotalJobOrder(selectedPeriod));
    }
  }, [selectedPeriod, customDate]);

  /**
   * Handle change custom date.
   * @param {string} value
   * @param {string} keyboardInputValue
   */
  const onChangeCustomDate = (value: string, keyboardInputValue?: string): void => {
    setSelectedPeriod(undefined);
    setCustomDate(moment(value).format('YYYY-MM-DD'));
  };

  const data = useMemo<IAnalyticsTotalJobOrder>(() => {
    let dataTotal: IAnalyticsTotalJobOrder;
    if (customDate) {
      dataTotal = totalJobOrder.data.customDate;
    } else {
      switch (selectedPeriod) {
        case 'today':
          dataTotal = totalJobOrder.data.today;
          break;
        case 'week':
          dataTotal = totalJobOrder.data.week;
          break;
        case 'month':
          dataTotal = totalJobOrder.data.month;
        case 'year':
          break;
          dataTotal = totalJobOrder.data.year;
          break;
        case 'overtime':
          dataTotal = totalJobOrder.data.overtime;
          break;
        default:
          dataTotal = totalJobOrder.data.overtime;
          break;
      }
    }
    return dataTotal;
  }, [selectedPeriod, customDate, totalJobOrder.data]);

  return (
    <Box sx={{ position: 'relative', marginTop: '-60px' }}>
      <Box
        sx={{
          mb: 3,
          color: 'common.white',
          display: 'flex',
          justifyContent: 'flex-end',
        }}
      >
        <Box>
          <Box
            sx={{
              px: 1.4,
              py: 0.8,
              borderRadius: 1.5,
              backgroundColor: (theme) => alpha(theme.palette.common.white, 0.75),
            }}
          >
            <StyledToggleButtonGroup
              color="standard"
              size="small"
              value={selectedPeriod}
              onChange={onChangePeriod}
              exclusive
            >
              {dropdownJobOrderPeriod.map((period) => (
                <StyledToggleButton disableRipple disableFocusRipple key={period.value} value={period.value}>
                  {period.label}
                </StyledToggleButton>
              ))}
            </StyledToggleButtonGroup>
          </Box>
          <Box sx={{ mt: 2 }}>
            <MobileDatePicker
              closeOnSelect
              openTo="day"
              views={['day']}
              label="Tanggal"
              inputFormat="dd-mm-yyyy"
              value={customDate}
              onChange={onChangeCustomDate}
              renderInput={(params) => <TextField {...params} size="small" fullWidth />}
            />
          </Box>
        </Box>
      </Box>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6} md={3}>
          <BaseAnalyticsWidget
            size="medium"
            cardColor="light"
            icon={<AlbumsOutline />}
            value={data.total}
            title="JO Hari Ini"
            color={indigo[500]}
            subtitle="Total Jo hari ini"
            isLoading={isLoading}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <BaseAnalyticsWidget
            size="medium"
            cardColor="light"
            value={data.waiting}
            icon={<TimeOutline />}
            title="Waiting List"
            color={orange[400]}
            subtitle="Menunggu Produksi"
            isLoading={isLoading}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <BaseAnalyticsWidget
            size="medium"
            cardColor="light"
            value={data.in_progress}
            icon={<SyncOutline />}
            title="In Progress"
            color={blue[500]}
            subtitle="Job Order In Progress"
            isLoading={isLoading}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <BaseAnalyticsWidget
            size="medium"
            cardColor="light"
            value={data.done}
            icon={<CheckmarkDoneCircleOutline />}
            title="Selesai"
            color={green[500]}
            subtitle="Job Order selesai"
            isLoading={isLoading}
          />
        </Grid>
      </Grid>
    </Box>
  );
};
