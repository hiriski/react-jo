import { lazy } from 'react'

const HomePage = lazy(() => import('./home'))

export default HomePage
