import { Box, Typography } from '@mui/material'
import { FC } from 'react'
import { FolderOpenOutline } from 'react-ionicons'

interface Props {
  title?: string
  subtitle?: string
}

export const BaseEmptyContent: FC<Props> = ({ title, subtitle }) => {
  return (
    <Box sx={{ py: 4, textAlign: 'center', color: "text.disabled" }}>
      <FolderOpenOutline style={{ height: 60, width: 60, color: "inherit", marginBottom: 5 }} />
      <Typography variant="h6">{title}</Typography>
      {subtitle && <Typography variant='subtitle2'>{subtitle}</Typography>}
    </Box>
  )
}

BaseEmptyContent.defaultProps = {
  title: 'No Data',
}
