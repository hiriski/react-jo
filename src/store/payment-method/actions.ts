import { IPaymentMethod } from '@src/types/jo'

import * as ActionTypes from './constants'

// Actions definition.
interface IFetchListPaymentMethodRequested {
  type: typeof ActionTypes.FETCHING_LIST_PAYMENT_METHOD_REQUESTED
}
interface IFetchListPaymentMethodLoading {
  type: typeof ActionTypes.FETCHING_LIST_PAYMENT_METHOD_LOADING
}
interface IFetchListPaymentMethodFailure {
  type: typeof ActionTypes.FETCHING_LIST_PAYMENT_METHOD_FAILURE
}
interface IFetchListPaymentMethodSuccess {
  type: typeof ActionTypes.FETCHING_LIST_PAYMENT_METHOD_SUCCESS
  payload: Array<IPaymentMethod>
}

// Union actions type
export type PaymentMethodAction =
  | IFetchListPaymentMethodRequested
  | IFetchListPaymentMethodLoading
  | IFetchListPaymentMethodFailure
  | IFetchListPaymentMethodSuccess

// Actions creator.
export const fetchListPaymentMethod = (): IFetchListPaymentMethodRequested => ({
  type: ActionTypes.FETCHING_LIST_PAYMENT_METHOD_REQUESTED,
})

export const fetchListPaymentMethodSuccess = (payload: Array<IPaymentMethod>): IFetchListPaymentMethodSuccess => ({
  type: ActionTypes.FETCHING_LIST_PAYMENT_METHOD_SUCCESS,
  payload,
})
