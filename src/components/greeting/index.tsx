import HelloComponent from '@components/hello'
import { FC } from 'react'

const Greeting: FC = () => {
  return (
    <div>
      <HelloComponent />
    </div>
  )
}

export default Greeting
