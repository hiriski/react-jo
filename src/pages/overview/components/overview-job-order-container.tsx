import { BaseCard, BaseEmptyContent } from '@/components/base';
import { Grid, Box } from '@mui/material';
import { FC } from 'react';
import { JobOrderCardItem } from '@/components/jo/job-order-card-item';
import { BoxSpinner } from '@/components/ui';

// Interfaces.
import { IJobOrder } from '@/interfaces/job-order';

interface Props {
  items: Array<IJobOrder>;
  isFetching: boolean;
}

export const OverviewJobOrderContainer: FC<Props> = ({ items, isFetching }) => {
  return (
    <>
      {isFetching ? (
        <BoxSpinner height={200} />
      ) : (
        <>
          {Array.isArray(items) && items.length > 0 ? (
            items.map((item) => <JobOrderCardItem key={item.id} item={item} />)
          ) : (
            <BaseEmptyContent />
          )}
        </>
      )}
    </>
  );
};
