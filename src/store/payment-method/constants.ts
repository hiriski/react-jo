export const FETCHING_LIST_PAYMENT_METHOD_REQUESTED = 'paymentStatus/FETCHING_LIST_PAYMENT_METHOD_REQUESTED'
export const FETCHING_LIST_PAYMENT_METHOD_LOADING = 'paymentStatus/FETCHING_LIST_PAYMENT_METHOD_LOADING'
export const FETCHING_LIST_PAYMENT_METHOD_FAILURE = 'paymentStatus/FETCHING_LIST_PAYMENT_METHOD_FAILURE'
export const FETCHING_LIST_PAYMENT_METHOD_SUCCESS = 'paymentStatus/FETCHING_LIST_PAYMENT_METHOD_SUCCESS'
