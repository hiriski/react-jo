import routes from '@src/lib/routes';
import { useAppSelector } from '@src/store/hook';
import { ACCESS_TOKEN } from '@src/utils/constants';
import Cookies from 'js-cookie';
import { FC, Suspense, useMemo } from 'react';
import { useRoutes } from 'react-router';

import { SuspenseFallback } from './components/ui';
import { app_rootSelector } from './store/app/app-selectors';

const RootRoutes: FC = () => {
  const { isAuthenticated } = useAppSelector((state) => state.auth);
  const { app_isLocked } = useAppSelector((state) => app_rootSelector(state));
  const accessToken = Cookies.get(ACCESS_TOKEN);

  const isLoggedIn = useMemo<boolean>(() => {
    return isAuthenticated && accessToken !== null;
  }, [isAuthenticated]);

  return <Suspense fallback={<SuspenseFallback size={36} />}>{useRoutes(routes(isLoggedIn, app_isLocked))}</Suspense>;
};

export default RootRoutes;
