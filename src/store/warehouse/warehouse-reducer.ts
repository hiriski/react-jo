// Enum action types.
import { UUID } from '@/interfaces/common';
import { IMaterial, IMaterialBrand, IMaterialCategory } from '@/interfaces/warehouse';
import { WAREHOUSE_ACTION_TYPES as ActionTypes } from './warehouse-action-types.enum';

// Union action types.
import { WarehouseActions } from './warehouse-actions';

export interface WarehouseState {
  dialogAddEditMaterial: {
    open: boolean;
    id?: number | null;
  };
  listMaterial: {
    isFetching: boolean;
    isError: boolean;
    data: Array<IMaterial>;
  };
  listMaterialBrand: {
    isFetching: boolean;
    isError: boolean;
    data: Array<IMaterialBrand>;
  };
  listMaterialCategory: {
    isFetching: boolean;
    isError: boolean;
    data: Array<IMaterialCategory>;
  };
  createMaterial: {
    isLoading: boolean;
    isError: boolean;
    isSuccess: boolean;
    data?: IMaterial;
    errorMessages: Array<string>;
  };

  dialogAddStockMaterial: {
    open: boolean;
    material: IMaterial | null;
  };
}

const initialState: WarehouseState = {
  dialogAddEditMaterial: {
    open: false,
    id: null,
  },
  listMaterial: {
    isFetching: false,
    isError: false,
    data: [],
  },
  listMaterialBrand: {
    isFetching: false,
    isError: false,
    data: [],
  },
  listMaterialCategory: {
    isFetching: false,
    isError: false,
    data: [],
  },
  createMaterial: {
    isLoading: false,
    isError: false,
    isSuccess: false,
    data: null,
    errorMessages: [],
  },
  dialogAddStockMaterial: {
    open: false,
    material: null,
  },
};

const warehouseReducer = (state: WarehouseState = initialState, action: WarehouseActions): WarehouseState => {
  switch (action.type) {
    case ActionTypes.SET_DIALOG_ADD_EDIT_MATERIAL:
      return {
        ...state,
        dialogAddEditMaterial: {
          open: action.payload.open,
          id: action.payload.id,
        },
      };

    case ActionTypes.FETCH_MATERIAL_LIST_LOADING:
      return {
        ...state,
        listMaterial: {
          ...state.listMaterial,
          isFetching: true,
          isError: false,
        },
      };

    case ActionTypes.FETCH_MATERIAL_LIST_FAILURE:
      return {
        ...state,
        listMaterial: {
          ...state.listMaterial,
          isFetching: false,
          isError: true,
        },
      };

    case ActionTypes.FETCH_MATERIAL_LIST_SUCCESS:
      return {
        ...state,
        listMaterial: {
          isFetching: false,
          isError: false,
          data: action.payload,
        },
      };

    case ActionTypes.CREATE_MATERIAL_LOADING:
      return {
        ...state,
        createMaterial: {
          ...state.createMaterial,
          isLoading: true,
          isError: false,
          isSuccess: false,
        },
      };

    case ActionTypes.CREATE_MATERIAL_FAILURE:
      return {
        ...state,
        createMaterial: {
          ...state.createMaterial,
          isLoading: false,
          isError: true,
          isSuccess: false,
          errorMessages: action.payload || [],
        },
      };

    case ActionTypes.CREATE_MATERIAL_SUCCESS:
      return {
        ...state,
        createMaterial: {
          ...state.createMaterial,
          isLoading: false,
          isError: false,
          isSuccess: true,
          data: action.payload,
        },
      };

    case ActionTypes.RESET_CREATE_MATERIAL_STATE:
      return {
        ...state,
        createMaterial: initialState.createMaterial,
      };

    case ActionTypes.FETCH_MATERIAL_BRAND_LIST_LOADING:
      return {
        ...state,
        listMaterialBrand: {
          ...state.listMaterialBrand,
          isFetching: true,
          isError: false,
        },
      };

    case ActionTypes.FETCH_MATERIAL_BRAND_LIST_FAILURE:
      return {
        ...state,
        listMaterialBrand: {
          ...state.listMaterialBrand,
          isFetching: false,
          isError: true,
        },
      };

    case ActionTypes.FETCH_MATERIAL_BRAND_LIST_SUCCESS:
      return {
        ...state,
        listMaterialBrand: {
          isFetching: false,
          isError: false,
          data: action.payload,
        },
      };

    case ActionTypes.FETCH_MATERIAL_CATEGORY_LIST_LOADING:
      return {
        ...state,
        listMaterialCategory: {
          ...state.listMaterialCategory,
          isFetching: true,
          isError: false,
        },
      };

    case ActionTypes.FETCH_MATERIAL_CATEGORY_LIST_FAILURE:
      return {
        ...state,
        listMaterialCategory: {
          ...state.listMaterialCategory,
          isFetching: false,
          isError: true,
        },
      };

    case ActionTypes.FETCH_MATERIAL_CATEGORY_LIST_SUCCESS:
      return {
        ...state,
        listMaterialCategory: {
          isFetching: false,
          isError: false,
          data: action.payload,
        },
      };

    case ActionTypes.SET_DIALOG_ADD_STOCK_MATERIAL:
      return {
        ...state,
        dialogAddStockMaterial: {
          open: action.payload.open,
          material: action.payload.material,
        },
      };

    default:
      return state;
  }
};

export default warehouseReducer;
