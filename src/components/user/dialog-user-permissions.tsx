import VerifiedUserOutlinedIcon from '@mui/icons-material/VerifiedUserOutlined'
import Box from '@mui/material/Box'
import Dialog from '@mui/material/Dialog'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import Typography from '@mui/material/Typography'
import Zoom, { ZoomProps } from '@mui/material/Zoom'
import { BoxSpinner, DialogContent } from '@src/components/ui'
import { useAppSelector } from '@src/store/hook'
import { setDialogPermissionsUser } from '@src/store/user/actions'
import { TRolePermissions } from '@src/types/role'
import { FC, ReactElement, forwardRef, useEffect } from 'react'
import { useDispatch } from 'react-redux'

const Transition = forwardRef<unknown, ZoomProps>(
  (props, ref): ReactElement => <Zoom unmountOnExit ref={ref} {...props} />
)

type Props = {
  userName: string
  permissions: TRolePermissions[]
}

const DialogUserPermissions: FC<Props> = ({ userName, permissions }: Props) => {
  const dispatch = useDispatch()
  const { dialogUserPermissions } = useAppSelector((state) => state.user)
  const { open } = dialogUserPermissions

  const handleClose = (): void => {
    dispatch(setDialogPermissionsUser(false, null, null))
  }

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleClose}
      aria-describedby="dialog-add-edit-inventory"
      PaperProps={{ elevation: 1, sx: { position: 'relative' } }}
      maxWidth="sm"
      fullWidth
    >
      <DialogContent sx={{ pt: 4 }}>
        <Box>
          <Typography sx={{ textAlign: 'center', mb: 3 }}>
            Permission for user : <strong>{userName}</strong>
          </Typography>
          <List disablePadding dense sx={{ maxHeight: 480 }}>
            {Array.isArray(permissions) && permissions.length > 0 ? (
              permissions.map((per) => (
                <ListItem disableGutters key={String(per.id)}>
                  <ListItemIcon>
                    <VerifiedUserOutlinedIcon sx={{ color: 'primary.main', fontSize: 26 }} />
                  </ListItemIcon>
                  <ListItemText
                    primary={
                      <Typography variant="h6" sx={{ fontSize: 16 }}>
                        {per.name}
                      </Typography>
                    }
                    secondary={
                      <>
                        <Typography component="span" sx={{ fontSize: 12, mr: 1 }}>
                          Permission code :
                        </Typography>
                        <Typography component="span" sx={{ color: 'primary.main', fontSize: 12 }}>
                          {per.code}
                        </Typography>
                      </>
                    }
                  />
                </ListItem>
              ))
            ) : (
              <Box sx={{ minHeight: 120, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <Typography color="text.disabled">No permission for this user</Typography>
              </Box>
            )}
          </List>
        </Box>
      </DialogContent>
    </Dialog>
  )
}

export default DialogUserPermissions
