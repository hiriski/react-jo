import { Box } from '@mui/material';
import BackgroundImage from '@/assets/images/SL-043021-42650-28.jpg';
import { FC, ReactNode } from 'react';

type Props = {
  children: ReactNode;
};

const AuthLayout: FC<Props> = ({ children }: Props) => (
  <Box sx={{ height: '100vh' }}>
    <Box
      sx={{
        width: '100%',
        height: '100vh',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'bottom left',
        backgroundImage: `url("${BackgroundImage}")`,
        position: 'fixed',
        top: 0,
        left: 0,
      }}
    />
    {children}
  </Box>
);

export default AuthLayout;
