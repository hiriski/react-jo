import { IJobOrder } from '@/interfaces/job-order';
import { LaravelPaginationLinks, LaravelPaginationMeta } from '@src/types/laravel-pagination';

import { TImage } from './image';
import { TInvoice } from './invoice';
import { TUser } from './user';

export type TJoUser = Partial<TUser>;

export type TJoCategory = {
  id: number;
  name: string;
  slug: string;
  icon?: string;
  description?: string;
};

export type TJoProductionStatus = {
  id: number;
  name: string;
  description?: string;
};

export type TProductionStatus = TJoProductionStatus;

export type TPaymentStatus = {
  id: number;
  name: string;
  description?: string;
};

export interface IPaymentMethod {
  id: number;
  name: string;
  description?: string;
  status: 'active' | 'inactive';
  image?: string;
}

export type TJoNotes = {
  id: number;
  body: string;
  user: Partial<TUser>;
  created_at: string | Date;
};

export type TCustomer = {
  id: number;
  name: string;
  phone_number?: string;
  email?: string;
  address?: string;
};

export type TJo = IJobOrder;

export type TCreateJo = {
  title: string;
  order_date: string | Date;
  notes?: string;
  body?: string;
  category_id: number;
  payment_status_id?: number;
  payment_method_id?: number;
  customer_id?: TCustomer | number;
  customer?: Omit<TCustomer, 'id'>;
  is_nullable_customer?: boolean;
  material_paper_id?: number;
  other_material_paper?: string;
  length_in_invoice: number;
  width_in_invoice: number;
  length_in_production: number;
  width_in_production: number;
  quantity: number;
  price: number;
  tax?: number;
  dp?: number;
  additional_price_name?: string;
  additional_price?: number;
  due_date: string | Date;
  image?: string;
  down_payment?: number;
};

export type TUpdateJo = {
  id: number;
  body: TCreateJo;
};

export type TJoInvoiceItemData = {
  id: number;
  description: string;
  quantity: number;
  price: number;
  notes?: string;
  discount?: number;
};

export type TJoInvoiceData = {
  bill_to_name: string;
  bill_to_phone_number?: string | null;
  bill_to_address?: string | null;
  invoice_date?: string | Date | null;
  invoice_due_date?: string | Date | null;
  tax?: number;
  notes?: string;
  items: Array<TJoInvoiceItemData>;
  is_multiple_customer: boolean;
  multiple_customer_info?: Array<TCustomer>;
};

export type TResponseListJo = {
  data: Array<TJo>;
  meta?: LaravelPaginationMeta;
  links: LaravelPaginationLinks;
};
