import { TImage } from './image'
import { LaravelPaginationLinks, LaravelPaginationMeta } from './laravel-pagination'
import { ITag } from './tag'
import { TUser } from './user'

export interface IProductCategory {
  id: number
  name: string
  icon?: string
  description?: string
}

export interface IProduct {
  id: string
  slug: string
  title: string
  description?: string
  body?: string
  user: TUser
  images?: Array<TImage>
  status: 'active' | 'inactive' | 'draft'
  category: IProductCategory
  tags: Array<ITag>
}

export interface IResponseProductList {
  data: Array<IProduct>
  meta?: LaravelPaginationMeta
  links: LaravelPaginationLinks
}
