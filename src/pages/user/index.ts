import { lazy } from 'react'

const UserPage = lazy(() => import('./user'))

export default UserPage
