import { FC } from 'react'

const HelloComponent: FC = () => {
  return <h1>Hello 👋</h1>
}

export default HelloComponent
