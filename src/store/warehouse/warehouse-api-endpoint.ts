export const WarehouseApiEndpoints = {
  index: '/warehouse',
  material: '/warehouse/material',
  materialBrand: '/warehouse/material/brand',
  materialCategory: '/warehouse/material/category',
  materialType: '/warehouse/material/type',
};
