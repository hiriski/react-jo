import type { Effect, SagaIterator } from '@redux-saga/types'
import { httpResponseOK } from '@src/utils/http'
import { SagaReturnType, call, put, takeEvery } from 'redux-saga/effects'

import { fetchingListPaymentStatusSuccess } from './actions'
import * as ActionTypes from './constants'
import PaymentStatusAPI from './service'

type APIResponsePaymentStatusList = SagaReturnType<
  typeof PaymentStatusAPI.findAll
>

function* fetchListPaymentStatus(): SagaIterator {
  yield put({ type: ActionTypes.FETCHING_LIST_PAYMENT_STATUS_LOADING })
  try {
    const response: APIResponsePaymentStatusList = yield call(
      PaymentStatusAPI.findAll
    )
    if (httpResponseOK(response.status)) {
      const { data } = response
      yield put(fetchingListPaymentStatusSuccess(data))
    }
  } catch (e) {
    yield put({ type: ActionTypes.FETCHING_LIST_PAYMENT_STATUS_FAILURE })
  }
}

export default function* paymentStatusSaga(): SagaIterator {
  yield takeEvery(
    ActionTypes.FETCHING_LIST_PAYMENT_STATUS_REQUESTED,
    fetchListPaymentStatus
  )
}
