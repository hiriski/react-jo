import { FC } from 'react'
import Box from '@mui/material/Box'

const PageNotFound: FC = () => (
  <Box
    sx={{
      display: 'flex',
      minHeight: '100vh',
      alignItems: 'center',
      flexDirection: 'column',
      justifyContent: 'center',
    }}
  >
    PageNotFound
  </Box>
)

export default PageNotFound
