import { masterData_fetchMasterData } from '@/store/master-data/master-data-actions';
import {
  warehouse_setDialogAddEditMaterial,
  warehouse_setDialogAddStockMaterial,
} from '@/store/warehouse/warehouse-actions';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import DialogActions from '@mui/material/DialogActions';
// import Slide, { SlideProps } from '@mui/material/Slide'
// import Grow, { GrowProps } from '@mui/material/Grow'
import Zoom, { ZoomProps } from '@mui/material/Zoom';
import { BoxSpinner, DialogContent } from '@src/components/ui';
import { useAppSelector } from '@src/store/hook';
import { jobOrder_fetchDetail, setDialogDetailJo } from '@/store/job-order/job-order-actions';
import { FC, ReactElement, forwardRef, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useWindowSize } from 'react-use';

// Interfaces
import { RequestBodyMasterData } from '@/interfaces/master-data';

// Selectors.
import { masterData_rootSelector } from '@/store/master-data/master-data-selectors';
import { warehouse_rootSelector } from '@/store/warehouse/warehouse-selectors';
import { BaseDialog } from '@/components/base';
import { MaterialForm } from './material-form';
import { TextField } from '@mui/material';
import { MaterialUnitConstants } from '@/constants/warehouse.constants';
import PaperRollImg from '@/assets/images/paper-roll.jpeg';

const Transition = forwardRef<unknown, ZoomProps>(
  (props, ref): ReactElement => (
    // eslint-disable-next-line react/jsx-props-no-spreading
    // <Slide direction="down" ref={ref} {...props} />
    // <Grow style={{ transformOrigin: '0 0 0' }} ref={ref} {...props} />
    <Zoom ref={ref} {...props} />
  ),
);

const masterDataPayload: RequestBodyMasterData = [
  {
    object_name: 'material_types',
  },
  {
    object_name: 'material_categories',
  },
];

export const WarehouseDialogAddStockMaterial: FC = () => {
  const dispatch = useDispatch();
  const { height: windowHeight } = useWindowSize();
  const { dialogAddStockMaterial } = useAppSelector((state) => warehouse_rootSelector(state));
  const { isFetching, data: masterData } = useAppSelector((state) => masterData_rootSelector(state));
  const { open, material } = dialogAddStockMaterial;

  /**
   * Handle dialog close.
   */
  const handleCloseDialog = (): void => {
    dispatch(warehouse_setDialogAddStockMaterial(false, null));
  };

  return (
    <BaseDialog
      disableDialogActions
      title="Tambah Stok Bahan Baku"
      maxWidth="sm"
      open={open}
      onClose={handleCloseDialog}
      onConfirm={handleCloseDialog}
    >
      <Box
        sx={{
          height: isFetching ? 200 : windowHeight - 300,
          transition: (theme) => theme.transitions.create(['height']),
        }}
      >
        {material && (
          <Box>
            {material.display_unit === MaterialUnitConstants.roll && (
              <Box component="img" sx={{ width: 100, height: 'auto' }} src={PaperRollImg} />
            )}
            <Typography>{material.name}</Typography>
            <Typography>{material.material_type_id}</Typography>
            <TextField label="Stok Tambahan" name="additional_stock_length" />
          </Box>
        )}
      </Box>
    </BaseDialog>
  );
};
