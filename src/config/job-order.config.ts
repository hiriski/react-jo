export const JobOrderConfig = {
  FetchInterval: IS_DEV ? 50000 : 15000,
  EnableAdditionalCosts: false,
};
