import { FC } from 'react';
import Spinner, { Size as SpinnerSize } from '@atlaskit/spinner';
import { Box, SxProps, alpha, Typography } from '@mui/material';

type Props = {
  sx?: SxProps;
  height?: number | string;
  spinnerSize?: SpinnerSize;
  opacity?: number;
  position?: 'absolute' | 'relative';
};

export const BaseLoader: FC<Props> = ({ sx, height, spinnerSize, opacity, position }: Props) => (
  <Box
    sx={{
      top: 0,
      left: 0,
      flex: 1,
      zIndex: 100,
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      flexDirection: 'column',
      justifyContent: 'center',
      backgroundColor: (theme) => alpha(theme.palette.background.paper, opacity),
      position: position,
      height: height ?? '100%',
      '& svg > circle': { stroke: (theme) => theme.palette.primary.main + ' !important' },
      ...sx,
    }}
  >
    <Spinner size={spinnerSize} appearance="inherit" />
    <Typography>Tunggu Sebentar...</Typography>
  </Box>
);

BaseLoader.defaultProps = {
  spinnerSize: 36,
  opacity: 0.75,
  height: '100%',
  position: 'absolute',
};
