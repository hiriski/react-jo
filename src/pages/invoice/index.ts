import { lazy } from 'react'

const InvoicePage = lazy(() => import('./invoice'))

export default InvoicePage
