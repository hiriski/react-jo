import { StyledButton } from '@/components/ui';
import Spinner from '@atlaskit/spinner';
import { Box, Button, TextField } from '@mui/material';
import { login } from '@src/store/auth/actions';
import { useAppSelector } from '@src/store/hook';
import { TRequestLogin } from '@src/types/auth';
import { FC } from 'react';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';

type Inputs = TRequestLogin;

const defaultValues = {
  email: '',
  password: '',
};

const LoginForm: FC = () => {
  const dispatch = useDispatch();
  const { loginLoading } = useAppSelector((state) => state.auth);
  const { control, handleSubmit } = useForm({
    defaultValues,
  });

  const onSubmit: SubmitHandler<Inputs> = (values) => {
    dispatch(login(values));
  };

  return (
    <Box
      sx={{
        display: 'flex',
        fontWeight: 'bold',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        width: '100%',
      }}
      component="form"
      onSubmit={handleSubmit(onSubmit)}
    >
      <Controller
        name="email"
        control={control}
        render={({ field }) => <TextField {...field} fullWidth size="small" margin="normal" label="Email" />}
      />
      <Controller
        name="password"
        control={control}
        render={({ field }) => (
          <TextField {...field} fullWidth size="small" type="password" margin="normal" label="Password" />
        )}
      />

      <Box sx={{ mb: 2 }} />

      <StyledButton size="medium" color="primary" type="submit" isLoading={loginLoading} disableHoverEffect fullWidth>
        Login
      </StyledButton>
    </Box>
  );
};

export default LoginForm;
