import { darken, Box, Grid, Typography } from '@mui/material';
import { FC, ReactElement, ReactNode } from 'react';
import { FolderOpenOutline } from 'react-ionicons';
import CountUp from 'react-countup';
import Spinner from '@atlaskit/spinner';

interface Props {
  value: number;
  icon?: ReactNode;
  color: string;
  title: string;
  subtitle?: string;
  variant?: 'default' | 'center';
  cardColor?: 'default' | 'light';
  size?: 'small' | 'medium' | 'large';
  disableShadow?: boolean;
  isLoading?: boolean;
}

const createIconSize = (size: 'small' | 'medium' | 'large'): number => {
  switch (size) {
    case 'small':
      return 40;
      break;
    case 'medium':
      return 54;
    default:
      return 60;
      break;
  }
};

export const BaseAnalyticsWidget: FC<Props> = ({
  value,
  icon,
  color,
  title,
  subtitle,
  variant,
  cardColor,
  size,
  disableShadow,
  isLoading,
}) => {
  return (
    <Box
      sx={{
        borderRadius: 1,
        ...(!disableShadow && {
          boxShadow: 2,
        }),

        ...(cardColor === 'light' && {
          boxShadow: 2,
        }),

        '&:hover': {
          ...(cardColor === 'light' && {
            boxShadow: 3,
          }),
        },
      }}
    >
      <Box
        sx={{
          ...(size === 'small' && {
            py: 1.8,
            px: 2.4,
          }),
          ...(size === 'medium' && {
            py: 2.4,
            px: 3,
          }),
          ...(size === 'large' && {
            py: 3.25,
            px: 3.5,
          }),
          borderRadius: 1,
          width: '100%',
          backgroundColor: cardColor === 'light' ? 'background.paper' : color,
          color: cardColor === 'light' ? color : 'primary.contrastText',
          userSelect: 'none',
          ...(variant === 'default' && {
            textAlign: 'left',
          }),

          ...(variant === 'center' && {
            textAlign: 'center',
          }),

          '&:hover': {
            backgroundColor: cardColor === 'light' ? 'background.paper' : darken(color, 0.25),
          },
        }}
      >
        <Box sx={{ mb: 1 }}>
          <Grid container spacing={0}>
            <Grid item xs={variant === 'default' ? 6 : 12}>
              <Box
                sx={{
                  width: createIconSize(size),
                  // height: createIconSize(size),
                  borderRadius: createIconSize(size),
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: '100%',

                  ...(variant === 'center' && {
                    margin: 'auto',
                  }),

                  // Icon style
                  '& span': {
                    color: 'inherit',
                    '& > svg': {
                      width: createIconSize(size),
                      height: createIconSize(size),
                      fill: 'currentColor',
                      color: 'inherit',
                    },
                  },
                }}
              >
                {icon ? (
                  icon
                ) : (
                  <FolderOpenOutline
                    style={{ height: createIconSize(size), width: createIconSize(size), color: 'inherit' }}
                  />
                )}
              </Box>
            </Grid>
            <Grid item xs={variant === 'default' ? 6 : 12}>
              <Box sx={{ height: 42, display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
                {isLoading ? (
                  <Box
                    sx={{
                      display: 'inherit',
                      mr: 1,
                      ml: -0.25,
                      "& [class$='-Spinner'] > svg > circle": { stroke: '#fff !important' },
                    }}
                  >
                    <Spinner />
                  </Box>
                ) : (
                  <Typography
                    sx={{
                      color: 'inherit',
                      fontWeight: 'bold',

                      ...(size === 'small' && {
                        fontSize: 32,
                      }),
                      ...(size === 'medium' && {
                        fontSize: 36,
                      }),
                      ...(size === 'large' && {
                        fontSize: 44,
                      }),

                      ...(variant === 'default' && {
                        textAlign: 'right',
                      }),
                      ...(variant === 'center' && {
                        textAlign: 'center',
                        mt: 1,
                      }),
                    }}
                  >
                    <CountUp start={0} end={value} duration={0.3} />
                  </Typography>
                )}
              </Box>
            </Grid>
          </Grid>
        </Box>
        <Typography variant="h5" sx={{ mb: 0.5 }}>
          {title}
        </Typography>
        {subtitle && <Typography variant="subtitle2">{subtitle}</Typography>}
      </Box>
    </Box>
  );
};

BaseAnalyticsWidget.defaultProps = {
  title: 'No Data',
  variant: 'default',
  size: 'medium',
  cardColor: 'default',
  disableShadow: false,
  isLoading: false,
};
