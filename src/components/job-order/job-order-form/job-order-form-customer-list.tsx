import { ChangeEvent, FC, KeyboardEvent, MouseEvent, useMemo, useState } from 'react';

// Mui components.
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import Popover from '@mui/material/Popover';

// Mui icons.
import DescriptionIcon from '@mui/icons-material/Description';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import LocationOffIcon from '@mui/icons-material/LocationOff';
import PersonIcon from '@mui/icons-material/Person';
import BusinessIcon from '@mui/icons-material/Business';

// Interfaces.
// import { SxProps } from '@mui/material';
import { ICustomer } from '@/interfaces/customer';
import {
  Card,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  styled,
  SelectChangeEvent,
  Checkbox,
  FormControlLabel,
  RadioGroup,
  Radio,
} from '@mui/material';

// Libs
import { colors } from '@/lib/colors';
import { getInitialsName } from '@/utils/misc';
import { UUID } from '@/interfaces/common';
import { displayGender } from '@/utils/common';
import { AttachEmailOutlined, CallOutlined } from '@mui/icons-material';

// lodash.
import _debounce from 'lodash/debounce';

interface StyledBoxTableColumnProps {
  isSelected: boolean;
  width?: number | string;
}

const StyledBoxTableColumn = styled(Box, {
  shouldForwardProp: (prop) => prop !== 'isSelected' && prop !== 'width',
})<StyledBoxTableColumnProps>(({ theme, isSelected, width }) => ({
  display: 'table-cell',
  verticalAlign: 'middle',
  padding: '6px 10px',
  maxWidth: width,
}));

const TextEllipsis = styled(Typography, { shouldForwardProp: (prop) => prop !== 'maxWidth' })<{
  maxWidth: number | string;
}>(({ theme, maxWidth }) => ({
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  textOverflow: 'ellipsis',
  maxWidth,
}));

interface Props {
  data: Array<ICustomer>;
  selectedId?: UUID;
  onSelect?: (id: UUID | null) => void;
}

const AVATAR_SIZE = 44;

export const JobOrderFormCustomerList: FC<Props> = ({ data, selectedId, onSelect }) => {
  const [search, setSearch] = useState<string>('');
  const [type, setType] = useState<string>('all');
  const [anchorElPopoverCustomerNotes, setAnchorElPopoverCustomerNotes] = useState<HTMLElement | null>(null);
  const [customerNotes, setCustomerNotes] = useState<string>('');

  const customerList = useMemo<Array<ICustomer>>(() => {
    if (data) {
      if (type !== 'all') return data.filter((x) => x.customer_type === type);
      else if (search !== '') return data.filter((x) => x.name.includes(search));
      else return data;
    } else return [];
  }, [data, type, search]);

  const debounceSearch = _debounce(function (value: string) {
    setSearch(value);
  }, 1000);

  const handleChange = (e: ChangeEvent<HTMLInputElement>): void => {
    // setSearch(e.target.value);
    debounceSearch(e.target.value);
  };

  /**
   * Prevent press enter.
   */
  const onKeyDown = (e: KeyboardEvent): void => {
    if (e.key === 'Enter') {
      e.preventDefault();
    }
  };

  // Handle open popover customer notes
  const handleOpenPopOverCustomerNotes = (e: MouseEvent<HTMLElement>, notes: string): void | undefined => {
    if (notes) {
      setAnchorElPopoverCustomerNotes(e.currentTarget);
      setCustomerNotes(notes);
    }
    if (!notes) return;
  };

  // Handle close anchor
  const handleClosePopoverCustomerNotes = (): void => {
    setAnchorElPopoverCustomerNotes(null);
    setCustomerNotes('');
  };

  return (
    <Box
      sx={{
        position: 'relative',
        borderRadius: (theme) => theme.spacing(0, 0, 1, 1),
        backgroundColor: 'background.paper',
        border: (theme) => `1px solid ${theme.palette.divider}`,
        boxShadow: 1,
      }}
    >
      <Box sx={{ display: 'flex', py: 3, px: 3, borderBottom: (theme) => `1px solid ${theme.palette.divider}` }}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <TextField
              name="search"
              label="Cari customer"
              fullWidth
              size="small"
              margin="none"
              onChange={handleChange}
              // onKeyPress={onKeyDown}
              onKeyDown={onKeyDown}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <FormControl fullWidth sx={{ ml: 3 }}>
              <RadioGroup row value={type} onChange={(e, newValue: string) => setType(newValue)}>
                <FormControlLabel
                  control={<Radio size="small" value="individual" />}
                  label={
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                      <PersonIcon sx={{ fontSize: 18, mr: 2 }} />
                      <Typography>Individu</Typography>
                    </Box>
                  }
                />
                <FormControlLabel
                  control={<Radio size="small" value="company" />}
                  label={
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                      <BusinessIcon sx={{ fontSize: 18, mr: 2 }} />
                      <Typography>Perusahaan</Typography>
                    </Box>
                  }
                />
              </RadioGroup>
              {/* <InputLabel>Kategori Customer</InputLabel>
              <Select
                value={type}
                onChange={(e: SelectChangeEvent) => setType(String(e.target.value))}
                label="Jenis Customer"
                size="small"
                margin="none"
              >
                <MenuItem value="all">Semua</MenuItem>
                <MenuItem value="individual">Individu</MenuItem>
                <MenuItem value="company">Perusahaan</MenuItem>
              </Select> */}
            </FormControl>
          </Grid>
        </Grid>
      </Box>

      {/* Popover customer notes */}
      <Popover
        id="popover-customer-notes"
        sx={{
          pointerEvents: 'none',
        }}
        open={Boolean(anchorElPopoverCustomerNotes)}
        anchorEl={anchorElPopoverCustomerNotes}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        // transformOrigin={{
        //   vertical: 'top',
        //   horizontal: 'left',
        // }}
        onClose={handleClosePopoverCustomerNotes}
        disableRestoreFocus
      >
        <Box
          sx={{
            px: 3,
            py: 2,
            maxWidth: 240,
            borderRadius: 1,
            backgroundColor: '#ffefc3',
            boxShadow: 'var(--shadow-small)',
          }}
        >
          <Box sx={{ display: 'flex', alignItems: 'center', mb: 1.2 }}>
            <DescriptionIcon sx={{ fontSize: 16, mr: 2, color: 'text.disabled' }} />
            <Typography variant="subtitle1">Catatan customer :</Typography>
          </Box>
          <Typography variant="body2">{customerNotes}</Typography>
        </Box>
      </Popover>

      <Box sx={{ display: 'flex', flex: 1, px: 2, py: 1.6, maxHeight: 300, overflowY: 'auto' }}>
        <Grid container spacing={2}>
          {customerList.map((customer, index) => (
            <Grid key={customer.id} item xs={12} sm={6} md={4}>
              <Box
                sx={{
                  py: 2,
                  px: 3.4,
                  height: 140,
                  cursor: 'pointer',
                  position: 'relative',
                  borderRadius: 1,
                  overflow: 'hidden',
                  border: (theme) => `1px dashed ${theme.palette.divider}`,
                  ...(selectedId === customer.id && {
                    border: (theme) => `1px dashed ${theme.palette.primary.main}`,
                    backgroundColor: (theme) => theme.palette.primary.light,
                  }),

                  ...(index === customerList.length - 1 && {
                    mb: 3,
                  }),
                }}
                onClick={() => onSelect(selectedId === customer.id ? null : customer.id)}
                onMouseEnter={(e) => handleOpenPopOverCustomerNotes(e, customer.notes)}
                onMouseLeave={handleClosePopoverCustomerNotes}
              >
                <Box sx={{ display: 'flex', alignItems: 'center', mb: 1.5 }}>
                  <Avatar
                    src={customer.photo_url}
                    sx={{
                      backgroundColor: customer.avatar_text_color ?? colors[Math.floor(Math.random() * colors.length)],
                      color: 'primary.contrastText',
                      width: AVATAR_SIZE,
                      height: AVATAR_SIZE,
                      fontSize: 14,
                      letterSpacing: 1,
                      mr: 2,
                    }}
                    alt={customer.name ?? 'Customer photo'}
                  >
                    {getInitialsName(customer.name)}
                  </Avatar>
                  <Box>
                    <Box sx={{ maxWidth: 160 }}>
                      <TextEllipsis sx={{ fontWeight: 700 }} maxWidth={200}>
                        {customer.name}
                      </TextEllipsis>
                    </Box>
                    <Box>
                      <Box sx={{ display: 'flex', alignItems: 'center' }}>
                        <CallOutlined sx={{ fontSize: 14, mr: 1, color: 'text.disabled' }} />
                        <Typography variant="subtitle1">{customer.phone_number ?? '-'}</Typography>
                      </Box>
                    </Box>
                  </Box>
                </Box>
                <Box>
                  {customer.email && (
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                      <AttachEmailOutlined sx={{ fontSize: 14, mr: 1, color: 'text.disabled' }} />
                      <Typography variant="subtitle1">{customer.email}</Typography>
                    </Box>
                  )}
                  {customer.addresses.length ? (
                    <Box sx={{ display: 'flex' }}>
                      <LocationOnIcon sx={{ fontSize: 16, mr: 1, mt: 1, color: 'text.disabled' }} />
                      <Box>
                        <TextEllipsis variant="subtitle1" maxWidth={190}>
                          {`${customer.addresses[0].address_details} ${customer.addresses[0].district}`}
                        </TextEllipsis>
                        <TextEllipsis variant="subtitle1" maxWidth={190}>
                          {`${customer.addresses[0].city} ${customer.addresses[0].province} `}
                        </TextEllipsis>
                      </Box>
                    </Box>
                  ) : (
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                      <LocationOffIcon sx={{ fontSize: 16, mr: 1, color: 'text.disabled' }} />
                      <Typography variant="subtitle1">Tidak ada alamat</Typography>
                    </Box>
                  )}
                </Box>

                {/* Selected Checkbox */}
                <Box sx={{ position: 'absolute', top: 4, right: 4 }}>
                  {selectedId === customer.id && <Checkbox size="small" checked={true} />}
                </Box>
              </Box>
            </Grid>
          ))}
        </Grid>
      </Box>
    </Box>
  );
};
