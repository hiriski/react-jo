import { IUser } from './user';

export interface INote {
  id: string;
  body: string;
  user: IUser;
  created_at: string;
}
