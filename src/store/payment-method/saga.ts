import type { Effect, SagaIterator } from '@redux-saga/types'
import { httpResponseOK } from '@src/utils/http'
import { SagaReturnType, call, put, takeEvery } from 'redux-saga/effects'

import { fetchListPaymentMethodSuccess } from './actions'
import * as ActionTypes from './constants'
import apiPaymentMethod from './service'

type APIResponseListPaymentMethod = SagaReturnType<typeof apiPaymentMethod.findAll>

function* fetchListPaymentMethod(): SagaIterator {
  yield put({ type: ActionTypes.FETCHING_LIST_PAYMENT_METHOD_LOADING })
  try {
    const response: APIResponseListPaymentMethod = yield call(apiPaymentMethod.findAll)
    if (httpResponseOK(response.status)) {
      const { data } = response
      yield put(fetchListPaymentMethodSuccess(data))
    }
  } catch (e) {
    yield put({ type: ActionTypes.FETCHING_LIST_PAYMENT_METHOD_FAILURE })
  }
}

export default function* paymentMethodSaga(): SagaIterator {
  yield takeEvery(ActionTypes.FETCHING_LIST_PAYMENT_METHOD_REQUESTED, fetchListPaymentMethod)
}
