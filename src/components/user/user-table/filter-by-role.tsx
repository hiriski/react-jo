import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import Select, { SelectChangeEvent } from '@mui/material/Select'
import { useAppSelector } from '@src/store/hook'
import { FC, memo, useEffect, useState } from 'react'

const FilterByRole: FC = () => {
  const { listRole } = useAppSelector((state) => state.role)
  const [selectedRole, setSelectedRole] = useState<string>('0')

  const handleFilterByRole = (event: SelectChangeEvent): void => {
    setSelectedRole(event.target.value as string)
  }

  useEffect(() => {
    if (selectedRole) {
      console.log('Call me', selectedRole)
    }
  }, [selectedRole])

  return (
    <FormControl size="small" variant="standard" sx={{ ml: 6, minWidth: 195 }}>
      <InputLabel id="select-role">Filter by Role</InputLabel>
      <Select
        size="small"
        labelId="select-role"
        id="select"
        value={selectedRole}
        label="Filter by Role"
        onChange={handleFilterByRole}
      >
        <MenuItem value={0}>None</MenuItem>
        {Array.isArray(listRole) &&
          listRole.length > 0 &&
          listRole.map((role) => (
            <MenuItem key={String(role.id)} value={String(role.id)}>
              {role.name}
            </MenuItem>
          ))}
      </Select>
    </FormControl>
  )
}

const MemoizedFilterByRole = memo(FilterByRole)
export default MemoizedFilterByRole
