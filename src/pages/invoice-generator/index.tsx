import { lazy } from 'react'

const InvoiceGenerator = lazy(() => import('./invoice-generator'))

export default InvoiceGenerator
