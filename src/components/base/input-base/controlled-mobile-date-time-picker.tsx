import { FC, useMemo, useRef, useState } from 'react';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import { MobileDateTimePicker, MobileDateTimePickerProps } from '@mui/x-date-pickers/MobileDateTimePicker';
import { Control, Controller, FieldErrors } from 'react-hook-form';
import moment from 'moment';

// Config.
import { AppConfig } from '@/config/theme/app/app-config';

interface Props
  extends Partial<MobileDateTimePickerProps<string, string>>,
    Pick<TextFieldProps, 'size' | 'fullWidth' | 'variant' | 'margin'> {
  // eslint-disable-next-line
  control: Control<any>;
  errors: FieldErrors;
  name: string;
  label: string;
  helperText?: string;
  inputFormat?: string;
  closeOnSelect?: boolean;
}

export const ControlledMobileDateTimePicker: FC<Props> = (props) => {
  const { control, errors, name, label, size, fullWidth, margin, helperText, inputFormat, closeOnSelect, ...rest } =
    props;

  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value } }) => (
        <MobileDateTimePicker
          label={label}
          inputFormat={inputFormat ?? 'MM/dd/yyyy'}
          value={value || null}
          closeOnSelect={closeOnSelect}
          onChange={(newValue: string | null): void => {
            // _onChange(moment(newValue).format(AppConfig.DatabaseFormatTimestamps));
            onChange(moment(newValue).format(AppConfig.DatabaseFormatTimestamps));
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              error={Boolean(errors[name]?.message)}
              helperText={errors[name]?.message ?? helperText}
              size={size}
              margin={margin}
              fullWidth={fullWidth}
            />
          )}
        />
      )}
    />
  );
};

ControlledMobileDateTimePicker.defaultProps = {
  size: 'small',
  margin: 'dense',
};
