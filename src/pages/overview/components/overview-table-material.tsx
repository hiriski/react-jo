import { FC, MouseEvent, useState } from 'react';
import { BaseCard } from '@/components/base';

import useTheme from '@mui/material/styles/useTheme';
import { IMaterial } from '@/interfaces/warehouse';
import { MaterialTable } from '@/components/warehouse/material/material-table';

import { masterData_fetchMasterData } from '@/store/master-data/master-data-actions';
import { masterData_rootSelector } from '@/store/master-data/master-data-selectors';
import { createLabelJoPaymentStatus } from '@/utils/jo';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import MoneyOffIcon from '@mui/icons-material/MoneyOff';
import SellIcon from '@mui/icons-material/Sell';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import Menu, { MenuProps } from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';
import { alpha, styled } from '@mui/material/styles';
import { useDispatch } from 'react-redux';
import Spinner from '@atlaskit/spinner';
import { jobOrder_rootSelector } from '@/store/job-order/job-order-selectors';
import { formatRupiah } from '@/utils/currency';

interface Props {
  data: Array<IMaterial>;
}

export const OverviewTableMaterial: FC<Props> = ({ data }) => {
  const theme = useTheme();

  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);

  const handleClick = (event: MouseEvent<HTMLElement>): void => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = (): void => {
    setAnchorEl(null);
  };

  return (
    <BaseCard
      sx={{ position: 'relative' }}
      title="Tabel Bahan Baku Produksi"
      subtitle="Tabel Rincian Bahan Baku Produksi"
      rightContent={
        <Box>
          <Button
            id="demo-customized-button"
            aria-controls={open ? 'demo-customized-menu' : undefined}
            aria-haspopup="true"
            aria-expanded={open ? 'true' : undefined}
            variant="outlined"
            disableElevation
            onClick={handleClick}
            endIcon={<KeyboardArrowDownIcon />}
            sx={{
              // color: createLabelJoPaymentStatus(currentPaymentStatus.id),
              // border: `1px solid ${createLabelJoPaymentStatus(currentPaymentStatus.id)}`,
              textAlign: 'center',
            }}
          >
            Kategori
          </Button>
          <Menu
            id="job-order-payment-status-menu"
            MenuListProps={{
              'aria-labelledby': 'demo-customized-button',
            }}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={handleClose}
            PaperProps={{
              elevation: 4,
            }}
            // anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            // transformOrigin={{
            //   vertical: 'top',
            //   horizontal: 'right',
            // }}
          >
            <MenuItem disableRipple>Kategori 1</MenuItem>
            <MenuItem disableRipple>Kategori 2</MenuItem>
            <MenuItem disableRipple>Kategori 3</MenuItem>
          </Menu>
        </Box>
      }
    >
      <MaterialTable isLoading={false} data={data} maxWidth={700} />
    </BaseCard>
  );
};
