import { FC } from 'react'

type Props = {
  width?: number | string
  height?: number | string
}

const Logo: FC<Props> = ({ width, height }: Props) => (
  <img style={{ width, height }} src="/static/images/logo-development.png" alt="development logo" />
)

Logo.defaultProps = {
  width: 180,
  height: 'auto',
}

export default Logo
