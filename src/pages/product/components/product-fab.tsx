import theme from '@/config/theme'
import { setDrawerAddEditProduct } from '@/store/product/product-actions'
import AddOutlinedIcon from '@mui/icons-material/AddOutlined'
import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined'
import Box from '@mui/material/Box'
import Fab from '@mui/material/Fab'
import { useTheme } from '@mui/material/styles'
import Tooltip from '@mui/material/Tooltip'
import { useAppSelector } from '@src/store/hook'
import { FC } from 'react'
import { useDispatch } from 'react-redux'

const ProductFAB: FC = () => {
  const dispatch = useDispatch()
  const { drawerAddEditProduct: drawer } = useAppSelector((state) => state.product)
  const theme = useTheme()

  const handleOpen = (): void => {
    dispatch(setDrawerAddEditProduct(true, null))
  }

  const handleClose = (): void => {
    dispatch(setDrawerAddEditProduct(false, null))
  }

  return (
    <Box
      sx={{
        zIndex: 10,
        position: 'fixed',

        bottom: (theme) => theme.spacing(5),
        right: (theme) => theme.spacing(5),

        transition: theme.transitions.create('bottom', {
          easing: theme.transitions.easing.easeOut,
          duration: theme.transitions.duration.enteringScreen,
        }),

        ...(drawer.open && {
          bottom: theme.spacing(-10),
          transition: theme.transitions.create('bottom', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
          }),
        }),
      }}
    >
      <Tooltip title="Buat Produk">
        <Fab size="medium" color="primary" onClick={!drawer.open ? handleOpen : handleClose} aria-label="Add product">
          {!drawer.open ? <AddOutlinedIcon /> : <CloseOutlinedIcon />}
        </Fab>
      </Tooltip>
    </Box>
  )
}

export default ProductFAB
