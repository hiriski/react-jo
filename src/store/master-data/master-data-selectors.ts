// Interfaces.
import type { AppState } from '@src/store/root-reducer';
import { MasterDataState } from './master-data-reducer';

export const masterData_rootSelector = (state: AppState): MasterDataState => state.masterData;
