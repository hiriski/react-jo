import Paper, { PaperProps } from '@mui/material/Paper'
import { alpha } from '@mui/material/styles'
import { FC, ReactNode } from 'react'

const Card: FC<PaperProps> = ({ children, sx }) => (
  <Paper
    elevation={0}
    sx={{
      borderRadius: 1.2,
      position: 'relative',
      backgroundColor: 'background.paper',
      border: (theme) => `1px solid ${alpha(theme.palette.text.primary, 0.08)}`,
      overflow: 'hidden',
      ...sx,
    }}
  >
    {children}
  </Paper>
)

Card.defaultProps = {
  sx: {},
}

export default Card
