import { FC } from 'react';

// Mui components.
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import RadioGroup from '@mui/material/RadioGroup';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import { Control, Controller, FieldErrors } from 'react-hook-form';
import { dropdownDiscountType } from '@/lib/dropdown';
import { alpha } from '@mui/material';

// Mui icons
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';

interface Props {
  // eslint-disable-next-line
  control: Control<any>;
  errors: FieldErrors;
}

export const ControlledInputDiscountType: FC<Props> = (props) => {
  const { control, errors } = props;

  return (
    <Box>
      <Controller
        name="discount_type"
        control={control}
        render={({ field: { value, onChange } }) => {
          return (
            <FormControl sx={{ width: '100%' }}>
              <RadioGroup
                value={value}
                onChange={(e, value: string | null) => onChange(value)}
                sx={{
                  width: '100%',
                }}
              >
                <Grid container spacing={3}>
                  {dropdownDiscountType.map((x) => (
                    <Grid key={String(x.value)} item xs={12} sm={4}>
                      <FormControlLabel
                        onClick={() => onChange(x.value)}
                        sx={{
                          px: 3,
                          py: 2,
                          margin: '0 !important',
                          width: '100%',
                          borderRadius: 1,
                          backgroundColor: x.value === value ? 'primary.light' : 'background.paper',
                          '&:hover': {
                            backgroundColor: 'primary.light',
                            border: (theme) => `1px dashed ${theme.palette.primary.main}`,
                          },
                          border: (theme) =>
                            x.value === value
                              ? `1px dashed ${theme.palette.primary.main}`
                              : `1px dashed ${alpha('#000', 0.2)}`,
                          '& .MuiFormControlLabel-label': {
                            display: 'none',
                          },
                        }}
                        control={
                          <Box sx={{ display: 'flex', alignItems: 'center' }}>
                            {x.value === value ? (
                              <RadioButtonCheckedIcon sx={{ color: 'primary.main', fontSize: 20 }} />
                            ) : (
                              <RadioButtonUncheckedIcon sx={{ color: 'text.disabled', fontSize: 20 }} />
                            )}
                            <Typography
                              sx={{
                                ml: 2,
                                fontSize: 16,
                                fontWeight: 600,
                                color: x.value === value ? 'primary.main' : 'text.disabled',
                              }}
                            >
                              {x.label}
                            </Typography>
                          </Box>
                        }
                        label={x.label}
                      />
                    </Grid>
                  ))}
                </Grid>
              </RadioGroup>
              {/* <FormHelperText error={Boolean(errors[name]?.message)}>
              {errors[name]?.message ? errors[name]?.message : helperText}
            </FormHelperText> */}
            </FormControl>
          );
        }}
      />
    </Box>
  );
};
