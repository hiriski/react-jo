import UploadImage from '@/assets/images/upload-image.png';
import { yupResolver } from '@hookform/resolvers/yup';
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import SendIcon from '@mui/icons-material/Send';
import DescriptionIcon from '@mui/icons-material/Description';
import DriveFolderUploadIcon from '@mui/icons-material/DriveFolderUpload';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import FormGroup from '@mui/material/FormGroup';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Collapse from '@mui/material/Collapse';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';
import InputAdornment from '@mui/material/InputAdornment';
import Paper from '@mui/material/Paper';
import Radio from '@mui/material/Radio';
import { alpha, styled } from '@mui/material/styles';
import FormHelperText from '@mui/material/FormHelperText';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import TextField from '@mui/material/TextField';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Slider from '@mui/material/Slider';
import Stack from '@mui/material/Stack';
import { SxProps } from '@mui/material';
import { useAppSelector } from '@src/store/hook';
import {
  jobOrder_createJobOrder,
  jobOrder_setFormHasChanges,
  jobOrder_updateJobOrder,
} from '@/store/job-order/job-order-actions';
import { initDataMasterDataState } from '@/store/master-data/master-data-reducer';
import { TCreateJo, TCustomer, TJoCategory } from '@src/types/jo';
import { IMasterData } from '@/interfaces/master-data';
import { DB_FORMAT_TIMESTAMPS, ROUTES } from '@src/utils/constants';
import moment from 'moment';
import { ChangeEvent, FC, MouseEvent, ReactNode, useCallback, useEffect, useMemo, useState } from 'react';
import { Controller, SubmitHandler, useFieldArray, useForm } from 'react-hook-form';
import ImageUploading, { ImageListType } from 'react-images-uploading';
import { useDispatch } from 'react-redux';
// import Swal from 'sweetalert2'
import * as yup from 'yup';

// Mui icons
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';

import SelectedCustomer from './selected-customer';
import { filterDependenciesDropdown, findCustomerById } from './utils';
import { ICustomer } from '@/interfaces/customer';
import { UUID } from '@/interfaces/common';

// Components.
import { JobOrderFormSection } from './job-order-form-section';

// Config,
import { AppConfig } from '@/config/theme/app/app-config';

// Validation schema.
import { jobOrderFormSchema } from './job-order-form.schema';

// Base components.
import { BaseSectionTitle } from '@/components/base/base-section-title';
import { IJobOrder, IRequestBodyJobOrder } from '@/interfaces/job-order';
import {
  ControlledCheckbox,
  ControlledTextField,
  ControlledSelect,
  ControlledMobileDateTimePicker,
  ControlledDateTimePicker,
  ControlledRadioGroup,
  ControlledAutocomplete,
  ControlledInputDiscountType,
  ControlledSlider,
} from '@/components/base/input-base';
import { dropdownCustomerType, dropdownGenders } from '@/lib/dropdown';
import { jobOrder_rootSelector } from '@/store/job-order/job-order-selectors';

// Lib wilayah administratif.
import { provinsi, kabupaten } from 'daftar-wilayah-indonesia';
import { BaseButton, BaseTabPanel, BaseTabs, BaseUploadImages } from '@/components/base';
import { JobOrderFormCustomerList } from './job-order-form-customer-list';

// Lib
import { colors } from '@/lib/colors';
import { useLocation, useNavigate, useRoutes } from 'react-router';
import { JobOrderCategoryIdConstants, MaterialTypeConstants } from '@/constants';
import { grey } from '@mui/material/colors';
import { formatRupiah } from '@/utils/currency';
import { JobOrderConfig } from '@/config';
import { IMaterial } from '@/interfaces/warehouse';

type CustomerOptions = TCustomer;

type Inputs = IRequestBodyJobOrder & { is_new_customer: boolean };

type SourcesProps = IMasterData;

type Props = {
  sx?: SxProps;
  id?: UUID | null;
  data?: IJobOrder;
  displayOnDrawer?: boolean;
};

const initialValues: Inputs = {
  title: '',
  product_id: undefined,
  category_id: 1, // Print Sticker
  customer_id: undefined,
  payment_status_id: undefined,
  payment_method_id: null,
  bank_account_id: null,
  order_date: moment(new Date()).format(AppConfig.DatabaseFormatTimestamps),
  due_date: undefined,
  body: '',
  down_payment: null,
  order_quantity: 1,
  order_price: undefined,
  tax: null,
  discount: null,
  discount_type: null,
  delivery_cost: null,
  additional_costs: [],
  notes: '',
  images: [],
  // customer: undefined,
  customer: {
    customer_type: 'individual',
    name: '',
    phone_number: null,
    email: null,
    gender: 'male',
    avatar_text_color: colors[Math.floor(Math.random() * colors.length)],
    addresses: [
      {
        label: '',
        address_details: '',
        district: '',
        city: '',
        province: '',
        postcode: '',
        country: 'Indonesia',
      },
    ],
  },
  materials: [
    {
      material_id: undefined,
      dimension_material_length: null,
      dimension_material_width: null,
      quantity_material_pcs: null,
      quantity_material_sheet: null,
    },
  ],
  generate_invoice: false,
  is_new_customer: false,
};

const StyledJobOrderRootSummary = styled(Box)(({ theme }) => ({
  borderRadius: Number(theme.shape.borderRadius),
  padding: theme.spacing(3, 4),
  marginBottom: theme.spacing(3),
  backgroundColor: theme.palette.primary.light,
  border: `1px dashed ${theme.palette.primary.main}`,
}));
const StyledJobOrderSummaryItem = styled(Box)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'row',
  [theme.breakpoints.up('sm')]: {
    flexDirection: 'row',
  },
  alignItems: 'center',
  justifyContent: 'space-between',
  marginBottom: theme.spacing(1.2),
}));

export const JobOrderForm: FC<Props> = ({ id, sx, data, displayOnDrawer }: Props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const {
    createLoading,
    updateLoading,
    isFormHasChanges: currentIsFormHasChanges,
  } = useAppSelector((state) => jobOrder_rootSelector(state));
  const { isFetching, data: masterData } = useAppSelector((state) => state.masterData);
  const { data: listOfPaymentStatus } = useAppSelector((state) => state.paymentStatus);

  const [notesSuggestions, setNotesSuggestions] = useState<Array<string>>([
    'Diambil besok siang',
    'Diambil lusa siang',
  ]);

  // const [orderDate, setOrderDate] = useState<string>(moment(new Date()).format(DB_FORMAT_TIMESTAMPS))
  // const [dueDate, setDueDate] = useState<string>('')
  const [images, setImages] = useState([]);
  // const [selectedCustomer, setSelectedCustomer] = useState<ICustomer | null>(null);

  const [selectedProviceId, setSelectedProviceId] = useState<string>('');
  const [selectedCityId, setSelectedCityId] = useState<string>('');

  const [selectedCustomerTab, setSelectedCustomerTab] = useState<number>(1);

  const [isThereAnAdditionalPrice, setIsThereAnAdditionalPrice] = useState<boolean>(false);

  // State is error cannot create invoice cause customer data is required.
  const [isErrorCannotCreateInvoice, setIsErrorCannotCreateInvoice] = useState<boolean>(false);

  // Is edit mode
  const isEdit = useMemo<boolean>(() => {
    if (id) return true;
    else return false;
  }, [id]);

  const {
    control,
    handleSubmit,
    formState: { errors, isDirty },
    setValue,
    getValues,
    watch,
    reset,
  } = useForm({
    defaultValues: initialValues,
    resolver: yupResolver(jobOrderFormSchema),
  });

  /**
   * Flag is new customer
   */
  // const isNewCustomer = useMemo<boolean>(() => {
  //   if (watch('customer')?.name !== '') return true;
  //   else return false;
  // }, [watch('customer').name]);

  // list materials
  const listMaterials = useMemo<Array<IMaterial>>(() => {
    if (masterData.materials.length > 0) {
      if (getValues('category_id') === JobOrderCategoryIdConstants.PrintSticker) {
        return masterData.materials.filter((x) => x.material_type_id === MaterialTypeConstants.MaterialSticker);
      } else if (getValues('category_id') === JobOrderCategoryIdConstants.Banner) {
        return masterData.materials.filter((x) => x.material_type_id === MaterialTypeConstants.MaterialBanner);
      }
      return masterData.materials;
    } else return [];
  }, [getValues('category_id')]);

  const displayMessageExperimentalFeature = useMemo(() => {
    if (Number(getValues('category_id')) === JobOrderCategoryIdConstants.PrintSticker) {
      return false;
    } else if (Number(getValues('category_id')) === JobOrderCategoryIdConstants.Banner) {
      return false;
    } else {
      return true;
    }
  }, [getValues('category_id')]);

  // Array field materials.
  const {
    fields: fieldsMaterials,
    append: appendMaterial,
    remove: removeMaterial,
  } = useFieldArray({
    control,
    name: 'materials',
  });

  // Array field customer.addresses.
  const { fields: fieldsCustomerAddresses } = useFieldArray({
    control,
    name: 'customer.addresses',
  });

  // Array field additional_costs
  const { fields: fieldAdditionalCosts, append: appendAdditionalCost } = useFieldArray({
    control,
    name: 'additional_costs',
  });

  const isFormError = (): boolean =>
    Object.keys(errors).length !== 0; /* && Object.getPrototypeOf(errors) !== Object.prototype */

  /**
   * Handle form submit.
   *
   * @param {Inputs} values
   * @return {SubmitHandler<Inputs>} void
   */
  const handleFormSubmit: SubmitHandler<Inputs> = (values) => {
    console.log('⚡ form values', values);
    if (!isEdit) dispatch(jobOrder_createJobOrder(values, navigate));
    else dispatch(jobOrder_updateJobOrder(id, values, navigate));
  };

  const handleChangeImage = (imageList: ImageListType, addUpdateIndex: number[] | undefined): void => {
    // data for submit
    console.log('imageList', imageList, addUpdateIndex);
    setImages(imageList as never[]);

    // Set value hook form.
    setValue(
      'images',
      imageList.map((x) => x.dataURL),
    );
  };

  // const handleCheckNullableCustomer = (
  //   event: ChangeEvent<HTMLInputElement>
  // ): void => {
  //   setIsNullableCustomer(event.target.checked)
  // }

  /**
   * Handle reset form.
   *
   * @param {MouseEvent<HTMLButtonElement>} event
   */
  const handleResetForm = (event: MouseEvent<HTMLButtonElement>): void => {
    event.preventDefault();
    reset(initialValues);
  };

  /**
   * Handle cancel
   *
   * @param {MouseEvent<HTMLButtonElement>} event
   */
  const handleCancel = (event: MouseEvent<HTMLButtonElement>): void => {
    navigate(ROUTES.JOB_ORDER);
  };

  const provinces = useMemo<Array<string>>(() => {
    return provinsi().map((x) => x.nama);
  }, []);

  const cities = useMemo<Array<string>>(() => {
    return kabupaten().map((x) => x.nama);
  }, []);

  // const districts = useMemo<Array<string>>(() => {
  //   return kecamatan().map((x) => x.nama);
  // }, []);

  // Effect to set form has changes.
  useEffect(() => {
    if (
      isDirty
      /*  getValues('images').length ||
      getValues('title') !== '' ||
      getValues('quantity') ||
      getValues('price') ||
      watch('notes') !== '' ||
      watch('due_date') */
    ) {
      if (!currentIsFormHasChanges) {
        dispatch(jobOrder_setFormHasChanges(true));
      }
    } else {
      dispatch(jobOrder_setFormHasChanges(false));
    }
  }, [
    isDirty /* getValues('images'), watch('title'), watch('quantity'), watch('price'), watch('notes'), watch('due_date') */,
  ]);

  console.log('❌❌ errors', errors);

  /**
   * Watch customer_id
   */
  // useEffect(() => {
  //   if (watch('customer_id') !== null) setValue('customer', null);
  // }, [watch('customer_id')]);

  /**
   * Watch customer
   */
  useEffect(() => {
    if (watch('is_new_customer')) setValue('customer_id', null);
  }, [watch('is_new_customer')]);

  /**
   * Watch tab new customer
   */
  useEffect(() => {
    if (selectedCustomerTab === 2) setValue('is_new_customer', true);
  }, [selectedCustomerTab]);

  /** Error cannot create invoice */
  // const isErrorCannotCreateInvoice = (): void => {
  //   useMemo
  // }

  /**
   * Set values for edit
   */
  useEffect(() => {
    if (id && data) {
      setValue('title', data.title);
      setValue('notes', data.notes[0].body);
      setValue('order_date', moment(data.order_date).format(AppConfig.DatabaseFormatTimestamps));
      setValue('due_date', moment(data.due_date).format(AppConfig.DatabaseFormatTimestamps));
      setValue('category_id', data.category.id);
      setValue('payment_status_id', data.transaction.payment_status_id);
      setValue('payment_method_id', data.transaction.payment_method_id);
      setValue('down_payment', data.transaction.down_payment ?? null);
      setValue('bank_account_id', data.transaction.bank_account_id);
      setValue('customer_id', data.customer ? data.customer_id : null);
      setValue('order_quantity', data.order_quantity);
      setValue('order_price', data.order_price);
    }
  }, [data, id]);

  // Watch discount_type
  useEffect(() => {
    if (watch('discount_type') !== null) {
      setValue('discount', 0);
    } else {
      setValue('discount', null);
      setValue('discount_type', null);
    }
  }, [watch('discount_type')]);

  const onChangeGenerateInvoice = useCallback(() => {
    setValue('generate_invoice', !getValues('generate_invoice'));
    setIsErrorCannotCreateInvoice(false);
    setTimeout(() => {
      if (getValues('customer_id')) {
        setIsErrorCannotCreateInvoice(false);
      } else if (getValues('is_new_customer') && getValues('customer.name')) {
        setIsErrorCannotCreateInvoice(false);
      } else {
        setIsErrorCannotCreateInvoice(true);
        setValue('generate_invoice', false);
      }
    }, 300);
  }, [
    getValues('customer_id'),
    getValues('is_new_customer'),
    getValues('customer.name'),
    getValues('generate_invoice'),
  ]);

  /** Get final total */
  const getFinalTotal = useMemo<number>(() => {
    return 100;
  }, [getValues('order_price'), getValues('tax'), getValues('delivery_cost')]);

  /** Get sub total */
  const getSubTotal = useMemo<number>(() => {
    console.log('call me');
    if (getValues('order_price') && getValues('order_quantity')) {
      const _subTotal = Number(getValues('order_price')) * Number(getValues('order_quantity'));
      // if (getValues('additional_costs').length > 0) {
      //   console.log('call me now');

      //   const additionalCostsValue = getValues('additional_costs').reduce((accumulator, object) => {
      //     return accumulator + object.additional_cost_value;
      //   }, 0);

      //   // return Number(additionalCostsValue) + _subTotal;
      // } else {
      // }
      return _subTotal;
    }

    return 0;
  }, [getValues('order_price'), getValues('order_quantity'), getValues('additional_costs')]);

  /** Get additional costs value */
  const getAdditionalCostsValue = useMemo<number>(() => {
    console.log('getAdditionalCostsValue');
    const additionalCostsValue = getValues('additional_costs').reduce((accumulator, object) => {
      return accumulator + object.additional_cost_value;
    }, 0);

    return 0;
  }, [watch('additional_costs')]);

  return (
    <Box component="form" onSubmit={handleSubmit(handleFormSubmit)}>
      {/* Form inner form */}
      <Box>
        {/* Begin upload image */}

        {displayOnDrawer && (
          <Box
            sx={{
              px: { xs: 4, md: 14 },
              pt: 5,
              pb: 2,
            }}
          >
            <BaseSectionTitle title={'Buat Jo Baru'} />
          </Box>
        )}

        {/* Begin upload image section */}
        <JobOrderFormSection>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <BaseUploadImages
                multiple
                maxNumber={6}
                acceptType={['jpg', 'png']}
                images={images}
                onChange={handleChangeImage}
                // isError={true}
              />
            </Grid>
          </Grid>
        </JobOrderFormSection>
        {/* End of upload image section */}

        {/* Begin title and date */}
        <JobOrderFormSection title="Spesifikasi Job Order">
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <ControlledTextField
                control={control}
                errors={errors}
                fullWidth
                name="title"
                label="Nama Job Order"
                helperText="Judul Job Order akan memudahkan saat pencarian data."
              />
            </Grid>

            <Grid item xs={12}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={4}>
                  <ControlledSelect
                    fullWidth
                    control={control}
                    name="category_id"
                    errors={errors}
                    helperText="Pilih Kategori Jo"
                    label="Kategori Jo"
                    data={masterData.job_order_categories.map((x) => ({ value: x.id, label: x.name }))}
                  />
                </Grid>
                <Grid item xs={12} md={4}>
                  <ControlledMobileDateTimePicker
                    fullWidth
                    control={control}
                    errors={errors}
                    name="order_date"
                    label="Tanggal Order"
                    helperText="Kapan Order Dibuat"
                    inputFormat={'dd/MM/yyyy HH:mm'}
                    closeOnSelect={true}
                    componentsProps={{
                      actionBar: {
                        actions: ['today'],
                      },
                    }}
                  />
                </Grid>
                <Grid item xs={12} md={4}>
                  <ControlledMobileDateTimePicker
                    fullWidth
                    control={control}
                    errors={errors}
                    name="due_date"
                    label="Tanggal Deadline"
                    helperText="Kapan Jo ini harus selesai produksi"
                    inputFormat={'dd/MM/yyyy HH:mm'}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sx={{ pt: '0.6rem !important' }}>
              {watch('category_id') && (
                <>
                  {displayMessageExperimentalFeature && (
                    <Alert severity="error" variant="filled">
                      <AlertTitle>Aplikasi ini masih dalam pengembangan.</AlertTitle>
                      Saat ini hanya tersedia Job Order untuk kategori <strong>Print Stiker</strong> dan{' '}
                      <strong>Spanduk/Banner</strong>
                    </Alert>
                  )}
                </>
              )}
            </Grid>
          </Grid>
        </JobOrderFormSection>
        {/* Enb of title and date */}

        {/* Begin customer section */}
        <JobOrderFormSection sx={{ backgroundColor: '#f4faff' }} title="Pelanggan">
          <Grid container spacing={2}>
            <Grid item xs={12} sx={{ pt: '0.4rem !important' }}>
              {/* Display seleceted customer info */}
              {/* {selectedCustomer && <SelectedCustomer data={selectedCustomer} />} */}
            </Grid>

            {/* Customer Tab */}
            <Grid item xs={12}>
              <BaseTabs
                value={selectedCustomerTab}
                onChange={(e, value) => setSelectedCustomerTab(value)}
                buttonVariant="contained"
                data={[
                  { label: 'Pilih Pelanggan yang sudah ada', value: 1 },
                  { label: 'Buat data pelanggan baru', value: 2 },
                ]}
              />
            </Grid>

            <Grid item xs={12} sx={{ paddingTop: '0 !important' }}>
              <Collapse in={selectedCustomerTab === 2}>
                {/* Box add new customer */}
                <Box
                  sx={{
                    px: { xs: 2, md: 4.4 },
                    py: { xs: 2, md: 4 },
                    borderRadius: 1,
                    // backgroundColor: '#e9f1fd',
                    backgroundColor: 'background.paper',
                    border: (theme) => `2px solid ${theme.palette.primary.main}`,
                  }}
                >
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <Typography sx={{ textTransform: 'uppercase', fontSize: 12 }} component="h6" variant="subtitle2">
                        Buat data customer baru :
                      </Typography>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <ControlledSelect
                        control={control}
                        errors={errors}
                        fullWidth
                        name="customer.customer_type"
                        label="Kategori Pelanggan"
                        helperText="Kategori Pelanggan"
                        data={dropdownCustomerType}
                      />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <Controller
                        name="customer.name"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth
                            margin="dense"
                            size="small"
                            label={watch('customer.customer_type') === 'company' ? 'Nama Perusahaan' : 'Nama Customer'}
                            error={Boolean(errors?.customer?.name?.message)}
                            helperText={errors?.customer?.name?.message}
                          />
                        )}
                      />
                      {/* <ControlledTextField
                        control={control}
                        errors={errors}
                        fullWidth
                        name="customer.name"
                        label={watch('customer.customer_type') === 'company' ? 'Nama Perusahaan' : 'Nama Customer'}
                        helperText={watch('customer.customer_type') === 'company' ? 'Nama Perusahaan' : 'Nama Customer'}
                      />
                      {Boolean(errors?.customer?.name) && (
                        <FormHelperText sx={{ color: 'red' }}>{errors?.customer?.name?.message}</FormHelperText>
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={6} md={4}>
                      <Controller
                        name="customer.phone_number"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth
                            margin="dense"
                            size="small"
                            error={Boolean(errors?.customer?.phone_number?.message)}
                            helperText={errors?.customer?.phone_number?.message ?? 'No. HP/WA Customer'}
                            InputProps={{
                              startAdornment: <InputAdornment position="start">+62</InputAdornment>,
                            }}
                          />
                        )}
                      />
                    </Grid>
                    <Grid item xs={12} sm={6} md={4}>
                      <ControlledTextField
                        control={control}
                        errors={errors}
                        fullWidth
                        name="customer.email"
                        label="Email"
                        helperText="Email Customer"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6} md={4}>
                      <ControlledSelect
                        control={control}
                        errors={errors}
                        fullWidth
                        name="customer.gender"
                        label="Jenis Kelamin"
                        helperText="Jenis kelamin"
                        data={dropdownGenders}
                        disabled={watch('customer.customer_type') === 'company'}
                      />
                    </Grid>

                    <Grid item xs={12}>
                      {fieldsCustomerAddresses.map((x, index) => (
                        <Grid key={String(index)} container spacing={2}>
                          {/* <Grid item xs={12} sm={6} md={4}>
                              <ControlledTextField
                                control={control}
                                errors={errors}
                                fullWidth
                                name={`customer.addresses.${index}.label`}
                                label="Label"
                                helperText="Label alamat"
                              />
                            </Grid> */}

                          <Grid item xs={12} sm={6} md={4}>
                            <ControlledAutocomplete
                              control={control}
                              errors={errors}
                              name={`customer.addresses.${index}.province`}
                              label="Provinsi"
                              helperText="Provinsi"
                              fullWidth
                              data={provinces}
                            />
                          </Grid>
                          <Grid item xs={12} sm={6} md={4}>
                            <ControlledAutocomplete
                              control={control}
                              errors={errors}
                              name={`customer.addresses.${index}.city`}
                              label="Kabupaten/Kota"
                              helperText="Kabupaten/Kota"
                              data={cities}
                              // disabled={!selectedProviceId}
                            />
                          </Grid>
                          <Grid item xs={12} sm={6} md={4}>
                            {/* <ControlledAutocomplete
                                control={control}
                                errors={errors}
                                name={`customer.addresses.${index}.district`}
                                label="Kecamatan"
                                helperText="Kecamatan"
                                data={districts}
                                // disabled={!selectedCityId}
                              /> */}
                            <ControlledTextField
                              control={control}
                              errors={errors}
                              fullWidth
                              name={`customer.addresses.${index}.district`}
                              label="Kecamatan"
                              helperText="Kecamatan"
                            />
                          </Grid>
                          <Grid item xs={12} sm={6} md={6}>
                            <ControlledTextField
                              control={control}
                              errors={errors}
                              fullWidth
                              name={`customer.addresses.${index}.address_details`}
                              multiline
                              minRows={2}
                              maxRows={3}
                              label="Detail alamat"
                              helperText="Nama gedung, jalan atau komplek"
                            />
                          </Grid>
                          <Grid item xs={12} sm={6} md={3}>
                            <ControlledTextField
                              control={control}
                              errors={errors}
                              fullWidth
                              name={`customer.addresses.${index}.postcode`}
                              label="Kode Pos"
                              helperText="Kode Pos"
                            />
                          </Grid>
                          <Grid item xs={12} sm={6} md={3}>
                            <ControlledTextField
                              control={control}
                              errors={errors}
                              fullWidth
                              name={`customer.addresses.${index}.country`}
                              label="Negara"
                              helperText="Negara"
                              disabled={true}
                            />
                          </Grid>
                        </Grid>
                      ))}
                    </Grid>

                    <Grid item xs={12}>
                      <Alert severity="warning">
                        Perhatian: Jika di isi data pelanggan baru, Jo ini akan atas nama pelanggan baru.
                      </Alert>
                    </Grid>
                  </Grid>
                </Box>
              </Collapse>
              <BaseTabPanel index={selectedCustomerTab} value={1}>
                <JobOrderFormCustomerList
                  data={masterData.customers}
                  onSelect={(selectedCustomerId: UUID | null) => setValue('customer_id', selectedCustomerId)}
                  selectedId={watch('customer_id')}
                />
              </BaseTabPanel>
            </Grid>

            {/* {!isNullableCustomer && (
              <Grid item xs={12}>
                <Box sx={{ textAlign: 'center' }}>
                  {isNewCustomer || isNullableCustomer ? (
                    <Button
                      size="medium"
                      variant="contained"
                      disableElevation
                      color="primary"
                      onClick={() => setIsNewCustomer(false)}
                      startIcon={<DescriptionIcon />}
                    >
                      Pilih pelanggan yang sudah ada
                    </Button>
                  ) : (
                    <Button
                      size="medium"
                      variant="outlined"
                      color="primary"
                      onClick={() => setIsNewCustomer(true)}
                      startIcon={<AddIcon />}
                    >
                      Buat data Pelanggan baru
                    </Button>
                  )}
                </Box>
              </Grid>
            )} */}
          </Grid>
        </JobOrderFormSection>
        {/* End customer section */}

        {/* Begin material spesification */}
        {/* <JobOrderFormSection title="Bahan Baku Produksi">
         
        </JobOrderFormSection> */}
        {/* End of material spesification */}

        {/* Begin invoice info */}
        <JobOrderFormSection title="Bahan Baku & Invoice">
          <Grid container spacing={3}>
            <Grid item xs={12}>
              {fieldsMaterials.map(
                (item, index) =>
                  (getValues('category_id') === JobOrderCategoryIdConstants.PrintSticker ||
                    getValues('category_id') === JobOrderCategoryIdConstants.Banner) && (
                    <Box key={item.id} sx={{ display: 'flex', alignItems: 'flex-start' }}>
                      <Box sx={{ display: 'flex', flex: 1, mr: 1 }}>
                        <Grid container spacing={3} sx={{ mb: fieldsMaterials.length > 0 ? 3 : 0 }}>
                          <Grid item xs={12} sm={6} md={4}>
                            <ControlledSelect
                              control={control}
                              fullWidth
                              name={`materials.${index}.material_id`}
                              label="Bahan Baku Produksi"
                              size="small"
                              errors={errors}
                              data={listMaterials.map((x) => ({ label: x.name, value: x.id }))}
                              helperText="Pilih bahan baku produksi"
                              margin="dense"
                            />
                          </Grid>
                          <Grid item xs={12} sm={6} md={4}>
                            <Controller
                              name={`materials.${index}.dimension_material_length`}
                              control={control}
                              render={({ field }) => (
                                <TextField
                                  {...field}
                                  fullWidth
                                  disabled={isFetching}
                                  margin="dense"
                                  size="small"
                                  label="Panjang Bahan (Y)"
                                  error={
                                    errors?.materials?.length
                                      ? Boolean(errors.materials[index]?.dimension_material_length?.message)
                                      : false
                                  }
                                  helperText={
                                    errors?.materials?.length
                                      ? errors.materials[index]?.dimension_material_length?.message
                                      : '( P ) Dimensi panjang bahan vertical (Y)'
                                  }
                                  InputProps={{
                                    endAdornment: <InputAdornment position="start">cm</InputAdornment>,
                                  }}
                                />
                              )}
                            />
                            {/* <ControlledTextField
                  control={control}
                  fullWidth
                  name={`materials.${index}.dimension_material_length`}
                  label="Dimensi Panjang"
                  size="small"
                  errors={errors}
                /> */}
                          </Grid>
                          <Grid item xs={12} sm={6} md={4}>
                            <Controller
                              name={`materials.${index}.dimension_material_width`}
                              control={control}
                              render={({ field }) => (
                                <TextField
                                  {...field}
                                  fullWidth
                                  disabled={isFetching}
                                  margin="dense"
                                  size="small"
                                  label="Lebar Bahan (X)"
                                  error={
                                    errors?.materials?.length
                                      ? Boolean(errors.materials[index]?.dimension_material_width?.message)
                                      : false
                                  }
                                  helperText={
                                    errors?.materials?.length
                                      ? errors.materials[index]?.dimension_material_width?.message
                                      : '( L ) Dimensi lebar bahan horizontal (X)'
                                  }
                                  InputProps={{
                                    endAdornment: <InputAdornment position="start">cm</InputAdornment>,
                                  }}
                                />
                              )}
                            />
                          </Grid>

                          <Grid item xs={12}>
                            {/* <FormHelperText error={materials?[1]?.?material_id}></FormHelperText> */}
                          </Grid>

                          {watch('category_id') === JobOrderCategoryIdConstants.LaserCutting && (
                            <>
                              <Grid item xs={12} md={6}>
                                <ControlledTextField
                                  control={control}
                                  fullWidth
                                  name={`materials.${index}.quantity_material_pcs`}
                                  label="Jumlah (Pcs)"
                                  size="small"
                                  errors={errors}
                                  InputProps={{
                                    endAdornment: <InputAdornment position="start">pcs</InputAdornment>,
                                  }}
                                />
                              </Grid>
                              <Grid item xs={12} md={6}>
                                <ControlledTextField
                                  control={control}
                                  fullWidth
                                  name={`materials.${index}.quantity_material_sheet`}
                                  label="Jumlah Kertas (Lembar)"
                                  size="small"
                                  errors={errors}
                                  InputProps={{
                                    endAdornment: <InputAdornment position="start">lembar</InputAdornment>,
                                  }}
                                />
                              </Grid>
                            </>
                          )}
                        </Grid>
                      </Box>
                      <Box sx={{ mt: 1.7 }}>
                        <IconButton
                          size="small"
                          color="error"
                          onClick={() => {
                            if (index === 0) return;
                            else removeMaterial(index);
                          }}
                        >
                          <CloseIcon />
                        </IconButton>
                      </Box>
                    </Box>
                  ),
              )}

              {/* {getValues('category_id') === JobOrderCategoryIdConstants.PrintSticker && (
                <Stack justifyContent="center" direction="row">
                  <BaseButton onClick={() => appendMaterial({})}>Tambah Bahan Finishing</BaseButton>
                </Stack>
              )} */}
            </Grid>
            <Grid item xs={12} sm={6}>
              <ControlledTextField
                control={control}
                fullWidth
                name="order_quantity"
                label="Quantity"
                size="small"
                errors={errors}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <ControlledTextField
                control={control}
                fullWidth
                name="order_price"
                label="Harga Satuan"
                size="small"
                errors={errors}
                InputProps={{
                  startAdornment: <InputAdornment position="start">Rp</InputAdornment>,
                }}
                helperText="Harga Satuan"
              />
            </Grid>
            {/* Render total order */}
            <Grid item xs={12} sx={{ mt: watch('order_price') > 0 && watch('order_quantity') > 1 ? 0 : -3 }}>
              <Collapse in={watch('order_price') > 0 && watch('order_quantity') > 1}>
                {getValues('order_price') > 0 && getValues('order_quantity') > 1 && (
                  <Box
                    sx={{
                      py: 3,
                      px: 2,
                      mt: -2,
                      mx: 'auto',
                      backgroundColor: 'primary.light',
                      color: 'primary.main',
                      minWidth: 120,
                      maxWidth: 500,
                      borderRadius: 1,
                      border: (theme) => `1px dashed ${theme.palette.primary.main}`,
                    }}
                  >
                    <Box
                      sx={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        mb: 0.3,
                        '& .MuiTypography-root': {
                          fontSize: '1.05rem',
                        },
                      }}
                    >
                      <Typography>Total order : </Typography>
                      <Typography sx={{ fontWeight: 700, ml: 1 }}>
                        Rp {formatRupiah(getValues('order_price') * getValues('order_quantity'))}
                      </Typography>
                    </Box>
                    <Box sx={{ textAlign: 'center' }}>
                      <Typography variant="subtitle2" color="text.secondary">
                        Total Order belum dikurangi Diskon atau Biaya lain (jika ada)
                      </Typography>
                    </Box>
                  </Box>
                )}
              </Collapse>
            </Grid>
            <Grid item xs={12}>
              <Box>
                <Typography sx={{ fontSize: 14 }}>Diskon</Typography>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <ControlledInputDiscountType control={control} errors={errors} />
            </Grid>
            <Grid item xs={12} sx={{ marginTop: watch('discount_type') === 'percentage' ? 0 : -2 }}>
              <Collapse in={watch('discount_type') === 'percentage'}>
                <Box
                  sx={{
                    width: { xs: '94%', sm: 500 },
                    mx: 'auto',
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',
                  }}
                >
                  <Box sx={{ textAlign: 'center', display: 'flex', alignItems: 'flex-start' }}>
                    <Typography component="span" sx={{ fontSize: 40, fontWeight: 700 }}>
                      {watch('discount')}
                    </Typography>
                    <Typography component="span" color="text.secondary" sx={{ mt: '10px', ml: 0.4 }}>
                      %
                    </Typography>
                  </Box>
                  <ControlledSlider
                    control={control}
                    errors={errors}
                    name="discount"
                    defaultValue={0}
                    valueLabelDisplay="off"
                    min={0}
                    max={100}
                  />
                  <Box sx={{ textAlign: 'center' }}>
                    <Typography variant="subtitle1" color="text.secondary">
                      Geser Slider untuk memasukan nilai % diskon untuk Job Order ini
                    </Typography>
                  </Box>
                </Box>
              </Collapse>
            </Grid>
            <Grid item xs={12} sx={{ marginTop: watch('discount_type') === 'fixed_price' ? 0 : -2 }}>
              <Collapse in={watch('discount_type') === 'fixed_price'}>
                <Box
                  sx={{
                    width: { xs: '100%', sm: 200 },
                    mx: 'auto',
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',
                  }}
                >
                  <ControlledTextField
                    control={control}
                    fullWidth
                    name="discount"
                    label="Diskon"
                    size="medium"
                    errors={errors}
                    InputProps={{
                      startAdornment: <InputAdornment position="start">Rp</InputAdornment>,
                    }}
                  />
                </Box>
                <Box sx={{ textAlign: 'center' }}>
                  <Typography variant="subtitle1" color="text.secondary">
                    Terapkan diskon untuk Job Order ini. Invoice akan dikurangi dengan jumlah diskon yang ditentukan
                  </Typography>
                </Box>
              </Collapse>
            </Grid>
            {/* <Grid item xs={12}>
              <Box>
                <Typography sx={{ fontSize: 14 }}>Pajak dan Biaya Pengiriman</Typography>
              </Box>
            </Grid>

            <Grid item xs={12} sm={6}>
              <ControlledTextField
                control={control}
                fullWidth
                name="tax"
                label="Pajak"
                size="small"
                errors={errors}
                InputProps={{
                  endAdornment: <InputAdornment position="start">%</InputAdornment>,
                }}
              />
            </Grid>

            <Grid item xs={12} sm={6}>
              <ControlledTextField
                control={control}
                fullWidth
                name="delivery_cost"
                label="Biaya Pengiriman"
                size="small"
                errors={errors}
                InputProps={{
                  endAdornment: <InputAdornment position="start">%</InputAdornment>,
                }}
              />
            </Grid> */}
            {JobOrderConfig.EnableAdditionalCosts && (
              <Grid item xs={12}>
                <Box sx={{ textAlign: 'center', mt: 3, mb: 2 }}>
                  {!isThereAnAdditionalPrice && (
                    <>
                      <Typography variant="h6" sx={{ color: 'text.secondary', mb: 2 }}>
                        Apakah ada biaya tambahan ?
                      </Typography>
                      <BaseButton
                        onClick={(e) => {
                          e.preventDefault();
                          setIsThereAnAdditionalPrice(true);
                          appendAdditionalCost({
                            additional_cost_name: '',
                            additional_cost_value: undefined,
                          });
                        }}
                        startIcon={<AddIcon />}
                        size="medium"
                        disableHoverEffect
                      >
                        Tambah kolom biaya tambahan
                      </BaseButton>
                    </>
                  )}
                </Box>
                <Collapse in={isThereAnAdditionalPrice}>
                  <Box
                    sx={{
                      border: '1px dashed #9bfabb',
                      borderRadius: 1,
                      px: 4,
                      py: 3,
                      backgroundColor: '#f3fff7',
                      position: 'relative',
                    }}
                  >
                    {/* Close button */}
                    <IconButton
                      size="small"
                      onClick={() => setIsThereAnAdditionalPrice(false)}
                      sx={{
                        position: 'absolute',
                        top: (theme) => theme.spacing(1.2),
                        right: (theme) => theme.spacing(2),
                      }}
                    >
                      <CloseIcon sx={{ fontSize: 22 }} />
                    </IconButton>

                    <Grid item xs={12}>
                      <Typography sx={{ color: 'text.secondary', mb: 2 }}>Biaya tambahan</Typography>
                    </Grid>

                    {fieldAdditionalCosts.map((item, index) => (
                      <Grid key={String(index)} container spacing={3}>
                        <Grid item xs={12} sm={6}>
                          <ControlledTextField
                            control={control}
                            fullWidth
                            name={`additional_costs.${index}.additional_cost_name`}
                            label="Nama biaya tambahan"
                            size="small"
                            errors={errors}
                            helperText="Misal : biaya potong"
                          />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                          <ControlledTextField
                            control={control}
                            fullWidth
                            name={`additional_costs.${index}.additional_cost_value`}
                            label="Jumlah biaya tambahan"
                            size="small"
                            errors={errors}
                            InputProps={{
                              startAdornment: <InputAdornment position="start">Rp</InputAdornment>,
                            }}
                            helperText="Nilai jumlah biaya tambahan (Rp)"
                          />
                        </Grid>
                      </Grid>
                    ))}

                    <Box>
                      <BaseButton
                        onClick={(e) => {
                          e.preventDefault();
                          appendAdditionalCost({
                            additional_cost_name: '',
                            additional_cost_value: null,
                          });
                        }}
                      >
                        Tambah Row
                      </BaseButton>
                    </Box>
                  </Box>
                </Collapse>
              </Grid>
            )}
            <Grid item xs={12} sx={{ mt: 2 }}>
              <Alert severity="warning">
                <AlertTitle>Perhatian!</AlertTitle>
                Harapa dicatat — <strong>Sistem akan otomatis mengurangi stock bahan dari database </strong> sesuai
                dengan jumlah yang di input disini. Pastikan sudah di isi dengan benar
              </Alert>
            </Grid>
            <Grid item xs={12}>
              <ControlledRadioGroup
                row
                control={control}
                errors={errors}
                name="payment_status_id"
                label="Status Pembayaran"
                data={masterData.payment_statuses.map((x) => ({ value: x.id, label: x.name }))}
              />
            </Grid>
            <Grid
              item
              xs={12}
              sx={{
                marginLeft: { xs: 0, md: '160px' },
                marginTop: '-10px',
                display: Number(watch('payment_status_id')) === 2 ? 'block' : 'none',
              }}
            >
              <Collapse in={Number(watch('payment_status_id')) === 2}>
                {Number(watch('payment_status_id')) === 2 && (
                  <Grid item xs={12}>
                    {/* <ControlledTextField control={control} errors={errros} name="down_payment" label="Masukan jumlah dp" /> */}
                    <Controller
                      name="down_payment"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          {...field}
                          fullWidth={false}
                          margin="none"
                          size="small"
                          label="Masukan jumlah dp"
                          sx={{ width: 300 }}
                          InputProps={{
                            startAdornment: <InputAdornment position="start">Rp.</InputAdornment>,
                          }}
                          error={Boolean(errors.down_payment?.message)}
                          helperText={errors.down_payment?.message}
                        />
                      )}
                    />
                  </Grid>
                )}
              </Collapse>
            </Grid>
            <Grid item xs={12}>
              <ControlledRadioGroup
                row
                control={control}
                errors={errors}
                name="payment_method_id"
                label="Metode Pembayaran"
                data={masterData.payment_methods.map((x) => ({ value: x.id, label: x.name }))}
              />
            </Grid>
            <Grid item xs={12} sx={{ my: 2 }}>
              <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                <Controller
                  name="generate_invoice"
                  control={control}
                  render={({ field: { value: isCheckedGenerateInvoice } }) => {
                    return (
                      <>
                        <FormControlLabel
                          onClick={onChangeGenerateInvoice}
                          sx={{
                            px: 4,
                            py: 3,
                            margin: '0 !important',
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                            borderRadius: 1,
                            width: { xs: '100%', sm: 420 },
                            backgroundColor: isCheckedGenerateInvoice ? 'primary.light' : 'background.paper',
                            '&:hover': {
                              backgroundColor: 'primary.light',
                              border: (theme) => `1px dashed ${theme.palette.primary.main}`,
                            },
                            border: (theme) =>
                              isCheckedGenerateInvoice
                                ? `1px dashed ${theme.palette.primary.main}`
                                : `1px dashed ${alpha('#000', 0.2)}`,
                            '& .MuiFormControlLabel-label': {
                              display: 'none',
                            },

                            ...(isErrorCannotCreateInvoice && {
                              border: `1px dashed #ff000e`,
                              backgroundColor: `${alpha('#ff000e', 0.05)}`,
                              '&:hover': {
                                border: `1px dashed #ff000e`,
                                backgroundColor: `${alpha('#ff000e', 0.05)}`,
                              },
                            }),
                          }}
                          control={
                            <Box sx={{ width: '100%' }}>
                              <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                {isCheckedGenerateInvoice ? (
                                  <CheckBoxIcon sx={{ color: 'primary.main', fontSize: 20 }} />
                                ) : (
                                  <CheckBoxOutlineBlankIcon sx={{ color: 'text.disabled', fontSize: 20 }} />
                                )}
                                <Typography
                                  sx={{
                                    ml: 2,
                                    fontSize: 16,
                                    fontWeight: 600,
                                    color: isCheckedGenerateInvoice ? 'primary.main' : 'text.disabled',
                                  }}
                                >
                                  Buatkan Invoice
                                </Typography>
                              </Box>
                              <Collapse in={watch('generate_invoice')}>
                                <Typography variant="subtitle1" sx={{ mt: 1.5, textAlign: 'center' }}>
                                  Invoice akan di generate otomatis setelah Job Order dibuat
                                </Typography>
                              </Collapse>
                            </Box>
                          }
                          label="Generate Invoice"
                        />
                        <FormHelperText error={isErrorCannotCreateInvoice} sx={{ mt: 1.5 }}>
                          {isErrorCannotCreateInvoice
                            ? 'Tidak dapat membuat invoice. Silahkan pilih pelanggan atau mengisi mengisi data pelanggan baru dibagian atas'
                            : 'Aktifkan opsi ini apabila perlu dibuatkan invoice'}
                        </FormHelperText>
                        {isErrorCannotCreateInvoice && (
                          <FormHelperText>
                            <strong>NOTES:</strong> Aktifkan kembali opsi ini setelah memilih data pelanggan
                          </FormHelperText>
                        )}
                      </>
                    );
                  }}
                />
                {/* <ControlledCheckbox
                  control={control}
                  name="generate_invoice"
                  label="Otomatis Buatkan Invoice"
                  errors={errors}
                  helperText="Check opsi ini apabila perlu dibuatkan otomatis."
                /> */}
              </Box>
            </Grid>
            {/* <Grid item xs={12}>
                    <ControlledRadioGroup
                      control={control}
                      errors={errors}
                      name="bank_account_id"
                      label="Bank Account"
                      data={masterData.bank_accounts.map((x) => ({ value: x.id, label: x.name }))}
                    />
                  </Grid> */}
            {/* {jobOrderHasPay ? (
              <Grid item xs={12} md={12}>
                <Paper
                  elevation={0}
                  sx={{
                    p: 1,
                    display: 'flex',
                    border: (theme) => `1px solid ${theme.palette.divider}`,
                    flexWrap: 'wrap',
                  }}
                >
                  <StyledToggleButtonGroup
                    exclusive
                    value={getValues('payment_method_id')}
                    onChange={handleChangePaymentMethod}
                  >
                    {sources.payment_methods.length > 0 &&
                      sources.payment_methods.map((paymentMethod) => (
                        <StyledToggleButton
                          key={paymentMethod.id}
                          value={paymentMethod.id}
                          // disabled={paymentMethod.status === 'inactive'}
                        >
                          {paymentMethod.image ? (
                            <img src={paymentMethod.image} alt={paymentMethod.name} />
                          ) : (
                            <Typography>{paymentMethod.name}</Typography>
                          )}
                        </StyledToggleButton>
                      ))}
                  </StyledToggleButtonGroup>
                </Paper>
              </Grid>
            ) : null} */}
          </Grid>
        </JobOrderFormSection>
        {/*End of invoice info */}

        {/* Begin others section */}
        <JobOrderFormSection title="Lainnya" sx={{ mb: 5 }}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Box sx={{ position: 'relative' }}>
                <Controller
                  name="notes"
                  control={control}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      multiline
                      fullWidth
                      minRows={6}
                      maxRows={8}
                      margin="none"
                      size="small"
                      label="Catatan Job Order"
                      error={Boolean(errors.notes?.message)}
                      helperText={
                        errors.notes?.message ? errors.notes?.message : 'Tulis catatan Job Order untuk tim produksi'
                      }
                    />
                  )}
                />

                {/* Notes, suggestions */}
                <Collapse in={watch('notes') === null || watch('notes') === ''}>
                  <Box sx={{ position: 'absolute', bottom: 40, left: 20 }}>
                    <Typography variant="subtitle1" color="text.disabled">
                      Suggestions :
                    </Typography>
                    {notesSuggestions.map((x, index) => (
                      <Box
                        component="button"
                        key={String(index)}
                        onClick={(e) => {
                          e.preventDefault();
                          setValue('notes', x);
                        }}
                        sx={{
                          backgroundColor: grey[200],
                          color: 'text.secondary',
                          px: 1.8,
                          py: 0.6,
                          mr: 1,
                          cursor: 'pointer',
                          borderRadius: 4,
                          border: 'none',
                          outline: 'none',
                        }}
                      >
                        <Typography variant="subtitle1" sx={{ fontStyle: 'italic' }}>
                          {x}
                        </Typography>
                      </Box>
                    ))}
                  </Box>
                </Collapse>
              </Box>
            </Grid>
          </Grid>
        </JobOrderFormSection>
        {/* End of Lainnya section */}
      </Box>
      {/* End of form iner */}

      {/* Begin submit actions */}
      <Box
        sx={{
          ...(displayOnDrawer && {
            position: 'fixed',
            bottom: 0,
            right: 0,
            backgroundColor: 'background.paper',
            width: `calc(100% - ${AppConfig.SidebarDrawerWidth}px)`,
            zIndex: 3,
            boxShadow: '0px -3px 27px 0px rgba(0,0,0,0.05)',
          }),
        }}
      >
        <JobOrderFormSection disableLastDivider sx={{ ...(displayOnDrawer && { py: 3 }) }}>
          <Box sx={{ width: { xs: '100%', md: 640 }, mx: 'auto' }}>
            <StyledJobOrderRootSummary>
              <Box sx={{ mb: 3 }}>
                <Typography sx={{ fontSize: 15, mb: 2, fontWeight: 600 }}>Job Order Sumary</Typography>

                {/* Total order */}
                <StyledJobOrderSummaryItem>
                  <Typography color="text.secondary">Total Order :</Typography>
                  <Typography color="primary.main" sx={{ fontWeight: 'bold' }}>
                    {getValues('order_price') && getValues('order_quantity')
                      ? `Rp ${formatRupiah(Number(getValues('order_price')) * Number(getValues('order_quantity')))}`
                      : '-'}
                  </Typography>
                </StyledJobOrderSummaryItem>

                {/* Additional costs */}
                {getValues('additional_costs').length > 0 && (
                  <>
                    <Typography variant="subtitle1" sx={{ color: 'text.disabled' }}>
                      Biaya Tambahan
                    </Typography>
                    <Box sx={{ mb: 1.8 }}>
                      {watch('additional_costs').map((x, index) => (
                        <StyledJobOrderSummaryItem key={String(index)}>
                          <Typography color="text.secondary">
                            {getValues(`additional_costs.${index}.additional_cost_name`) !== ''
                              ? `+ ${getValues(`additional_costs.${index}.additional_cost_name`)} :`
                              : '-'}
                          </Typography>
                          <Typography color="primary.main" sx={{ fontWeight: 'bold' }}>
                            Rp{' '}
                            {getValues(`additional_costs.${index}.additional_cost_value`)
                              ? formatRupiah(Number(getValues(`additional_costs.${index}.additional_cost_value`)))
                              : '-'}
                          </Typography>
                        </StyledJobOrderSummaryItem>
                      ))}
                    </Box>
                  </>
                )}

                {/* Sub total (Total Order + Additional cost) */}
                {JobOrderConfig.EnableAdditionalCosts && getValues('additional_costs').length > 0 && (
                  <StyledJobOrderSummaryItem>
                    <Typography color="text.secondary">Sub Total :</Typography>
                    <Typography color="primary.main" sx={{ fontWeight: 'bold' }}>
                      {getSubTotal ? `Rp ${formatRupiah(getSubTotal)}` : '-'}
                    </Typography>
                  </StyledJobOrderSummaryItem>
                )}

                {/* Discount */}
                <StyledJobOrderSummaryItem>
                  <Typography color="text.secondary">Diskon :</Typography>
                  <Typography color="primary.main" sx={{ fontWeight: 'bold' }}>
                    {getValues('discount') ? (
                      <>
                        {getValues('discount_type') === 'percentage'
                          ? getValues('discount') + '%'
                          : '- Rp ' + formatRupiah(getValues('discount'))}
                      </>
                    ) : (
                      '-'
                    )}
                  </Typography>
                </StyledJobOrderSummaryItem>

                {/* tax */}
                <StyledJobOrderSummaryItem>
                  <Typography color="text.secondary">Pajak :</Typography>
                  <Typography color="primary.main" sx={{ fontWeight: 'bold' }}>
                    Rp {getValues('tax') ? formatRupiah(getValues('tax')) : '-'}
                  </Typography>
                </StyledJobOrderSummaryItem>

                {getValues('delivery_cost') && (
                  <StyledJobOrderSummaryItem>
                    <Typography color="text.secondary">Pajak :</Typography>
                    <Typography color="primary.main" sx={{ fontWeight: 'bold' }}>
                      Rp {formatRupiah(getValues('delivery_cost'))}
                    </Typography>
                  </StyledJobOrderSummaryItem>
                )}
                {getValues('down_payment') && (
                  <StyledJobOrderSummaryItem>
                    <Typography color="text.secondary">DP :</Typography>
                    <Typography color="primary.main" sx={{ fontWeight: 'bold' }}>
                      Rp {formatRupiah(getValues('delivery_cost'))}
                    </Typography>
                  </StyledJobOrderSummaryItem>
                )}
                {getValues('down_payment') && (
                  <StyledJobOrderSummaryItem>
                    <Typography color="text.secondary">Sisa :</Typography>
                    <Typography color="primary.main" sx={{ fontWeight: 'bold' }}>
                      Rp. ?
                    </Typography>
                  </StyledJobOrderSummaryItem>
                )}
                {getValues('down_payment') && (
                  <StyledJobOrderSummaryItem>
                    <Typography color="text.secondary">Final Total :</Typography>
                    <Typography color="primary.main" sx={{ fontWeight: 'bold' }}>
                      {getFinalTotal}
                    </Typography>
                  </StyledJobOrderSummaryItem>
                )}
              </Box>

              <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2} justifyContent="space-between">
                <BaseButton
                  onClick={handleCancel}
                  disabled={isFetching || createLoading || updateLoading}
                  variant="outlined"
                  size="large"
                  color="error"
                  startIcon={<CloseIcon />}
                >
                  Batal
                </BaseButton>

                <BaseButton
                  endIcon={<ArrowForwardIcon />}
                  disabled={isFetching || createLoading || updateLoading}
                  type="submit"
                  variant="contained"
                  size="large"
                >
                  Buat Job Order
                </BaseButton>
              </Stack>
            </StyledJobOrderRootSummary>
          </Box>
        </JobOrderFormSection>
      </Box>

      {/* End of submit actions */}
    </Box>
  );
};

JobOrderForm.defaultProps = {
  id: null,
  displayOnDrawer: false,
};
