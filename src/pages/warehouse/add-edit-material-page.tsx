import { BaseCard, BaseLoader } from '@/components/base';
import { PageTitle } from '@/components/shared';
import { BoxSpinner } from '@/components/ui';
import { MaterialForm } from '@/components/warehouse';
import { RequestBodyMasterData } from '@/interfaces/master-data';
import { useAppSelector } from '@/store/hook';
import { masterData_fetchMasterData } from '@/store/master-data/master-data-actions';
import { masterData_rootSelector } from '@/store/master-data/master-data-selectors';
import { warehouse_resetCreateMaterialState } from '@/store/warehouse/warehouse-actions';
import { warehouse_rootSelector } from '@/store/warehouse/warehouse-selectors';
import { ROUTES } from '@/utils/constants';
import { Grid } from '@mui/material';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { MainLayout } from '@src/components/layouts';
import { FC, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';

import { useTheme } from '@mui/material';

const masterDataPayload: RequestBodyMasterData = [
  {
    object_name: 'material_types',
  },
  {
    object_name: 'material_categories',
  },
  {
    object_name: 'material_brands',
  },
];

const AddEditMaterialPage: FC = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const theme = useTheme();

  const { data: masterData, isFetching: isFetchingMasterData } = useAppSelector((state) =>
    masterData_rootSelector(state),
  );
  const { createMaterial } = useAppSelector((state) => warehouse_rootSelector(state));

  useEffect(() => {
    dispatch(masterData_fetchMasterData(masterDataPayload));
  }, []);

  /**
   * Navigate to warehouse page when create material success
   */
  useEffect(() => {
    if (createMaterial.isSuccess) {
      navigate(ROUTES.WAREHOUSE);
      return () => {
        dispatch(warehouse_resetCreateMaterialState());
      };
    }
    return () => undefined;
  }, [createMaterial.isSuccess]);

  return (
    <MainLayout>
      <Grid container justifyContent="center">
        <Grid item sm={12} md={10}>
          <PageTitle
            title="Tambah Bahan Baku"
            subtitle="Lorem ipsum dolor sit amet"
            rightContent={
              <Box>
                <Typography>Right content</Typography>
              </Box>
            }
          />
          <BaseCard
            sx={{
              p: {
                xs: theme.spacing(5, 7),
                md: theme.spacing(8, 10),
              },
              position: 'relative',
              minHeight: 400,
            }}
          >
            {isFetchingMasterData && <BaseLoader spinnerSize={48} />}
            <MaterialForm id={undefined} sources={masterData} />
          </BaseCard>
        </Grid>
      </Grid>
    </MainLayout>
  );
};

export default AddEditMaterialPage;
