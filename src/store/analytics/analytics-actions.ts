import { ANALYTICS_ACTION_TYPES as ActionTypes } from './analytics-action-types.enum';
import { IAnalyticsTotalJobOrder, JobOrderTotalPeriod } from './analytics.interface';

// Action definitions.
interface IFetchTotalJobOrderRequested {
  type: typeof ActionTypes.FETCH_TOTAL_JOB_ORDER_REQUESTED;
  payload: {
    period?: JobOrderTotalPeriod;
    date?: string;
  };
}
interface IFetchTotalJobOrderLoading {
  type: typeof ActionTypes.FETCH_TOTAL_JOB_ORDER_LOADING;
}
interface IFetchTotalJobOrderFailure {
  type: typeof ActionTypes.FETCH_TOTAL_JOB_ORDER_FAILURE;
}
interface IFetchTotalJobOrderSuccess {
  type: typeof ActionTypes.FETCH_TOTAL_JOB_ORDER_SUCCESS;
  payload: {
    data: IAnalyticsTotalJobOrder;
    period?: JobOrderTotalPeriod;
    date?: string;
  };
}

// Union action types.
export type AnalyticsActions = IFetchTotalJobOrderLoading | IFetchTotalJobOrderFailure | IFetchTotalJobOrderSuccess;

// Actions creator.
export const fetchTotalJobOrder = (period?: JobOrderTotalPeriod, date?: string): IFetchTotalJobOrderRequested => ({
  type: ActionTypes.FETCH_TOTAL_JOB_ORDER_REQUESTED,
  payload: { period, date },
});

export const fetchTotalJobOrderLoading = (): IFetchTotalJobOrderLoading => ({
  type: ActionTypes.FETCH_TOTAL_JOB_ORDER_LOADING,
});

export const fetchTotalJobOrderFailure = (): IFetchTotalJobOrderFailure => ({
  type: ActionTypes.FETCH_TOTAL_JOB_ORDER_FAILURE,
});

export const fetchTotalJobOrderSuccess = (
  data: IAnalyticsTotalJobOrder,
  period?: JobOrderTotalPeriod,
  date?: string,
): IFetchTotalJobOrderSuccess => ({
  type: ActionTypes.FETCH_TOTAL_JOB_ORDER_SUCCESS,
  payload: { data, period, date },
});
