import AddIcon from '@mui/icons-material/Add'
import Close from '@mui/icons-material/Close'
import { Box, Button, IconButton } from '@mui/material'
import Drawer from '@mui/material/Drawer'
import { styled, useTheme } from '@mui/material/styles'
import { useAppSelector } from '@src/store/hook'
import { setDrawerDetailInventory } from '@src/store/inventory/actions'
import { DRAWER_DETAIL_INVENTORY_WIDTH } from '@src/utils/constants'
import { FC, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useLocation } from 'react-router-dom'

const DrawerDetailInventory: FC = () => {
  const theme = useTheme()
  const { drawerDetailInventory } = useAppSelector((state) => state.inventory)
  const dispatch = useDispatch()

  const handleDrawerClose = (): void => {
    dispatch(setDrawerDetailInventory({ open: false, type: null, id: null }))
  }

  useEffect(
    () =>
      // if (drawerDetailInventory.status) {

      // }
      // clean up
      () => {
        handleDrawerClose()
      },
    [],
  )

  return (
    <>
      {/* close button */}
      <Box
        sx={{
          position: 'absolute',
          top: 60,
          right: 0,
          zIndex: 2000,
          visibility: 'hidden',
          transition: theme.transitions.create('right', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
          }),
          ...(drawerDetailInventory.open && {
            visibility: 'visible',
            right: DRAWER_DETAIL_INVENTORY_WIDTH - 16,
            transition: theme.transitions.create('right', {
              easing: theme.transitions.easing.easeOut,
              duration: theme.transitions.duration.enteringScreen,
            }),
          }),
        }}
      >
        <IconButton
          onClick={handleDrawerClose}
          sx={{
            color: 'red',
            backgroundColor: 'background.paper',
            boxShadow: '0 12px 32px 0 rgb(10 10 10 / 16%)',
            '&:hover': {
              backgroundColor: 'background.paper',
            },
          }}
        >
          <Close sx={{ fontSize: 22 }} />
        </IconButton>
      </Box>

      <Drawer
        sx={{
          flexShrink: 0,
          width: DRAWER_DETAIL_INVENTORY_WIDTH,
          '& .MuiDrawer-paper': {
            width: DRAWER_DETAIL_INVENTORY_WIDTH,
            boxSizing: 'border-box',
          },
        }}
        variant="persistent"
        anchor="right"
        open={drawerDetailInventory.open}
        PaperProps={{
          sx: {
            backgroundColor: 'background.default',
            border: 'none !important',
          },
        }}
      >
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            minHeight: '100vh',
          }}
        >
          <Box>
            Detail inventory ID : {drawerDetailInventory.id}, type: {drawerDetailInventory.type}
          </Box>
          <Box>
            <Button variant="outlined" startIcon={<AddIcon />}>
              Tambah Jumlah Stock
            </Button>
          </Box>
        </Box>
      </Drawer>
    </>
  )
}

export default DrawerDetailInventory
