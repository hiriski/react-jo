import { StyledButton } from '@/components/ui';
import { IMaterial } from '@/interfaces/warehouse';
import { useAppSelector } from '@/store';
import { Add, DeleteOutlined } from '@mui/icons-material';
import { Box, Divider, styled, Typography } from '@mui/material';
import { FC, memo } from 'react';

// Utils
import { isWarehouseTeam } from '@/utils/role';
import { useDispatch } from 'react-redux';
import { warehouse_setDialogAddStockMaterial } from '@/store/warehouse/warehouse-actions';

interface Props {
  item: IMaterial;
}

const StyledBoxTableCell = styled(Box)(({ theme }) => ({
  display: 'table-cell',
  padding: theme.spacing(1),
}));

export const MaterialItem: FC<Props> = memo(({ item }) => {
  const dispatch = useDispatch();
  const { authenticatedUser: user } = useAppSelector((state) => state.auth);

  const handleClickAddStock = (): void => {
    dispatch(warehouse_setDialogAddStockMaterial(true, item));
  };

  return (
    <Box sx={{ display: 'table-row' }}>
      <StyledBoxTableCell>{item.name}</StyledBoxTableCell>
      <StyledBoxTableCell>{item.brand.name}</StyledBoxTableCell>
      <StyledBoxTableCell>
        {item.categories.length > 0 && item.categories.map((x) => <Typography>{x.name}</Typography>)}
      </StyledBoxTableCell>
      <StyledBoxTableCell>{item.status}</StyledBoxTableCell>
      <StyledBoxTableCell>{item.stock_length}</StyledBoxTableCell>
      <StyledBoxTableCell>{item.stock_width}</StyledBoxTableCell>
      <StyledBoxTableCell>
        <Box sx={{ display: 'flex' }}>
          <StyledButton
            onClick={handleClickAddStock}
            disabled={!isWarehouseTeam(user)}
            disableHoverEffect
            startIcon={<Add />}
          >
            Tambah Stock
          </StyledButton>
          <Divider orientation="vertical" />
          <StyledButton disableHoverEffect startIcon={<DeleteOutlined />}></StyledButton>
        </Box>
      </StyledBoxTableCell>
    </Box>
  );
});
