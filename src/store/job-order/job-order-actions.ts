import { TCreateJo, TJo, TJoInvoiceData, TResponseListJo, TUpdateJo } from '@src/types/jo';
import { AnyAction } from 'redux';

import {
  IJobOrderPayloadUpdatePaymentStatus,
  IJobOrderFilters,
  IPayloadJobOrderDelete,
  IPayloadJobOrderStartProduction,
} from './job-order.interfaces';
import { TPayloadGetJoInvoiceData } from './job-order-api-service';

// Enum
import { JOB_ORDER_ACTION_TYPES as ActionTypes } from './job-order-action-types.enum';
import { UUID } from '@/interfaces/common';
import { IJobOrder, IRequestBodyJobOrder } from '@/interfaces/job-order';
import { NavigateFunction } from 'react-router';

// Actions definiition
interface SetDrawerAddEditJo {
  type: typeof ActionTypes.SET_DRAWER_ADD_EDIT_JO;
  payload: {
    status: boolean;
    id?: string;
  };
}
interface SetDialogDetailJo {
  type: typeof ActionTypes.SET_DIALOG_DETAIL_JO;
  payload: {
    open: boolean;
    id?: UUID;
  };
}

interface FetchingListJo {
  type: typeof ActionTypes.FETCHING_LIST_JO_REQUESTED;
  payload?: { shouldLoading: boolean };
}
interface FetchingListJoLoading {
  type: typeof ActionTypes.FETCHING_LIST_JO_LOADING;
  payload?: { shouldLoading: boolean };
}
interface FetchingListJoFailure {
  type: typeof ActionTypes.FETCHING_LIST_JO_FAILURE;
}
interface FetchingListJoSuccess {
  type: typeof ActionTypes.FETCHING_LIST_JO_SUCCESS;
  payload: TResponseListJo;
}
// interface SetRowsPerPageJo {
//   type: typeof ActionTypes.SET_ROWS_PER_PAGE_JO;
//   payload: number;
// }
// interface SetPageListJo {
//   type: typeof ActionTypes.SET_PAGE_LIST_JO;
//   payload: number;
// }
interface ICreateJobOrderRequested {
  type: typeof ActionTypes.CREATE_JO_REQUESTED;
  payload: IRequestBodyJobOrder;
  navigate: NavigateFunction;
}
interface ICreateJobOrderLoading {
  type: typeof ActionTypes.CREATE_JO_LOADING;
}
interface ICreateJobOrderFailure {
  type: typeof ActionTypes.CREATE_JO_FAILURE;
}
interface ICreateJobOrderSuccess {
  type: typeof ActionTypes.CREATE_JO_SUCCESS;
  payload?: TJo;
}

interface IUpdateJobOrderRequested {
  type: typeof ActionTypes.UPDATE_JO_REQUESTED;
  payload: {
    id: UUID;
    body: Partial<IRequestBodyJobOrder>;
  };
  navigate: NavigateFunction;
}
interface IUpdateJobOrderLoading {
  type: typeof ActionTypes.UPDATE_JO_LOADING;
}
interface IUpdateJobOrderFailure {
  type: typeof ActionTypes.UPDATE_JO_FAILURE;
}
interface IUpdateJobOrderSuccess {
  type: typeof ActionTypes.UPDATE_JO_SUCCESS;
  payload?: TJo;
}

interface IJobOrderFetchDetailRequested {
  type: typeof ActionTypes.JOB_ORDER_FETCH_DETAIL_REQUESTED;
  payload: {
    job_order_id: UUID;
    shouldLoading?: boolean;
  };
}
interface IJobOrderFetchDetailLoading {
  type: typeof ActionTypes.JOB_ORDER_FETCH_DETAIL_LOADING;
  payload: { shouldLoading?: boolean };
}
interface IJobOrderFetchDetailFailure {
  type: typeof ActionTypes.JOB_ORDER_FETCH_DETAIL_FAILURE;
}
interface IJobOrderFetchDetailSuccess {
  type: typeof ActionTypes.JOB_ORDER_FETCH_DETAIL_SUCCESS;
  payload: TJo;
}

interface FetchingInvoiceData {
  type: typeof ActionTypes.FETCHING_INVOICE_DATA_REQUESTED;
  payload: TPayloadGetJoInvoiceData;
}
interface FetchingInvoiceDataLoading {
  type: typeof ActionTypes.FETCHING_INVOICE_DATA_LOADING;
}
interface FetchingInvoiceDataFailure {
  type: typeof ActionTypes.FETCHING_INVOICE_DATA_FAILURE;
}
interface FetchingInvoiceDataSuccess {
  type: typeof ActionTypes.FETCHING_INVOICE_DATA_SUCCESS;
  payload: TJoInvoiceData;
}

interface UpdatePaymentStatusJo {
  type: typeof ActionTypes.UPDATE_PAYMENT_STATUS_REQUESTED;
  payload: IJobOrderPayloadUpdatePaymentStatus;
}
interface UpdatePaymentStatusJoLoading {
  type: typeof ActionTypes.UPDATE_PAYMENT_STATUS_LOADING;
}
interface UpdatePaymentStatusJoFailure {
  type: typeof ActionTypes.UPDATE_PAYMENT_STATUS_FAILURE;
}
interface UpdatePaymentStatusJoSuccess {
  type: typeof ActionTypes.UPDATE_PAYMENT_STATUS_SUCCESS;
  payload: IJobOrder;
}

interface UpdateProductionStatus {
  type: typeof ActionTypes.UPDATE_PRODUCTION_STATUS_REQUESTED;
  payload: { joId: UUID; productionStatusId: number };
}
interface UpdateProductionStatusLoading {
  type: typeof ActionTypes.UPDATE_PRODUCTION_STATUS_LOADING;
}
interface UpdateProductionStatusFailure {
  type: typeof ActionTypes.UPDATE_PRODUCTION_STATUS_FAILURE;
}
interface UpdateProductionStatusSuccess {
  type: typeof ActionTypes.UPDATE_PRODUCTION_STATUS_SUCCESS;
  payload: TJo;
}
interface UpdatePercentageProgress {
  type: typeof ActionTypes.UPDATE_PERCENTAGE_PROGRESS_REQUESTED;
  payload: { joId: string; value: number };
}
interface UpdatePercentageProgressLoading {
  type: typeof ActionTypes.UPDATE_PERCENTAGE_PROGRESS_LOADING;
}
interface UpdatePercentageProgressFailure {
  type: typeof ActionTypes.UPDATE_PERCENTAGE_PROGRESS_FAILURE;
}
interface UpdatePercentageProgressSuccess {
  type: typeof ActionTypes.UPDATE_PERCENTAGE_PROGRESS_SUCCESS;
  payload: TJo;
}
interface AddNotesJo {
  type: typeof ActionTypes.ADD_NOTES_JO_REQUESTED;
  payload: { joId: UUID; notes: string };
}
interface AddNotesJoLoading {
  type: typeof ActionTypes.ADD_NOTES_JO_LOADING;
}
interface AddNotesJoFailure {
  type: typeof ActionTypes.ADD_NOTES_JO_FAILURE;
}
interface AddNotesJoSuccess {
  type: typeof ActionTypes.ADD_NOTES_JO_SUCCESS;
  payload: TJo;
}

interface SetDialogInputDownPayment {
  type: typeof ActionTypes.SET_DIALOG_INPUT_DOWN_PAYMENT;
  payload: {
    open: boolean;
    id: string | null;
    totalOrder: number;
  };
}

interface ISetFormHasChanges {
  type: typeof ActionTypes.SET_FORM_HAS_CHANGES;
  payload: boolean;
}
interface IJobOrderSetDialogQr {
  type: typeof ActionTypes.JOB_ORDER_SET_DIALOG_QR;
  payload: {
    open: boolean;
    qr?: string | null;
  }; // string of base64
}

interface IJobOrderStartProductionRequested {
  type: typeof ActionTypes.JOB_ORDER_START_PRODUCTION_REQUESTED;
  payload: IPayloadJobOrderStartProduction;
}
interface IJobOrderStartProductionLoading {
  type: typeof ActionTypes.JOB_ORDER_START_PRODUCTION_LOADING;
}
interface IJobOrderStartProductionFailure {
  type: typeof ActionTypes.JOB_ORDER_START_PRODUCTION_FAILURE;
}
interface IJobOrderStartProductionSuccess {
  type: typeof ActionTypes.JOB_ORDER_START_PRODUCTION_SUCCESS;
  payload?: TJo;
}

interface IJobOrderSetDialogStartProduction {
  type: ActionTypes.JOB_ORDER_SET_DIALOG_START_PRODUCTION;
  payload: {
    open: boolean;
    job_order_id?: UUID | null;
  };
}

interface IJobOrderFinishProductionRequested {
  type: typeof ActionTypes.JOB_ORDER_FINISH_PRODUCTION_REQUESTED;
  payload: { job_order_id: UUID };
}
interface IJobOrderFinishProductionLoading {
  type: typeof ActionTypes.JOB_ORDER_FINISH_PRODUCTION_LOADING;
}
interface IJobOrderFinishProductionFailure {
  type: typeof ActionTypes.JOB_ORDER_FINISH_PRODUCTION_FAILURE;
}
interface IJobOrderFinishProductionSuccess {
  type: typeof ActionTypes.JOB_ORDER_FINISH_PRODUCTION_SUCCESS;
  payload?: TJo;
}

interface IJobOrderSetFilters {
  type: ActionTypes.JOB_ORDER_SET_FILTERS;
  payload: { property: keyof IJobOrderFilters; value?: null | string | number };
}
interface IJobOrderResetFilters {
  type: ActionTypes.JOB_ORDER_RESET_FILTERS;
}

interface IJobOrderSetDialogDelete {
  type: ActionTypes.JOB_ORDER_SET_DIALOG_DELETE;
  payload: {
    open: boolean;
    job_order_id?: UUID | null;
  };
}
interface IJobOrderDeleteRequested {
  type: typeof ActionTypes.JOB_ORDER_DELETE_REQUESTED;
  payload: IPayloadJobOrderDelete;
}
interface IJobOrderDeleteLoading {
  type: typeof ActionTypes.JOB_ORDER_DELETE_LOADING;
}
interface IJobOrderDeleteFailure {
  type: typeof ActionTypes.JOB_ORDER_DELETE_FAILURE;
}
interface IJobOrderDeleteSuccess {
  type: typeof ActionTypes.JOB_ORDER_DELETE_SUCCESS;
  payload?: TJo;
}

// Union action types
export type JobOrderActions =
  | SetDrawerAddEditJo
  | SetDialogDetailJo
  | FetchingListJo
  | FetchingListJoLoading
  | FetchingListJoFailure
  | FetchingListJoSuccess
  // | SetRowsPerPageJo
  // | SetPageListJo
  | ICreateJobOrderRequested
  | ICreateJobOrderLoading
  | ICreateJobOrderSuccess
  | ICreateJobOrderFailure
  | IUpdateJobOrderRequested
  | IUpdateJobOrderFailure
  | IUpdateJobOrderLoading
  | IUpdateJobOrderSuccess
  | IJobOrderFetchDetailRequested
  | IJobOrderFetchDetailLoading
  | IJobOrderFetchDetailFailure
  | IJobOrderFetchDetailSuccess
  | FetchingInvoiceData
  | FetchingInvoiceDataLoading
  | FetchingInvoiceDataFailure
  | FetchingInvoiceDataSuccess
  | UpdatePaymentStatusJo
  | UpdatePaymentStatusJoLoading
  | UpdatePaymentStatusJoFailure
  | UpdatePaymentStatusJoSuccess
  | UpdateProductionStatus
  | UpdateProductionStatusLoading
  | UpdateProductionStatusFailure
  | UpdateProductionStatusSuccess
  | UpdatePercentageProgress
  | UpdatePercentageProgressLoading
  | UpdatePercentageProgressFailure
  | UpdatePercentageProgressSuccess
  | AddNotesJo
  | AddNotesJoLoading
  | AddNotesJoFailure
  | AddNotesJoSuccess
  | SetDialogInputDownPayment
  | ISetFormHasChanges
  | IJobOrderSetDialogQr
  | IJobOrderSetDialogStartProduction
  | IJobOrderStartProductionRequested
  | IJobOrderStartProductionLoading
  | IJobOrderStartProductionFailure
  | IJobOrderStartProductionSuccess
  | IJobOrderFinishProductionRequested
  | IJobOrderFinishProductionLoading
  | IJobOrderFinishProductionFailure
  | IJobOrderFinishProductionSuccess
  | IJobOrderSetFilters
  | IJobOrderResetFilters
  | IJobOrderSetDialogDelete
  | IJobOrderDeleteRequested
  | IJobOrderDeleteLoading
  | IJobOrderDeleteFailure
  | IJobOrderDeleteSuccess;
// Actions creator.

export const setDrawerAddEditJo = (status: boolean, id?: string): SetDrawerAddEditJo => ({
  type: ActionTypes.SET_DRAWER_ADD_EDIT_JO,
  payload: { status, id },
});

export const setDialogDetailJo = (open: boolean, id?: UUID): SetDialogDetailJo => ({
  type: ActionTypes.SET_DIALOG_DETAIL_JO,
  payload: { open, id },
});

/** Fetch list jo */
export const fetchListJo = (payload?: { shouldLoading: boolean }): FetchingListJo => ({
  type: ActionTypes.FETCHING_LIST_JO_REQUESTED,
  payload,
});
export const fetchingListJoLoading = (payload?: { shouldLoading: boolean }): FetchingListJoLoading => ({
  type: ActionTypes.FETCHING_LIST_JO_LOADING,
  payload,
});
export const fetchingListJoFailure = (): FetchingListJoFailure => ({
  type: ActionTypes.FETCHING_LIST_JO_FAILURE,
});
export const fetchingListJoSuccess = (payload: TResponseListJo): FetchingListJoSuccess => ({
  type: ActionTypes.FETCHING_LIST_JO_SUCCESS,
  payload,
});
// export const setRowsPerPageJo = (payload: number): SetRowsPerPageJo => ({
//   type: ActionTypes.SET_ROWS_PER_PAGE_JO,
//   payload,
// });
// export const setPageListJo = (payload: number): SetPageListJo => ({
//   type: ActionTypes.SET_PAGE_LIST_JO,
//   payload,
// });

/** Create jo */
export const jobOrder_createJobOrder = (
  payload: IRequestBodyJobOrder,
  navigate: NavigateFunction,
): ICreateJobOrderRequested => ({
  type: ActionTypes.CREATE_JO_REQUESTED,
  payload,
  navigate,
});
export const jobOrder_createJobOrderSuccess = (payload: TJo): ICreateJobOrderSuccess => ({
  type: ActionTypes.CREATE_JO_SUCCESS,
  payload,
});
export const jobOrder_createJobOrderLoading = (): ICreateJobOrderLoading => ({
  type: ActionTypes.CREATE_JO_LOADING,
});
export const jobOrder_createJobOrderFailure = (): ICreateJobOrderFailure => ({
  type: ActionTypes.CREATE_JO_FAILURE,
});

/** Update jo */
export const jobOrder_updateJobOrder = (
  id: UUID,
  body: Partial<IRequestBodyJobOrder>,
  navigate: NavigateFunction,
): IUpdateJobOrderRequested => ({
  type: ActionTypes.UPDATE_JO_REQUESTED,
  payload: { id, body },
  navigate,
});
export const jobOrder_updateJobOrderLoading = (): IUpdateJobOrderLoading => ({
  type: ActionTypes.UPDATE_JO_LOADING,
});
export const jobOrder_updateJobOrderSuccess = (payload: TJo): IUpdateJobOrderSuccess => ({
  type: ActionTypes.UPDATE_JO_SUCCESS,
  payload,
});
export const jobOrder_updateJobOrderFailure = (): IUpdateJobOrderFailure => ({
  type: ActionTypes.UPDATE_JO_FAILURE,
});

export const jobOrder_fetchDetail = (job_order_id: UUID, shouldLoading?: boolean): IJobOrderFetchDetailRequested => ({
  type: ActionTypes.JOB_ORDER_FETCH_DETAIL_REQUESTED,
  payload: { job_order_id, shouldLoading },
});
export const jobOrder_fetchDetailLoading = (shouldLoading?: boolean): IJobOrderFetchDetailLoading => ({
  type: ActionTypes.JOB_ORDER_FETCH_DETAIL_LOADING,
  payload: { shouldLoading },
});
export const jobOrder_fetchDetailSuccess = (payload: TJo): IJobOrderFetchDetailSuccess => ({
  type: ActionTypes.JOB_ORDER_FETCH_DETAIL_SUCCESS,
  payload,
});

export const fetchInvoiceData = (payload: TPayloadGetJoInvoiceData): FetchingInvoiceData => ({
  type: ActionTypes.FETCHING_INVOICE_DATA_REQUESTED,
  payload,
});

export const fetchingInvoiceDataSuccess = (payload: TJoInvoiceData): FetchingInvoiceDataSuccess => ({
  type: ActionTypes.FETCHING_INVOICE_DATA_SUCCESS,
  payload,
});

/** Update payment status jo */
export const updatePaymentStatusJo = (payload: IJobOrderPayloadUpdatePaymentStatus): UpdatePaymentStatusJo => ({
  type: ActionTypes.UPDATE_PAYMENT_STATUS_REQUESTED,
  payload: payload,
});

export const jobOrder_updatePaymentStatusLoading = (): UpdatePaymentStatusJoLoading => ({
  type: ActionTypes.UPDATE_PAYMENT_STATUS_LOADING,
});

export const jobOrder_updatePaymentStatusFailure = (): UpdatePaymentStatusJoFailure => ({
  type: ActionTypes.UPDATE_PAYMENT_STATUS_FAILURE,
});

export const jobOrder_updatePaymentStatusSuccess = (payload: IJobOrder): UpdatePaymentStatusJoSuccess => ({
  type: ActionTypes.UPDATE_PAYMENT_STATUS_SUCCESS,
  payload,
});

/** Update production status jo */
export const updateProductionStatus = (joId: UUID, productionStatusId: number): UpdateProductionStatus => ({
  type: ActionTypes.UPDATE_PRODUCTION_STATUS_REQUESTED,
  payload: { joId, productionStatusId },
});

export const updateProductionStatusLoading = (): UpdateProductionStatusLoading => ({
  type: ActionTypes.UPDATE_PRODUCTION_STATUS_LOADING,
});

export const updateProductionStatusFailure = (): UpdateProductionStatusFailure => ({
  type: ActionTypes.UPDATE_PRODUCTION_STATUS_FAILURE,
});

export const updateProductionStatusSuccess = (payload: TJo): UpdateProductionStatusSuccess => ({
  type: ActionTypes.UPDATE_PRODUCTION_STATUS_SUCCESS,
  payload,
});

/** Update percentage progress jo */
export const updatePercentageProgress = (joId: string, value: number): UpdatePercentageProgress => ({
  type: ActionTypes.UPDATE_PERCENTAGE_PROGRESS_REQUESTED,
  payload: { joId, value },
});

export const updatePercentageProgressLoading = (): UpdatePercentageProgressLoading => ({
  type: ActionTypes.UPDATE_PERCENTAGE_PROGRESS_LOADING,
});

export const updatePercentageProgressFailure = (): UpdatePercentageProgressFailure => ({
  type: ActionTypes.UPDATE_PERCENTAGE_PROGRESS_FAILURE,
});

export const updatePercentageProgressSuccess = (payload: TJo): UpdatePercentageProgressSuccess => ({
  type: ActionTypes.UPDATE_PERCENTAGE_PROGRESS_SUCCESS,
  payload,
});

/** Add notes to job order */
export const addNotesJo = (joId: UUID, notes: string): AddNotesJo => ({
  type: ActionTypes.ADD_NOTES_JO_REQUESTED,
  payload: { joId, notes },
});

export const addNotesJoLoading = (): AddNotesJoLoading => ({
  type: ActionTypes.ADD_NOTES_JO_LOADING,
});

export const addNotesJoFailure = (): AddNotesJoFailure => ({
  type: ActionTypes.ADD_NOTES_JO_FAILURE,
});

export const addNotesJoSuccess = (payload: TJo): AddNotesJoSuccess => ({
  type: ActionTypes.ADD_NOTES_JO_SUCCESS,
  payload,
});

export const setDialogInputDownPayment = (
  open: boolean,
  id: string | null,
  totalOrder: number,
): SetDialogInputDownPayment => ({
  type: ActionTypes.SET_DIALOG_INPUT_DOWN_PAYMENT,
  payload: { open, id, totalOrder },
});

export const jobOrder_setFormHasChanges = (payload: boolean): ISetFormHasChanges => ({
  type: ActionTypes.SET_FORM_HAS_CHANGES,
  payload,
});

export const jobOrder_setDialogQr = (open: boolean, qr?: string | null): IJobOrderSetDialogQr => ({
  type: ActionTypes.JOB_ORDER_SET_DIALOG_QR,
  payload: {
    open,
    qr,
  },
});

export const jobOrder_setDialogStartProduction = (
  open: boolean,
  job_order_id?: UUID | null,
): IJobOrderSetDialogStartProduction => ({
  type: ActionTypes.JOB_ORDER_SET_DIALOG_START_PRODUCTION,
  payload: { open, job_order_id },
});

export const jobOrder_startProduction = (
  payload: IPayloadJobOrderStartProduction,
): IJobOrderStartProductionRequested => ({
  type: ActionTypes.JOB_ORDER_START_PRODUCTION_REQUESTED,
  payload,
});

export const jobOrder_startProductionLoading = (): IJobOrderStartProductionLoading => ({
  type: ActionTypes.JOB_ORDER_START_PRODUCTION_LOADING,
});

export const jobOrder_startProductionFailure = (): IJobOrderStartProductionFailure => ({
  type: ActionTypes.JOB_ORDER_START_PRODUCTION_FAILURE,
});

export const jobOrder_startProductionSuccess = (): IJobOrderStartProductionSuccess => ({
  type: ActionTypes.JOB_ORDER_START_PRODUCTION_SUCCESS,
});

export const jobOrder_setFilters = (
  property: keyof IJobOrderFilters,
  value?: null | string | number,
): IJobOrderSetFilters => ({
  type: ActionTypes.JOB_ORDER_SET_FILTERS,
  payload: { property, value },
});

export const jobOrder_resetFilters = (): IJobOrderResetFilters => ({
  type: ActionTypes.JOB_ORDER_RESET_FILTERS,
});

export const jobOrder_finishProduction = (payload: { job_order_id: UUID }): IJobOrderFinishProductionRequested => ({
  type: ActionTypes.JOB_ORDER_FINISH_PRODUCTION_REQUESTED,
  payload,
});

export const jobOrder_finishProductionLoading = (): IJobOrderFinishProductionLoading => ({
  type: ActionTypes.JOB_ORDER_FINISH_PRODUCTION_LOADING,
});

export const jobOrder_finishProductionFailure = (): IJobOrderFinishProductionFailure => ({
  type: ActionTypes.JOB_ORDER_FINISH_PRODUCTION_FAILURE,
});

export const jobOrder_finishProductionSuccess = (): IJobOrderFinishProductionSuccess => ({
  type: ActionTypes.JOB_ORDER_FINISH_PRODUCTION_SUCCESS,
});

export const jobOrder_setDialogDelete = (open: boolean, job_order_id?: UUID | null): IJobOrderSetDialogDelete => ({
  type: ActionTypes.JOB_ORDER_SET_DIALOG_DELETE,
  payload: { open, job_order_id },
});

export const jobOrder_delete = (payload: IPayloadJobOrderDelete): IJobOrderDeleteRequested => ({
  type: ActionTypes.JOB_ORDER_DELETE_REQUESTED,
  payload,
});

export const jobOrder_deleteLoading = (): IJobOrderDeleteLoading => ({
  type: ActionTypes.JOB_ORDER_DELETE_LOADING,
});

export const jobOrder_deleteFailure = (): IJobOrderDeleteFailure => ({
  type: ActionTypes.JOB_ORDER_DELETE_FAILURE,
});

export const jobOrder_deleteSuccess = (): IJobOrderDeleteSuccess => ({
  type: ActionTypes.JOB_ORDER_DELETE_SUCCESS,
});
