import { CommonAction } from './actions';
import * as ActionTypes from './constants';

export type CommonState = {
  visibleSidebarDrawer: boolean;
  popUpSearchBox: boolean;
};

const initialState: CommonState = {
  visibleSidebarDrawer: true,
  popUpSearchBox: false,
};

const commonReducer = (state: CommonState = initialState, action: CommonAction): CommonState => {
  switch (action.type) {
    case ActionTypes.SET_SIDEBAR_DRAWER:
      return {
        ...state,
        visibleSidebarDrawer: action.payload,
      };

    case ActionTypes.SET_POPUP_SEARCH_BOX:
      return {
        ...state,
        popUpSearchBox: action.payload,
      };
    default:
      return state;
  }
};

export default commonReducer;
