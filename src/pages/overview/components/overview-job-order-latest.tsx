import { FC, ReactElement, useState } from 'react';
// Mui components.
import { IconButton, Box, Typography, Avatar, alpha } from '@mui/material';

// Base components.
import { BaseCard, BaseEmptyContent } from '@/components/base';

// Mui icons
import ArrowForward from '@mui/icons-material/ArrowForward';
import ArrowBack from '@mui/icons-material/ArrowBack';

// Mui colors.
import { green, blue, indigo, orange } from '@mui/material/colors';

// React Image Lightbox
import Lightbox from 'react-image-lightbox';

// Hooks
import { useDispatch } from 'react-redux';
import { IJobOrder } from '@/interfaces/job-order';
import { getInitialsName } from '@/utils/misc';

import Slider, { Settings } from 'react-slick';
import { VisibilityOutlined } from '@mui/icons-material';
import { StyledButton } from '@/components/ui';
import { setDialogDetailJo } from '@/store/job-order/job-order-actions';
import { UUID } from '@/interfaces/common';

interface NextPrevArrow {
  onClick?: () => void;
  type: 'next' | 'prev';
  className?: 'string';
}

const NextPrev = (props: NextPrevArrow): ReactElement => {
  const { onClick, type, className } = props;
  return (
    <IconButton color="primary" onClick={onClick} className={className}>
      {type === 'next' ? <ArrowForward sx={{ fontSize: 22 }} /> : <ArrowBack sx={{ fontSize: 22 }} />}
    </IconButton>
  );
};

const sliderConfig: Settings = {
  infinite: true,
  autoplay: false,
  speed: 350,
  slidesToShow: 1,
  slidesToScroll: 1,
  // prevArrow: <NextPrev type="prev" />,
  // nextArrow: <NextPrev type="next" />,
};

interface Props {
  isFetching: boolean;
  data: Array<IJobOrder>;
}

export const OverviewJobOrderLatest: FC<Props> = ({ isFetching, data }) => {
  const [openImage, setOpenImage] = useState<boolean>(false);
  const dispatch = useDispatch();

  const handleClickDetail = (id: UUID): void => {
    dispatch(setDialogDetailJo(true, id));
  };

  // const images = data[0].images.map((x) => x);

  if (isFetching) return <h1>Loading</h1>;

  return (
    <BaseCard
      // title="Job Order (In Progress)"
      // subtitle="Ada 2 Jo yang sedang in progress hari ini"
      sx={{ height: '100%' }}
    >
      {openImage && <Lightbox mainSrc={data[0].images[0].image_lg} onCloseRequest={() => setOpenImage(false)} />}

      {data.length > 0 ? (
        <Slider {...sliderConfig}>
          {data.map((item) => (
            <Box key={item.id}>
              <Box sx={{ display: 'flex', flexDirection: { xs: 'column', sm: 'row' }, flex: 1 }}>
                {Array.isArray(item.images) && item.images.length > 0 && (
                  <Box sx={{ width: '45%', position: 'relative', paddingTop: '383px' }}>
                    <Box
                      sx={{
                        overflow: 'hidden',
                        height: '100%',
                        width: '100%',
                        borderRadius: 1,
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        cursor: 'pointer',
                        '&:hover': {
                          '& .image-action-box': {
                            display: 'flex',
                          },
                        },
                      }}
                    >
                      <Box
                        className="image-action-box"
                        sx={{
                          width: '100%',
                          height: '100%',
                          display: 'none',
                          alignItems: 'center',
                          justifyContent: 'center',
                          position: 'absolute',
                          top: 0,
                          left: 0,
                          backgroundColor: alpha('#000', 0.25),
                        }}
                      >
                        <IconButton sx={{ color: 'common.white' }} onClick={() => setOpenImage(true)}>
                          <VisibilityOutlined />
                        </IconButton>
                      </Box>
                      <img
                        style={{ objectFit: 'cover', width: '100%', height: '100%' }}
                        src={item.images[0].image_lg}
                        alt={item.title}
                      />
                    </Box>
                  </Box>
                )}
                <Box sx={{ width: '55%', pl: 3, display: 'flex', flexDirection: 'column' }}>
                  <Box sx={{ mb: 3 }}>
                    <Typography sx={{ color: 'text.disabled', fontWeight: 700 }} variant="subtitle1">
                      #{item.order_number}
                    </Typography>
                    <Typography sx={{ mb: 0.25 }} component="h2" variant="h5">
                      {item.title || ''}
                    </Typography>
                  </Box>

                  {/* Customer */}
                  {item.customer && (
                    <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
                      <Avatar sx={{ backgroundColor: green[500], width: 34, height: 34, fontSize: 16 }}>
                        {getInitialsName(item.customer.name, true)}
                      </Avatar>
                      <Box sx={{ ml: 1.5 }}>
                        <Typography variant="subtitle2">Customer</Typography>
                        <Typography variant="subtitle1" sx={{ fontWeight: 'bold' }}>
                          {item.customer.name}
                        </Typography>
                      </Box>
                    </Box>
                  )}

                  {/* Box */}
                  <Box
                    sx={{
                      // mx: 'auto',
                      width: '100%',
                      border: (theme) => `1px solid ${theme.palette.divider}`,
                      borderRadius: 1,
                      textAlign: 'center',
                      py: 4,
                      px: 2,
                    }}
                  >
                    <Box sx={{ mb: 1 }}>
                      <Typography variant="subtitle1">Status</Typography>
                      <Typography variant="body1">{item.production_status.name}</Typography>
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <Typography variant="subtitle1">Kategori</Typography>
                      <Typography variant="body1">{item.category.name}</Typography>
                    </Box>
                  </Box>

                  {/* Detail button */}
                  <Box sx={{ mt: 'auto' }}>
                    <StyledButton
                      onClick={() => handleClickDetail(item.id)}
                      disableHoverEffect
                      size="small"
                      startIcon={<VisibilityOutlined />}
                    >
                      Lihat detail
                    </StyledButton>
                  </Box>
                </Box>
              </Box>
            </Box>
          ))}
        </Slider>
      ) : (
        <BaseEmptyContent />
      )}
    </BaseCard>
  );
};
