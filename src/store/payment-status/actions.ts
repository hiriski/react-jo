import { TPaymentStatus } from '@src/types/jo'

import * as ActionTypes from './constants'

// Actions definiton.
interface FetchingListPaymentStatusRequested {
  type: typeof ActionTypes.FETCHING_LIST_PAYMENT_STATUS_REQUESTED
}
interface FetchingListPaymentStatusLoading {
  type: typeof ActionTypes.FETCHING_LIST_PAYMENT_STATUS_LOADING
}
interface FetchingListPaymentStatusFailure {
  type: typeof ActionTypes.FETCHING_LIST_PAYMENT_STATUS_FAILURE
}
interface FetchingListPaymentStatusSuccess {
  type: typeof ActionTypes.FETCHING_LIST_PAYMENT_STATUS_SUCCESS
  payload: Array<TPaymentStatus>
}

// Union actions type
export type PaymentStatusAction =
  | FetchingListPaymentStatusRequested
  | FetchingListPaymentStatusLoading
  | FetchingListPaymentStatusFailure
  | FetchingListPaymentStatusSuccess

// Actions creator.
export const fetchListPaymentStatus =
  (): FetchingListPaymentStatusRequested => ({
    type: ActionTypes.FETCHING_LIST_PAYMENT_STATUS_REQUESTED,
  })

export const fetchingListPaymentStatusSuccess = (
  payload: Array<TPaymentStatus>
): FetchingListPaymentStatusSuccess => ({
  type: ActionTypes.FETCHING_LIST_PAYMENT_STATUS_SUCCESS,
  payload,
})
