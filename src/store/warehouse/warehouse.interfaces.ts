export interface IJobOrderPayloadUpdatePaymentStatus {
  joId: number;
  paymentStatusId: number;
  downPayment?: number;
}
