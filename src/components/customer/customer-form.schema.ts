import * as yup from 'yup';

export const customerFormSchema = yup.object().shape({
  name: yup.string().max(255, 'Opsss. Nama Pelanggan terlalu panjang').required('Nama pelanggan tidak boleh kosong!'),
  customer_type: yup.mixed().oneOf(['individual', 'company']),
  phone_number: yup.string().nullable(true),
  email: yup.string().nullable(true),
  notes: yup.string().nullable(true),
  photo: yup.string().nullable(true),
  avatar_text_color: yup.string(), // invisible field.
  addresses: yup
    .array(
      yup.object({
        address_details: yup.string(),
        district: yup.string().nullable(true),
        city: yup.string(),
        province: yup.string(),
        country: yup.string().nullable(true),
      }),
    )
    .required('Silahkan pilih bahan baku'),
});
