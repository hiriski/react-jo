import Box from '@mui/material/Box'
import Skeleton from '@mui/material/Skeleton'
import { FC } from 'react'

const CustomerItemSkeleton: FC = () => (
  <Box
    sx={{
      display: 'flex',
      position: 'relative',
      px: 2.8,
      py: 2.4,
      border: (theme) => `1px solid ${theme.palette.divider}`,
      borderRadius: 1.2,
    }}
  >
    <Box sx={{ mr: 2 }}>
      <Skeleton variant="circular" animation="wave" height={40} width={40} />
    </Box>
    <Box sx={{ flex: 1 }}>
      <Skeleton sx={{ mt: 1 }} variant="rectangular" height={26} width="65%" />

      <Box sx={{ mt: 2 }}>
        <Skeleton width={'60%'} variant="text" />
        <Skeleton width={'70%'} variant="text" />
        <Skeleton width={'75%'} variant="text" />
      </Box>
    </Box>
  </Box>
)

export default CustomerItemSkeleton
