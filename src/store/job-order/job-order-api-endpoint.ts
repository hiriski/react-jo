export const JobOrderApiEndpoints = {
  index: '/job-order',
  create: '/job-order',
  startProduction: '/job-order/start-production',
  finishProduction: '/job-order/finish-production',
  delete: '/job-order/delete',
  updatePaymentStatus: '/job-order/update-payment-status',
};
