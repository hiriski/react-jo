/**
 * Pop Up search box
 * Inspired by Postman Pop Up Search
 */

import SearchIcon from '@mui/icons-material/Search'
import Box from '@mui/material/Box'
import Dialog from '@mui/material/Dialog'
import InputBase from '@mui/material/InputBase'
import Slide, { SlideProps } from '@mui/material/Slide'
import { SxProps, styled } from '@mui/material/styles'
import ToggleButton from '@mui/material/ToggleButton'
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup'
import Typography from '@mui/material/Typography'
import { FC, MouseEvent, ReactElement, forwardRef, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'

import { setPopUpSearchBox } from '@src/store/common/actions'
import { useAppSelector } from '@src/store/hook'

import { searchTypes } from './data'

const Transition = forwardRef<unknown, SlideProps>(
  (props, ref): ReactElement => (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Slide direction="down" ref={ref} {...props} />
  )
)

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  width: '100%',
  borderBottom: `1px solid ${theme.palette.divider}`,
}))

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 1, 0, 2),

  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}))

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  width: '100%',
  '& .MuiInputBase-input': {
    fontSize: 14,
    padding: theme.spacing(1.5, 1),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
  },
}))

const StyledToggleButtonGroup = styled(ToggleButtonGroup)(({ theme }) => ({
  '& .MuiToggleButtonGroup-grouped': {
    margin: theme.spacing(0.5),
    border: 0,
    '&.Mui-disabled': {
      border: 0,
    },
    '&:not(:first-of-type)': {
      borderRadius: theme.shape.borderRadius,
    },
    '&:first-of-type': {
      borderRadius: theme.shape.borderRadius,
    },
  },
}))

const StyledToggleButton = styled(ToggleButton)(({ theme }) => ({
  '&.MuiToggleButton-root': {
    padding: '3px 10px',
  },
  '&.Mui-selected': {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
    },
  },
}))

const PopUpSearchBox: FC = () => {
  const dispatch = useDispatch()
  const { popUpSearchBox } = useAppSelector((state) => state.common)
  const [searchFor, setSearchFor] = useState<string | undefined>(undefined)

  const handleChange = (event: MouseEvent<HTMLElement>, newSearchFor: string): void => {
    setSearchFor(newSearchFor)
  }

  const handleClose = (): void => {
    dispatch(setPopUpSearchBox(false))
  }

  return (
    <Dialog
      open={popUpSearchBox}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleClose}
      aria-describedby="dialog-search-box"
      PaperProps={{
        elevation: 1,
        sx: {
          margin: 1,
        },
      }}
      maxWidth="sm"
      fullWidth
      sx={{
        '& .MuiDialog-container': {
          alignItems: 'flex-start',
        },
      }}
    >
      <Box
        sx={{
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Box
          sx={{
            backgroundColor: 'background.paper',
            boxShadow: '0 5px 23px 0 rgb(10 10 10 / 12%)',
            borderRadius: 1,
            width: 660,
          }}
        >
          <Search>
            <SearchIconWrapper>
              <SearchIcon sx={{ fontSize: 18, color: 'text.disabled' }} />
            </SearchIconWrapper>
            <StyledInputBase placeholder="Search…" inputProps={{ 'aria-label': 'search' }} />
          </Search>
          <Box sx={{ px: 2, py: 1, display: 'flex', alignItems: 'center' }}>
            <Typography sx={{ mr: 2 }} variant="subtitle2" color="text.secondary">
              Search for:
            </Typography>
            <StyledToggleButtonGroup
              size="small"
              value={searchFor}
              exclusive
              /** <- only allow one child to be selected */ onChange={handleChange}
            >
              {searchTypes.map(({ value, label, icon }) => (
                <StyledToggleButton key={value} value={value}>
                  <Box sx={{ display: 'flex', alignItems: 'center' }}>
                    {icon}
                    <Typography sx={{ textTransform: 'capitalize', fontSize: 14, ml: 1 }}>{label}</Typography>
                  </Box>
                </StyledToggleButton>
              ))}
            </StyledToggleButtonGroup>
          </Box>
        </Box>
      </Box>
    </Dialog>
  )
}

export default PopUpSearchBox
