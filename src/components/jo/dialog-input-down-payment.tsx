import { BoxSpinner, DialogContent, DialogTitle } from '@/components/ui';
import { setDialogInputDownPayment, updatePaymentStatusJo } from '@/store/job-order/job-order-actions';
import Box from '@mui/material/Box';
import InputAdornment from '@mui/material/InputAdornment';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import TextField from '@mui/material/TextField';
import Zoom, { ZoomProps } from '@mui/material/Zoom';
import { useAppSelector } from '@src/store/hook';
import { ChangeEvent, FC, ReactElement, forwardRef, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

const Transition = forwardRef<unknown, ZoomProps>(
  (props, ref): ReactElement => (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Zoom unmountOnExit ref={ref} {...props} />
  ),
);
const DialogInputDownPayment: FC = () => {
  const dispatch = useDispatch();
  const { dialogInputDownPayment } = useAppSelector((state) => state.jobOrder);
  const { open, id: joId, totalOrder } = dialogInputDownPayment;
  const [value, setValue] = useState<number | null>(null);
  const [disabledButton, setDisabledButton] = useState(true);

  const handleClose = (): void => {
    dispatch(setDialogInputDownPayment(false, null, 0));
  };

  const handleSubmit = (): void => {
    if (Number(value) > 0)
      dispatch(updatePaymentStatusJo({ job_order_id: joId, payment_status_id: 2, down_payment: value }));
  };

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setValue(Number(event.target.value));
  };

  useEffect(() => {
    if (value > totalOrder) {
      setDisabledButton(true);
    } else {
      setDisabledButton(false);
    }
  }, [value]);

  // console.log('value', setValue)

  return (
    <div>
      <Dialog
        open={open && Boolean(joId)}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="dialog-input-down-payment"
        PaperProps={{ elevation: 1, sx: { position: 'relative' } }}
        maxWidth="sm"
        fullWidth
      >
        <DialogTitle>Update Status Pembayaran</DialogTitle>
        <DialogContent sx={{ pt: 4 }}>
          <Box sx={{ display: 'flex', flexDirection: 'column' }}>
            <TextField
              fullWidth
              name="down_payment"
              margin="normal"
              size="small"
              type="number"
              label="Nominal DP"
              helperText="Isi nominal DP"
              onChange={handleInputChange}
              InputProps={{ startAdornment: <InputAdornment position="start">Rp</InputAdornment> }}
              inputProps={{ inputMode: 'numeric', pattern: '[0-9]' }}
            />
            <Button onClick={handleSubmit} sx={{ ml: 'auto' }} variant="contained" disableElevation>
              Simpan
            </Button>
          </Box>
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default DialogInputDownPayment;
