import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import { Divider, Typography, useMediaQuery } from '@mui/material';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { styled, useTheme } from '@mui/material/styles';
import { FC, ReactElement, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';

import { navigations, otherNavigations } from '@src/lib/navigation';
import { setSidebarDrawer } from '@src/store/common/actions';
import { useAppSelector } from '@src/store/hook';
import { APP_NAME, SIDEBAR_DRAWER_WIDTH } from '@src/utils/constants';

// Components.
import SidebarUserInfo from './sidebar-user-info';

// Selectors.
import { getAuthRootState } from '@/store/auth/auth-selectors';

// Interfaces.
import { SidebarNavigation } from '@/interfaces/sidebar-navigation';

// Assets.
import SampleLogo from '@/assets/images/sample-logo.png';
import { BaseDialog } from '../base';

import InfoIcon from '@mui/icons-material/Info';
import { blue } from '@mui/material/colors';

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  position: 'relative',
  padding: theme.spacing(3),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-start',
}));

const Sidebar: FC = () => {
  const theme = useTheme();
  const navigate = useNavigate();
  const [dialogAppInfo, setDialogAppInfo] = useState<boolean>(false);
  const { isAuthenticated, authenticatedUser: user } = useAppSelector((state) => getAuthRootState(state));
  const { visibleSidebarDrawer: isVisible } = useAppSelector((state) => state.common);
  const dispatch = useDispatch();
  const { pathname } = useLocation();

  const matchSmallScreen = useMediaQuery(theme.breakpoints.down('md'));

  const handleDrawerClose = (): void => {
    dispatch(setSidebarDrawer(false));
  };

  const renderNavItem = (nav: SidebarNavigation, index: number, isActive: boolean): ReactElement => (
    <ListItem
      disablePadding
      button
      disableRipple
      key={String(nav.path + index)}
      onClick={() => navigate(nav.path)}
      sx={{
        mb: 0.5,
        px: 2.2,
        py: 1,
        borderRadius: (theme) => `${Number(theme.shape.borderRadius) * 2}px !important`,
        color: 'text.secondary',
        ...(isActive && {
          backgroundColor: 'primary.main',
          color: 'primary.contrastText',
          '& svg': {
            color: '#fff !important',
            fill: '#fff !important',
          },
        }),
        '&:hover': {
          backgroundColor: 'primary.light',
          ...(isActive && {
            '&:hover': {
              backgroundColor: 'primary.main',
              color: 'primary.contrastText',
            },
          }),
        },
      }}
    >
      <ListItemIcon
        sx={{
          minWidth: 40,
          color: 'text.secondary',
          '& span': {
            height: 18,
          },
          ...(isActive && {
            color: 'text.primary',
            '& svg': {
              color: '#fff !important',
              fill: '#fff !important',
            },
          }),
        }}
      >
        {isActive ? nav.activeIcon : nav.icon}
      </ListItemIcon>
      <ListItemText>{nav.label}</ListItemText>
    </ListItem>
  );

  return (
    <>
      <Drawer
        PaperProps={{
          sx: {
            width: SIDEBAR_DRAWER_WIDTH,
            boxSizing: 'border-box',
            border: 'none !important',
            boxShadow: 1,
            transition: (theme) =>
              `${theme.transitions.create(['transform'], {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen * 2,
              })} !important`,
          },
        }}
        variant={matchSmallScreen ? 'temporary' : 'persistent'}
        anchor="left"
        open={isVisible}
        onClose={handleDrawerClose}
      >
        <Box
          sx={{
            position: 'relative',
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
          }}
        >
          <DrawerHeader>
            <Box sx={{ width: 36, height: 'auto', mr: 2 }} component="img" src={SampleLogo} alt="Sample logo" />
            <Box onClick={() => setDialogAppInfo(true)}>
              <Typography
                component="h1"
                sx={{
                  fontWeight: 'bold',
                  fontSize: 17,
                  color: 'text.primary',
                }}
              >
                {APP_NAME}
              </Typography>
              <Box
                sx={{
                  px: 1.6,
                  py: 0.4,
                  fontSize: '0.74rem',
                  borderRadius: 5,
                  textAlign: 'center',
                  display: 'inline-block',
                  backgroundColor: '#04a572',
                  color: 'primary.contrastText',
                }}
              >
                Development Version
              </Box>
            </Box>
          </DrawerHeader>

          <Box sx={{ px: 4, mt: 1, mb: 2 }}>
            <Divider />
          </Box>

          <Box sx={{ overflowY: 'auto', paddingBottom: '80px', px: 3 }}>
            <List
              sx={{
                minHeight: 3,
              }}
            >
              {navigations.map((nav, index) => {
                const isActive: boolean = pathname === nav.path;
                return renderNavItem(nav, index, isActive);
              })}
            </List>

            <List sx={{ mt: 2 }}>
              {otherNavigations.map((nav, index) => {
                const isActive: boolean = pathname === nav.path;
                return renderNavItem(nav, index, isActive);
              })}
            </List>
          </Box>

          {isAuthenticated && user && (
            <Box
              sx={{
                mt: 'auto',
                mb: 0,
                position: 'absolute',
                bottom: 0,
                left: 0,
                width: '100%',
                backgroundColor: 'background.paper',
              }}
            >
              <SidebarUserInfo user={user} />
            </Box>
          )}
        </Box>
      </Drawer>

      {/* Dialog app info */}
      <BaseDialog
        // disableDialogActions
        disableCloseButton
        disableCancelButton
        confirmButtonText="Okay!"
        title="Hey !"
        open={dialogAppInfo}
        // icon={<InfoIcon sx={{ color: 'primary.main' }} />}
        onClose={() => setDialogAppInfo(false)}
        onConfirm={() => setDialogAppInfo(false)}
      >
        <Typography>
          Aplikasi ini masih dalam tahap pengembangan, beberapa fitur belum bisa digunakan dan mungkin masih terdapat
          bug.
        </Typography>
        <Typography>Jika kamu menemukan bug mohon untuk melaporkan</Typography>
        <Typography sx={{ mt: 4 }}>Thank you!</Typography>
      </BaseDialog>
    </>
  );
};

export default Sidebar;
