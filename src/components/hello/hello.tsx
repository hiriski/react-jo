import { FC } from 'react'

const HelloComponent: FC = () => <h3>Hello</h3>

export default HelloComponent
