import { FC, ReactNode } from 'react';

// date-fns
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// Provider.
import { LocalizationProvider } from '@mui/x-date-pickers';

interface Props {
  children: ReactNode;
}

const MuiDateAdapterProvider: FC<Props> = ({ children }) => {
  return <LocalizationProvider dateAdapter={AdapterDateFns}>{children}</LocalizationProvider>;
};

export default MuiDateAdapterProvider;
