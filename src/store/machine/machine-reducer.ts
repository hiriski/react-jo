// Enum action types.
import { MACHINE_ACTION_TYPES as ActionTypes } from './machine-action-types.enum';

// Union action types.
import { MachineActions } from './machine-actions';

// Interfaces.
import { IMachine } from '@/interfaces/machine';

export interface MachineState {
  listMachine: {
    isFetching: boolean;
    isError: boolean;
    data: Array<IMachine> | null;
  };
}

const initialState: MachineState = {
  listMachine: {
    isFetching: false,
    isError: false,
    data: [],
  },
};

const machineReducer = (state: MachineState = initialState, action: MachineActions): MachineState => {
  switch (action.type) {
    case ActionTypes.FETCH_LIST_MACHINE_LOADING:
      return {
        ...state,
        listMachine: {
          ...state.listMachine,
          isFetching: true,
          isError: false,
        },
      };
    case ActionTypes.FETCH_LIST_MACHINE_FAILURE:
      return {
        ...state,
        listMachine: {
          ...state.listMachine,
          isFetching: false,
          isError: true,
        },
      };
    case ActionTypes.FETCH_LIST_MACHINE_SUCCESS:
      return {
        ...state,
        listMachine: {
          ...state.listMachine,
          isFetching: false,
          isError: false,
          data: action.payload,
        },
      };

    default:
      return state;
  }
};

export default machineReducer;
