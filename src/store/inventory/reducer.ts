import { TBrandMaterialPaper, TBrandWithMaterialPaper, TMaterialPaper } from '@src/types/inventory'
import { isError } from 'lodash'

import { InventoryAction } from './actions'
import * as ActionTypes from './constants'
import { TPayloadDialogAddEditInventory, TPayloadDrawerDetailInventory } from './types'

export interface InventoryState {
  isFetching: boolean
  isError: boolean
  listBrandMaterialPaper: Array<TBrandMaterialPaper> | null
  brandMaterialPaper: TBrandMaterialPaper | null

  listMaterialPaper: Array<TMaterialPaper> | null
  materialPaper: TMaterialPaper | null

  dialogAddEditInventory: TPayloadDialogAddEditInventory
  drawerDetailInventory: TPayloadDrawerDetailInventory
  dialogAddEditBrandMaterial: {
    open: boolean
    id: number | null
  }

  create: {
    isLoading: boolean
    isError: boolean
    data: TMaterialPaper | null
  }

  detail: {
    isFetching: boolean
    isError: boolean
    data: TMaterialPaper | null
  }

  updateLoading: boolean
  updateError: boolean
  updateSuccess: boolean

  listBrandWithMaterialPapers: {
    isFetching: boolean
    isError: boolean
    data: Array<TBrandWithMaterialPaper> | null
  }

  dialogAddStockMaterialPaper: {
    open: boolean
    id: number | null
    current_stock: number | null
  }
  addStockMaterialPaper: {
    isLoading: boolean
    isError: boolean
    data: TMaterialPaper | null
  }
}

const initialState: InventoryState = {
  isFetching: false,
  isError: false,

  listBrandMaterialPaper: [],
  brandMaterialPaper: null,

  listMaterialPaper: [],
  materialPaper: null,

  dialogAddEditInventory: {
    open: false,
    brand_id: null,
    type: null,
    id: null,
  },
  drawerDetailInventory: {
    open: false,
    type: null,
    id: null,
  },

  dialogAddEditBrandMaterial: {
    open: false,
    id: null,
  },

  create: {
    isLoading: false,
    isError: false,
    data: null,
  },

  updateLoading: false,
  updateError: false,
  updateSuccess: false,

  listBrandWithMaterialPapers: {
    isFetching: false,
    isError: false,
    data: null,
  },

  detail: {
    isFetching: false,
    isError: false,
    data: null,
  },
  dialogAddStockMaterialPaper: {
    open: false,
    id: null,
    current_stock: null,
  },
  addStockMaterialPaper: {
    isLoading: false,
    isError: false,
    data: null,
  },
}

const inventoryReducer = (state: InventoryState = initialState, action: InventoryAction): InventoryState => {
  switch (action.type) {
    case ActionTypes.SET_DIALOG_ADD_EDIT_INVENTORY:
      return {
        ...state,
        dialogAddEditInventory: action.payload,
      }

    case ActionTypes.SET_DIALOG_ADD_EDIT_BRAND_MATERIAL:
      return {
        ...state,
        dialogAddEditBrandMaterial: {
          open: action.payload.open,
          id: action.payload.id ?? null,
        },
      }

    case ActionTypes.SET_DRAWER_DETAIL_INVENTORY:
      return {
        ...state,
        drawerDetailInventory: action.payload,
      }

    case ActionTypes.FETCHING_LIST_BRAND_MATERIAL_PAPER_LOADING:
      return {
        ...state,
        isFetching: true,
        isError: false,
        listBrandMaterialPaper: [],
      }
    case ActionTypes.FETCHING_LIST_BRAND_MATERIAL_PAPER_FAILURE:
      return {
        ...state,
        isFetching: false,
        isError: true,
        listBrandMaterialPaper: [],
      }
    case ActionTypes.FETCHING_LIST_BRAND_MATERIAL_PAPER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        listBrandMaterialPaper: action.payload,
      }

    case ActionTypes.FETCHING_LIST_MATERIAL_PAPER_LOADING:
      return {
        ...state,
        isFetching: true,
        isError: false,
        listMaterialPaper: [],
      }
    case ActionTypes.FETCHING_LIST_MATERIAL_PAPER_FAILURE:
      return {
        ...state,
        isFetching: false,
        isError: true,
        listMaterialPaper: [],
      }
    case ActionTypes.FETCHING_LIST_MATERIAL_PAPER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        listMaterialPaper: action.payload,
      }

    /** Fetching brand list with material papers (v2) */
    case ActionTypes.FETCHING_LIST_MATERIAL_PAPER_V2_LOADING:
      return {
        ...state,
        listBrandWithMaterialPapers: {
          isFetching: true,
          isError: false,
          data: null,
        },
      }
    case ActionTypes.FETCHING_LIST_MATERIAL_PAPER_V2_FAILURE:
      return {
        ...state,
        listBrandWithMaterialPapers: {
          isFetching: false,
          isError: true,
          data: null,
        },
      }
    case ActionTypes.FETCHING_LIST_MATERIAL_PAPER_V2_SUCCESS:
      return {
        ...state,
        listBrandWithMaterialPapers: {
          isFetching: false,
          isError: false,
          data: action.payload,
        },
      }

    /** Create inventory (Material paper) */
    case ActionTypes.CREATE_INVENTORY_MATERIAL_PAPER_LOADING:
      return {
        ...state,
        create: {
          isLoading: true,
          isError: false,
          data: null,
        },
      }
    case ActionTypes.CREATE_INVENTORY_MATERIAL_PAPER_FAILURE:
      return {
        ...state,
        create: {
          isLoading: false,
          isError: true,
          data: null,
        },
      }
    case ActionTypes.CREATE_INVENTORY_MATERIAL_PAPER_SUCCESS:
      return {
        ...state,
        create: {
          isLoading: false,
          isError: false,
          data: action.payload,
        },
      }

    /** Fetch material paper by id */
    case ActionTypes.FETCH_MATERIAL_PAPER_LOADING:
      return {
        ...state,
        detail: {
          isFetching: true,
          isError: false,
          data: null,
        },
      }
    case ActionTypes.FETCH_MATERIAL_PAPER_FAILURE:
      return {
        ...state,
        detail: {
          isFetching: false,
          isError: true,
          data: null,
        },
      }
    case ActionTypes.FETCH_MATERIAL_PAPER_SUCCESS:
      return {
        ...state,
        detail: {
          isFetching: false,
          isError: false,
          data: action.payload,
        },
      }

    /** Add stock material paper */
    case ActionTypes.SET_DIALOG_ADD_STOCK_MATERIAL_PAPER:
      return {
        ...state,
        dialogAddStockMaterialPaper: {
          open: action.payload.open,
          id: action.payload.id ?? null,
          current_stock: action.payload.current_stock ?? null,
        },
      }
    case ActionTypes.ADD_STOCK_LENGTH_MATERIAL_PAPER_LOADING:
      return {
        ...state,
        addStockMaterialPaper: {
          isLoading: true,
          isError: false,
          data: null,
        },
      }
    case ActionTypes.ADD_STOCK_LENGTH_MATERIAL_PAPER_FAILURE:
      return {
        ...state,
        addStockMaterialPaper: {
          isLoading: false,
          isError: true,
          data: null,
        },
      }
    case ActionTypes.ADD_STOCK_LENGTH_MATERIAL_PAPER_SUCCESS:
      return {
        ...state,
        addStockMaterialPaper: {
          isLoading: false,
          isError: false,
          data: action.payload,
        },
      }

    default:
      return state
  }
}

export default inventoryReducer
