import * as yup from 'yup';

const validationErrorFormatNumber = 'Hanya format angka yang diperbokehkan';
const validationErrorOnlyDigits = 'Format hanya boleh angka tanpa (tanda titik)';
const digitsOnly = (value: string): boolean => /^\d+$/.test(value);

export const jobOrderFormSchema = yup.object().shape({
  title: yup
    .string()
    .required('Nama Job Order tidak boleh kosong, ini akan memudahkan untuk proses pencarian')
    .min(3, 'Nama Job Order minimal 3 karakter')
    .max(255, 'Opsss. Nama Jo terlalu panjang'),
  product_id: yup.string().nullable(true),
  category_id: yup.number().required('Pilih jenis jo'),
  customer_id: yup.string().nullable(true),
  payment_status_id: yup.number().required('Status pembayaran wajib di isi'),
  payment_method_id: yup.number().nullable(true),
  bank_account_id: yup.number().nullable(true),
  order_date: yup.string().required('Isi tanggal order'),
  due_date: yup.string().required('Isi tanggal deadline'),
  body: yup.string().nullable(true),
  down_payment: yup
    .number()
    .positive()
    .integer()
    .when('payment_status_id', {
      is: 2,
      then: yup
        .number()
        .required('Masukan jumlah dp')
        .typeError('Masukan format angka yang benar')
        .min(1, 'Minimal quantity 1')
        .max(99999999999, 'Maksimal jumlah dp 99999999999'),
    })
    .nullable(true),
  order_quantity: yup
    .number()
    .required('Quantity tidak boleh kosong')
    .positive()
    .integer()
    .typeError(validationErrorFormatNumber)
    .min(1, 'Minimal quantity 1')
    .max(999999, 'Maksimal quantity 999999'),
  order_price: yup
    .string()
    .required('Harga tidak boleh kosong')
    .test('Digits only', validationErrorOnlyDigits, digitsOnly),
  // .min(10, 'Mininum 10 rupiah')
  // .max(99999999999, 'Maksimum 99999999999')
  tax: yup.number().nullable(true).typeError(validationErrorFormatNumber),
  discount: yup.number().nullable(true).typeError(validationErrorFormatNumber),
  discount_type: yup.mixed().oneOf(['percentage', 'fixed_price', null]).nullable(true),
  delivery_cost: yup.number().nullable(true).typeError(validationErrorFormatNumber),
  additional_costs: yup.array(
    yup.object({
      additional_cost_name: yup.string(),
      additional_cost_value: yup.number().nullable(true).typeError(validationErrorFormatNumber),
    }),
  ),
  notes: yup.string().required('Isi catatan Job Order. Misalnya: "Diambil siang setelah jam makan siang"'),
  images: yup.array().nullable(true),
  customer: yup
    .object({
      name: yup.string(),
      customer_type: yup.string(),
      email: yup.string().nullable(true),
      phone_number: yup
        .string()
        .nullable(true)
        .min(9, 'Minimal 9 karakter setelah +62')
        .max(16, 'Nomor hp/wa kepanjangan'),
      photo_url: yup.string().nullable(true),
    })
    .when('is_new_customer', {
      is: true,
      then: yup.object({
        name: yup.string().required('Nama pelanggan tidak boleh kosong!'),
      }),
    })
    .nullable(true),
  materials: yup
    .array(
      yup.object({
        material_id: yup.number().required('Pilih bahan baku minimal 1'),
        dimension_material_length: yup.number().nullable(true).typeError(validationErrorFormatNumber),
        dimension_material_width: yup
          .number()
          .nullable(true)
          .when('dimension_material_length', (materialLength: number, schema: any) => {
            return schema.test({
              test: (dimension_material_width: any) => {
                if (!dimension_material_width) return true;
                return dimension_material_width <= materialLength;
              },
              message: 'Lebar Material (X) harusnya lebih kecil dari  Panjang Material (Y)!',
            });
          })
          .typeError(validationErrorFormatNumber),
        quantity_material_pcs: yup.number().nullable(true).typeError(validationErrorFormatNumber),
        quantity_material_sheet: yup.number().nullable(true).typeError(validationErrorFormatNumber),
      }),
    )
    .min(1)
    .required('Silahkan pilih bahan baku'),

  is_new_customer: yup.boolean(), // Just flag for new customer.
  generate_invoice: yup.boolean(),
});
