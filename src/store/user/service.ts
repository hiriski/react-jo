import { TCreateUser, TResponseListUser, TUser } from '@src/types/user'
import api from '@src/utils/http'
import { AxiosResponse } from 'axios'

const UserAPI = {
  findAll: async (params?: Record<string, string>): Promise<AxiosResponse<TResponseListUser>> =>
    api.get(`/user`, { params }),
  find: async (id: number): Promise<AxiosResponse<TUser>> => api.get(`/user/${id}`),
  create: async (body: TCreateUser): Promise<AxiosResponse<TUser>> => api.post(`/user`, body),
  update: async (id: number, body: TCreateUser): Promise<AxiosResponse<TUser>> => api.put(`/user/${id}`, body),
  delete: async (id: number): Promise<AxiosResponse> => api.delete(`/user/${id}`),
}
export default UserAPI
