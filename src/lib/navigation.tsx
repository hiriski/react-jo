import { SidebarNavigation } from '@src/interfaces/sidebar-navigation';
import { TNavigation } from '@src/types/navigation';
import { ROUTES } from '@src/utils/constants';
import {
  BagHandle,
  BagHandleOutline,
  Bookmarks,
  BookmarksOutline,
  FolderOpen,
  FolderOpenOutline,
  Grid,
  GridOutline,
  Heart,
  HeartOutline,
  LockClosed,
  LockClosedOutline,
  People,
  PeopleOutline,
  Receipt,
  ReceiptOutline,
  Settings,
  SettingsOutline,
  StatsChart,
  StatsChartOutline,
} from 'react-ionicons';

const iconSize = 18;

export const navigations: Array<SidebarNavigation> = [
  {
    label: 'Dashboard',
    icon: <GridOutline width="18px" height="18px" />,
    activeIcon: <Grid width="18px" height="18px" />,
    path: '/',
  },
  // {
  //   label: 'Analytics',
  //   icon: <StatsChartOutline width="18px" height="18px" />,
  //   activeIcon: <StatsChart width="18px" height="18px" />,
  //   path: ROUTES.ANALYTICS,
  // },
  {
    label: 'Stock Opname',
    icon: <FolderOpenOutline width="18px" height="18px" />,
    activeIcon: <FolderOpen width="18px" height="18px" />,
    path: ROUTES.WAREHOUSE,
  },
  {
    label: 'Job Order',
    icon: <BookmarksOutline width="18px" height="18px" />,
    activeIcon: <Bookmarks width="18px" height="18px" />,
    path: ROUTES.JO,
  },
  {
    label: 'Produk',
    icon: <BagHandleOutline width="18px" height="18px" />,
    activeIcon: <BagHandle width="18px" height="18px" />,
    path: ROUTES.PRODUCT,
  },
  {
    label: 'Invoices',
    icon: <ReceiptOutline width="18px" height="18px" />,
    activeIcon: <Receipt width="18px" height="18px" />,
    path: ROUTES.INVOICE,
  },
  {
    label: 'Customer',
    icon: <HeartOutline width="18px" height="18px" />,
    activeIcon: <Heart width="18px" height="18px" />,
    path: ROUTES.CUSTOMER,
  },
  {
    label: 'Manajemen User',
    icon: <PeopleOutline width="18px" height="18px" />,
    activeIcon: <People width="18px" height="18px" />,
    path: ROUTES.USER,
  },
  {
    label: 'Manajemen Role',
    icon: <LockClosedOutline width="18px" height="18px" />,
    activeIcon: <LockClosed width="18px" height="18px" />,
    path: ROUTES.ROLE,
  },
];

export const otherNavigations: Array<SidebarNavigation> = [
  {
    label: 'Settings',
    icon: <SettingsOutline width="18px" height="18px" />,
    activeIcon: <Settings width="18px" height="18px" />,
    path: ROUTES.SETTING,
  },
  {
    label: 'Pusat Bantuan',
    icon: <GridOutline width="18px" height="18px" />,
    activeIcon: <Grid width="18px" height="18px" />,
    path: ROUTES.SUPPORT_CENTER,
  },
];
