import { TypographyOptions } from '@mui/material/styles/createTypography';

export const fontFamily = ['Plus Jakarta Sans', 'sans-serif'].join(',');

const typography: TypographyOptions = {
  fontFamily,
  fontWeightLight: 300,
  fontWeightRegular: 500,
  fontWeightMedium: 600,
  fontWeightBold: 700,
  h1: {
    fontWeight: 700,
    fontSize: 28,
  },
  h2: {
    fontWeight: 700,
    fontSize: 26,
  },
  h3: {
    fontSize: 22,
    fontWeight: 700,
  },
  h4: {
    fontSize: 20,
    fontWeight: 700,
  },
  h5: {
    fontSize: 17,
    fontWeight: 700,
  },
  h6: {
    fontSize: 15,
    fontWeight: 700,
  },
  body1: {
    fontSize: '0.9rem',
  },
  body2: {
    fontSize: '0.85rem',
  },
  subtitle1: {
    fontSize: '0.75rem',
    fontWeight: 500,
  },
  subtitle2: {
    fontSize: '0.72rem',
    fontWeight: 600,
  },
};

export default typography;
