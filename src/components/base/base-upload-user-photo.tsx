import { FC, ReactElement, useState } from 'react';
import { styled } from '@mui/material';
import Box from '@mui/material/Box';
import { alpha } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import ReactImagesUploading, { ImageListType, ImageType, ImageUploadingPropsType } from 'react-images-uploading';

const IMAGE_SIZE = 144;

// Mui icons.
import PhotoCameraIcon from '@mui/icons-material/PhotoCamera';

interface IStyledUploadPhotoMarker {
  isDragging: boolean;
  hasSelectedImage: boolean;
}

const StyledUploadPhotoMarker = styled(Box, {
  shouldForwardProp: (prop) => prop !== 'isDragging',
})<IStyledUploadPhotoMarker>(({ theme, isDragging, hasSelectedImage }) => ({
  position: 'relative',
  height: IMAGE_SIZE,
  width: IMAGE_SIZE,
  borderRadius: IMAGE_SIZE,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  textAlign: 'center',
  cursor: 'pointer',
  borderWidth: 1,
  border: 'solid',
  borderStyle: 'dotted',
  borderColor: isDragging ? theme.palette.primary.main : `${alpha(theme.palette.primary.main, 0.5)}`,
  backgroundColor: isDragging ? alpha(theme.palette.primary.main, 0.2) : alpha(theme.palette.primary.main, 0.08),
  color: theme.palette.text.secondary,

  overflow: 'hidden',

  opacity: 0.75,

  '& h6': {
    fontSize: 11,
    margin: '0 1rem',
    '& strong': {
      color: theme.palette.primary.main,
    },
  },

  '& svg': {
    fontSize: 34,
    color: theme.palette.primary.main,
  },

  '&:hover': {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    opacity: 1,
    '& h6': {
      '& strong': {
        color: theme.palette.primary.contrastText,
      },
    },
    '& svg': {
      color: theme.palette.primary.contrastText,
    },
  },

  '& > .MuiBox-root': {
    opacity: 0.7,
  },

  ...(hasSelectedImage && {
    opacity: 1,
    borderColor: 'transparent',
    '&:hover': {
      borderWidth: 1,
      border: 'solid',
      borderStyle: 'dotted',
      borderColor: isDragging ? theme.palette.primary.main : `${alpha(theme.palette.primary.main, 0.5)}`,
      backgroundColor: isDragging ? alpha(theme.palette.primary.main, 0.2) : alpha(theme.palette.primary.main, 0.08),
    },
  }),
}));

const StyledPlaceholder = styled(Box)(({ theme }) => ({
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: IMAGE_SIZE,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection: 'column',
}));

const StyledSelectedPhoto = styled(Box)(({ theme }) => ({
  '& > div': { display: 'none' },
  '&:hover': {
    '& > div': {
      display: 'flex',
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.primary.contrastText,
      '& h6': {
        fontSize: 11,
        margin: '0 1rem',
        '& strong': {
          color: theme.palette.primary.contrastText,
        },
      },
      '& svg': {
        color: theme.palette.primary.contrastText,
      },
    },
  },
}));

interface Props extends Omit<ImageUploadingPropsType, 'value' | 'onChange'> {
  photo: string;
  onChange: (newPhoto: string) => void;
}

export const BaseUploadUserPhoto: FC<Props> = (props) => {
  const [images, setImages] = useState<ImageListType>([]);
  const { photo, onChange, ...rest } = props;

  /**
   *
   * Handle change upload image.
   * @param imageList
   * @param addUpdateIndex
   */
  const handleChange = (imageList: ImageListType, addUpdateIndex: number[] | undefined): void => {
    setImages(imageList);
    onChange(imageList[0].dataURL);
  };

  const renderPlaceholder = (): ReactElement => (
    <StyledPlaceholder>
      <PhotoCameraIcon />
      <Typography component="h6">
        <strong>Browse </strong>
        <span>or drag image here</span>
      </Typography>
    </StyledPlaceholder>
  );

  return (
    <ReactImagesUploading {...rest} value={images} maxNumber={1} multiple={false} onChange={handleChange}>
      {({ imageList, onImageUpload, onImageUpdate, onImageRemove, isDragging, dragProps }) => (
        <StyledUploadPhotoMarker
          hasSelectedImage={images.length > 0}
          isDragging={isDragging}
          onClick={images.length > 0 ? (index) => onImageUpdate(0) : onImageUpload}
          {...dragProps}
        >
          {images.length > 0 ? (
            <StyledSelectedPhoto>
              <Box
                component="img"
                src={images[0].dataURL}
                alt="Selected photo"
                sx={{ width: '100%', height: 'auto' }}
              />
              {renderPlaceholder()}
            </StyledSelectedPhoto>
          ) : (
            renderPlaceholder()
          )}
        </StyledUploadPhotoMarker>
      )}
    </ReactImagesUploading>
  );
};
