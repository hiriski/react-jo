import {
  Avatar,
  Box,
  Grid,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@mui/material'
import { BoxSpinner, SectionTitle } from '@src/components/ui'
import { useAppSelector } from '@src/store/hook'
import { TBrandMaterialPaper } from '@src/types/inventory'
import { FC } from 'react'

import {
  BrandMaterialPaperEmpty,
  BrandMaterialPaperItem,
  BrandMaterialPaperItemSkeleton,
} from '../index'

type Props = {
  items: Array<TBrandMaterialPaper>
}

const BrandMaterialPaperList: FC<Props> = ({ items }: Props) => {
  const { isFetching } = useAppSelector((state) => state.inventory)
  return (
    <>
      <SectionTitle title="Brand Material" />
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
          mb: 6,
        }}
      >
        <Grid container spacing={1}>
          <Grid item xs={12} md={6}>
            <Box sx={{ p: 1, boxShadow: '0 12px 32px 0 rgb(10 10 10 / 8%)' }}>
              {isFetching ? (
                <BoxSpinner height={200} />
              ) : (
                <List>
                  {items.map((item) => (
                    <BrandMaterialPaperItem key={String(item.id)} item={item} />
                  ))}
                </List>
              )}
            </Box>
          </Grid>
        </Grid>
      </Box>
      {!isFetching && items.length === 0 && (
        <BrandMaterialPaperEmpty text="Empty :(" />
      )}
    </>
  )
}

export default BrandMaterialPaperList
