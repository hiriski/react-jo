import { RoleIdConstants } from '@/constants/role-id-constants';
import { IUser } from '@/interfaces/user';

export const ROLE_IDS = {
  SA: 1,
  ADMIN: 2,
  WAREHOUSE_TEAM: 3,
  PRODUCTION_TEAM: 4,
  OWNER: 5,
  MANAGEMENT: 6,
  CASHIER: 7,
  CS: 8,
  MAINTAINER: 9,
};

const checkRole = (userRoleId: number, roleId: number): boolean => {
  if (!userRoleId || !roleId) return false;
  return userRoleId === roleId;
};

export const isSuperAdmin = (user: IUser): boolean => checkRole(user.role_id, RoleIdConstants.SuperAdmin);

export const isAdmin = (user: IUser): boolean => checkRole(user.role_id, RoleIdConstants.Admin);

export const isOwner = (user: IUser): boolean => checkRole(user.role_id, RoleIdConstants.Owner);

export const isCustomerService = (user: IUser): boolean => checkRole(user.role_id, RoleIdConstants.CustomerService);

export const isCashier = (user: IUser): boolean => checkRole(user.role_id, RoleIdConstants.Cashier);

export const isWarehouseTeam = (user: IUser): boolean => checkRole(user.role_id, RoleIdConstants.WarehouseTeam);

export const isProductionTeam = (user: IUser): boolean => checkRole(user.role_id, RoleIdConstants.ProductionTeam);

export const isManagement = (user: IUser): boolean => checkRole(user.role_id, RoleIdConstants.Management);

export const isMaintainer = (user: IUser): boolean => checkRole(user.role_id, RoleIdConstants.Maintainer);
