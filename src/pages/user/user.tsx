import { MainLayout } from '@components/layouts'
import { DialogAddEditUser, DialogDetailUser, DialogUserPermissions, UserTable } from '@components/user'
import Alert from '@mui/material/Alert'
import Box from '@mui/material/Box'
import { useAppSelector } from '@src/store/hook'
import { fetchUserList } from '@src/store/user/actions'
import { FC, useEffect } from 'react'
import { useDispatch } from 'react-redux'

const UserPage: FC = () => {
  const dispatch = useDispatch()
  const { listUser, dialogUserPermissions } = useAppSelector((state) => state.user)
  const { data, isFetching } = listUser
  const { userName, permissions } = dialogUserPermissions

  useEffect(() => {
    dispatch(fetchUserList())
  }, [])

  return (
    <MainLayout>
      <Box sx={{}}>
        <Alert severity="warning" sx={{ mb: 2 }}>
          Halaman ini digunakan untuk me-manage user di dalam aplikasi. Harap berhati-hati.
        </Alert>
        <UserTable isFetching={isFetching} data={data} />
      </Box>
      <DialogAddEditUser />
      <DialogDetailUser />
      <DialogUserPermissions userName={userName} permissions={permissions} />
    </MainLayout>
  )
}

export default UserPage
