import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward'
import Box from '@mui/material/Box'
import CircularProgress from '@mui/material/CircularProgress'
import MuiPagination, { PaginationProps } from '@mui/material/Pagination'
import PaginationItem from '@mui/material/PaginationItem'
import { SxProps } from '@mui/material/styles'
import { FC } from 'react'

type PickedPaginationProps = Pick<PaginationProps, 'page' | 'variant' | 'size' | 'color' | 'count' | 'onChange'>

type Props = PickedPaginationProps & {
  fixed?: boolean
  isFetching: boolean
  sx?: SxProps
}

const Pagination: FC<Props> = (props: Props) => {
  const { count, page, variant, size, color, fixed, isFetching, onChange, sx } = props
  return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'center',
        py: 1.8,
        ...(fixed && {
          bottom: 0,
          left: 0,
          position: 'fixed',
          width: '100%',
          backgroundColor: 'background.paper',
          boxShadow: '0px -8px 30px -10px rgb(150 170 180 / 30%)',
        }),
        ...sx,
      }}
    >
      {isFetching ? (
        <CircularProgress size={24} />
      ) : (
        <MuiPagination
          size={size}
          count={count}
          page={page}
          variant={variant}
          color={color ?? 'primary'}
          onChange={onChange}
          renderItem={(item) => (
            <PaginationItem components={{ previous: ArrowBackIcon, next: ArrowForwardIcon }} {...item} />
          )}
        />
      )}
    </Box>
  )
}

export default Pagination
