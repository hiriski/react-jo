import { FC, ReactElement } from 'react';

// Mui components.
import TextField, { TextFieldProps } from '@mui/material/TextField';

// Hook form
import { Control, Controller, FieldErrors } from 'react-hook-form';

interface Props
  extends Pick<
    TextFieldProps,
    | 'name'
    | 'label'
    | 'type'
    | 'helperText'
    | 'size'
    | 'margin'
    | 'disabled'
    | 'fullWidth'
    | 'multiline'
    | 'rows'
    | 'minRows'
    | 'maxRows'
    | 'InputProps'
  > {
  // eslint-disable-next-line
  control: Control<any>;
  errors: FieldErrors;
}

export const ControlledTextField: FC<Props> = ({
  control,
  errors,
  name,
  label,
  helperText,
  ...rest
}: Props): ReactElement => (
  <Controller
    name={name}
    control={control}
    render={({ field }) => (
      <TextField
        {...field}
        label={label}
        error={Boolean(errors[name]?.message)}
        /**
         * TODO
         * Errors for array and object values
         *
         * example :
         *  - customer.name,
         *  - materials.1.material_id
         */
        helperText={errors[name]?.message ?? helperText}
        {...rest}
      />
    )}
  />
);

ControlledTextField.defaultProps = {
  size: 'small',
  margin: 'dense',
};
