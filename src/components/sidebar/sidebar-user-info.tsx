import { FC } from 'react';

// Mui components.
import { Box, Avatar, Typography, IconButton } from '@mui/material';
import { getInitialsName } from '@/utils/misc';
import { IUser } from '@/interfaces/user';
import LockIcon from '@mui/icons-material/Lock';

// Utils.
import { getUserAvatarFallback } from '@/utils/common';

// Hooks
import { useDispatch } from 'react-redux';

// Action creators.
import { app_setIsLocked } from '@/store/app/app-actions';

interface Props {
  user?: IUser;
}

const SidebarUserInfo: FC<Props> = ({ user }) => {
  const dispatch = useDispatch();

  const handleLockScreen = (): void => {
    dispatch(app_setIsLocked(true));
  };

  return (
    <Box
      sx={{
        px: 4,
        py: 2,
        display: 'flex',
        alignItems: 'center',
      }}
    >
      <Box sx={{ mr: 2 }}>
        <Avatar sx={{ width: 44, height: 44 }} src={user.photo_url || getUserAvatarFallback(user)}>
          {getInitialsName(user.name, true)}
        </Avatar>
      </Box>
      <Box>
        <Typography sx={{ height: 22, overflow: 'hidden', textOverflow: 'elipsis', fontWeight: 'bold' }}>
          {user.name}
        </Typography>
        <Typography variant="subtitle2" color="text.secondary">
          {user.role.name}
        </Typography>
      </Box>
      <IconButton sx={{ ml: 'auto' }} onClick={handleLockScreen}>
        <LockIcon />
      </IconButton>
    </Box>
  );
};

export default SidebarUserInfo;
