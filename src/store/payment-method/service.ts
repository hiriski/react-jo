import { IPaymentMethod } from '@src/types/jo'
import api from '@src/utils/http'
import { AxiosResponse } from 'axios'

const PaymentMethodAPI = {
  findAll: async (): Promise<AxiosResponse<Array<IPaymentMethod>>> => api.get('/payment-method'),
}

export default PaymentMethodAPI
