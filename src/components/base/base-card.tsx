import { FC, ReactNode } from 'react';

// Mui components.
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { SxProps } from '@mui/material';

interface Props {
  children: ReactNode;
  padding?: number;
  title?: string;
  subtitle?: string;
  rightContent?: ReactNode;
  sx?: SxProps;
  sxTitle?: SxProps;
}

export const BaseCard: FC<Props> = ({ children, padding, title, subtitle, rightContent, sx, sxTitle }) => {
  return (
    <Box
      sx={{
        borderRadius: 1,
        boxShadow: 1,
        padding: padding,
        backgroundColor: 'background.paper',
        overflow: 'hidden',
        position: 'relative',
        ...sx,
      }}
    >
      <Box
        sx={{
          display: 'flex',
          alignItem: 'center',
          justifyContent: 'space-between',
          mb: 2,
          flexDirection: {
            xs: 'column',
            md: 'row',
          },
        }}
      >
        {title && (
          <>
            <Box sx={{ mb: 2.25 }}>
              <Typography sx={{ mb: 0.65, ...sxTitle }} component="h3" variant="h5">
                {title}
              </Typography>
              {subtitle && (
                <Typography variant="body2" sx={{ color: 'text.secondary' }}>
                  {subtitle}
                </Typography>
              )}
            </Box>
          </>
        )}
        {rightContent}
      </Box>
      {children}
    </Box>
  );
};

BaseCard.defaultProps = {
  padding: 3,
  rightContent: null,
};
