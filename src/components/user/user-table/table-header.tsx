import Box from '@mui/material/Box'
import Checkbox from '@mui/material/Checkbox'
import TableCell from '@mui/material/TableCell'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import TableSortLabel from '@mui/material/TableSortLabel'
import Typography from '@mui/material/Typography'
import { visuallyHidden } from '@mui/utils'
import { ChangeEvent, FC, MouseEvent } from 'react'

import type { Order } from './types'

interface HeadCell {
  disablePadding: boolean
  id: string /* keyof Data */
  label: string
  numeric: boolean
  enableSort: boolean
  width?: number
}

const headCells: Array<HeadCell> = [
  {
    id: 'name',
    numeric: false,
    disablePadding: true,
    label: 'Name',
    enableSort: false,
    width: 200,
  },
  {
    id: 'roleName',
    numeric: false,
    disablePadding: false,
    label: 'Role',
    enableSort: false,
    width: 140,
  },
  {
    id: 'permissions',
    numeric: false,
    disablePadding: false,
    label: 'Permissions',
    enableSort: false,
    width: 110,
  },
  {
    id: 'status',
    numeric: false,
    disablePadding: false,
    label: 'Status',
    enableSort: false,
    width: 120,
  },
  {
    id: 'createdAt',
    numeric: false,
    disablePadding: false,
    label: 'Created at',
    enableSort: false,
    width: 100,
  },
  {
    id: 'action',
    numeric: true,
    disablePadding: false,
    label: 'Actions',
    enableSort: false,
    width: 80,
  },
]

interface Props {
  numSelected: number
  onRequestSort: (event: MouseEvent<unknown>, property: string /* keyof Data */) => void
  onSelectAllClick: (event: ChangeEvent<HTMLInputElement>) => void
  order: Order
  orderBy: string
  rowCount: number
}

const UserTableHeader: FC<Props> = (props: Props) => {
  const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props
  const createSortHandler = (property: string /* keyof Data */) => (event: MouseEvent<unknown>) => {
    onRequestSort(event, property)
  }

  return (
    <TableHead
      sx={{
        '& *': {
          lineHeight: '1.4 !important',
        },
        '& .MuiTableCell-sizeMedium': {
          padding: '12px 6px !important',
          borderBottom: 'none !important',
        },
      }}
    >
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            size="small"
            color="primary"
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{
              'aria-label': 'select all desserts',
            }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
            sx={{ width: headCell.width ? headCell.width : 'auto' }}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              <Typography sx={{ fontSize: 16, fontWeight: 'bold' }}>{headCell.label}</Typography>
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  )
}

export default UserTableHeader
