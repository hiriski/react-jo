import type { Effect, SagaIterator, SimpleEffect } from '@redux-saga/types';
import { TCreateJo, TUpdateJo } from '@src/types/jo';
import { httpResponseCreated, httpResponseOK } from '@src/utils/http';
import { SagaReturnType, call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';
import Swal from 'sweetalert2';

import { setAlert } from '../alert/actions';
import { AppState } from '../root-reducer';
import {
  addNotesJoFailure,
  addNotesJoLoading,
  addNotesJoSuccess,
  fetchingInvoiceDataSuccess,
  jobOrder_fetchDetailSuccess,
  fetchingListJoFailure,
  fetchingListJoLoading,
  fetchingListJoSuccess,
  fetchListJo as reFetchListJo,
  jobOrder_createJobOrderSuccess,
  setDrawerAddEditJo,
  jobOrder_updateJobOrderSuccess,
  jobOrder_updatePaymentStatusFailure,
  jobOrder_updatePaymentStatusLoading,
  jobOrder_updatePaymentStatusSuccess,
  updatePercentageProgressFailure,
  updatePercentageProgressLoading,
  updatePercentageProgressSuccess,
  updateProductionStatusFailure,
  updateProductionStatusLoading,
  updateProductionStatusSuccess,
  jobOrder_updateJobOrderFailure,
  jobOrder_updateJobOrderLoading,
  jobOrder_createJobOrderLoading,
  jobOrder_startProductionLoading,
  jobOrder_startProductionSuccess,
  jobOrder_startProductionFailure,
  jobOrder_setDialogStartProduction,
  jobOrder_finishProductionLoading,
  jobOrder_finishProductionSuccess,
  jobOrder_finishProductionFailure,
  setDialogDetailJo,
  jobOrder_deleteLoading,
  jobOrder_deleteSuccess,
  jobOrder_deleteFailure,
  jobOrder_setDialogDelete,
  jobOrder_fetchDetailLoading,
} from './job-order-actions';

// Enum
import { JOB_ORDER_ACTION_TYPES as ActionTypes } from './job-order-action-types.enum';

import {
  IJobOrderPayloadUpdatePaymentStatus,
  IPayloadJobOrderDelete,
  IPayloadJobOrderStartProduction,
} from './job-order.interfaces';
import { JobOrderState } from './job-order-reducer';
import JobOrderApiService, { TPayloadGetJoInvoiceData } from './job-order-api-service';
import { IRequestBodyJobOrder } from '@/interfaces/job-order';
import { app_setAlert, app_setLoading } from '../app/app-actions';
import { jobOrder_rootSelector } from './job-order-selectors';
import { UUID } from '@/interfaces/common';
import { ROUTES } from '@/constants';

// Interfaces
import type { SagaEffectWithNavigateFunction } from '@/interfaces/common';

const getRootState = (state: AppState): JobOrderState => state.jobOrder;

// Type definitions of return of result.
type APIResponseListJo = SagaReturnType<typeof JobOrderApiService.findAll>;
type APIResponseCreateJo = SagaReturnType<typeof JobOrderApiService.create>;
type APIResponseUpdateJo = SagaReturnType<typeof JobOrderApiService.update>;
type APIResponseJo = SagaReturnType<typeof JobOrderApiService.find>;
type APIResponseInvoiceData = SagaReturnType<typeof JobOrderApiService.getInvoiceData>;
type APIResponseUpdatePaymentStatus = SagaReturnType<typeof JobOrderApiService.updatePaymentStatus>;
type APIResponseUpdateProductionStatus = SagaReturnType<typeof JobOrderApiService.updateProductionStatus>;
type APIResponseUpdatePercentageStatus = SagaReturnType<typeof JobOrderApiService.updatePercentageProgress>;
type APIResponseAddNotesJo = SagaReturnType<typeof JobOrderApiService.addNotes>;
type APIResponseJobOrderStartProduction = SagaReturnType<typeof JobOrderApiService.startProduction>;
type APIResponseJobOrderFinishProduction = SagaReturnType<typeof JobOrderApiService.finishProduction>;
type APIResponseJobOrderDelete = SagaReturnType<typeof JobOrderApiService.delete>;

function* saga_jobOrderFetchList({
  payload,
}: SimpleEffect<typeof ActionTypes.FETCHING_LIST_JO_REQUESTED, { shouldLoading: boolean }>): SagaIterator {
  const state: JobOrderState = yield select((s) => jobOrder_rootSelector(s));
  yield put(fetchingListJoLoading({ shouldLoading: payload?.shouldLoading }));
  try {
    const response: APIResponseListJo = yield call(
      JobOrderApiService.findAll,
      state.filters as unknown as Record<string, string | number>,
    );
    if (httpResponseOK(response.status)) {
      const { data } = response;
      yield put(fetchingListJoSuccess(data));
    }
  } catch (error) {
    // yield put({ type: ActionTypes.FETCHING_LIST_JO_FAILURE })
    yield put(fetchingListJoFailure());
  }
}

function* saga_createJobOrder(
  params: SagaEffectWithNavigateFunction<typeof ActionTypes.CREATE_JO_REQUESTED, IRequestBodyJobOrder>,
): SagaIterator {
  yield put(jobOrder_createJobOrderLoading());

  const { payload, navigate } = params;

  // Set loading
  yield put(app_setLoading(true));

  try {
    const response: APIResponseCreateJo = yield call(JobOrderApiService.create, payload);
    if (httpResponseCreated(response.status)) {
      yield put(jobOrder_createJobOrderSuccess(response.data));

      yield put(app_setLoading(false));

      Swal.fire({
        icon: 'success',
        title: 'Job Order Berhasil Dibuat!',
        text: ' ',
      });
      // .then((_res) => {
      //   if (_res.isConfirmed || _res.dismiss) window.location.reload()
      // })
      yield put(reFetchListJo());
      yield put(setDrawerAddEditJo(false, null));

      // Navigate to Job Order Page
      navigate(ROUTES.JOB_ORDER);
    } else {
      yield put(app_setLoading(false));
    }
  } catch (e) {
    yield put(app_setLoading(false));
    yield put({ type: ActionTypes.CREATE_JO_FAILURE });
  }
}

function* jobOrder_fetchDetail({
  payload,
}: Effect<
  typeof ActionTypes.JOB_ORDER_FETCH_DETAIL_REQUESTED,
  { job_order_id: UUID; shouldLoading?: boolean }
>): SagaIterator {
  yield put(jobOrder_fetchDetailLoading(payload.shouldLoading));
  try {
    const response: APIResponseJo = yield call(JobOrderApiService.find, payload.job_order_id);
    if (httpResponseOK(response.status)) {
      yield put(jobOrder_fetchDetailSuccess(response.data));
    }
  } catch (error) {
    yield put({ type: ActionTypes.FETCHING_LIST_JO_FAILURE });
  }
}

function* saga_updateJobOrder(
  params: SagaEffectWithNavigateFunction<typeof ActionTypes.CREATE_JO_REQUESTED, TUpdateJo>,
): SagaIterator {
  yield put(jobOrder_updateJobOrderLoading());

  const { payload, navigate } = params;

  // Set loading
  yield put(app_setLoading(true));

  try {
    const { id, body } = payload;
    const response: APIResponseUpdateJo = yield call(JobOrderApiService.update, id, body);
    if (httpResponseOK(response.status)) {
      yield put(jobOrder_updateJobOrderSuccess(response.data));
      yield put(app_setAlert({ show: true, messages: 'Job Order berhasil di perbarui!', severity: 'success' }));
      // Set loading
      yield put(app_setLoading(false));

      // Navigate to Job Order Page
      navigate(ROUTES.JOB_ORDER);
    } else {
      yield put(jobOrder_updateJobOrderFailure());
      // Set loading
      yield put(app_setLoading(false));
    }
  } catch (e) {
    yield put(jobOrder_updateJobOrderFailure());
    // Set loading
    yield put(app_setLoading(false));
  }
}

// function* fetchInvoiceData({ payload }: Effect<TPayloadGetJoInvoiceData>): SagaIterator {
//   yield put({ type: ActionTypes.FETCHING_INVOICE_DATA_LOADING })
//   try {
//     const response: APIResponseInvoiceData = yield call(JobOrderApiService.getInvoiceData, payload)
//     if (httpResponseOK(response.status)) {
//       yield put(fetchingInvoiceDataSuccess(response.data))
//       yield put(setDrawerEditableInvoice(true))
//       yield put(setInvoiceGeneratorData(response.data))
//     }
//   } catch (e) {
//     // eslint-disable-next-line no-console
//     console.log(e)
//     Swal.fire({
//       icon: 'error',
//       title: 'Opss...',
//       text: 'Ada kesalahan saat mengambil data invoice.',
//     })
//   }
// }

function* saga_jobOrderUpdatePaymentStatus({
  payload,
}: Effect<typeof ActionTypes.UPDATE_PAYMENT_STATUS_REQUESTED, IJobOrderPayloadUpdatePaymentStatus>): SagaIterator {
  yield put(jobOrder_updatePaymentStatusLoading());
  try {
    const { status, data }: APIResponseUpdatePaymentStatus = yield call(
      JobOrderApiService.updatePaymentStatus,
      payload,
    );
    if (httpResponseOK(status)) {
      yield put(jobOrder_updatePaymentStatusSuccess(data));
      yield put(reFetchListJo());
      Swal.fire({
        icon: 'success',
        title: 'OK!',
        text: 'Status Pembayaran Berhasil diperbaharui.',
      });
    } else {
      yield put(jobOrder_updatePaymentStatusFailure());
    }
  } catch (e) {
    yield put(jobOrder_updatePaymentStatusFailure());
  }
}

function* updateProductionStatusJo({ payload }: Effect<{ joId: number; productionStatusId: number }>): SagaIterator {
  yield put(updateProductionStatusLoading());
  try {
    const response: APIResponseUpdateProductionStatus = yield call(
      JobOrderApiService.updateProductionStatus,
      payload.joId,
      payload.productionStatusId,
    );
    if (httpResponseOK(response.status)) {
      yield put(updateProductionStatusSuccess(response.data));
      yield put(reFetchListJo());
      yield put(
        setAlert({
          open: true,
          message: 'Status produksi berhasil diperbaharui',
          severity: 'success',
        }),
      );
      // Swal.fire({
      //   icon: 'success',
      //   title: 'OK!',
      //   text: 'Status Pembayaran Berhasil diperbaharui.',
      // })
    }
  } catch (e) {
    yield put(updateProductionStatusFailure());
  }
}

function* updatePercentageProgress({ payload }: Effect<{ joId: number; value: number }>): SagaIterator {
  yield put(updatePercentageProgressLoading());
  try {
    const response: APIResponseUpdatePercentageStatus = yield call(
      JobOrderApiService.updatePercentageProgress,
      payload.joId,
      payload.value,
    );
    if (httpResponseOK(response.status)) {
      yield put(updatePercentageProgressSuccess(response.data));
      yield put(reFetchListJo());
      yield put(
        setAlert({
          open: true,
          message: 'Status produksi berhasil diperbaharui',
          severity: 'success',
        }),
      );
    }
  } catch (e) {
    yield put(updatePercentageProgressFailure());
  }
}

function* addNotesJo({ payload }: Effect<{ joId: number; notes: string }>): SagaIterator {
  yield put(addNotesJoLoading());
  try {
    const response: APIResponseAddNotesJo = yield call(JobOrderApiService.addNotes, payload.joId, payload.notes);
    if (httpResponseOK(response.status)) {
      yield put(addNotesJoSuccess(response.data));
      yield put(reFetchListJo());
      yield put(
        setAlert({
          open: true,
          message: 'Catatan berhasil ditambahkan ke job order.',
          severity: 'success',
        }),
      );
    }
  } catch (e) {
    yield put(addNotesJoFailure());
  }
}

function* saga_jobOrderStartProduction({
  payload,
}: Effect<ActionTypes.JOB_ORDER_START_PRODUCTION_REQUESTED, IPayloadJobOrderStartProduction>): SagaIterator {
  yield put(jobOrder_startProductionLoading());
  try {
    const { status }: APIResponseJobOrderStartProduction = yield call(JobOrderApiService.startProduction, payload);
    if (httpResponseOK(status)) {
      yield put(jobOrder_startProductionSuccess());
      yield put(
        app_setAlert({ show: true, messages: 'Status Produksi Job Order berhasil dimulai!', severity: 'success' }),
      );

      yield put(jobOrder_setDialogStartProduction(false, null));

      // Close dialog detail
      yield put(setDialogDetailJo(false, null));

      // Refresh list job order
      yield put(reFetchListJo());
    }
  } catch (e) {
    yield put(jobOrder_startProductionFailure());
  }
}

function* saga_jobOrderFinishProduction({
  payload,
}: Effect<ActionTypes.JOB_ORDER_FINISH_PRODUCTION_REQUESTED, { job_order_id: UUID }>): SagaIterator {
  yield put(jobOrder_finishProductionLoading());
  try {
    const { status }: APIResponseJobOrderFinishProduction = yield call(JobOrderApiService.finishProduction, payload);
    if (httpResponseOK(status)) {
      yield put(jobOrder_finishProductionSuccess());
      yield put(
        app_setAlert({ show: true, messages: 'Status Produksi Job Order berhasil diselesaikan!', severity: 'success' }),
      );

      // Close dialog detail
      yield put(setDialogDetailJo(false, null));

      // Refresh list job order
      yield put(reFetchListJo());
    }
  } catch (e) {
    yield put(jobOrder_finishProductionFailure());
  }
}

function* saga_jobOrderDelete({
  payload,
}: Effect<ActionTypes.JOB_ORDER_DELETE_REQUESTED, IPayloadJobOrderDelete>): SagaIterator {
  yield put(jobOrder_deleteLoading());
  try {
    const { status }: APIResponseJobOrderDelete = yield call(JobOrderApiService.delete, payload);
    if (httpResponseOK(status)) {
      yield put(jobOrder_deleteSuccess());
      yield put(app_setAlert({ show: true, messages: 'Job Order berhasil dihapus!', severity: 'success' }));

      // Close dialog detail.
      yield put(setDialogDetailJo(false, null));

      // Close dialog delete jo
      yield put(jobOrder_setDialogDelete(false, null));

      // Refresh list job order
      yield put(reFetchListJo());
    }
  } catch (e) {
    yield put(app_setAlert({ show: true, messages: 'Job Order gagal dihapus!', severity: 'error' }));
    yield put(jobOrder_deleteFailure());
  }
}

export default function* jobOrderSaga(): SagaIterator {
  yield takeEvery(ActionTypes.FETCHING_LIST_JO_REQUESTED, saga_jobOrderFetchList);
  yield takeEvery(ActionTypes.JOB_ORDER_FETCH_DETAIL_REQUESTED, jobOrder_fetchDetail);
  yield takeLatest(ActionTypes.CREATE_JO_REQUESTED, saga_createJobOrder);
  yield takeLatest(ActionTypes.UPDATE_JO_REQUESTED, saga_updateJobOrder);
  // yield takeLatest(ActionTypes.FETCHING_INVOICE_DATA_REQUESTED, fetchInvoiceData)
  yield takeLatest(ActionTypes.UPDATE_PAYMENT_STATUS_REQUESTED, saga_jobOrderUpdatePaymentStatus);
  yield takeLatest(ActionTypes.UPDATE_PRODUCTION_STATUS_REQUESTED, updateProductionStatusJo);
  yield takeLatest(ActionTypes.UPDATE_PERCENTAGE_PROGRESS_REQUESTED, updatePercentageProgress);
  yield takeLatest(ActionTypes.ADD_NOTES_JO_REQUESTED, addNotesJo);
  yield takeLatest(ActionTypes.JOB_ORDER_START_PRODUCTION_REQUESTED, saga_jobOrderStartProduction);
  yield takeLatest(ActionTypes.JOB_ORDER_FINISH_PRODUCTION_REQUESTED, saga_jobOrderFinishProduction);
  yield takeLatest(ActionTypes.JOB_ORDER_DELETE_REQUESTED, saga_jobOrderDelete);
}
