import BookmarksIcon from '@mui/icons-material/Bookmarks'
import EditIcon from '@mui/icons-material/Edit'
import FavoriteIcon from '@mui/icons-material/Favorite'
import {
  Avatar,
  Box,
  Grid,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@mui/material'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import { TBrandMaterialPaper } from '@src/types/inventory'
import { FC, MouseEvent, MouseEventHandler } from 'react'
import { useDispatch } from 'react-redux'

type Props = {
  item: TBrandMaterialPaper
}
const BrandMaterialPaperItem: FC<Props> = ({ item }: Props) => {
  const dispatch = useDispatch()

  const onEdit = (phraseId: number): void => {
    // dispatch(setDrawerAddEditPhrase(true, phraseId))
  }

  const onClick = (e: MouseEvent<HTMLLIElement>): void => {
    e.preventDefault()
    console.log('brandId', item.id)
  }

  const createAvailableSizesString = (stringSizes: string): string =>
    stringSizes

  return (
    <ListItem onClick={onClick} sx={{ cursor: 'pointer' }}>
      <ListItemAvatar>
        <Avatar
          sx={{
            color: 'primary.contrastText',
            backgroundColor: 'primary.main',
          }}
        >
          <BookmarksIcon />
        </Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={item.name}
        secondary={createAvailableSizesString(item.available_sizes)}
      />
    </ListItem>
  )
}

export default BrandMaterialPaperItem
