import { UUID } from '@/interfaces/common';
import { JobOrderFilterPeriod, JobOrderFilterProductionStatusId } from '@/interfaces/job-order';
import { ROWS_PER_PAGE } from '@/utils/constants';
import { TJo, TJoInvoiceData } from '@src/types/jo';
import { LaravelPaginationLinks, LaravelPaginationMeta } from '@src/types/laravel-pagination';
import { boolean } from 'yup';

// Enum
import { JOB_ORDER_ACTION_TYPES as ActionTypes } from './job-order-action-types.enum';

// Union action types.
import { JobOrderActions } from './job-order-actions';
import { IJobOrderFilters } from './job-order.interfaces';

export interface JobOrderState {
  listJo: {
    isFetching: boolean;
    isLoading: boolean; // Loading UI
    isError: boolean;
    data: TJo[] | null;
    meta: LaravelPaginationMeta | null;
    links: LaravelPaginationLinks | null;
  };
  // isFetching: boolean
  // isError: boolean
  // data: TJo[] | null
  // jo: TJo | null
  detail: {
    data: TJo | null;
    isFetching: boolean;
    isError: boolean;
    isLoading: boolean;
  };
  invoiceData: {
    isFetching: boolean;
    isError: boolean;
    data: TJoInvoiceData | null;
  };
  drawerAddEditJo: {
    status: boolean;
    id?: string | null;
  };
  dialogDetailJo: {
    open: boolean;
    id?: string | null;
  };
  dialogInputDownPayment: {
    open: boolean;
    id?: string | null;
    totalOrder: number;
  };

  createLoading: boolean;
  createError: boolean;
  createSuccess: boolean;

  updateLoading: boolean;
  updateError: boolean;
  updateSuccess: boolean;

  updatePaymentStatus: {
    isLoading: boolean;
    isError: boolean;
  };
  updateProductionStatus: {
    isLoading: boolean;
    isError: boolean;
    isSuccess: boolean;
  };
  updatePercentageProgress: {
    isLoading: boolean;
    isError: boolean;
    isSuccess: boolean;
  };
  addNotes: {
    isLoading: boolean;
    isError: boolean;
    isSuccess: boolean;
  };

  isFormHasChanges: boolean;

  dialogQr: {
    open: boolean;
    qr?: string | null;
  };

  dialogStartProduction: {
    open: boolean;
    job_order_id?: UUID | null;
  };

  filters: IJobOrderFilters;

  startProduction: {
    isLoading: boolean;
    isError: boolean;
  };

  finishProduction: {
    isLoading: boolean;
    isError: boolean;
  };

  dialogDelete: {
    open: boolean;
    job_order_id?: UUID | null;
  };

  deleteJobOrder: {
    isLoading: boolean;
    isError: boolean;
  };
}

const initialState: JobOrderState = {
  listJo: {
    isFetching: false,
    isLoading: false,
    isError: false,
    data: [],
    meta: null,
    links: null,
  },
  // isFetching: false,
  // isError: false,
  // data: [],
  // jo: null,
  detail: {
    data: null,
    isFetching: false,
    isError: false,
    isLoading: false,
  },
  invoiceData: {
    isFetching: false,
    isError: false,
    data: null,
  },

  drawerAddEditJo: {
    status: false,
    id: null,
  },

  dialogDetailJo: {
    open: false,
    id: null,
  },

  dialogInputDownPayment: {
    open: false,
    id: null,
    totalOrder: 0,
  },

  createLoading: false,
  createError: false,
  createSuccess: false,

  updateLoading: false,
  updateError: false,
  updateSuccess: false,

  updatePaymentStatus: {
    isLoading: false,
    isError: false,
  },

  updateProductionStatus: {
    isLoading: false,
    isError: false,
    isSuccess: false,
  },

  updatePercentageProgress: {
    isLoading: false,
    isError: false,
    isSuccess: false,
  },

  addNotes: {
    isLoading: false,
    isError: false,
    isSuccess: false,
  },

  isFormHasChanges: false,

  dialogQr: {
    open: false,
    qr: null,
  },

  dialogStartProduction: {
    open: false,
    job_order_id: null,
  },

  filters: {
    period: undefined,
    production_status_id: null,
    per_page: ROWS_PER_PAGE[0],
    page: 1,
  },
  startProduction: {
    isLoading: false,
    isError: false,
  },
  finishProduction: {
    isLoading: false,
    isError: false,
  },

  dialogDelete: {
    open: false,
    job_order_id: null,
  },

  deleteJobOrder: {
    isLoading: false,
    isError: false,
  },
};

const jobOrderReducer = (state: JobOrderState = initialState, action: JobOrderActions): JobOrderState => {
  switch (action.type) {
    case ActionTypes.SET_DRAWER_ADD_EDIT_JO:
      return {
        ...state,
        drawerAddEditJo: {
          status: action.payload.status,
          id: action.payload.id,
        },
      };

    case ActionTypes.SET_DIALOG_DETAIL_JO:
      return {
        ...state,
        dialogDetailJo: {
          open: action.payload.open,
          id: action.payload.id || null,
        },
      };

    /** fetch list jo */
    case ActionTypes.FETCHING_LIST_JO_LOADING:
      return {
        ...state,
        listJo: {
          ...state.listJo,
          isFetching: true,
          isError: false,
          isLoading: action?.payload?.shouldLoading ? true : false,
        },
      };
    case ActionTypes.FETCHING_LIST_JO_FAILURE:
      return {
        ...state,
        listJo: {
          ...state.listJo,
          isFetching: false,
          isError: true,
          isLoading: false,
        },
      };
    case ActionTypes.FETCHING_LIST_JO_SUCCESS:
      return {
        ...state,
        listJo: {
          ...state.listJo,
          isFetching: false,
          isError: false,
          isLoading: false,

          /**
           * data
           * links
           * meta
           */
          // ...action.payload,
          data: action.payload.data,
          meta: action.payload.meta ?? null,
          links: action.payload.links ?? null,
        },
      };
    // case ActionTypes.SET_ROWS_PER_PAGE_JO:
    //   return {
    //     ...state,
    //     listJo: {
    //       ...state.listJo,
    //       limit: action.payload,
    //     },
    //   };
    // case ActionTypes.SET_PAGE_LIST_JO:
    //   return {
    //     ...state,
    //     listJo: {
    //       ...state.listJo,
    //       page: action.payload,
    //     },
    //   };

    case ActionTypes.CREATE_JO_LOADING:
      return {
        ...state,
        createLoading: true,
        createError: false,
        createSuccess: false,
      };
    case ActionTypes.CREATE_JO_FAILURE:
      return {
        ...state,
        createLoading: false,
        createError: true,
        createSuccess: false,
      };
    case ActionTypes.CREATE_JO_SUCCESS:
      return {
        ...state,
        createLoading: false,
        createError: false,
        createSuccess: true,
      };

    case ActionTypes.UPDATE_JO_LOADING:
      return {
        ...state,
        updateLoading: true,
        updateError: false,
        updateSuccess: false,
      };
    case ActionTypes.UPDATE_JO_FAILURE:
      return {
        ...state,
        updateLoading: false,
        updateError: true,
        updateSuccess: false,
      };
    case ActionTypes.UPDATE_JO_SUCCESS:
      return {
        ...state,
        updateLoading: false,
        updateError: false,
        updateSuccess: true,
      };

    case ActionTypes.JOB_ORDER_FETCH_DETAIL_LOADING:
      return {
        ...state,
        detail: {
          ...state.detail,
          isError: false,
          isFetching: true,
          isLoading: action.payload.shouldLoading ? true : false,
        },
      };
    case ActionTypes.JOB_ORDER_FETCH_DETAIL_FAILURE:
      return {
        ...state,
        detail: {
          ...state.detail,
          isError: true,
          isFetching: false,
          isLoading: false,
        },
      };
    case ActionTypes.JOB_ORDER_FETCH_DETAIL_SUCCESS:
      return {
        ...state,
        detail: {
          isError: false,
          isFetching: false,
          isLoading: false,
          data: action.payload,
        },
      };

    // fetching invoice data from jo.
    case ActionTypes.FETCHING_INVOICE_DATA_LOADING:
      return {
        ...state,
        invoiceData: {
          isFetching: true,
          isError: false,
          data: null,
        },
      };
    case ActionTypes.FETCHING_INVOICE_DATA_FAILURE:
      return {
        ...state,
        invoiceData: {
          isFetching: false,
          isError: true,
          data: null,
        },
      };
    case ActionTypes.FETCHING_INVOICE_DATA_SUCCESS:
      return {
        ...state,
        invoiceData: {
          isFetching: false,
          isError: false,
          data: action.payload,
        },
      };

    /** Update payment status */
    case ActionTypes.UPDATE_PAYMENT_STATUS_LOADING:
      return {
        ...state,
        updatePaymentStatus: {
          isLoading: true,
          isError: false,
        },
      };
    case ActionTypes.UPDATE_PAYMENT_STATUS_FAILURE:
      return {
        ...state,
        updatePaymentStatus: {
          isLoading: false,
          isError: true,
        },
      };
    case ActionTypes.UPDATE_PAYMENT_STATUS_SUCCESS:
      return {
        ...state,
        updatePaymentStatus: {
          isLoading: false,
          isError: false,
        },
        detail: {
          ...state.detail,
          data: action.payload,
        },
      };
    /** Update production status */
    case ActionTypes.UPDATE_PRODUCTION_STATUS_LOADING:
      return {
        ...state,
        updateProductionStatus: {
          isLoading: true,
          isError: false,
          isSuccess: false,
        },
      };
    case ActionTypes.UPDATE_PRODUCTION_STATUS_FAILURE:
      return {
        ...state,
        updateProductionStatus: {
          isLoading: false,
          isError: true,
          isSuccess: false,
        },
      };
    case ActionTypes.UPDATE_PRODUCTION_STATUS_SUCCESS:
      return {
        ...state,
        updateProductionStatus: {
          isLoading: false,
          isError: false,
          isSuccess: true,
        },
        detail: {
          ...state.detail,
          data: action.payload,
        },
      };

    /** Update production status */
    case ActionTypes.UPDATE_PERCENTAGE_PROGRESS_LOADING:
      return {
        ...state,
        updatePercentageProgress: {
          isLoading: true,
          isError: false,
          isSuccess: false,
        },
      };
    case ActionTypes.UPDATE_PERCENTAGE_PROGRESS_FAILURE:
      return {
        ...state,
        updatePercentageProgress: {
          isLoading: false,
          isError: true,
          isSuccess: false,
        },
      };
    case ActionTypes.UPDATE_PERCENTAGE_PROGRESS_SUCCESS:
      return {
        ...state,
        updatePercentageProgress: {
          isLoading: false,
          isError: false,
          isSuccess: true,
        },
        detail: {
          ...state.detail,
          data: action.payload,
        },
      };

    /** Add notes jo */
    case ActionTypes.ADD_NOTES_JO_LOADING:
      return {
        ...state,
        addNotes: {
          isLoading: true,
          isError: false,
          isSuccess: false,
        },
      };
    case ActionTypes.ADD_NOTES_JO_FAILURE:
      return {
        ...state,
        addNotes: {
          isLoading: false,
          isError: true,
          isSuccess: false,
        },
      };
    case ActionTypes.ADD_NOTES_JO_SUCCESS:
      return {
        ...state,
        addNotes: {
          isLoading: false,
          isError: false,
          isSuccess: true,
        },
        detail: {
          ...state.detail,
          data: action.payload,
        },
      };

    case ActionTypes.SET_DIALOG_INPUT_DOWN_PAYMENT:
      return {
        ...state,
        dialogInputDownPayment: action.payload,
      };

    case ActionTypes.SET_FORM_HAS_CHANGES:
      return {
        ...state,
        isFormHasChanges: action.payload,
      };

    case ActionTypes.JOB_ORDER_SET_DIALOG_QR:
      return {
        ...state,
        dialogQr: {
          open: action.payload.open,
          qr: action.payload.qr,
        },
      };

    case ActionTypes.JOB_ORDER_SET_DIALOG_START_PRODUCTION:
      return {
        ...state,
        dialogStartProduction: {
          open: action.payload.open,
          job_order_id: action.payload.job_order_id,
        },
      };

    case ActionTypes.JOB_ORDER_SET_FILTERS:
      return {
        ...state,
        filters: {
          ...state.filters,
          [action.payload.property]: action.payload.value,
        },
      };

    case ActionTypes.JOB_ORDER_RESET_FILTERS:
      return {
        ...state,
        filters: initialState.filters,
      };

    case ActionTypes.JOB_ORDER_START_PRODUCTION_LOADING:
      return {
        ...state,
        startProduction: {
          isLoading: true,
          isError: false,
        },
      };
    case ActionTypes.JOB_ORDER_START_PRODUCTION_FAILURE:
      return {
        ...state,
        startProduction: {
          isLoading: false,
          isError: true,
        },
      };
    case ActionTypes.JOB_ORDER_START_PRODUCTION_SUCCESS:
      return {
        ...state,
        startProduction: {
          isLoading: false,
          isError: false,
        },
      };

    case ActionTypes.JOB_ORDER_FINISH_PRODUCTION_LOADING:
      return {
        ...state,
        finishProduction: {
          isLoading: true,
          isError: false,
        },
      };
    case ActionTypes.JOB_ORDER_FINISH_PRODUCTION_FAILURE:
      return {
        ...state,
        finishProduction: {
          isLoading: false,
          isError: true,
        },
      };
    case ActionTypes.JOB_ORDER_FINISH_PRODUCTION_SUCCESS:
      return {
        ...state,
        finishProduction: {
          isLoading: false,
          isError: false,
        },
      };

    case ActionTypes.JOB_ORDER_SET_DIALOG_DELETE:
      return {
        ...state,
        dialogDelete: {
          open: action.payload.open,
          job_order_id: action.payload.job_order_id,
        },
      };
    case ActionTypes.JOB_ORDER_DELETE_LOADING:
      return {
        ...state,
        deleteJobOrder: {
          isLoading: true,
          isError: false,
        },
      };
    case ActionTypes.JOB_ORDER_DELETE_FAILURE:
      return {
        ...state,
        deleteJobOrder: {
          isLoading: false,
          isError: true,
        },
      };
    case ActionTypes.JOB_ORDER_DELETE_SUCCESS:
      return {
        ...state,
        deleteJobOrder: {
          isLoading: false,
          isError: false,
        },
      };

    default:
      return state;
  }
};

export default jobOrderReducer;
