// Interfaces.
import type { AppState } from '@src/store/root-reducer';
import { JobOrderState } from './job-order-reducer';

export const jobOrder_rootSelector = (state: AppState): JobOrderState => state.jobOrder;
