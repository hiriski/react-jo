import { SagaIterator } from 'redux-saga';
import { SagaReturnType, Effect, call, put, takeEvery, takeLatest } from 'redux-saga/effects';

import { setAlert } from '../alert/actions';
import { WAREHOUSE_ACTION_TYPES as ActionTypes } from './warehouse-action-types.enum';

// Warehouse Api Service.
import { WarehouseApiService } from './warehouse-api-service';

// Utils.
import { httpResponseCreated, httpResponseOK, httpResponseUnprocessableEntity } from '@/utils/http';

// Action creators.
import {
  ICreateMaterialRequested,
  IFetchMaterialBrandListRequested,
  IFetchMaterialCategoryListRequested,
  IFetchMaterialListRequested,
  warehouse_createMaterialFailure,
  warehouse_createMaterialLoading,
  warehouse_createMaterialSuccess,
  warehouse_fetchMaterialBrandListFailure,
  warehouse_fetchMaterialBrandListLoading,
  warehouse_fetchMaterialBrandListSuccess,
  warehouse_fetchMaterialCategoryListFailure,
  warehouse_fetchMaterialCategoryListLoading,
  warehouse_fetchMaterialCategoryListSuccess,
  warehouse_fetchMaterialListFailure,
  warehouse_fetchMaterialListLoading,
  warehouse_fetchMaterialListSuccess,
} from './warehouse-actions';

import { app_setAlert } from '@/store/app/app-actions';

// Interfaces.
import { RequestBodyMaterial } from '@/interfaces/warehouse';
import Swal from 'sweetalert2';
import axios, { AxiosError, AxiosResponse } from 'axios';
import { mapResponseErrorValidation } from '@/utils/validation';

type ApiResponseMaterialLIst = SagaReturnType<typeof WarehouseApiService.getMaterialList>;
type ApiResponseCreateMaterial = SagaReturnType<typeof WarehouseApiService.create>;
type ApiResponseMaterialBrandList = SagaReturnType<typeof WarehouseApiService.getMaterialBrandList>;
type ApiResponseMaterialCategoryList = SagaReturnType<typeof WarehouseApiService.getMaterialCategoryList>;

function* sagaFetchMaterialList({ payload }: Effect<IFetchMaterialListRequested>): SagaIterator {
  yield put(warehouse_fetchMaterialListLoading());
  try {
    const { status, data }: ApiResponseMaterialLIst = yield call(WarehouseApiService.getMaterialList, payload);
    if (httpResponseOK(status)) {
      yield put(warehouse_fetchMaterialListSuccess(data));
    }
  } catch (e) {
    yield put(warehouse_fetchMaterialListFailure());
    yield put(
      app_setAlert({ show: true, messages: 'Failed to fetch material', severity: 'error', variant: 'outlined' }),
    );
  }
}

function* sagaCreateMaterial({ payload }: Effect<ICreateMaterialRequested>): SagaIterator {
  yield put(warehouse_createMaterialLoading());
  try {
    const { status, data }: ApiResponseCreateMaterial = yield call(WarehouseApiService.create, payload);
    if (httpResponseCreated(status)) {
      yield put(warehouse_createMaterialSuccess(data));
      yield put(app_setAlert({ show: true, messages: 'Bahan baku berhasil dibuat', severity: 'success' }));
    }
  } catch (e) {
    // check if the error was thrown from axios
    if (axios.isAxiosError(e)) {
      // do something
      if (httpResponseUnprocessableEntity(e.response.status)) {
        const errorMessages = mapResponseErrorValidation(e.response.data);
        yield put(warehouse_createMaterialFailure(errorMessages));
        Swal.fire({
          title: 'Opss..',
          icon: 'error',
          html: `<div>
            ${errorMessages.map((x) => x + '<br />')}
          </div>`,
        });
      } else {
        Swal.fire({
          title: 'Opss..',
          icon: 'error',
          text: 'Gagal saat membuat bahan baku',
        });
      }
    } else {
      // do something else
      // or creating a new error
      yield put(app_setAlert({ show: true, messages: 'Failed to create material', severity: 'error' }));
      throw new Error('different error than axios');
    }
  }
}

function* sagaFetchMaterialBrandList({ payload }: Effect<IFetchMaterialBrandListRequested>): SagaIterator {
  yield put(warehouse_fetchMaterialBrandListLoading());
  try {
    const { status, data }: ApiResponseMaterialBrandList = yield call(
      WarehouseApiService.getMaterialBrandList,
      payload,
    );
    if (httpResponseOK(status)) {
      yield put(warehouse_fetchMaterialBrandListSuccess(data));
    }
  } catch (e) {
    yield put(warehouse_fetchMaterialBrandListFailure());
    yield put(
      app_setAlert({ show: true, messages: 'Failed to fetch material brand', severity: 'error', variant: 'outlined' }),
    );
  }
}

function* sagaFetchMaterialCategoryList({ payload }: Effect<IFetchMaterialCategoryListRequested>): SagaIterator {
  yield put(warehouse_fetchMaterialCategoryListLoading());
  try {
    const { status, data }: ApiResponseMaterialCategoryList = yield call(
      WarehouseApiService.getMaterialCategoryList,
      payload,
    );
    if (httpResponseOK(status)) {
      yield put(warehouse_fetchMaterialCategoryListSuccess(data));
    }
  } catch (e) {
    yield put(warehouse_fetchMaterialCategoryListFailure());
    yield put(
      app_setAlert({ show: true, messages: 'Failed to fetch material brand', severity: 'error', variant: 'outlined' }),
    );
  }
}

export default function* warehouseSaga(): SagaIterator {
  yield takeEvery(ActionTypes.FETCH_MATERIAL_LIST_REQUESTED, sagaFetchMaterialList);
  yield takeLatest(ActionTypes.CREATE_MATERIAL_REQUESTED, sagaCreateMaterial);
  yield takeEvery(ActionTypes.FETCH_MATERIAL_BRAND_LIST_REQUESTED, sagaFetchMaterialBrandList);
  yield takeEvery(ActionTypes.FETCH_MATERIAL_CATEGORY_LIST_REQUESTED, sagaFetchMaterialCategoryList);
}
