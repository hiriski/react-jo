import { UUID } from '@/interfaces/common';
import { IJobOrder } from '@/interfaces/job-order';
import InventoryIcon from '@mui/icons-material/Inventory';
import { Theme } from '@mui/material';
import Box from '@mui/material/Box';
import { green, red } from '@mui/material/colors';
import Typography from '@mui/material/Typography';
import { setDrawerDetailInventory } from '@src/store/inventory/actions';
import { FC } from 'react';
import { useDispatch } from 'react-redux';

type Props = {
  item: IJobOrder;
};
const JobOrderItem: FC<Props> = ({ item }: Props) => {
  const dispatch = useDispatch();

  const onEdit = (phraseId: number): void => {
    // dispatch(setDrawerAddEditPhrase(true, phraseId))
  };

  const onClick = (paramId: UUID): void => {
    dispatch(
      setDrawerDetailInventory({
        open: true,
        type: 'material_paper',
        id: Number(paramId),
      }),
    );
  };

  return (
    <Box
      onClick={() => onClick(item.id)}
      sx={{
        borderRadius: 1,
        cursor: 'pointer',
        position: 'relative',
        backgroundColor: 'background.paper',
        border: (theme: Theme) => `1px solid ${theme.palette.divider}`,
      }}
    >
      {/* Inner */}
      <Box sx={{ display: 'flex', px: 2, py: 2 }}>
        {/* Icon */}
        <Box sx={{ width: 30, mt: 0.2 }}>
          <InventoryIcon sx={{ color: 'text.disabled', fontSize: 20 }} />
        </Box>
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            flex: 1,
          }}
        >
          <Box>
            <Typography component="h6" variant="h4" sx={{ fontSize: 17, mb: 0.3 }}>
              {item.title}
            </Typography>
            <Typography variant="subtitle2" color="text.secondary">
              {item.body}
            </Typography>
          </Box>
          <Box sx={{ ml: 'auto', textAlign: 'right' }}>
            <Typography component="h6" variant="h4">
              {item.due_date}
            </Typography>
            <Typography variant="subtitle2">{item.production_status.name}</Typography>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default JobOrderItem;
