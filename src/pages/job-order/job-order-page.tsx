import Box from '@mui/material/Box';
import { alpha, styled } from '@mui/material/styles';
import { DialogDetailCustomer } from '@src/components/customer';
import { MainLayout } from '@src/components/layouts';
import { useAppSelector } from '@src/store/hook';
import { fetchListJo, jobOrder_resetFilters } from '@/store/job-order/job-order-actions';
import { ROUTES, ROWS_PER_PAGE, SIDEBAR_DRAWER_WIDTH } from '@src/utils/constants';
import { FC, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { BaseButton } from '@/components/base';
import { useNavigate } from 'react-router';

import HeaderTopBackgroundImage from '@/assets/images/SL-043021-42650-28-2.jpg';

// Components.
import { JobOrderTable } from '@/components/job-order/job-order-table';
import { JobOrderWidgetAnalytics } from './components';
import { PageTitle } from '@/components/shared';
import AddIcon from '@mui/icons-material/Add';
import { jobOrder_rootSelector } from '@/store/job-order/job-order-selectors';
import { JobOrderConfig } from '@/config';
import { permissions_canCreateJobOrder } from '@/utils/permissions';
import { auth_rootSelector } from '@/store/auth/auth-selectors';
import { PermissionConstants } from '@/constants/permissions.constants';

const JobOrderPage: FC = () => {
  const dispatch = useDispatch();
  const user = useAppSelector((s) => auth_rootSelector(s).authenticatedUser);
  const { visibleSidebarDrawer } = useAppSelector((state) => state.common);
  const { listJo, filters } = useAppSelector((state) => jobOrder_rootSelector(state));
  const { data, isFetching } = listJo;

  const navigate = useNavigate();

  const getJobOrder = (shouldLoading: boolean): void => {
    dispatch(fetchListJo({ shouldLoading }));
  };

  useEffect(() => {
    getJobOrder(true);
  }, [filters.period, filters.production_status_id, filters.page, filters.per_page]);

  // Clean up
  useEffect(() => {
    const intervalFetchJobOrder = setInterval(() => {
      getJobOrder(false);
    }, JobOrderConfig.FetchInterval);

    return () => {
      dispatch(jobOrder_resetFilters());
      clearInterval(intervalFetchJobOrder);
    };
  }, []);

  return (
    <MainLayout>
      {/* -- Header background image -- */}
      <Box
        sx={{
          position: 'fixed',
          top: 0,
          right: 0,
          width: visibleSidebarDrawer ? `calc(100% - ${SIDEBAR_DRAWER_WIDTH}px)` : '100%',
          height: 218,
          backgroundImage: `url(${HeaderTopBackgroundImage})`,
          backgroundPosition: 'top right',
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
        }}
      />
      <Box
        sx={{
          position: 'fixed',
          backgroundColor: alpha('#000', 0.5),
          width: visibleSidebarDrawer ? `calc(100% - ${SIDEBAR_DRAWER_WIDTH}px)` : '100%',
          height: 218,
          top: 0,
          right: 0,
        }}
      />

      <Box sx={{ position: 'relative' }}>
        <PageTitle
          title="Job Order"
          sx={{ color: 'common.white' }}
          rightContent={
            <Box>
              {permissions_canCreateJobOrder(user) && (
                <BaseButton
                  startIcon={<AddIcon />}
                  color="secondary"
                  onClick={() => navigate(ROUTES.ADD_JOB_ORDER)}
                  disableHoverEffect
                >
                  Buat Job Order
                </BaseButton>
              )}
            </Box>
          }
        />

        {/* Job Order Analytics Widget */}
        <JobOrderWidgetAnalytics />

        <Box
          sx={{
            display: 'flex',
            marginBottom: 3,
            flexDirection: 'column',
          }}
        >
          <JobOrderTable data={data} isFetching={isFetching} />
        </Box>
        {/* </Main> */}
      </Box>
    </MainLayout>
  );
};

export default JobOrderPage;
