import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
// import Zoom, { ZoomProps } from '@mui/material/Zoom'
import { BoxSpinner, DialogContent } from '@src/components/ui'
import { useAppSelector } from '@src/store/hook'
import { fetchListRole } from '@src/store/role/actions'
import { fetchUser, setDialogAddEditUser } from '@src/store/user/actions'
import { FC, ReactElement, forwardRef, useEffect } from 'react'
import { useDispatch } from 'react-redux'

import { FormUser } from '.'

const DialogAddEditUser: FC = () => {
  const dispatch = useDispatch()
  const { dialogAddEdit, detail } = useAppSelector((state) => state.user)
  const { listRole, isFetching: isFetchingRole } = useAppSelector((state) => state.role)
  const { open, id } = dialogAddEdit
  const { isFetching, isError, data } = detail

  const handleClose = (): void => {
    dispatch(setDialogAddEditUser(false, null))
  }

  useEffect(() => {
    if (open && id) {
      dispatch(fetchUser(id))
    } else {
      dispatch(fetchListRole())
    }
  }, [open, id])

  return (
    <Dialog
      open={open}
      keepMounted
      onClose={handleClose}
      aria-describedby="dialog-add-edit-user"
      PaperProps={{ elevation: 1, sx: { position: 'relative' } }}
      maxWidth="sm"
      fullWidth
    >
      <DialogContent sx={{ pt: 4 }}>
        {isFetching || isFetchingRole ? <BoxSpinner height={360} /> : <FormUser listRole={listRole} />}
      </DialogContent>
    </Dialog>
  )
}

export default DialogAddEditUser
