import { IProduct, IResponseProductList } from '@/types/product'
import { AnyAction } from 'redux'

import { PRODUCT_ACTION_TYPES } from './product-action-types.enum'
import { ICreateProduct } from './product.interface'

// Action definitions.
interface SetDrawerAddEditProduct {
  type: typeof PRODUCT_ACTION_TYPES.SET_DRAWER_ADD_EDIT_PRODUCT
  payload: {
    open: boolean
    id: string | null
  }
}

interface IFetchProductListLoading {
  type: typeof PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LIST_LOADING
}
interface IFetchProductListFailure {
  type: typeof PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LIST_FAILURE
}
interface IFetchProductListSuccess {
  type: typeof PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LIST_SUCCESS
  payload: IResponseProductList
}

interface ISetDialogDetailProduct {
  type: typeof PRODUCT_ACTION_TYPES.SET_DIALOG_DETAIL_PRODUCT
  payload: { open: boolean; id: string | null }
}

interface IFetchProductLoading {
  type: typeof PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LOADING
}
interface IFetchProductFailure {
  type: typeof PRODUCT_ACTION_TYPES.FETCH_PRODUCT_FAILURE
}
interface IFetchProductSuccess {
  type: typeof PRODUCT_ACTION_TYPES.FETCH_PRODUCT_SUCCESS
  payload: IProduct
}

interface ICreateProductRequested {
  type: typeof PRODUCT_ACTION_TYPES.CREATE_PRODUCT_REQUESTED
  payload: ICreateProduct
}
interface ICreateProductLoading {
  type: typeof PRODUCT_ACTION_TYPES.CREATE_PRODUCT_LOADING
}
interface ICreateProductFailure {
  type: typeof PRODUCT_ACTION_TYPES.CREATE_PRODUCT_FAILURE
}
interface ICreateProductSuccess {
  type: typeof PRODUCT_ACTION_TYPES.CREATE_PRODUCT_SUCCESS
  payload: IProduct
}
interface ISetFormIsDirty {
  type: typeof PRODUCT_ACTION_TYPES.SET_FORM_IS_DIRTY
  payload: boolean
}

// Union action types.
export type ProductAction =
  | SetDrawerAddEditProduct
  | IFetchProductListLoading
  | IFetchProductListFailure
  | IFetchProductListSuccess
  | ISetDialogDetailProduct
  | IFetchProductLoading
  | IFetchProductFailure
  | IFetchProductSuccess
  | ICreateProductRequested
  | ICreateProductLoading
  | ICreateProductFailure
  | ICreateProductSuccess
  | ISetFormIsDirty

// Actions creator.
export const setDrawerAddEditProduct = (open: boolean, id: string | null): SetDrawerAddEditProduct => ({
  type: PRODUCT_ACTION_TYPES.SET_DRAWER_ADD_EDIT_PRODUCT,
  payload: { open, id },
})
export const setDialogDetailProduct = (open: boolean, id: string | null): ISetDialogDetailProduct => ({
  type: PRODUCT_ACTION_TYPES.SET_DIALOG_DETAIL_PRODUCT,
  payload: { open, id },
})

// fetch product list.
export const fetchProductList = (payload: Record<string, string | number>): AnyAction => ({
  type: PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LIST_REQUESTED,
  payload,
})
export const fetchProductListLoading = (): IFetchProductListLoading => ({
  type: PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LIST_LOADING,
})
export const fetchProductListFailure = (): IFetchProductListFailure => ({
  type: PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LIST_FAILURE,
})
export const fetchProductListSuccess = (payload: IResponseProductList): IFetchProductListSuccess => ({
  type: PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LIST_SUCCESS,
  payload,
})

// fetch product by id.
export const fetchProduct = (payload: string): AnyAction => ({
  type: PRODUCT_ACTION_TYPES.FETCH_PRODUCT_REQUESTED,
  payload,
})
export const fetchProductLoading = (): IFetchProductLoading => ({
  type: PRODUCT_ACTION_TYPES.FETCH_PRODUCT_LOADING,
})
export const fetchProductFailure = (): IFetchProductFailure => ({
  type: PRODUCT_ACTION_TYPES.FETCH_PRODUCT_FAILURE,
})
export const fetchProductSuccess = (payload: IProduct): IFetchProductSuccess => ({
  type: PRODUCT_ACTION_TYPES.FETCH_PRODUCT_SUCCESS,
  payload,
})

// Create product.
export const createProduct = (payload: ICreateProduct): ICreateProductRequested => ({
  type: PRODUCT_ACTION_TYPES.CREATE_PRODUCT_REQUESTED,
  payload,
})
export const createProductFailure = (): ICreateProductFailure => ({
  type: PRODUCT_ACTION_TYPES.CREATE_PRODUCT_FAILURE,
})
export const createProductLoading = (): ICreateProductLoading => ({
  type: PRODUCT_ACTION_TYPES.CREATE_PRODUCT_LOADING,
})
export const createProductSuccess = (payload: IProduct): ICreateProductSuccess => ({
  type: PRODUCT_ACTION_TYPES.CREATE_PRODUCT_SUCCESS,
  payload,
})

// set form is dirty
export const setFormIsDirty = (payload: boolean): ISetFormIsDirty => ({
  type: PRODUCT_ACTION_TYPES.SET_FORM_IS_DIRTY,
  payload,
})
