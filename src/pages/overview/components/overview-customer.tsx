import { BaseAnalyticsWidget, BaseCard } from '@/components/base';
import { ICustomer } from '@/interfaces/customer';
import { Typography, Box, AvatarGroup, Avatar } from '@mui/material';
import { green, blue, indigo, orange } from '@mui/material/colors';
import { FC } from 'react';
import { AlbumsOutline, FolderOpenOutline, SyncOutline, TimeOutline } from 'react-ionicons';
import { getInitialsName } from '@/utils/misc';

export const OverviewCustomer: FC = () => {
  return (
    <BaseCard padding={6}>
      <Box sx={{ mb: 6 }}>
        <Typography variant="h1" sx={{ fontSize: 42 }}>
          326
        </Typography>
        <Typography>List Customer</Typography>
      </Box>
      <Box
        sx={{
          position: 'relative',
          display: 'flex',
          alignItems: 'flex-start',
          justifyContent: 'center',
          flexDirection: 'column',
        }}
      >
        <Typography sx={{ mb: 1.5 }} component="h6" variant="h6">
          Today’s Heroes
        </Typography>
        {/* <AvatarGroup total={listOfCustomers.length} max={6}>
          {listOfCustomers.map((customer) => (
            <Avatar
              key={customer.id}
              src={customer.photo_url}
              alt={customer.name}
              sx={{ backgroundColor: 'primary.main' }}
            >
              {getInitialsName(customer.name, true)}
            </Avatar>
          ))}
        </AvatarGroup> */}
      </Box>
    </BaseCard>
  );
};
