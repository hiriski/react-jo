import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import { FC } from 'react'
import { LogoNodejs } from 'react-ionicons'

import { MainLayout } from '@src/components/layouts'

const AnalyticsPage: FC = () => (
  <MainLayout>
    <LogoNodejs color={'#00000'} rotate height="250px" width="250px" onClick={() => alert('Hi!')} />
    <Box
      sx={{
        height: 400,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Typography component="h1" variant="h5">
        Analytics Screen
      </Typography>
    </Box>
  </MainLayout>
)

export default AnalyticsPage
