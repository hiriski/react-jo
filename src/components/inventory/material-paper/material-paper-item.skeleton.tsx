import React, { FC } from 'react'
import { Box, Skeleton } from '@mui/material'

const MaterialPaperItemSkeleton: FC = () => (
  <Box
    sx={{
      borderRadius: 1,
      cursor: 'pointer',
      position: 'relative',
    }}
  >
    <Skeleton variant="rectangular" animation="wave" width="100%" height={75} />
  </Box>
)

export default MaterialPaperItemSkeleton
